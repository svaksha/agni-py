# https://gist.github.com/pydanny/2d3778ec941b9c64b04c
from datetime import datetime
 
special = datetime(2013, 12, 27, 14, 15)
 
now = datetime.now()
 
delta = special - now
 
print("days: ", (delta.total_seconds() / (3600 * 24)))
 
print("hours: ", (delta.total_seconds() / 3600))
 
print("minutes: ", (delta.total_seconds() / 60))
 
print("seconds: ", delta.total_seconds())

