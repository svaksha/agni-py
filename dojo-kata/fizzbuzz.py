#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2011 SVAKSHA <svakshaATgmailDOTcom>
# License: GNU AGPLv3, <http://www.gnu.org/licenses/agpl-3.0.html>
#**************************************************************************

# Write a program that prints the numbers from 1 to 100. For multiples
# of three, print "fizz" instead of the number and for multiples of five
# print "buzz" and print "FizzBuzz" for multiples of both three and five.

for numList in range(1, 101):
   # Multiples of 3 && 5, print "FizzBuzz".
    if numList % 3 == 0 and numList % 5 == 0:
        print "FizzBuzz"
    # Multiples of three, print "Fizz"
    elif numList % 3 == 0:
        print "Fizz"
   # Multiples of five, print "Buzz".
    elif numList % 5 == 0:
        print "Buzz"
    else:
        print numList
