#!/usr/bin/env python3.3
# -*- coding: utf-8 -*-

import sys
import os
# http://www.zentut.com/python-tutorial/python-dictionary/

def print_dict(dict):
    """
    print dictionary
    """
    for key,value in dict.items():
        print(key + ':' + value, '\n')

empty_dict = {}
# stock quote dictionary
stock_quote = {'AIG':'American International Group. Inc.',
               'CSCO':'Cisco Systems, Inc.',
               'GE':'General Electric Company'}

# changing existing value
stock_quote['AIG'] = 'What company is that?'
# add new item to the dictionary
stock_quote['AFFY'] = 'Affymax, Inc.'

print_dict(stock_quote) #output dictionary content

#removing single item
del stock_quote['AIG']

print_dict(stock_quote)  #output dictionary content

'''
stock_quote.clear() #removing all the items of dictionary
del stock_quote     # delete dictionary
'''

# Second dict
myLibrary = {'book1' : 'Introduction to Physics',
            'book2' : 'Biology-101',
            'book3' : 'Mathematica',
            'book4' : 'Chemistry'}

print_dict(myLibrary)

'''
Methods 	 Description
'''
#dict.copy( )	Returns a shallow copy of the dictionary
copy_quote = dict.copy(myLibrary)
print_dict(copy_quote)

# dict.has_key(k)	Returns true if key is a key in dict; otherwise, returns False, just like key in dict
#print(myLibrary.has_key(k))

# dict.items( )	Returns a new list with all items (key/value pairs) in dict
print (myLibrary.items())

# dict.keys( )	Returns a new list with all keys in dict
print (stock_quote.keys())
print (myLibrary.keys())

# dict.values( )	Returns a new list with all values in dict
print (stock_quote.values())

##iterkey, iteritems, itervalues has been removed in 3.x, just use 'items()'
#dict.iteritems( )	Returns an iterator on all items (key/value pairs) in dict
print (stock_quote.items())

#dict.get(key[, x])	Returns dict[key] if key is a key in dict; otherwise, returns x(or None, if x is not given)
#print (myLibrary.get(book1[x]))

# dict.clear( )	Removes all items from dict, leaving dict empty

# dict.updictate(dict1)	For each key in dict1, sets dict[key] equal to dict1[key]

# dict.setdictefault(key[,x])	Returns dict[key] if key is found in dict; otherwise, setsdict[key] equal to x and returns x

# dict.pop(key[, x])	Removes and returns dict[key] if key is found in the dictionary dict; otherwise, returns x  or raises an exception if x is not given.

# dict.popitem( )	Removes and return returns an arbitrary item (key/value pair)
