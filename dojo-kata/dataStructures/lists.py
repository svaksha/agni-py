#!/usr/bin/env python3.3
# -*- coding: utf-8 -*-

import sys
import os

list1 = ['a', 'b', 'c', 'd', 'e', 'f','g','h','i']
print('list1=')
print('list1[1:3] =', list1[1:3])
print('list1[:4] =', list1[:4])
#print('t[3:] =', t[3:]) #slicing
#print(t[:])

listInt = ['1', '2', '3', '4','5','6','7','8', '9','10']
listFloat = ['2.2', '3.5','45.5', '452.3', '25.7', '5.9', '436.5', '456.7', '12467.567', '2.5']
listFruits = ['apple', 'mango', 'pineapple', 'gauva', 'litchee', 'strawberry', 'jackfruit', 'banana', 'blueberry', 'peaches']

#==============================================================================
# intFloat = []
# listFloat.append(listInt)
# print(listFloat)
#==============================================================================
#==============================================================================
emptyL1 = []
emptyL1.append(listFloat)
print(emptyL1)
emptyL1.append(listInt)
print(emptyL1)
print("Concatenated list is ",  emptyL1)

