# -*- coding: utf-8 -*-
"""
Created on Sun Jan  5 22:05:37 2014

Project Description:
In this pastebin you can see a Python script, which enables our developers to work really fast: http://pastebin.com/eYiG3E0Q

It downloads all forks of our GitHub repository and places each in its own directory on our test-server. 
This means that if a new developer joins our crew and forks our repository, then 
she can immediately start experimenting with our code in the same environment as everyone else. 
This way we completely avoid the "but it works on my laptop", since everyone is working in the same environment.

What I need you to do, is to get the script to work with GitLab using its APIs. 
Specifically with our installation of GitLab running at dev.antfarmhq.com.

"""

#!/usr/bin/env python

# gitsync.py - This script clones a repository and all its forks to a specified
# folder. It also polls all the forks of the repository for changes and updates
# the local copy. This script should be invoked from rc.local

import os
import sys
import time
import fcntl
import shutil
import logging
import requests
import simplejson
import subprocess
from email.mime.text import MIMEText

DEST               = '/var/www/html'
GITHUB_API         = 'https://api.github.com/repos/OO-developer/PhotoAccounting/forks'
REPO               = 'https://github.com/OO-developer/PhotoAccounting.git'
USER               = 'automaticuser'
PASSWORD           = 'ourpassword'
ARCHIVE_REPO_OWNER = 'OO-developer'
ARCHIVE_COUNT      = 75
STOP_FILE          = '/var/run/gitsync/stop'
PID_FILE           = '/var/run/gitsync/pid'

# logging.basicConfig(level=logging.DEBUG,
logging.basicConfig(level=logging.INFO,
  format='%(asctime)s - gitsync.py - %(levelname)-7s - %(message)s')
logger = logging.getLogger('gitsync.py')


def ensure_sudo():
  """
  Ensure that the script is run using sudo if not already done
  """
  if os.geteuid() != 0:
    logger.debug('<ensure_sudo> relaunching with sudo')
    os.execvp('sudo', ['sudo'] + sys.argv)

def send_restart_email():
  msg = MIMEText('sldkjfdkfj')
  msg["To"] = ", ".join('mrdavidandersen@gmail.com')
  msg["Subject"] = "Need to restart gitsync.sh"

  logger.error('<send_restart_email> sending email')
  p = subprocess.Popen(["/usr/sbin/sendmail", "-t"], stdin=subprocess.PIPE)
  p.communicate(msg.as_string())

def get_commits_from_repo(owner, repo, repo_user, repo_passwd):
  """
  Get indexed list of commits from the repo passed as argument
  """
  results = []
  clone_dest = os.path.join(DEST, owner, repo)

  log_output = check_call_output(['git', 'log', '--pretty=format:%H'], cwd=clone_dest)

  for idx, sha in enumerate(reversed(log_output['stdout'].split('\n'))):
    results.append({'idx': idx+1, 'sha': sha})

  logger.debug('<get_commits_from_repo> #commits => {0}'.format(len(results)))

  return results

def get_forks(owner, repo, repo_user, repo_passwd):
  """
  Get the forks, if exists, of the repo passed as the argument
  """
  results = []
  results.append({'clone_url': REPO, 'user': 'OO-developers'})
  request_url = 'https://api.github.com/repos/{0}/{1}/forks?page=1&per_page=100'.format(owner, repo)

  try:
    r = requests.get(request_url, auth=(repo_user, repo_passwd))

    if r.ok:
      logger.info('<get_forks> API requests remaining: {0}'.format(r.headers['x-ratelimit-remaining']))
      data = simplejson.loads(r.text or r.content)
      logger.info('<get_forks> got {0} forks for {1}'.format(len(data), repo))

      for repo in data:
        results.append({'clone_url': repo['clone_url'], 'user': repo['clone_url'].split('/')[3]})
    else:
      logger.error('<get_forks> error: {0}'.format(r.status_code))
  except Exception as e:
    logger.warn('<get_forks> exception: {0}'.format(e))

  return results

def check_call_output(args, cwd):
  """
  Call subprocess.Popen with the given arguments and wait for the child process to
  finish. Return the STDOUT and the return code
  """
  out = None
  err = None
  retcode = None

  logger.debug('<check_call_output> {0} for {1}'.format(args, cwd))
  try:
    output = subprocess.Popen(args, cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = output.communicate()
    out = out.decode('utf-8').strip()
    err = err.decode('utf-8').strip()
    retcode = output.poll()
    if retcode != 0:
      raise Exception(str(retcode) + ' : ' + err)
  except Exception as e:
    logger.error('<check_call_output> error: [{0}, {1}] - {2}'.format(args, cwd, e))

  return {
    'stdout': None if out == '' else out,
    'stderr': None if err == '' else err,
    'retcode': retcode
  }


def process_local_copy(repo, clone_url, owner, repo_user, repo_passwd):
  """
  Create/update the local copy of a fork
  """
  clone_dest = os.path.join(DEST, owner, repo)
  merge_output = {}
  merge_output['retcode'] = 0

  if not os.path.isfile(clone_dest) and not os.path.isdir(clone_dest):
    logger.info('<process_local_copy> create directory {0}'.format(clone_dest))
    os.makedirs(clone_dest)

    logger.info('<process_local_copy> clone {0} to {1}'.format(clone_url, clone_dest))
    check_call_output(['git', 'clone', clone_url.replace('github.com', '{0}:{1}@{2}'.format(repo_user, repo_passwd, 'github.com')), clone_dest], cwd=clone_dest)
  else:
    check_call_output(['git', 'fetch', '-q'], cwd=clone_dest)
    rev_output = check_call_output(['git', 'rev-list', 'HEAD...origin/master', '--count'], cwd=clone_dest)

    if (rev_output['retcode'] == 0 and rev_output['stdout'] and int(rev_output['stdout']) > 0):
      logger.info('<process_local_copy> need to merge {0} changes to {1}'.format(rev_output['stdout'], clone_dest))
      merge_output = check_call_output(['git', 'merge', 'origin/master'], cwd=clone_dest)

    if rev_output['retcode'] != 0 or merge_output['retcode'] != 0:
        logger.info('<process_local_copy> need to recreate {0}'.format(clone_dest))
        logger.info('<process_local_copy> delete directory {0}'.format(clone_dest))
        shutil.rmtree(clone_dest.replace(repo, ''), ignore_errors=True)

        logger.info('<process_local_copy> create directory {0}'.format(clone_dest))
        os.makedirs(clone_dest)

        logger.info('<process_local_copy> clone {0} to {1}'.format(clone_url, clone_dest))
        check_call_output(['git', 'clone', clone_url.replace('github.com', '{0}:{1}@{2}'.format(repo_user, repo_passwd, 'github.com')), clone_dest], cwd=clone_dest)

def create_local_commit(owner, repo, index, commit_hash, repo_user, repo_passwd):
  """
  Create local archive copy of the repo with the code at commit_hash in folder
  'index'
  """
  clone_dest = os.path.join(DEST, owner + '_older_versions', repo, str(index))
  logger.debug('<create_local_commit> {0} => {1}'.format(index, commit_hash))

  if not os.path.isfile(clone_dest) and not os.path.isdir(clone_dest):
    logger.debug('<create_local_commit> create directory {0}'.format(clone_dest))
    os.makedirs(clone_dest)

    logger.info('<create_local_commit> clone {0} to {1}'.format(REPO, clone_dest))
    check_call_output(['git', 'clone', REPO.replace('github.com', '{0}:{1}@{2}'.format(repo_user, repo_passwd, 'github.com')), clone_dest], cwd=clone_dest)

    logger.info('<create_local_commit> checkout commit {0}'.format(commit_hash))
    check_call_output(['git', 'checkout', commit_hash], cwd=clone_dest)

    #ToDo:: create files describing this particular checkout
    logger.info('<create_local_commit> write commit hash to git.commit')
    f = open(os.path.join(clone_dest, 'git.commit'), 'w')
    f.write(commit_hash + '\n')
    f.close()

    logger.info('<create_local_commit> write commit log to git.log')
    log_output = check_call_output(['git', 'log', '-1'], cwd=clone_dest)
    f = open(os.path.join(clone_dest, 'git.log'), 'w')
    contents = log_output['stdout'].replace('\n', '<br/>\n')
    contents = contents.replace('commit', '<strong>Commit:</strong>')
    contents = contents.replace('Author:', '<strong>Author:</strong>')
    contents = contents.replace('Date:', '<strong>Date:</strong>')
    f.write(contents + '\n')
    f.close()

    logger.info('<create_local_commit> delete .git directory')
    shutil.rmtree(os.path.join(clone_dest, '.git'), ignore_errors=True)

def delete_old_commit(index, repo_name):
  """
  Delete local archive copy of the repo in folder 'index'
  """
  dest = os.path.join(DEST, ARCHIVE_REPO_OWNER + '_older_versions', repo_name, str(index))

  if os.path.isdir(dest):
    logger.info('<delete_old_commit> delete old version {0} at {1}'.format(index, dest))
    shutil.rmtree(dest, ignore_errors=True)


if __name__ == '__main__':
  logger.debug('start')

  logger.debug('checking for superuser permissions')
  ensure_sudo()

  # Create runtime configuration directory
  if not os.path.isdir('/var/run/gitsync'):
    logger.info('create run configuration directory at "{0}"'.format('/var/run/gitsync'))
    os.makedirs('/var/run/gitsync')

  # Ensure only one instance is running
  fp = open(PID_FILE, 'w')
  try:
    fcntl.lockf(fp, fcntl.LOCK_EX | fcntl.LOCK_NB)
    fp.write(str(os.getpid()) + '\n')
    fp.flush()
  except IOError:
    logger.error('another instance of gitsync.py is running')
    sys.exit(0)

  repo_owner = REPO.split('/')[3]
  repo_name = REPO.split('/')[4].replace('.git', '')

  while True:
    for idx, fork in enumerate(get_forks(repo_owner, repo_name, USER, PASSWORD)):
      logger.debug('[{0}]{1:25} => {2}'.format(idx, fork['user'], fork['clone_url']))
      process_local_copy(repo_name, fork['clone_url'], fork['user'], USER, PASSWORD)

    # send_restart_email()

    commits = get_commits_from_repo(ARCHIVE_REPO_OWNER, repo_name, USER, PASSWORD)

    for idx, commit in enumerate(commits[:-ARCHIVE_COUNT]):
      delete_old_commit(commit['idx'], repo_name)

    for idx, commit in enumerate(commits[-ARCHIVE_COUNT:]):
      create_local_commit(ARCHIVE_REPO_OWNER, repo_name, commit['idx'], commit['sha'], USER, PASSWORD)

    if os.path.isfile(STOP_FILE):
      if os.path.isfile(PID_FILE):
        logger.info('delete PID file at {0}'.format(PID_FILE))
      logger.warn('*** found {0}. Stopping ***'.format('/var/run/gitsync/stop'))
      os.remove(STOP_FILE)
      logger.debug('~'*50)
      break

    logger.info('='*50)
    time.sleep(30)

  logger.debug('finish')