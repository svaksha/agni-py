#!/usr/bin/env python
# -*- coding: utf-8 -*-
#**************************************************************************
# Copyright (C) 2011 VidAyer <vid@svaksha.com>
# License: GNU AGPLv3, <http://www.gnu.org/licenses/agpl-3.0.html>
#**************************************************************************
# How do you find out if a number is a power of 2? And how do you know if it is an odd number?

