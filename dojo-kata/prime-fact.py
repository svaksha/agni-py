#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2011 Vid_Ayer <svaksha@gmail.com>
# License: GNU AGPLv3, <http://www.gnu.org/licenses/agpl-3.0.html>

def prime_factors(n):
    """ Return the prime factors of the given number. """
    factors = []
    lastresult = n

    # 1 is a special case
    if n == 1:
        return [1]

    while 1:
        if lastresult == 1:
            break

        c = 2

        while 1:
            if lastresult % c == 0:
                break

            c += 1

        factors.append(c)
        lastresult /= c

    return factors


# Wed 13 Apr 2011 11:39:25 IST 
# Added tiny NOSE test
  def test_1() :
     x = add(1,1)
     assert x == 2
# Wed 13 Apr 2011 11:39:25 IST 

#exit a program after catching and dealing with an exception
try:
    raise Exception
except Exception:
    print 'handled it'
    raise
