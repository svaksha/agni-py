#!/usr/bin/env python3.3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 15 17:22:21 2013
@author: svaksha
"""
'''
Question: Print the 5th, 10, 15, 20th number
'''

numbers = [
    951, 402, 984, 651, 360, 69, 408, 319, 601, 485, 980, 507, 725, 547, 544,
    615, 83, 165, 141, 501, 263, 617, 865, 575, 219, 390, 984, 592, 236, 105,
    942, 941, 386, 462, 47, 418, 907, 344, 236, 375, 823, 566, 597, 978, 328,
    615, 953, 345, 399, 162, 758, 219, 918, 237, 412, 566, 826, 248, 866, 950,
    626, 949, 687, 217, 815, 67, 104, 58, 512, 24, 892, 894, 767, 553, 379,
    843, 831, 445, 742, 717, 958, 609, 842, 451, 688, 753, 854, 685, 93, 857,
    440, 380, 126, 721, 328, 753, 470, 743, 527, 81, ]

def multiplesFive():
    counter = 0
    while (counter < len(numbers)):
        counter += 1
        if (counter < len(numbers)) and (((counter % 5) == 0)):
            print ('The count is: ', counter, numbers[counter])
            continue
print(multiplesFive())
