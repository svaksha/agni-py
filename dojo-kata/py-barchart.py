# Message-ID: <4D1C2D84.9070609@bravegnu.org>

# We will start off with a very simple problem - a bar chart generator.
# The input will be a file in the following format.

# item-label:value-label
# item1:value1
# item2:value2
# item3:value3

# The first line will be the labels for the item axis and the value
# axis. The remaining lines are item value pairs. An example dataset is
# given below.

# Browser:Percent Usage
# IE:28.6
# Firefox:44.0
# Chrome:20.5
# Safari:4.0
# Opera:2.3

# The program takes the data filename as argument. The bar chart is to
# be output on the text console. Send in your solutions.

import sys

def get_input():
    if len(sys.argv) <> 2:
        return None, None
    values = list()
    for line in open(sys.argv[1]).readlines():
        key, val = line.split(':')
        values.append((key.strip(), val.strip()))
    return values[0], [ (k, float(v)) for k, v in values[1:] ]

def bar_chart(label, values):
    if None in (label, values):
        print 'No input.'
        return

    m = max(v for k, v in values)
    values = [ (k, int(v*66/m)) for k, v in values ]
    print '%10s' % label[0][:10]
    for k, v in values:
        print '%10s |' % k[:10], v * '-'
    print ' ' * 12, label[1]

if __name__ == '__main__':
    label, values = get_input()
    bar_chart(label, values)


#exit a program after catching and dealing with an exception
try:
    raise Exception
except Exception:
    print 'handled it'
    raise
