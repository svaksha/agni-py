#!/usr/bin/env python3.3
# -*- coding: utf-8 -*-

# https://gist.github.com/jschneier

import sys
import os

def permute(items):
    if len(items) < 2:
        yield items
    else:
        for i in xrange(len(items)):
            dup = items[:]
            cur = dup.pop(i)
            for perm in permute(dup):
                yield [cur] + perm

