from soaplib.service import rpc
from soaplib.service import DefinitionBase
from soaplib.serializers.primitive import *
import cElementTree as et
from soaplib.serializers.binary import Attachment

from soaplib.wsgi import Application
from soaplib.serializers.clazz import * 

import os
import hashlib
import base64

wlist = []
dlist = []
slist = []
stoken = []

# Subworkflow class holds subworkflow information
# files has format:
#     { relative_filepath1/filename2: base64_encode(rawdata1),
#               relative_filepath2/filename2: base64_encode(rawdata2)}

class Subworkflow(ClassSerializer):
	subworkflowid = String
	files = AnyAsDict(String) 

class Workflow(DefinitionBase):
##
# Get security token
# The security token is passed for each call
# This function will return a security token for username/password
#
# @param username string
# @param password string
# @return securitytoken string
##
	@rpc(String, String, _returns=String)
	def getSecurityToken(self, username, password):
		m = hashlib.md5("%s:%s"%(username,password))
		return m.hexdigest()

	
##
# Add dispatch id
# Prepares a new dispatch with dispatchid.
#
# @param dispatchid string
# @param securitytoken string
# @return true / false on error
##

	def addDispatch(self, dispatchid, securitytoken):
		if len(dlist) > 0:
			for index in range(len(dlist)):
				if((dlist[index][0] == dispatchid) & (dlist[index][1] == securitytoken)):
					return false
					break
		dlist.append([dispatchid, securitytoken])
		return true
		

##
# Add workflow with workflowid to dispatch
# Prepares a new workflow with workflowid for dispatchid
#
# @param dispatchid string
# @param workflowid string
# @param securitytoken string
# @return true / false on error
##
	def addWorkflow(self, dispatchid, workflowid, securitytoken):
		wlist = [] #this list for storing the workflow ; donno where to place it
		for index in range(len(wlist)):
			if((wlist[index][0] == dispatchid) & (wlist[index][1] == workflowid) & (wlist[index][2] == securitytoken)):
				return false
				break
		wlist.append([dispatchid, workflowid, securitytoken]) 
    		return true
			


##
# Add subworkflows with subworkflowids to workflowid of dispatchid
# Prepares new subworkflows with specified files
#
# @param dispatchid string
# @param workflowid string
# @param subworkflows Array(Subworkflow)
# @param securitytoken string
# @return true / false on error
##
	def addSubworkflows(self, dispatchid, workflowid, subworkflows, securitytoken):
		slist = []
		for value in subworkflows:
			for index in range(len(slist)):
				if((slist[index][0] == dispatchid) & (slist[index][1] == workflowid) & (slist[index][2] == securitytoken) & (slist[index][3] == subworkflowid)):
					return false
					break
		slist.append([dispatchid, workflowid, securitytoken, value])
    		return true
	


##
# Remove dispatches with dispatchids
# Removes all files, subworkflows, workflows, and dispatch itself with dispatchids
#
# @param dispatchids Array(string) dispatch ids to remove
# @param securitytoken string
# @return true / false on error
##
	def removeDispatches(self, dispatchids, securitytoken):
		for index in range(len(dlist)):
				if((dlist[index][0] == dispatchid) & (dlist[index][1] == securitytoken)):
					dlist.remove([dispatchid, securitytoken]) 
					return true
					break
		return false


##
# Remove workflows workflowids from dispatchid
# Removes all files, subworkflows, and workflow itself from dispatchid
#
# @param dispatchid string
# @param workflowids Array(string)
# @param securitytoken string
# @return true / false on error
##
	def removeWorkflows(self, dispatchid, workflowids, securitytoken):
		for index in range(len(wlist)):
				if((wlist[index][0] == dispatchid) & (wlist[index][1] == workflowid) & (wlist[index][2] == securitytoken)):
					wlist.remove([dispatchid, workflowid, securitytoken]) 
					return true
					break
		return false


##
# Remove subworkflows subworkflowids from workflowid of dispatchid
# Removes all files and subworkflow itself from workflowid of dispatchid
#
# @param dispatchid string
# @param workflowid string
# @param subworkflowid Array(string)
# @param securitytoken string
# @return true / false on error
##
	def removeSubworkflows(self, dispatchid, workflowid, subworkflowids, securitytoken):
				for value in subworkflows:
					for index in range(len(slist)):
						if((slist[index][0] == dispatchid) & (slist[index][1] == workflowid) & (slist[index][2] == securitytoken) & (slist[index][3] == subworkflowid)):
							slist.remove([dispatchid, workflowid, securitytoken, value])
							return true
							break
				return false


##
# Dispatch the prepared dispatch id
# This function will dispatch the prepared dispatchid to condor system
# A supplied callback uri is supplied for post-dispatch communication of
# dispatch status, statistics, results
#
# @param dispatchid string
# @param callbackuri string
# @param securitytoken string
# @return true / false on error
# #
	def dispatch(self, dispatchid, callbackuri, securitytoken):
		os.system("ls -l") #same command to try if its working or not.
		
		
if __name__=='__main__':
	try:
		from wsgiref.simple_server import make_server
		server = make_server('localhost',7789, Application([Workflow],'tns'))
		server.serve_forever()
	except ImportError:
			print "Error: server code requires Python >= 2.5"

