#!/usr/bin/env python
# -*- coding: utf-8 -*-
#*****************************************************************************
# COPYRIGHT (C) 2011,2012 VidAyer <vid@svaksha.com>
# LICENSE: GNU AGPLv3, http://www.gnu.org/licenses/agpl-3.0.html
#*****************************************************************************
'''<http://www.techinterview.org/post/526342692/bumblebee>
Bumblebee problem: two trains enter a tunnel 200 miles long (yeah, its a 
big tunnel) travelling at 100 mph at the same time from opposite 
directions. as soon as they enter the tunnel a supersonic bee flying 
at 1000 mph starts from one train and heads toward the other one. 
As soon as it reaches the other one it turns around and heads back 
toward the first, going back and forth between the trains until the 
trains collide in a fiery explosion in the middle of the tunnel (the 
bee survives). how far did the bee travel?
'''
