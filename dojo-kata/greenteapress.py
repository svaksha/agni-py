#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2011 VidAyer <vid@svaksha.com>
# License: GNU AGPLv3, <http://www.gnu.org/licenses/agpl-3.0.html>
#**************************************************************************

# import math functions
import math
degrees = 45
radians = (degrees / 360.0) * 2 * math.pi
math.sin(radians)
print radians

# How to call a function
def print_lyrics():
        print "I'm a lumberjack, and I'm okay."
        print "I sleep all night and I work all day."
print_lyrics()

def repeat_lyrics():
    print_lyrics()
    print_lyrics()
repeat_lyrics()  # function object 

# 3.8.Parameters and arguments
def print_twice(bruce):
    print bruce
    print bruce
print_twice('SpamMonkey, ' *4)  # function object 

# 3.9.Variables and parameters are local

def cat_twice(part1, part2):
    cat = part1 + part2
    print_twice(cat)  # function object 


line1 = 'Bing tiddle '
line2 = 'tiddle bang.'
cat_twice(line1, line2)  # function object 


# 3.15  Exercises
'''
Python provides a built-in function called len that returns the length of a string, so the value of len('allen') is 5.
Write a function named right_justify that takes a string named s as a parameter and prints 
the string with enough leading spaces so that the last letter of the string is in column 70 of the display.
'''
def twice(right):
    print right

def right_justify(part1,  part3):
    concatvar = part1 + part3
    twice(concatvar)  #prints the concatvar from first function defined

line1 = ' '
line3 = ''
length = len('str')
#right_justify(line1,  line3)

right_justify(line1  *(70-length) , 'pythonallen')  # function object 


# Exercise 4  
def do_twice(f,  val):
    f(val)   # calling 'f' function with a parameter.
    f(val) 

ls = 'test'
def print_spam(ls):
    print  ls + 'spam'  # two arguments, a function object and a value,
do_twice(print_spam,  ls)  # function object 
