# fetch ROW elements in a two dimentional array.

for row in range(len(a)):
    for column in range(len(a[0])): #start from 0-th position
        answer[row][column]=a[row][column]-a[row][column+1 ]


'''
The same as above but with a twist:
The function diffRow, will check for the last but one row column, as we need
lesser elements from the matrix when you are doing array row-wise difference
or row-wise addition).
'''
def diffRow():
    new = numpy.array(bmfloat[:-1, :-1]) #last elem, lastElem
    for row in range(len(new)):
        for col in range(len(new)):
            new[row][col] = bmfloat[row][col] - bmfloat[row+1][col]
    print new
diffRow()