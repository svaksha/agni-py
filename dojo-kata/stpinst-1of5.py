#!/usr/bin/python 
# coding: utf-8 
'''
LICENSE: Copyright(C)2010, Vidya [svaksha] Ayer <vid@svaksha.com>, GPLv3.
USAGE: python trytonmods.py (While the execution feedback will be displayed 
on the terminal, its output is also logged in the "log-trytonmods.txt" file).
'''
#python modules
import sys
import os
import os.path
import popen2
import time
import string
import datetime
import subprocess

#local paths
filepath, filename = os.path.split(os.path.abspath(os.path.dirname(__file__)))
sys.path.append(filepath)
sys.path.append("/usr/lib/python2.6/")

#print $user 
user = os.environ['USER']
print '\n User:', user
#Add the start time.
startTime = datetime.datetime.now()
print "\nStarting Time for script 'trytonmods.py': ", str(startTime)
#logging to file and scr.
sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)
tee = subprocess.Popen(["tee", "log-trytonmods.txt"], stdin=subprocess.PIPE)
os.dup2(tee.stdin.fileno(), sys.stdout.fileno())
os.dup2(tee.stdin.fileno(), sys.stderr.fileno())

#INSTALLING TRYTON MODULES
#*****************************************************************************

os.system(script) = '''
sudo easy_install analytic_invoice 	
sudo easy_install analytic_purchase
sudo easy_install analytic_sale
sudo easy_install calendar
sudo easy_install calendar_classification
sudo easy_install calendar_scheduling
sudo easy_install calendar_todo
sudo easy_install company
sudo easy_install company_work_time
sudo easy_install country
sudo easy_install currency 	
dashboard 	
google_maps 
ldap_authentication
ldap_connection 	
party 	
party_siret
product
product_cost_fifo
product_cost_history
product_price_list 	
project 	
project_plan
project_revenue
purchase 	
purchase_invoice_line_standalone
sale
sale_opportunity 	
sale_price_list 	
stock 	
stock_forecast
stock_inventory_location
stock_location_sequence 
stock_product_location 	
stock_supply 	
stock_supply_day
timesheet 
'''

#Printing Completion and difference TIME.
#=============================================================================
endTime = datetime.datetime.now()
print "\nEnding Time 'trytonmods.py' is: ", str(endTime)
timedelta = endTime - startTime
#Printing total installation time taken by 'trytonmods' for the system.
#=============================================================================
installationTime = timedelta
print "\nTotal time taken by 'trytonmods.py' for Installation: ", installationTime
