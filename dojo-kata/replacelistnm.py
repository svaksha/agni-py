#!/usr/bin/env python
# -*- coding: utf-8 -*-
#**************************************************************************
# Copyright (C) 2010 VidAyer <vid@svaksha.com>
# License: GNU AGPLv3, <http://www.gnu.org/licenses/agpl-3.0.html>
#**************************************************************************
'''
replacelistnm.py is the top-level python program that will search and replace 
the list_name (or bug_number) embedded in URI's within the selenese-py 
scripts in "auto-functional-test" folder.
LICENSE: Copyright2010, Vidya [svaksha] Ayer <vid@svaksha.com> as GPLv3.
'''

import sys
import re
import os
import os.path
import string 
from StringIO import StringIO

#*****************************************************************************
#SEARCH AND REPLACE the list_name (or bug_number) embedded in URI's within the
#selenese-py scripts in "auto-functional-test" folder.
#*****************************************************************************

#Create and return 'fileslist[]' with all files inside 'folder' matching 'regexp'.
def make_files_list(folder, regexp):

    #If folder is not a folder, exit with an error message.
    if not os.path.isdir(folder):
        sys.exit(folder + ' is not a valid folder. Please re-enter as /path/to/folder!')
    #Compiling search regexp.
    cregex=re.compile(regexp)
    #Initializing file list.
    fileslist = []
    #Loop on all files and select files matching 'regexp'.
    for root, dirs, files in os.walk(folder):
        for name in files:
            if cregex.search(name):
                path = os.path.join(root, name)
                fileslist.append(path)
    #Return the file list.
    return fileslist[:]


#In all files in 'fileslist' search the regexp 'searchregexp' and replace
#with 'replacestring'; real substitution in files only if 'simulation' = 0;
#real substitution may also be step by step (if 'stepbystep' = 1).
def replace_in_files(fileslist, searchregexp, replacestring, simulation, stepbystep):

    cregex=re.compile(searchregexp)
    print fileslist, searchregexp, replacestring
    if simulation == 1:
        #Simulation of the replacement/changes.
        print '\nReplaced (simulation):\n'
    else:
        print '\nReplaced the following:\n'
    for filepath in fileslist:        
        replaceflag=0        
        readlines=open(filepath,'r').readlines()        
        listindex = -1
        #search and replace in current file printing to the user changed lines.
        for currentline in readlines:
            listindex = listindex + 1
            if cregex.search(currentline):
                f=re.sub(searchregexp,replacestring,currentline)
                #print current filepath, the old string and the new string.
                print '\n' + filepath
                print '- ' + currentline ,
                if currentline[-1:]!='\n': print '\n' ,
                print '+ ' + f ,
                if f[-1:]!='\n': print '\n' ,
                #if substitution is real.
                if simulation == 0:
                    #if substitution is step by step.
                    if stepbystep == 1:
                        #ask tester if the current line must be replaced.
                        question = raw_input('write(Y), skip (n), quit (q) ? ')
                        question = string.lower(question)
                        if question=='q':
                            sys.exit('\nInterrupted by the user !!!')
                        elif question=='n':
                            pass
                        #if write.
                        else:
                            #update the whole file variable ('readlines').
                            readlines[listindex] = f
                            replaceflag=1
                    #if substitution is not step by step.
                    else:
                            #update the whole file variable ('readlines').
                            readlines[listindex] = f
                            replaceflag=1

        #When text was replaced overwrite the original file.
        if replaceflag==1:
            write_file=open(filepath,'w') 
            #Write to file  
            for line in readlines:
                write_file.write(line)
            # close the file
            write_file.close()


#main function
def main():
    #if parameters are wrong, exit with error.
    if len(sys.argv) < 5:
        #print parameter.
        print sys.argv  
        print '\nUsage:'
        #four arguments are: 'folderpath', idtag(.py), searchregexp, replacestring.
        print 'python replacelistnm.py foldername files-regexp search-regexp replace-string'
        print "For EXAMPLE: python replacelistnm.py '/home/PATH/to/Folder/anv-d' .py 583379 555864"
        sys.exit(1)    
    #setting complete execution
    question1 = 'y'
    question2 = 'n'    
    #Make the file list.
    fileslist = make_files_list(sys.argv[1], sys.argv[2])
    #If real substitution
    if question1=='y':
        #if step by step?
        if question2!='n':
            replace_in_files(fileslist, sys.argv[3], sys.argv[4], 0, 1)
        #if not step by step
        else:
            replace_in_files(fileslist, sys.argv[3], sys.argv[4], 0, 0)        
    else:
        replace_in_files(fileslist, sys.argv[3], sys.argv[4], 1, 0)
             
            
if __name__ == '__main__':
    main()            
