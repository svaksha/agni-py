#!/usr/bin/env python
# -*- coding: utf-8 -*-
#**************************************************************************
# Copyright (C) 2011 VidAyer <vid@svaksha.com>
# License: GNU AGPLv3, <http://www.gnu.org/licenses/agpl-3.0.html>
#**************************************************************************
for n in range(2, 10):
     for x in range(2, n):
         if n % x == 0:
             print n, 'equals', x, '*', n/x
             break
     else:
         # loop fell through without finding a factor
         print n, 'is a prime number'
