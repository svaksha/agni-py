#!/usr/bin/env python3.3
# -*- coding: iso-8859-15 -*-
###########################################################################
"""
Created on Thu Jan 17 10:45:23 2013
"""
###=======================================================================
import sys, os, os.path
fullPath = os.path.realpath(__file__)
dirPath, prog_file = os.path.split(fullPath)
sys.path.append(dirPath)
import pandas as pd
from pandas import Series, DataFrame, date_range
import numpy as np
import datetime, time

data = {'state': ['Ohio', 'Ohio', 'Ohio', 'Nevada', 'Nevada'],
    'year': [2000, 2004, 2002, 2001, 2002],
    'pop': [1.5, 6.7, 9.6, 2.4, 2.9]}
frame = DataFrame(data)
print (frame)
frame2 = DataFrame(data, columns=['year', 'state', 'pop'])
print (frame2)
print(frame2.columns)
