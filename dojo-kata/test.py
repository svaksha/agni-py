#!/usr/bin/python

#create an instance of each of the two classes / modules
print "\nHere is the key: \
    \n* ch==chain \
    \n* dc==double crochet \
    \n* sc==single crochet"

for i in range(20):
    print "ch",

for i in range(6):
    for j in range(2):
        print "ch",
    print "\nNext: sc==single crochet"
    for j in range(20):
        print "sc",
    print "ch",
    for j in range(10):
        for k in range(3):
            print "ch",
        print "\nnext: dc==double crochet" 
        for k in range(10):
            print "dc ch",
    for j in range(2):
        print "ch",
    print "\nNext: sc==single crochet"
    for k in range(20):
        print "sc",




'''
# tuple
(b,) a = a, b

l =['David', 'Pythonista', '+1-514-555-1234']
name, title, phone = l

"""
Useful in loops over structured data: l (L) above is the list we just made (David's info). 
So people is a list containing two items, each a 3-item list.
"""

people = [l, ['Guido', 'BDFL', 'unlisted']]
 for (name, title, phone) in people:
     print name, phone

'''
