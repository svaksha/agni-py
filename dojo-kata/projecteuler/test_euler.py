#!/usr/bin/env python
# -*- coding: utf-8 -*-

# simple skeleton tests file for eulerPy
import unittest
import sys
import datetime
class MyTest:
    def test_one(self):
        # some test code
        pass
    def test_two(self):
        print "I RAN!"
        pass

  
def get_answer(euler):
	name = 'eulerPython.euler{0}'.format(euler)
	__import__(name, globals(), locals(), [], -1)
	mod = sys.modules[name]
	if hasattr(mod, 'run'):
		return mod.run()
	raise Exception('{0} does not provide a run() method'.format(name))

class EulerTests(unittest.TestCase):
	def test_eulerPython(self):
		self._test(1, 233168)
        #self._test(1.3, 233168)
        #self._test(1_2, 233168)
    
	'''	self._test(3, 6857)
		self._test(4, 906609)
		self._test(5, 232792560)
		self._test(6, 25164150)
		self._test(7, 104743)
		self._test(8, 40824)
		self._test(9, 31875000)
		self._test(10, 142913828922)
		self._test(11, 70600674)
		self._test(12, 76576500)
		self._test(13, 5537376230)
		self._test(14, 837799)
		self._test(15, 137846528820)
		self._test(16, 1366)
		self._test(17, 21124)
		self._test(18, 1074)
		self._test(19, 171)
		self._test(20, 648)
		self._test(21, 31626)
		self._test(22, 871198282)
		self._test(23, 4179871)
		self._test(24, 2783915460)
		self._test(25, 4781)
		self._test(26, 983)
		self._test(27, -59231)
		self._test(28, 669171001)
		self._test(29, 9183)
		self._test(30, 443839)
		self._test(31, 73682)
		self._test(32, 45228)
		self._test(33, 100)
		self._test(34, 40730)
		self._test(35, 55)
		self._test(36, 872187)
		self._test(37, 748317)
		self._test(38, 932718654)
		self._test(39, 840)
		self._test(40, 210)
		self._test(41, 7652413)
		self._test(42, 162)
		self._test(43, 16695334890)
		self._test(44, 5482660)
		self._test(45, 1533776805)
		self._test(46, 5777)
		self._test(47, 134043)
		self._test(48, 9110846700)
		self._test(49, 296962999629)
		self._test(50, 997651)
	'''
	def _test(self, i, answer):
		print "Solving problem:", i
		time_start = datetime.datetime.now()
		#self.assertEqual(get_answer(i), answer)
		time_end = datetime.datetime.now()
		print "Took:", time_end-time_start, "seconds"

if __name__ == "__main__":
	unittest.main()


