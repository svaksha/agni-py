#!/usr/bin/env python
# -*- coding: utf-8 -*-
##############################################################################
#
# COPYRIGHT (C) 2011 VidAYER <svaksha@gmail.com> All Rights Reserved.
# LICENSE: GNU AGPLv3 <http://www.gnu.org/licenses/agpl-3.0.html>
#
##############################################################################
# setup.py file for eulerPy

'''
try:
from setuptools import setup
    except ImportError:
from distutils.core import setup
config = {
'description': 'EulerPyProject',
'author': 'VidAYER',
'url': 'URL to get it at.',
'download_url': 'Where to download i'author_email': 'My email.',
'version': '0.1',
'install_requires': ['nose'],
'packages': ['eulerPy'],
'scripts': [],
'name': 'EulerPyProject'
}
setup(**config)
'''    
