#!/usr/bin/env python
# -*- coding: utf-8 -*-
#*****************************************************************************
# COPYRIGHT (C) 2011-2012 VidAyer <vid@svaksha.com>
# LICENSE: GNU AGPLv3 <http://www.gnu.org/licenses/agpl-3.0.html>
#*****************************************************************************
'''<http://projecteuler.net/problem=7>
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can 
see that the 6th prime is 13. What is the 10,001'st prime number?
'''
#*****************************************************************************

natnum=[a for a in range (1, 10)]
multiplesum =sum([a for a in natnum if a % 3 == 0] + [a for a in natnum if a % 5 == 0])
print "Sum total is:", multiplesum
