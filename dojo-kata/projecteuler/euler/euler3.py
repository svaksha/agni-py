#!/usr/bin/env python
# -*- coding: utf-8 -*-
#*****************************************************************************
# File: euler003.py
# COPYRIGHT (C) 2012 VidAyer <vid@svaksha.com>
# LICENSE: GNU AGPLv3 <http://www.gnu.org/licenses/agpl-3.0.html>
# Distributed under the terms of the GNU General Public License (GPL)
#
# This code is distributed in the hope that it will be useful, but WITHOUT 
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
# License for more details. The full text of the GPL is available at:
# <http://www.gnu.org/licenses/agpl-3.0.html>
#*****************************************************************************
# URI: http://projecteuler.net/problem=3
# The prime factors of 13195 are 5, 7, 13 and 29.
# What is the largest prime factor of the number 600851475143 ?
#
# Usage: python euler003.py
#*****************************************************************************
