#!/usr/bin/env python
# -*- coding: utf-8 -*-
#*****************************************************************************
# File: euler348.py
# COPYRIGHT (C) 2012 VidAyer <vid@svaksha.com>
# LICENSE: GNU AGPLv3 <http://www.gnu.org/licenses/agpl-3.0.html>
# Distributed under the terms of the GNU General Public License (GPL)
#
# This code is distributed in the hope that it will be useful, but WITHOUT 
# ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
# See the GNU General Public License for more details. The full text of 
# the GPL is available at: <http://www.gnu.org/licenses/agpl-3.0.html>
#*****************************************************************************
#
# Question URI: http://projecteuler.net/problem=348
# Many numbers can be expressed as the sum of a square and a cube. 
# Some of them in more than one way. Consider the palindromic numbers 
# that can be expressed as the sum of a square and a cube, both greater 
# than 1, in exactly 4 different ways.  For example, 5229225 is a 
# palindromic number and it can be expressed in exactly 4 different ways:
#
# (22852)**2 + 203)**3
# (22232)**2 + 663**3
# (18102)**2 + 1253**3
# (11972)**2 + 1563**3
# Find the sum of the five smallest such palindromic numbers.
#
# Usage: python euler348.py
#*****************************************************************************

