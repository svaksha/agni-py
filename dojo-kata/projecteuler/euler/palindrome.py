#!/usr/bin/env python
# -*- coding: utf-8 -*-
#*****************************************************************************
# File: euler2.py
# COPYRIGHT (C) 2012 VidAyer <vid@svaksha.com>
# LICENSE: GNU AGPLv3, http://www.gnu.org/licenses/agpl-3.0.html
#*****************************************************************************
# Write a function that takes a string as an argument and displays the 
# letters backward, one per line. 
#*****************************************************************************

prefix = "hello"
fruit = "world"
suffix= "123"
char=None
for char in fruit:
        print char
for letter in prefix:
    print letter + suffix + fruit
