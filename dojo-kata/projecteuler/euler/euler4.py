#!/usr/bin/env python
# -*- coding: utf-8 -*-
#*****************************************************************************
# COPYRIGHT (C) 2011, 2012 VidAyer <vid@svaksha.com>
# LICENSE: GNU AGPLv3, http://www.gnu.org/licenses/agpl-3.0.html
#*****************************************************************************
# http://projecteuler.net/problem=4
# A palindromic number reads the same both ways. The largest 
# palindrome made from the product of two 2-digit numbers 
# is 9009 = 91 × 99. Find the largest palindrome made from the 
# product of two 3-digit numbers.
#*****************************************************************************

declare a range of digits
product of range of 2 digits and reverse the digit to check if its a palindrome
store if its a palindrome  and repeat until you reach the largest palindrome 
number 999999 by compring with earlier result



def palindrome(tridigit):
