#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# COPYRIGHT (C) 2011 Vid_Ayer <svaksha@gmail.com>
# LICENSE: GNU AGPLv3 <http://www.gnu.org/licenses/agpl-3.0.html>
    
'''
http://projecteuler.net/problem=9
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which, a**2 + b**2 = c**2
For example, 3**2 + 4**2 = 9 + 16 = 25 = 5**2.
There exists exactly one Pythagorean triplet for which a + b + c = 1000. Find the product abc.
'''

# ALGO:  to divide 1000,  use 2,  5 and find LCD

def square_A(self, more):
    A = 250
    self.squareA += more
    return self.squareA

def square_B(self, more):
    B= 333
    self.squareB += more
    return self.squareB


def square_C(self, more):
    C=417
    self.squareC += more
    return self.squareC

'''
for triPythagor in range(1, 1001):
   # Multiples of 2 && 5, print "LCD". 
    if triPythagor % 2 == 0 and triPythagor % 5 == 0:
        print "LCD" 333
    else:
        print triPythagor
'''
