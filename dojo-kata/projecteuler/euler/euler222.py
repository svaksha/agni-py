#!/usr/bin/env python
# -*- coding: utf-8 -*-
#*****************************************************************************
# File: euler222.py
# COPYRIGHT (C) 2012 VidAyer <vid@svaksha.com>
# LICENSE: GNU AGPLv3 <http://www.gnu.org/licenses/agpl-3.0.html>
# Distributed under the terms of the GNU General Public License (GPL)
#
# This code is distributed in the hope that it will be useful, but WITHOUT 
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
# License for more details. The full text of the GPL is available at:
# <http://www.gnu.org/licenses/agpl-3.0.html>
#*****************************************************************************
# Question URI: http://projecteuler.net/problem=222
# What is the length of the shortest pipe, of internal radius 50mm, that can 
# fully contain 21 balls of radii 30mm, 31mm, ..., 50mm?
# Give your answer in micrometres (10-6 m) rounded to the nearest integer.
#
# Usage: python euler222.py
#*****************************************************************************

