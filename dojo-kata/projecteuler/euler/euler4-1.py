#!/usr/bin/env python
# -*- coding: utf-8 -*-
#*****************************************************************************
# COPYRIGHT (C) 2011, 2012 VidAyer <vid@svaksha.com>
# LICENSE: GNU AGPLv3, http://www.gnu.org/licenses/agpl-3.0.html
#*****************************************************************************
# http://projecteuler.net/problem=4
# A palindromic number reads the same both ways. The largest 
# palindrome made from the product of two 2-digit numbers 
# is 9009 = 91 × 99. Find the largest palindrome made from the 
# product of two 3-digit numbers.
#*****************************************************************************

def is_palindrome(tridigit):
	d = str(tridigit)
	return d == d[::-1] # end of function block indentation

def run():
    z=[0]
    palindromes = [0]
    for x in range(100,1000):
        for y in range(100, x + 1):
            z = x * y
            if is_palindrome(z):
                palindromes.append(z)
    palindromes.sort()
    palindromes.reverse()
    print palindromes[0]   # end of function block indentation

if __name__ == "__main__":
	print run()
