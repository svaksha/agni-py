#!/usr/bin/env python
# -*- coding: utf-8 -*-
#**************************************************************************
# COPYRIGHT (C) 2011-2012 VidAyer <vid@svaksha.com>
# LICENSE: GNU AGPLv3, http://www.gnu.org/licenses/agpl-3.0.html
#**************************************************************************
'''<http://projecteuler.net/problem=1>
 If we list all the natural numbers below 10 that are multiples of 3 or 5, 
 we get 3, 5, 6 and 9. The sum of these multiples is 23.
 Find the sum of all the multiples of 3 or 5 below 1000.
'''
#**************************************************************************

def run():
    listMult= []                                                         # a list for multiples
    sum, count=0, 0                                               # counter
    for natnum in range(1, 1000):
        if natnum % 3 == 0 or natnum % 5 == 0:   #divisibility remainder check
            listMult.append(natnum)
            print "The multiple of 3 or 5 is", listMult[count]
            sum += natnum
            print "The sum of multiples of 3 or 5 is", sum
            count += 1                                                # counter  increments list elements

if __name__ == "__main__":
        print run()
