#!/usr/bin/env python
# -*- coding: utf-8 -*-
#*****************************************************************************
# File: euler60.py
# COPYRIGHT (C) 2011-2012 VidAyer <vid@svaksha.com>
# LICENSE: GNU AGPLv3, http://www.gnu.org/licenses/agpl-3.0.html
#*****************************************************************************
'''<http://projecteuler.net/problem=60>
The primes 3, 7, 109, and 673, are quite remarkable. By taking any 
two primes and concatenating them in any order the result will always 
be prime. For example, taking 7 and 109, both 7109 and 1097 are prime. 
The sum of these four primes, 792, represents the lowest sum for a 
set of four primes with this property. Find the lowest sum for a set of 
five primes for which any two primes concatenate to produce another prime.
'''
#*****************************************************************************
natnum=[a for a in range (1, 10)]
multiplesum =sum([a for a in natnum if a % 3 == 0] + [a for a in natnum if a % 5 == 0])
print "Sum total is:", multiplesum
