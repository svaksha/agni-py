#!/usr/bin/env python
# -*- coding: utf-8 -*-
#*****************************************************************************
# File: euler125.py
# COPYRIGHT (C) 2011-2012 VidAyer <vid@svaksha.com>
# LICENSE: GNU AGPLv3, http://www.gnu.org/licenses/agpl-3.0.html
#*****************************************************************************
'''<http://projecteuler.net/problem=125>
The palindromic number 595 is interesting because it can be written as 
the sum of consecutive squares: 62 + 72 + 82 + 92 + 102 + 112 + 122.
There are exactly eleven palindromes below one-thousand that can be 
written as consecutive square sums, and the sum of these palindromes 
is 4164. Note that 1 = 02 + 12 has not been included as this problem 
is concerned with the squares of positive integers.
Find the sum of all the numbers less than 108 that are both 
palindromic and can be written as the sum of consecutive squares.
'''
#*****************************************************************************
natnum=[a for a in range (1, 10)]
multiplesum =sum([a for a in natnum if a % 3 == 0] + [a for a in natnum if a % 5 == 0])
print "Sum total is:", multiplesum

