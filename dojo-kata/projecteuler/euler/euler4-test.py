#!/usr/bin/env python
# -*- coding: utf-8 -*-

def is_palindrome(value):
	s = str(value)
	return s == s[::-1]

def run():
	return max(a*b for a in xrange(100, 1000) for b in xrange(100, 1000) if is_palindrome(a*b))

if __name__ == "__main__":
	print run()


'''
word = (for word in word range (100, 999))
word = str(word)
reverse = word[::-1]
if word == reverse:
    print 'This is a palindrome!'
else:
    print 'This is not a palindrome.'

# http://stackoverflow.com/questions/952110/recursive-function-palindrome-in-python
tridigit = [100,  999] 
'''
'''
def isapalindrome(tridigit):
    return tridigit == tridigit[::-1]
    return max(a*b for a in xrange(100, 1000) for b in xrange(100, 1000) if isapalindrome(a*b))
tridigit=[a for a in range (1, 999)]
print "Product of two largest 3-digit palindromes is:", tridigit
'''
'''
'''
