#!/usr/bin/env python
# -*- coding: utf-8 -*-
#*****************************************************************************
# COPYRIGHT (C) 2011 VidAyer <vid@svaksha.com>
# TimeDate: 2011June15, 12:08 +0530
# LICENSE: GNU AGPLv3, http://www.gnu.org/licenses/agpl-3.0.html
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#*****************************************************************************
'''<http://projecteuler.net/problem=1>
If we list all the natural numbers below 10 that are multiples of 3 or 5, 
we get 3, 5, 6 and 9. The sum of these multiples is 23.
Find the sum of all the multiples of 3 or 5 below 1000.
'''
#*****************************************************************************

def run():
    natnum = [a for a in range (1, 10)]
    multiplesum = sum([a for a in natnum if a % 3 == 0] + [a for a in natnum if a % 5 == 0])
    print "Sum total is:", multiplesum
    return 0

if __name__ == "__main__":
        print run()
