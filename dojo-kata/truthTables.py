from collections import defaultdict

def returning(a, do):
    do(a)
    return a

def group_by(key, iterable):
    return dict(reduce(lambda a, e: returning(a, lambda x: x[key(e)].append(e)), iterable, defaultdict(list)))

truthiness = bool

class MethodStub:
    def __init__(self, val): self.val = val
    def __repr__(self): return '%s(%s)' % (self.__class__.__name__, self.val)
    
class LenTruth(MethodStub):
    def __len__(self): return self.val

class NonZeroTruth(MethodStub):
    def __nonzero__(self): return self.val

print group_by(truthiness, [True, False, -1, 0, 1, 2, None, object(), '', (), [], 'a string', (object()), [object()], {}, {'foo': object()}, LenTruth(0), NonZeroTruth(False), LenTruth(1), NonZeroTruth(True)])

#{False: [False, 0, None, '', (), [], {}, LenTruth(0), NonZeroTruth(False)], True: [True, -1, 1, 2, <object object at 0x7f19ce06d090>, 'a string', <object object at 0x7f19ce06d0a0>, [<object object at 0x7f19ce06d0b0>], {'foo': <object object at 0x7f19ce06d0c0>}, LenTruth(1), NonZeroTruth(True)]} 
