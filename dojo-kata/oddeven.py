#!/usr/bin/env python
# coding: utf-8 
#
# Copyright (C) 2011 Vid_Ayer <svaksha@gmail.com>
# License: GNU AGPLv3, <http://www.gnu.org/licenses/agpl-3.0.html>

even = [] #creates an empty list
odd = []
for x in range(0, 100):
    if x % 2 == 0:
        even.append(x)
    else:
        odd.append(x)
