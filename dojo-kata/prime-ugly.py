#!/usr/bin/env python
# -*- coding: utf-8 -*-

def sieve_eratosthenes(limit):
    """ Return all the primes below (NOT INCLUDING) the specified limit. """

    numbers = []
    primes = []

    for i in range(2, limit):
        numbers.append(i)

    while True:
        if len(numbers) == 0:
            break

        prime = numbers[0]
        primes.append(prime)
        multiples = []

        for n in numbers:
            if n % prime == 0:
                multiples.append(n)

        for n in multiples:
            numbers.remove(n)

    return primes
