#!/usr/bin/env python

gnp = [1, 2, 3, 5, 8]
poverty = [0.11, 0.12, 0.13, 0.15, 0.18]
vectorgnpgnp = 0.0
vectorpovertypoverty = 0.0
vectorgnppoverty = 0.0
costheta = 0.0
n = 1.0

vectorgnpgnp = vectorgnpgnp + gnp[n]*gnp[n]
vectorpovertypoverty = vectorpovertypoverty + poverty[n]*poverty[n]
vectorgnppoverty = vectorgnppoverty + gnp[n]*poverty[n]
costheta = vectorgnppoverty / sqrt(vectorgnpgnp)*sqrt(vectorpovertypoverty)

# terminal output
println("The CosTheta value is:          ",costheta)
println("GNP and Poverty index value is: ",vectorgnppoverty)
println("The GNP value is:               ",vectorgnpgnp)
println("Poverty value is:               ",vectorpovertypoverty)
