#!/usr/bin/env python
# -*- coding: utf-8 -*-
##=============================================================================
## Problem Set: https://class.coursera.org/biostats-2012-001/lecture/54
##=============================================================================
## Bootstrap procedure for calculating the median from the data set of
## n observations
## 1. sample 'n' observations with replacement from the observed data resulting
##    in one simulated complete data set.
## 2. Take the median of the simulated data set.
## 3. Repeat these two steps B times, resulting in B simulated medians
## 4. These medians are approximately draws from the sampling distriution of
##    the median of n observations. Therefore we can,
##        - Draw a histogram of them.
##        - Calculate their standard deviation to estimate the standard error of the median.
##        - Take the 2.5th and 97.5th percentiles as a confidence interval for the median.

# Numpy libs
from numpy import *
## SciPy libs
# from scipy import linspace
from scipy import stats
from scipy import pi, sqrt, exp
##from cmath import sqrt, pi, exp # standard python lib
from pylab import plot,show

B = 1000
n = length(gmVol)
resamples = matrix()



((2n-1)/n)

T-statistic sample mean
e t-statistic to estimate the distribution of the sample mean,
bar(x) = (1/10) (x1 + x2 + .... + x10)


## Suppose we simulated a large amount of random uniform numbers and a large
## amount of exponential(1) numbers. What would a plot of the quantiles of one
## versus the other look like? (Uniform on the horizontal axis and the
## exponential on the vertical axis.)