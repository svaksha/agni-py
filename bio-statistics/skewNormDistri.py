#!/usr/bin/env python
# -*- coding: utf-8 -*-
## https://en.wikipedia.org/wiki/Skew_normal_distribution
## http://stackoverflow.com/questions/5884768/skew-normal-distribution-in-scipy

# Numpy libs
from numpy import *
# SciPy libs
from scipy import linspace
from scipy import stats
from scipy import pi, sqrt, exp
##from cmath import sqrt, pi, exp
from pylab import plot,show

# Formulae: \phi(x)=\frac{1}{\sqrt{2\pi}}e^{-\frac{x^2}{2}}

def skewNormDist(snd): # unwrapped function (method) for SND
    phiX = {[1/sqrt(2*pi)]*[exp(-x**2/2)]}
    skewNormDist(snd)
    print phiX

#user = raw_input('enter a number =')

# Cumulative distribution function is given by
# \Phi(x) = \int_{-\infty}^{x} \phi(t)\ dt = \frac{1}{2} \left[ 1 + \text{erf} \left(\frac{x}{\sqrt{2}}\right)\right]
# where erf is the error function.

def CumuDistFunc(cdf):
    """
    Integration, http://docs.scipy.org/doc/scipy/reference/integrate.html
    Error functions and fresnel integrals, http://docs.scipy.org/doc/scipy/reference/special.html
    """
    return (1 + erf(x/sqrt(2))) / 2

# The ProbabilityDensityFunction of the SND (skewNormDist) with parameter α is
# f(x) = 2\phi(x)\Phi(\alpha x). \


