# Suppose that hospital infection counts are models as Poison with mean μ.
# Recall the Poison mass function with mean μ is μxe−μx! for x=0,1,….
# Three independent hospitals are observed for one year and their infection
# counts where 5, 4 and 6, respectively. What is the ML estimate for μ?





# Let X be a geometric random variable. That is X counts the number of coin
# flips until one obtains the first head.
# The mass function is P(X=x)=p(1−p)x−1. for x=1,2,….
# What is the maximum likelihood estimate for p if one observes a
# geometric random variable?the median