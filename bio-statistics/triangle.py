#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
# Computing the area of the triangle
def triangle_area(base, height):  # header of the function
    area = (1/2) * base * height # parameters for my function
    return area  #returns the output of the value in the variable parameter
##
# to call the function
a1 = triangle_area(3,8)
print a1
a2=triangle_area(14,2)
print a2

# convert farenheit to celsius
def farenheit2celsius(farenheit):
    celsius=(5/9)*(farenheit-32)
    return celsius

# test
c1=faraenheit2celsius(32)
c2=farenheit2celsius(212)
print c1, c2
