#!/usr/bin/env python
# -*- coding: utf-8 -*-

# https://class.coursera.org/biostats-2012-001/lecture/58

from numpy import *
from scipy import linspace
from scipy import stats

alpha = 0.05
nTilde = (X+2)/(n+4)
snd = 1 - (alpha/2)
## Agresti-Coull interval is an approximate binomial confidence interval
## pTilde = pTilde + z_snd(sqrt(pTilde(1-pTilde)/nTilde)
