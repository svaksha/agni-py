-- performing no normalization - using names exactly as they were entered
-- count    names
-- 1        228 names that appear once
-- 2        32 names that appear twice
-- 3        ryan, nick, marc, justin, jeff, brian, brandon, alex, aaron
-- 5        mike, david, daniel, dan, christopher
-- 6        james
-- 7        jason, chris
-- 8        adam
-- 9        michael
-- 279      Distinct names
-- 381      Total attendees

drop view if exists normalized_names;
create view normalized_names as (
	select
		lower(`First Name`) as name
	from tickets_raw
);

select
	count,
	case
		when count = 1 then CONCAT(count(*), " names that appear once")
		when count = 2 then CONCAT(count(*), " names that appear twice")
		else group_concat(distinct name order by name desc separator ', ')
	end as names
from (
	select
		name,
		count(*) as count
	from normalized_names
	group by name
	order by count(*)
) as x
group by count
union
select count(*), "Distinct names" from (select distinct name from normalized_names) as y
union
select count(*), "Total attendees" from normalized_names


