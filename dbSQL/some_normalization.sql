-- Performing some normalization on names (ex, 'mike' -> 'michael')
-- count  name
-- 1      226 names that appear once
-- 2      32 names that appear twice
-- 3      ryan, nick, marc, justin, brian, brandon, alex, aaron
-- 4      jeffrey
-- 6      james, david
-- 7      jason
-- 8      adam
-- 10     daniel
-- 12     christopher
-- 14     michael
-- 274    Distinct names
-- 381    Total attendees


drop view if exists normalized_names;
create view normalized_names as (
	select
		case lower(`First Name`)
			-- appologies to those who feel strongly that their first name
			-- is unique from the historically recognized longer form.
			when 'mike' then 'michael'
			when 'dan' then 'daniel'
			when 'chris' then 'christopher'
			when 'dave' then 'david'
			when 'jeff' then 'jeffrey'
			else lower(`First Name`)
		end as name
	from tickets_raw
);

select
	count,
	case
		when count = 1 then CONCAT(count(*), " names that appear once")
		when count = 2 then CONCAT(count(*), " names that appear twice")
		else group_concat(distinct name order by name desc separator ', ')
	end as names
from (
	select
		name,
		count(*) as count
	from normalized_names
	group by name
	order by count(*)
) as x
group by count
union
select count(*), "Distinct names" from (select distinct name from normalized_names) as y
union
select count(*), "Total attendees" from normalized_names
