-- looking at the first 4 characters of names
-- count    name
-- 1        197 names that appear once
-- 2        30 names that appear twice
-- 3        ryan, nick, just, juli, jess, jenn, fran, bria, bran, aaro
-- 4        matt, marc, jeff, anto, andr
-- 5        mike, dani, dan
-- 6        jame, davi
-- 7        jaso
-- 8        alex, adam
-- 10       mich
-- 14       chri
-- 252      Distinct names
-- 381      Total attendees

drop view if exists normalized_names;
create view normalized_names as (
        select
                substr(lower(`First Name`) from 1 for 4) as name 
        from tickets_raw
);

select
        count,
        case
                when count = 1 then CONCAT(count(*), " names that appear once")
                when count = 2 then CONCAT(count(*), " names that appear twice")
                else group_concat(distinct name order by name desc separator ', ')
        end as names
from (
        select
                name,
                count(*) as count
        from normalized_names
        group by name
        order by count(*)
) as x
group by count
union
select count(*), "Distinct names" from (select distinct name from normalized_names) as y
union
select count(*), "Total attendees" from normalized_names

