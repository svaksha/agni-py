#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#*****************************************************************************
# COPYRIGHT (C) 2012 VidAyer <vid@svaksha.com>
# LICENSE: GNU AGPLv3 <http://www.gnu.org/licenses/agpl-3.0.html>
# Distributed under the terms of the GNU General Public License (GPL)
#
# This code is distributed in the hope that it will be useful, but WITHOUT 
# ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
# See the GNU General Public License for more details. The full text of 
# the GPL is available at: <http://www.gnu.org/licenses/agpl-3.0.html>
#*****************************************************************************


        l: a "light" syllable (L), called laghu
        g: a "heavy" syllable (H), called guru

    The gaṇas:[9]

        m : H-H-H, called ma-gaṇa
        y : L-H-H, called ya-gaṇa
        r : H-L-H, called ra-gaṇa
        s : L-L-H, called sa-gaṇa
        t : H-H-L, called ta-gaṇa
        j : L-H-L, called ja-gaṇa
        bh: H-L-L, called bha-gaṇa
        n : L-L-L, called na-gaṇa

    ya-gaṇa: ya-mā-tā = L-H-H
    ma-gaṇa: mā-tā-rā = H-H-H
    ta-gaṇa: tā-rā-ja = H-H-L
    ra-gaṇa: rā-ja-bhā = H-L-H
    ja-gaṇa: ja-bhā-na = L-H-L
    bha-gaṇa: bhā-na-sa = H-L-L
    na-gaṇa: na-sa-la = L-L-L
    sa-gaṇa: sa-la-gā = L-L-H
