
import scipy
'''
def test():
    a1 = scipy.arange(5, 10)
    print a1
    a2 = scipy.zeros((4,5), dtype='f')
    print a2
test()
'''
import scipy
a1 = scipy.array([1, 2, 3, 4,])
a2 = scipy.array([4, 3, 2, 1,])
a3 = a1 * a2
print a3
a1 = scipy.zeros((4,5))
print a1
a2 = scipy.empty((4,5))
print a2
a3 = scipy.zeros((4,5), dtype='f')
print a3
