

Dear Sir/Madam,

This application letter is with reference to 'Research fellow (PhD candidate) in informatics' at the University of Bergen.

My Master's degree in Computer Application's academic course-work along with nearly 5 years of technical volunteering experience across Floss communities, would be symmetric with the vision and goals of the University, as it has provided me with in-depth technical knowledge.

Free software volunteer details: http://svaksha.com/pages/svaksha

I, herewith, enclose the Abstract paper for a "Sanskrit Grammar Parser" along with my CV,  which has my contact information. Besides I am also enclosing the scanned BCA and MCA transcripts for your perusal.

Thank you for your consideration !


Sincerely,

Vidya Ayer





TITLE: 
Sanskrit Grammar Parser 
    by Vidya AYER, IGNOU, India.


ABSTRACT: 
In this paper, I propose using artificial intelligence to approach and translate Sanskrit morphology in the Ashtadhyayi grammar[0] and Paninian[1] rules that form the core in Sanskrit grammar, to create a parser/lexical analyser written in the Sanskrit script (Unicode/utf-8 support).

KEYWORDS: 
Sanskrit syntax, lexical analyser, Ashtadhyayi, Panini grammar.

[0] http://en.wikipedia.org/wiki/Sanskrit_grammar
[1] http://en.wikipedia.org/wiki/P%C4%81%E1%B9%87ini


The idea that syntax and semantics in the programming language will be quite similar to that in a natural language from which the programming language draws its lexicon (vocabulary) isnt much because the syntax and semantics are generally heavily simplified and modified to make things easier for both the human (programmer) and the machine (computer). However, this largely depends on the functionality of the lexical analyser -- Unlike English, Sanskrit has a number of compound words, which are governed by "Sandhi", "Samasa" and case rules. The Sandhi rules for fusing words are precise and systematically technical while 'Samasa' are rules for forming compound words which enhances the possibility of forming new words.

An UTF-8 based Libre parser/lexical analyser would change the way analysis is done. Here is a Syntax example, Consider the sentence "स्वक्ष अस्ति" (svaksha asti)  where, 'asti' means 'existent' or 'present'. Hence, the sentences "Svaksha is present" and "Svaksha (a person) exists" have the same meaning. However, "स्वक्ष अस्ति" (svaksha asti) could also have the following multiple meanings if 'स्वक्ष' is split as स्व + क्ष , which is derived from the compounds "स + व" and "क + ष" : 

0. I exist/I am here (svaksha can be either masculine or feminine proper name.).
1. Nothing exists. (--philosophically, svaksha means "a void, or nothingness" and hence a metaphor for "zephyrum/zero").
2. A chariot having a beautiful axle is here [Masculine/Feminine/Neuter].
3. A person having perfect organs of sense exists [Masculine/Feminine/Neuter].
4. The beautiful-eyed person (known above as 'svaksha') is here/exists [Masculine/Feminine/Neuter].

Here I have applied sandhi rules (which are standard conjugation grammar rules) to one sentence, resulting in multiple meanings because each consonant has root morphemes. In comparision, say, a language like English would not assign root values (meanings) to singular consonants like 'B', 'C', or 'D' ; nor would the sentence "Rami is here" equate to "Rami exists". 

In computational linguistics this study would be valuable since typically, existing parsers draw heavily from the English (or latin-based) languages, which in comparision to Sanskrit has lesser number of root morphemes. As suggested above, creating a regular expression based Sanskrit-UTF8 parser for certain common constructs will enable us to parse the sanskrit script as per the rules in Ashtadhyayi and Paninian grammar. The different perspective in analysis can be achieved by creating a bunch of databases of language-pairs for declensions, samasa and sandhi that will be analysed. For starters, a language corpus would have to be created since the existing corpuses are non-free. 


    © 2010 VidyaAYER. This is an Abstract paper written for the Research fellowship (PhD candidate) in informatics at The University of Bergen (UiB).




