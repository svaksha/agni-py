#!/usr/bin/env python
#
# Simple command-line sorter example; takes as an argument a
# comma-separated list of values to configure a MultiValueSorter to
# use. The list may start or end with 'R' for relevance to be included
# in the sort.
#
# Copyright (C) 2003,2008 James Aylett
# Copyright (C) 2004,2007 Olly Betts
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA

import sys
import xapian

# We require at least three command line arguments.
if len(sys.argv) < 4:
    print >> sys.stderr, "Usage: %s PATH_TO_DATABASE SORTSPEC QUERY" % sys.argv[0]
    print >> sys.stderr, "SORTSPEC = key[,key,...], where key is a value number or 'R' for relevance"
    sys.exit(1)

# Open the database for searching.
database = xapian.Database(sys.argv[1])

# Start an enquire session.
enquire = xapian.Enquire(database)

# Create a MultiValueSorter and configure it suitably
sort = 'VALUES' # or 'VALUES_THEN_RELEVANCE', 'RELEVANCE_THEN_VALUES'
nkeys = 0
sorter = xapian.MultiValueSorter()
for key in sys.argv[2].split(','):
    if key=='':
        continue
    if key=='R':
        if nkeys==0:
            sort = 'RELEVANCE_THEN_VALUES'
        else:
            sort = 'VALUES_THEN_RELEVANCE'
    else:
        if sort=='VALUES_THEN_RELEVANCE':
            print >> sys.stderr, "Relevance can only be the first or last sort key."
            sys.exit(1)
        forward = True
        if key[0]=='-':
            forward = False
            key = key[1:]
        sorter.add(int(key), forward)
        nkeys += 1
if nkeys==0:
    enquire.set_sort_by_relevance()
elif sort=='VALUES':
    enquire.set_sort_by_key(sorter)
elif sort=='VALUES_THEN_RELEVANCE':
    enquire.set_sort_by_key_then_relevance(sorter)
elif sort=='RELEVANCE_THEN_VALUES':
    enquire.set_sort_by_relevance_then_key(sorter)

# Combine the rest of the command line arguments with spaces between
# them, so that simple queries don't have to be quoted at the shell
# level.
query_string = " ".join(sys.argv[3:])

# Parse the query string to produce a Xapian::Query object.
qp = xapian.QueryParser()
stemmer = xapian.Stem("english")
qp.set_stemmer(stemmer)
qp.set_database(database)
qp.set_stemming_strategy(xapian.QueryParser.STEM_SOME)
query = qp.parse_query(query_string)
print "Parsed query is: %s" % str(query)

# Find the top 10 results for the query.
enquire.set_query(query)
matches = enquire.get_mset(0, 10)

# Display the results.
print "%i results found." % matches.get_matches_estimated()
print "Results 1-%i:" % matches.size()

for m in matches:
    print "%i: %i%% docid=%i [%s]" % (m.rank + 1, m.percent, m.docid, m.document.get_data())
