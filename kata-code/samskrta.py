#!/usr/bin/env python
# coding: utf-8 
'''
LICENSE: Copyright(C)2010, Vidya [svaksha] Ayer <vid@svaksha.com>, GPLv3.
USAGE: python samskrta.py (While the execution feedback will be displayed 
on the terminal, its output is also logged in the "log-samskrta.txt" file).
'''
#python standard modules
import datetime
import os
import popen2
import string
import subprocess
import sys
import time


#samskrt vowels dict-array
savowel = {
            'अ' : 'a',
            'आ' : 'ā',
            'इ' : 'i',
            'ई' : 'ī',
            'उ' : 'u',
            'ऊ' : 'ū',
            'ऋ' : 'ṛ',
            'ॠ' : 'ṝ',            
            'ऌ' : 'ḷ'
            'ॡ' : 'ḹ'
            'ए' : 'e',
            'ऐ' : 'ai',
            'ओ' : 'o',
            'औ' : 'au',
          }
        

#samskrit consonant dict-array
saconsonant = {
            'क' : 'k',
            'ख' : 'kh',
            'ग' : 'g',
            'घ' : 'gh',
            'ङ' : 'ŋ',
            #Palatal==Tālavya
            'च' : 'ch',
            'छ' : 'chh',
            'ज' : 'j',            
            'झ' : 'jh',
            'ञ' : 'ɲ',
            #Retroflex==Mūrdhanya
            'ट','ठ','ड','ढ','ण',
            #Dantya
            'त','थ','द','ध','न',
            #Labial-Ōshtya
            'प','फ','ब','भ','म',
          }


#Returns the month code for regular years
def getMonthCode(monthname):
    value = str(monthname)
    m_code = {      '1' : '1',
                    '2' : '4',
                    '3' : '4',
                    '4' : '0',
                    '5' : '2',
                    '6' : '5',
                    '7' : '0',
                    '8' : '3',
                    '9' : '6',
                    '10' : '1', 
                    '11' : '4',
                    '12' : '6'
                    }
    return m_code[value]

#sandhi for vowel + consonant
def leapMonthCode(monthname):
    leapvalue = str(monthname)
    leap_m_code = { '1'  : '0',
                    '2' : '3',
                    '3' : '4',
                    '4' : '0',
                    '5' : '2',
                    '6' : '5',
                    '7' : '0',
                    '8' : '3',
                    '9' : '6',
                    '10' : '1', 
                    '11' : '4',
                    '12' : '6'
                    }
    return leap_m_code[leapvalue]
    
def firtTwoDigitCode(year):
    b = int(year) / 100
    mod_value = b % 4
    if mod_value == 0:
        firstResult = 6
    elif mod_value ==1:
        firstResult = 4
    elif mod_value ==2:
        firstResult = 2
    elif mod_value ==3:
        firstResult = 0
    return firstResult


#gets the year, takes last two digit, and returns the result of two digit by modulus 7
def lastTwoDigit(year):
    yr = year[2:4]
    l = int (yr)
    val = l % 7
    return val

#gets the last two digit of the year and returns the last two digit divided by 4
def oneFourth(year):
    lasttwo = year[2:4]
    one_frth = int(lasttwo) / 4
    return one_frth


def calculate():
    print "***********ENTER THE YEAR BETWEEN 0000 to 9999****************"
    date = raw_input("Enter the date :")
    d = int ( date )    
    month = raw_input("Enter the month :")
    m = int(month)
    year = raw_input("Enter the year :")
    month_code = getMonthCode(month)
    leap_month_code = leapMonthCode(month)
    first2DigitCode = firtTwoDigitCode(year)
    last2Digit = lastTwoDigit(year)
    one_fourth = oneFourth(year)
    cal = calendar.isleap(int(year))
    if d > 31:
        print "Invalid Date"
    elif m > 12:
        print "Invalid Month"    
    elif cal:
        if ( d == 30 )  and ( m == 2 ):
            print "Invalid Date"
        elif ( d == 31 ) and (m == 2):
            print "Invalid Date"
        else:            
            output = d + int(leap_month_code) + first2DigitCode + last2Digit + one_fourth
            final_result = output % 7
            print "The final_result is inside leap else",final_result           
            print "The Corresponding Weekday is ",day_name[str(final_result)] 
    elif ( d == 30 ) and ( m == 2 ):
        print "Invalid Date"             
    elif ( d == 29 )  and  ( m == 2 ):
        print "Invalid Date" 
    elif ( d == 31 ) and  ( m ==  2 ): 
        print "Invalid Date"
    elif ( d == 31 )  and  ( m == 4 ):
        print "Invalid Date"
    elif ( d == 31 )  and  ( m == 6 ):
        print "Invalid Date"
    elif ( d == 31 )  and  ( m == 9 ):
        print "Invalid Date"
    elif ( d == 31 )  and  ( m == 11 ):
        print "Invalid Date"                       
    else:
        output = d + int (month_code) + first2DigitCode + last2Digit + one_fourth
        final_result = output % 7
        print "The final_result is inside else",final_result
        print "The Corresponding Weekday is ",day_name[str(final_result)]
    
calculate() 
