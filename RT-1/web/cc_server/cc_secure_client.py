""" cc_secure_client.py:
	This mod;le will demonstrate the secure functionality of the Requests library with the credit card server.
    	It will verify an SSL certificate from the server and pass in some json for processing. You must start the server with secure funcitonality.
"""
import requests
import json

CERT_PATH = "/etc/httpd/ssl_keys/proto2_revenuetrades_com.crt"
KEY_PATH = "/etc/httpd/ssl_keys/myserver.key"

def secure_get():
    print("Attempting a secure get")
    try:
        r = requests.get('https://proto2.revenuetrades.com:8888/process', cert=(CERT_PATH, KEY_PATH), verify=True)
        print("secure get successful")
        print("Here's what the page says: ")
        print((r.text))
        print("Here's the status code:")
        print((str(r.status_code) + "\n"))
    except requests.exceptions.SSLError as sse:
        print(("SSL Error: " + str(sse) + "\n"))

def secure_post():
    print("Attempting a secure post")
    try:
        r = requests.post('https://proto2.revenuetrades.com:8888/process', verify=False)
        print("posted successfully")
        print("Here's what the page says:")
        print((r.text))
        print("Here's the status code:")
        print((str(r.status_code) + "\n"))
    except requests.exceptions.SSLError as ssle:
        print(("SSL error: " + str(ssle) + "\n"))

def secure_json_post():
    print("Attempting a secure post w/json")
    info = json.dumps({'name': 'Sarah Connor', 'cc_num': '12345'})
    headers = {'content-type': 'application/json'}
    try:
        r = requests.post('https://proto2.revenuetrades.com:8888/process', data=info, headers=headers, verify=False)
        print("After a secure JSON post, here's what the page says:")
        print((r.text))
        print("Here's the status code:")
        print((r.status_code))
    except requests.exceptions.SSLError as ssle:
        print((str(ssle) + "\n"))


if __name__ == "__main__":
    secure_get()
    secure_post()
    secure_json_post()
