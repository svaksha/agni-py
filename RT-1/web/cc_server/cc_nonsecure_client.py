""" cc_nonsecure_client.py:
	This module will demonstrate the basic functionality of the Requests library with the credit card server.
    	It does not use SSL and is merely for testing purposes.
"""
import requests
import json

CERT_PATH = "/etc/httpd/ssl_keys/proto2_revenuetrades_com.crt"
KEY_PATH = "/etc/httpd/ssl_keys/myserver.key"


def basic_get():
    print("Doing a basic get w/unsecure url")
    r = requests.get("http://localhost:8888")
    print(("The response is: " + r.text))

def basic_post():
    print("Doing a basic post")
    r = requests.post('http://proto2.revenuetrades.com:8888')
    print(("The response is: " + r.text))

def basic_json_post():
    print("Doing a post w/json")
    info = json.dumps({'name': 'Sarah Connor', 'cc_num': '12345'})
    headers = {'content-type': 'application/json'}
    r = requests.post('http://proto2.revenuetrades.com:8888', data=info, headers=headers)
    print((r.text))

if __name__ == "__main__":
    basic_get()
    basic_post()
    basic_json_post()
