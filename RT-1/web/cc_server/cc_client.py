"""This module will demonstrate the basic functionality of the Requests library with the credit card server.
    It will ultimately verify an SSL certificate from the server and pass in some json for processing.
"""
import requests
import json

URL = 'http://proto2.revenuetrades.com:8888/process'
SECURE_URL = 'https://proto2.revenuetrades.com:8888/process'
CERT_PATH = "./certificates/server.crt"
KEY_PATH = "./certificates/server.key"

"""Just in case you want to try basic functionality w/out SSL and certs
    Make sure you start the server without SSL functionality."""

def basic_get():
    print("Doing a basic get w/unsecure url")
    r = requests.get("http://localhost:8888")
    print(("The response is: " + r.text))

def basic_post():
    print("Doing a basic post")
    r = requests.post(URL)
    print(("The response is: " + r.text))

def basic_json_post():
    print("Doing a post w/json")
    info = json.dumps({'name': 'Sarah Connor', 'cc_num': '12345'})
    headers = {'content-type': 'application/json'}
    r = requests.post(URL, data=info, headers=headers)
    print((r.text))

"""Here's the secure functionality. Make sure you start the server with SSL."""

def secure_get():
    import pdb
    pdb.set_trace()
    print("Attempting a secure get")
    try:
        r = requests.get(SECURE_URL, cert=(CERT_PATH, KEY_PATH), verify=False)
        print("secure get successful")
        print("Here's what the page says: ")
        print((r.text))
        print("Here's the status code:")
        print((str(r.status_code) + "\n"))
    except requests.exceptions.SSLError as sse:
        print(("SSL Error: " + str(sse) + "\n"))

def secure_post():
    print("Attempting a secure post")
    try:
        #need to add cert path?
        r = requests.post(SECURE_URL, verify=False)
        print("posted successfully")
        print("Here's what the page says:")
        print((r.text))
        print("Here's the status code:")
        print((str(r.status_code) + "\n"))
    except requests.exceptions.SSLError as ssle:
        print(("SSL error: " + str(ssle) + "\n"))

def secure_json_post():
    print("Attempting a secure post w/json")
    info = json.dumps({'name': 'Sarah Connor', 'cc_num': '12345'})
    headers = {'content-type': 'application/json'}
    try:
        r = requests.post(SECURE_URL, data=info, headers=headers, verify=False)
        print("After a secure JSON post, here's what the page says:")
        print((r.text))
        print("Here's the status code:")
        print((r.status_code))
    except requests.exceptions.SSLError as ssle:
        print((str(ssle) + "\n"))


if __name__ == "__main__":
    #basic_get()
    #basic_post()
    #basic_json_post()
    secure_get()
    secure_post()
    secure_json_post()
