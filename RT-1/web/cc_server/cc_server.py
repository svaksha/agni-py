""" This is a basic Tornado server app. It will serve a secure page and exchange JSON data."""

import tornado.ioloop
import tornado.web
import tornado.httpserver
import os
import sys
import json

class MainHandler(tornado.web.RequestHandler):
    """Main Page"""
    def get(self):
        self.write("Hello, Sarah Connor. Welcome to the RT CC server.")

class CCHandler(tornado.web.RequestHandler):
    """Credit card submission Page"""
    def get(self):
        self.write("This is the Processing page.")

    def post(self):
        self.set_header("Content-Type", "application/json")
        if self.request.body:
            cc_info = json.loads(self.request.body.decode("utf-8"))
            #Do some wacky stuff w/cc data here
            self.write("Processing cc data:")
            self.write(cc_info)
        else:
            self.write("No cc data to process")

if __name__ == "__main__": 
    application = tornado.web.Application([ 
        (r"/", MainHandler), 
        (r"/process", CCHandler),
    ]) 

#Start standard web server. Uncomment to start server w/out https--------------------
#    application.listen(8888)
#    tornado.ioloop.IOLoop.instance().start() 
#---------------------------------------------------------------------------------

#Start Secure web server. Uncomment to start server w/https-------------------
    port = 8888
    http_server = tornado.httpserver.HTTPServer(application, ssl_options={ 
           "certfile": os.path.join("/etc/httpd/ssl_keys", "proto2_revenuetrades_com.crt"), 
           "keyfile": os.path.join("/etc/httpd/ssl_keys", "myserver.key"), 
       })
    print("Listening on port %d for connections..." % port)
    http_server.listen(port) 
    tornado.ioloop.IOLoop.instance().start() 
#-----------------------------------------------------------------------------------


