# widgets.py

from django.utils.html import escape, conditional_escape
from django.utils.encoding import force_text
from django.utils.safestring import mark_safe
from django.forms.widgets import ClearableFileInput, CheckboxInput, TextInput, SelectMultiple, Select

from decorators import assert_on_exception
    
class AdvancedFileInput(ClearableFileInput):

    def __init__(self, *args, **kwargs):

        self.url_length = kwargs.pop('url_length',30)
        self.preview = kwargs.pop('preview',True)
        self.image_width = kwargs.pop('image_width',200)
        super(AdvancedFileInput, self).__init__(*args, **kwargs)

    @assert_on_exception
    def render(self, name, value, attrs=None,):

        substitutions = {
            'initial_text': self.initial_text,
            'input_text': self.input_text,
            'clear_template': '',
            'clear_checkbox_label': self.clear_checkbox_label,
        }
        template = u'%(input)s'

        substitutions['input'] = super(ClearableFileInput, self).render(name, value, attrs)

        if value and hasattr(value, "url"):

            template = self.template_with_initial
            if self.preview:
                substitutions['initial'] = (u'<a href="{0}">{1}</a><br>\
                <a href="{0}" target="_blank"><img src="{0}" width="{2}"></a><br>'.format
                    (escape(value.url),'...'+escape(force_text(value))[-self.url_length:],
                     self.image_width))
            else:
                substitutions['initial'] = (u'<a href="{0}">{1}</a>'.format
                    (escape(value.url),'...'+escape(force_text(value))[-self.url_length:]))
            if not self.is_required:
                checkbox_name = self.clear_checkbox_name(name)
                checkbox_id = self.clear_checkbox_id(checkbox_name)
                substitutions['clear_checkbox_name'] = conditional_escape(checkbox_name)
                substitutions['clear_checkbox_id'] = conditional_escape(checkbox_id)
                substitutions['clear'] = CheckboxInput().render(checkbox_name, False, attrs={'id': checkbox_id})
                substitutions['clear_template'] = self.template_with_clear % substitutions

        #return mark_safe("<h1>Hi there</h1>")
        return mark_safe(template % substitutions)

class SpanViewTextWidget(TextInput):
    def __init__(self, *args, **kwargs):

        self.cssclass = kwargs['attrs'].get('class','')
        super(SpanViewTextWidget, self).__init__(*args, **kwargs)
        
    def render(self, name, value, attrs=None):
        template = """ class="%(class_attr)s"\>%(value)s"""
        substitutions = {
            'value': value,
            'class_attr' : self.cssclass, 
            }
        result = "<span" + template  % substitutions + "</span>" 
        return mark_safe(result)

class SpanEditableTextWidget(TextInput):
    def __init__(self, *args, **kwargs):
        self.cssclass = kwargs['attrs'].get('class','')
        self.action = kwargs['attrs'].get('action','#')
        super(SpanEditableTextWidget, self).__init__(*args, **kwargs)
        
    def render(self, name, value, attrs=None):
        template = """ class="%(class_attr)s simple_cms jqtransformdone " rel="textbox" fieldID="%(field_name)s" action="%(action)s">%(value)s"""
        substitutions = {
            'value': value,
            'class_attr' : self.cssclass, 
            'attrcount' : 1,
            'field_name' : name,
            'action' : self.action,
            }
        result = "<span" + template  % substitutions + "</span>" 
        return mark_safe(result)

class FormattedSelectionWidget(Select):
    def __init__(self, *args, **kwargs):

        #self.cssclass = kwargs['attrs'].get('class','')
        self.label = kwargs.get('label','')
        self.passed_choices = kwargs.get('choices',{})
        if 'label' in kwargs: del kwargs['label']
        super(FormattedSelectionWidget, self).__init__(*args, **kwargs)
        
    def render(self, name, value, attrs=None):
        template = """<div class="rowElem clearfix">
                                      <H5>%(label)s:</H5>
                                      <select class="auto_submit_item" name="%(fieldName)s">
										  %(choices)s
                                      </select>
                                  </div>"""
        choices = "".join(["""<option value="%d">%s</option>\n""" % (val, label) for val, label in self.passed_choices])
        substitutions = {
            'fieldName' : name,
            'label': self.label,
            'choices' : choices, 
            }
        result = template  % substitutions 
        return mark_safe(result)

class FormattedListWidget(Select):
    def __init__(self, *args, **kwargs):

        #self.cssclass = kwargs['attrs'].get('class','')
        self.label = kwargs.get('label','')
        self.choices = kwargs.get('choices',{})
        if 'label' in kwargs: del kwargs['label']
        super(FormattedListWidget, self).__init__(*args, **kwargs)
        
    def render(self, name, value, attrs=None):
        template = """<div class="rowElem">
                                      <H5>%(label)s:</H5>
                                      <ul class="arrow-listC">
										  %(choices)s
                                      </ul>
                                  </div>"""
        choices = "".join(["""<li>%s</li>""" % (label) for val, label in self.choices])
        substitutions = {
            'label': self.label,
            'choices' : choices, 
            }
        result = template  % substitutions 
        return mark_safe(result)

class IndustriesWidget(SelectMultiple):
    def __init__(self, *args, **kwargs):
        self.label = kwargs.get('label','')
        self.choices = kwargs.get('choices',{})
        if 'label' in kwargs: del kwargs['label']
        super(IndustriesWidget, self).__init__(*args, **kwargs)
        
    def render(self, name, value, attrs=None):
        template = """
                            <div class="rowElem" style="width: 530px;">
											<H5>%(label)s: <em>(Add all that apply)</em></H5>
											<div class="industries">
												<div class="boxstyle-B clearfix">
													<div class="inside-border clearfix">
														<select name="known-industries" id="known_industries" size="4" multiple>
														  %(choices)s
														</select>
													</div>
												</div> 
											</div>
											<div class="industries-btn">
												<div id="add_industry"><input name="add_industry" type="button" value="&raquo;"></div>
												<div id="remove_industry"><input name="remove_industry" type="button" value="&laquo;"></div>
											</div>
											<div class="industries">
												<div class="boxstyle-B clearfix">
													<div class="inside-border clearfix">
														<select name="operating_industries" id="operating_industries" size="4" multiple>
															<!--<option value="Item 1">Item 1</option>-->
														</select>
													</div>
												</div> 
											</div>
										</div>
"""
        choices = "".join(["""<option value="%s">%s</option>""" % (industry, industry) for val, industry in self.choices])
        substitutions = {
            'label': self.label,
            'choices': choices,
            }
        result = template % substitutions 
        return mark_safe(result)

class IndustriesFormWidget(SelectMultiple):
    def __init__(self, *args, **kwargs):
        self.label = kwargs.get('label','')
        self.choices = kwargs.get('choices',{})
        if 'label' in kwargs: del kwargs['label']
        super(IndustriesFormWidget, self).__init__(*args, **kwargs)
        
    def decompress(self,value):
        if value:
            print ("value",value.industry)
        else:
            print("no value")
        
    def render(self, name, value, attrs=None):
        template = """<form method="POST">
                            <div class="rowElem" style="width: 530px;">
											<H5>%(label)s: <em>(Add all that apply)</em></H5>
											<div class="industries">
												<div class="boxstyle-B clearfix">
													<div class="inside-border clearfix">
														<select name="known-industries" id="known_industries" size="4" multiple>
														  %(choices)s
														</select>
													</div>
												</div> 
											</div>
											<div class="industries-btn">
												<div id="add_industry"><input name="add_industry" type="button" value="&raquo;"></div>
												<div id="remove_industry"><input name="remove_industry" type="button" value="&laquo;"></div>
											</div>
											<div class="industries">
												<div class="boxstyle-B clearfix">
													<div class="inside-border clearfix">
														<select name="operating_industries" id="operating_industries" size="4" multiple>
															<!--<option value="Item 1">Item 1</option>-->
														</select>
													</div>
												</div> 
											</div>
										</div>
									</form>
"""
        choices = "".join(["""<option value="%s">%s</option>""" % (industry, industry) for val, industry in self.choices])
        substitutions = {
            'label': self.label,
            'choices': choices,
            }
        result = template % substitutions 
        return mark_safe(result)

