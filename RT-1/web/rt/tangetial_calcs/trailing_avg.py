from decimal import *
import math      # http://docs.python.org/3.0/library/math.html

getcontext().prec = 6
eff_annual_int_rate = 0.09
days_in_period = 28.0

'''
use math function pow(x,y) in compound interest formula (# FV = PV ( 1+i )^n) to compute the principal amount due after 28 days
'''
principal = Decimal( math.pow((1+(eff_annual_int_rate/12)),(12*(days_in_period/365))) )

print ("Principal:", principal)

milestone_po = 50000.00
growth_rate_applied = 0.15
periods_compound_per_year = 6.0

'''
The above formula is applied to model - project sales using growth rate
'''
periods_in_months_passed = 3.0
sales_total_for_periods_in_months = Decimal( milestone_po * ( math.pow((1+ (growth_rate_applied/periods_compound_per_year)),(periods_compound_per_year*(periods_in_months_passed/12))) ) )

print ("Sales total for %d periods in months passed:" % periods_in_months_passed, sales_total_for_periods_in_months)

seasonal_percentage = 0.40 # How much is sold in-season, which may only last a few weeks
in_season_months = 2.0 # How many months make up the in-season period
nonseasonal_percentage = 1.00 - seasonal_percentage

#change these variables, from above
growth_rate_applied = 0.15 * (1 + seasonal_percentage)
periods_compound_per_year = 12.0
periods_in_months_passed = 1.0

'''
The above formula is applied to another model - using seasonal variation in sales
'''
sales_total_for_in_season = Decimal( milestone_po * ( math.pow((1+ (growth_rate_applied/periods_compound_per_year)),(periods_compound_per_year*(periods_in_months_passed/12))) / in_season_months) )
print ("Sales total for in-season:", sales_total_for_in_season)

#raw_input("Hit enter to close window.")