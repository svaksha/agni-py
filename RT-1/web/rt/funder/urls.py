from django.conf.urls import patterns, url

from funder import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
	#url(r'^funder/',  'funder_profiles.views.view', {'template_name': 'funder/view_details.html' }),
	url(r'^(\d+)/dashboard/$',  views.dashboard, name = 'dashboard'),
	url(r'^(\d+)/profile/$',  views.profile, name='profile by funder'),
	url(r'^(\d+)/verify/$',  views.profile, name='profile by funder'),
	url(r'^show_profile/$',  views.user_profile, name='profile by user'),
	url(r'^(\d+)/public_profile/$',  views.profile, name='profile by funder'),
)