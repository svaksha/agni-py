from django.db import models

from django.db.models import BooleanField
from django.db.models import CharField
from django.db.models import DateTimeField
from django.db.models import DecimalField
from django.db.models import EmailField
from django.db.models import ForeignKey 
from django.db.models import IntegerField
from django.db.models import ManyToManyField
from django.db.models import OneToOneField
from django.db.models import URLField

from django.contrib.auth.models import User

from django.forms import ModelForm

#from business.models import Terms
from home.models import RevenueTradesMenus
import business.models
import home.models
import milestones.models

        

class Investment(models.Model):
    company = ForeignKey('business.Business')
    amount = DecimalField(decimal_places=2, max_digits=10)
    terms = ForeignKey('milestones.Terms')
    
class Industry(models.Model):
    industry = models.CharField(max_length=200)
    @classmethod
    def create(cls, description):
        biztype = cls(industry = description)
        biztype.save()
        return biztype
    def __unicode__(self):
        return str(self.industry)
    
NET_WORTH = tuple(enumerate((
    '$0-100,000', 
    '$100,000-500,000', 
    '$500,000-1,000,000', 
    'Over $1M', )))
class Funder(models.Model):
        
    funder = ForeignKey('home.RevenueTradesPerson') 
    net_worth = IntegerField(max_length=1,null=True, choices=RevenueTradesMenus.menu_list("FunderNetWorth"))
    website = URLField(null=True)
    # might want email here as well
    stage_of_interest = IntegerField(max_length=1,null=True, choices=RevenueTradesMenus.menu_list("RevenueTradesIndustries"))
    industries_of_interest = ManyToManyField(Industry, null=True, choices=RevenueTradesMenus.menu_list("RevenueTradesDevelopmentStages"))
    investments = ManyToManyField(Investment)
    TOUPS_agreement = BooleanField()

    @classmethod
    def create(cls,funder_info):
        fnder = cls(funder = funder_info,
            net_worth = 0.00,
            )
        return fnder

    def __unicode__(self):
        return str("Funder " + self.funder.first_name + " " + self.funder.last_name) # wtf .get_username()
    