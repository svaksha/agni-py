from PIL import Image as PImage
from os.path import join as pjoin
from settings import MEDIA_ROOT

from django.http import HttpResponse, HttpResponseNotFound
from django.template import RequestContext, Context, loader
TEMPLATE_CONTEXT_PROCESSORS = ( 
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
)
from django.http import Http404
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import ensure_csrf_cookie
from django.db import IntegrityError, DatabaseError
from django.forms import ModelForm
from django.forms import CharField, MultipleChoiceField, ChoiceField
from django.forms.widgets import Widget
from django.forms.widgets import ClearableFileInput
from django.forms.widgets import HiddenInput

from home.models import RevenueTradesMenus, RevenueTradesPerson, RevenueTradesBusiness

from funder.models import Funder, Industry, NET_WORTH

from widgets import SpanEditableTextWidget
from widgets import SpanViewTextWidget
from widgets import IndustriesWidget
from widgets import IndustriesFormWidget
from widgets import FormattedSelectionWidget
from widgets import FormattedListWidget

class ContactViewForm(ModelForm):
    first_name = CharField(widget=SpanViewTextWidget(attrs={'size':'40'}), label='',  )
    last_name = CharField(widget=SpanViewTextWidget(attrs={'size':'40'}), label='',  )
    street_address = CharField(widget=SpanViewTextWidget(attrs={'size':'40'}), label='',  )
    city_state_zip = CharField(widget=SpanViewTextWidget(attrs={'size':'40'}), label='',  )
    email = CharField(widget=SpanViewTextWidget(attrs={'size':'40'}), label='',)
    phone = CharField(widget=SpanViewTextWidget(attrs={'size':'40'}), label='',  )
    
    class Meta:
        model = RevenueTradesPerson 
        fields = {'first_name', 'last_name', 'street_address', 'city_state_zip', 'email', 'phone', }

class ContactEditForm(ModelForm):
    first_name = CharField(widget=SpanEditableTextWidget(attrs={'size':'40', }, ), label='', )
    last_name = CharField(widget=SpanEditableTextWidget(attrs={'size':'40', }, ),  label='', )
    street_address = CharField(widget=SpanEditableTextWidget(attrs={'size':'40', }, ), label='', )
    city_state_zip = CharField(widget=SpanEditableTextWidget(attrs={'size':'40', }, ), label='', )
    email = CharField(widget=SpanEditableTextWidget(attrs={'size':'40', }, ), label='', )
    phone = CharField(widget=SpanEditableTextWidget(attrs={'size':'40', }, ), label='', )
    
    class Meta:
        model = RevenueTradesPerson 
        fields = {'first_name', 'last_name', 'street_address', 'city_state_zip', 'email', 'phone', }

class FunderViewForm(ModelForm):
    net_worth = CharField(label='Net worth', )
    stage_of_interest = CharField(label='Stage of companies I like',)
    industries_of_interest = MultipleChoiceField(label='Industry Interests', )

    class Meta:
        model = Funder
        fields = ('net_worth', 'stage_of_interest', 'industries_of_interest', )

class FunderEditForm(ModelForm):
    net_worth = ChoiceField(widget=FormattedSelectionWidget(label='Net worth', choices = RevenueTradesMenus.menu_list("FunderNetWorth"), ), label='',)
    stage_of_interest = ChoiceField(widget=FormattedSelectionWidget(label='Stage of companies I like', choices = RevenueTradesMenus.menu_list("RevenueTradesDevelopmentStages"), ),
	label='', )

    class Meta:
        model = Funder
        fields = ('net_worth', 'stage_of_interest', )

class IndustriesEditForm(ModelForm):
    industries_of_interest = MultipleChoiceField(widget=IndustriesFormWidget(), 
                                                choices = RevenueTradesMenus.menu_list("RevenueTradesIndustries"),
                                                label='', 
                                                )

    class Meta:
        model = Funder
        fields = ('industries_of_interest', )

def index(request):
    return HttpResponse("Hello, world. You're at the funder index.")
    
def publicProfile(request):
    return HttpResponse("Hello, world. You're at the funder public profile.")

def dashboard(request, id=0):
    editable = False
    if id == 0:
        return HttpResponseNotFound('<h1>No such funder or maybe this should be a list of funder</h1>')
    else:
        funder = Funder.objects.get(id=id)
        
    if request.user.is_authenticated():
        rtuser = RevenueTradesPerson.objects.filter(user=request.user)
        if len(rtuser) == 1:
            rtuser = rtuser[0]
        else: 
            return HttpResponseNotFound('<h1>invalid user count %d</h1>' % len(rtuser) + " " + repr(rtuser))
        if rtuser == funder.funder:
            editable = True
        else:
            return HttpResponseNotFound('<h1>You are not allowed to view that page</h1>')
        context = RequestContext(request, {
            'funder' : funder,
            'rtuser': rtuser,
        })
        template = loader.get_template('funder/funder-investments.html')
        return HttpResponse(template.render(context))
    return HttpResponseNotFound('<h1>You are not allowed to view that page</h1>')


def liveMilestones(request):
    return HttpResponse("Hello, world. You're at the funder milestones list.")

@login_required
def user_profile(request):
    logged_in_funder = RevenueTradesPerson.objects.get(user=request.user)
    funder = Funder.objects.get(funder=logged_in_funder)
    return profile(request, funder.id)
    
def profile(request, id=0):
    editable = False
    if id == 0:
        return HttpResponseNotFound('<h1>No such funder or maybe this should be a list of funder</h1>')
    else:
        funder = Funder.objects.get(id=id)
        
    if request.user.is_authenticated():
        try:
            rtuser = RevenueTradesPerson.objects.get(user=request.user)
        except :
            return HttpResponseNotFound('<h1>User is not found</h1>' +  request.user.username)
        if not rtuser:
            return HttpResponseNotFound('<h1>User is not registered</h1>' +  rtuser)
        if rtuser == funder.funder:
            editable = True
        else:
            return HttpResponseNotFound('<h1>You are not allowed to view that page</h1>')
            
        
    #funder = Funder.create(funder_info = rtuser)
    if (funder):
        if (request.method == 'POST'):
            print("posting to funder profile",request.POST)
            funder.net_worth = request.POST.get('net_worth', funder.net_worth)
            rtuser.first_name = request.POST.get('first_name', rtuser.first_name)
            rtuser.last_name = request.POST.get('last_name', rtuser.last_name)
            rtuser.street_address = request.POST.get('street_address', rtuser.street_address)
            rtuser.city_state_zip = request.POST.get('city_state_zip', rtuser.city_state_zip)
            rtuser.email = request.POST.get('email', rtuser.email)
            rtuser.phone = request.POST.get('phone', rtuser.phone)
            #funder.url = request.POST.get('RT_URL', funder.url)
            funder.website = request.POST.get('company_website', funder.website)
            funder.stage_of_interest = request.POST.get('stage_of_interest', funder.stage_of_interest)
            funder.TOUPS_agreement = request.POST.get('TOUPS_agreement', funder.TOUPS_agreement)
            try:
                industries = request.POST.getlist('operating_industries[]')
                #return HttpResponseNotFound('<h1>industries</h1> '  + repr(industries))
                funder.save()
                for industry in funder.industries_of_interest.all():
                    if not industry in industries:
                        industry.delete()
                for industry in industries:
                    new_industry_obj = Industry.create(industry)
                    funder.industries_of_interest.add(new_industry_obj)
                funder.save()
            except  KeyError:
                pass
    
            rtuser.save()
            funder.save()
            if not request.POST.get('initial'):
                return HttpResponse()
        funder_form = FunderEditForm(instance = funder)
        industries_list = [ industry for val, industry in REVENUE_TRADES_INDUSTRIES  ]
        for industry in funder.industries_of_interest.all():
            industries_list.remove( industry.industry )
        funder_info = FunderViewForm(instance = funder,)
        contact_form = ContactEditForm(instance = funder.funder,)
        contact_info = ContactViewForm(instance = funder.funder, 
                    initial={
                            'street_address': 'Street address', 
                            'city_state_zip': 'City/state/zip', 
                            'email': 'email address', 
                            'phone': 'phone number', },)
        context = RequestContext(request, {
            'rtuser': rtuser,
            'funder' : funder,
            'funder_form' : funder_form,
            'label': 'Industry Interests',
            'industries_list' : industries_list,
            'interested_industries_list' : funder.industries_of_interest.all(),
            'contact' : funder.funder,
            'contact_form' : contact_form,
            'editable' : editable,
            
        })
        template = loader.get_template('funder/funder-profile-editor.html')
        return HttpResponse(template.render(context))
    else:
        return HttpResponseNotFound('<h1>No funder was created for</h1> '  + repr(rtuser))

