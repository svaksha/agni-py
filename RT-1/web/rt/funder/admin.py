from django.contrib import admin
from .models import *

class FunderAdmin(admin.ModelAdmin):
    pass

class InvestmentAdmin(admin.ModelAdmin):
    pass

class IndustryAdmin(admin.ModelAdmin):
    pass


admin.site.register(Funder, FunderAdmin)
admin.site.register(Investment, InvestmentAdmin)
admin.site.register(Industry, IndustryAdmin)




    

