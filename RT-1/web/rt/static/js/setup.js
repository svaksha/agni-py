window.onload = function () {
    //Add backgorund to table rows
    styleRows();
    //Attach events
    $("#ButtonNextRaise").on("click", addNewRaise);
    $("#ButtonNextAchievement").on("click", addNewAchievement);
    //Datepicker
    $("#txt_date_achieve").datepicker();
    //Reduce width for text inputs
    $("#milestone-creation-table-step4 tr div").width(100);
    //Hide Total Row
    $("#milestone-creation-table-step1 tr:last-child").hide();
    //Step table
    $(".step5-column-table tr td:first-child").css({
        "background": "url('images/paperbg_light.png') repeat"
    });
    $(".step5-column-table tr td:nth-child(2)").css({
        "color": "#3A87A7"
    });
    //Readonly terms
    $("#txt_terms").attr("readonly", "readonly");
    //Terms edit
    $("#edit_terms").on("click", function () {
        event.preventDefault();
        $("#txt_terms").removeAttr("readonly");
    });
    //Set z-index for tabs
    var count = 1;
    for (i = 7; i > 0; i--) {
        $("#menu .tab:nth-child(" + i + ")").css({
            "z-index": count,
            "margin-left": (i - 1) * 131
        });
        count = count + 1;
    }

};

function styleRows() {
    $("#milestone-creation-table-step1 tr:even").css({
        'background': "url('images/paperbg_light.png') repeat"
    });
    $("#milestone-creation-table-step1 tr:odd").css({
        'background': "none",
        'background-color': "white"
    });
    $("#milestone-creation-table-step2 tr:even").css({
        'background': "url('images/paperbg_light.png') repeat"
    });
    $("#milestone-creation-table-step2 tr:odd").css({
        'background': "none",
        'background-color': "white"
    });
    $("#milestone-creation-table-step4 tr:even").css({
        'background': "url('images/paperbg_light.png') repeat"
    });
    $("#milestone-creation-table-step4 tr:last-child td").css({
        'border': "none"
    });
    $("#milestone-creation-table-step4a tr:even").css({
        'background': "url('images/paperbg_light.png') repeat"
    });
    $("#milestone-creation-table-step4a tr:last-child td").css({
        'border': "none"
    });
};

function addNewRaise() {
    //Get values
    var cost_val = parseInt($("#txt_cost_raise").val(), 10);
    var need_val = $("#txt_need_raise").val();
    var required_val = $("input:radio[name='qRequired_raise']:checked").val();
    //Insert new row
    var table = document.getElementById("milestone-creation-table-step1");
    var row = table.insertRow(2);
    $(row).append("<td><div><label>" + need_val + "</label></div></td><td><div>" + cost_val + "</div></td><td><div>" + required_val + "</div></td>");
    //Update sum
    var sum = 0;
    if ($("#txt_sum_raise").val() != "") {
        sum = parseInt($("#txt_sum_raise").val(), 10);
    }
    sum = sum + cost_val;
    $("#txt_sum_raise").val(sum);
    //Show sum row
    styleRows();
    $("#milestone-creation-table-step1 tr:last-child").show();
    //Clear input
    $("#txt_cost_raise").val("");
    $("#txt_need_raise").val("");
};

function addNewAchievement() {
    //Get values
    var cost_val = parseInt($("#txt_cost_achieve").val(), 10);
    var need_val = $("#ddl_type_achieve :selected").val();
    var required_val = $("#txt_date_achieve").val();
    //Insert new row
    var table = document.getElementById("milestone-creation-table-step2");
    var row = table.insertRow(2);
    $(row).append("<td><div><label>" + need_val + "</label></div></td><td><div>" + cost_val + "</div></td><td><div>" + required_val + "</div></td>");
    styleRows();
    //Clear input
    $("#txt_cost_achieve").val("");
    $("#txt_date_achieve").val("");
};

function next(id) {
    //Show next step
    $("#step-" + id).hide();
    $("#step-" + (id + 1)).fadeIn();
    //Update menu
    $("#menu li:nth-child(" + id + ")").removeClass("active");
    $("#menu li:nth-child(" + (id + 1) + ")").addClass("active");
};

function back(id) {
    //show previous step
    $("#step-" + id).hide();
    $("#step-" + (id - 1)).fadeIn();
    //Update menu
    //Update menu
    $("#menu li:nth-child(" + id + ")").removeClass("active");
    $("#menu li:nth-child(" + (id - 1) + ")").addClass("active");
};

function showStep(num) {
    $(".milestone-step").hide();
    $("#step-" + num).fadeIn();
    //Update menu
    $("#menu li").removeClass("active");
    $("#menu li:nth-child(" + num + ")").addClass("active");
};