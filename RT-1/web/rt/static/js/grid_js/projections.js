var Projections = {
  config: {},
  editablegrid : {},

  initialize: function(config){
    this.config = config;
    this.buildGrid();
    this.extendErrorMessages();
    this.initializeData();
  },

  extendErrorMessages: function(){
    TextCellEditor.prototype.updateStyle = function(htmlInput) {
      if (this.column.isValid(this.getEditorValue(htmlInput))) {
        this.editablegrid.removeClassName(htmlInput, this.editablegrid.invalidClassName);
        Projections.gridHelper.clearErrors();
      } else {
        this.editablegrid.addClassName(htmlInput, this.editablegrid.invalidClassName);
        Projections.gridHelper.displayErrors("Data must be of type " + this.column.datatype);
      }
    };
  },

  buildGrid: function(){
    this.editableGrid = new EditableGrid(Projections.config.gridName, {
      modelChanged: function(rowIndex, columnIndex, oldValue, newValue, current_row) {
        var formData = Projections.dataAdapter.prepareFormSubmissionDataFrom(current_row);
        Projections.dataLoader.fetchDataFrom(formData);
      },
      enableSort: false
    });
  },

  initializeData:function(){
    Projections.dataLoader.parseAndLoadJSON(Projections.config.initialData);
  },

  gridHelper: {
    removeEmptyRow: function(){
      $("table tbody").find("tr:nth-child(1)").remove();
    },

    displayErrors:function(message){
      this.clearErrors();
      $("#projection_errors").text(message);
    },

    clearErrors:function(){
      $("#projection_errors").text("");
    },

    showLoading:function(){
      $("#loading").show();
    },

    hideLoading:function(){
      $("#loading").hide();
    }
  },

  dataAdapter: {
    prepareFormSubmissionDataFrom: function(row){
      var dataRow = $(row);
      var start_date, days_in_cycle, number_of_years;
      start_date = dataRow.find("td:nth-child(1)").html();
      days_in_cycle = dataRow.find("td:nth-child(2)").html();
      number_of_years = dataRow.find("td:nth-child(3)").html();

      return {start_date: start_date,
              days_in_cycle: days_in_cycle,
              number_of_years: number_of_years,
              csrfmiddlewaretoken: Projections.config.CSRF };
    }
  },

  dataLoader: {
    parseAndLoadJSON:function(this_json){
      Projections.editableGrid.processJSON(this_json);
      Projections.editableGrid.renderGrid(Projections.config.divID, Projections.config.tableID);
      Projections.gridHelper.removeEmptyRow();
    },

    fetchDataFrom: function(formData){
      var projections_scope = this;
      $.ajax({
        type: "POST",
        url: Projections.config.endpoint,
        data: formData,
        beforeSend: Projections.gridHelper.showLoading,
        complete: Projections.gridHelper.hideLoading,
        success: function(response, textStatus, jqXHR){
          projections_scope.parseAndLoadJSON(jQuery.parseJSON(jqXHR.responseText));
        },
        error: function(jqXHR, textStatus, errorThrown){
          message = "The following error occurred: " + textStatus + "; " + errorThrown;
          Projections.gridHelper.displayErrors(message);
        },
      });
    }
  }
};


