$(document).ready(function(){
	$('.submenu .mcontainer').hover(function(){},function(){
		$(this).stop(true,false);
		$(this).find('.fcontainer').stop(true,false).css('overflow','visible');
		
		$(this).find('.fcontainer').animate({'width':'31px'},300);
		$(this).animate({'width':'200px'},500);
		$('.submenu-inner').html($('.submenu ul.fcontainer li.active ul').html());
	});
	
	$('.submenu .fcontainer').hover(function(){
		$(this).parent().stop(true,false);
		$(this).stop(true,false).css('overflow','visible');
		
		$(this).parent().animate({'width':'370px'},300);
		$(this).animate({'width':'200px'},500);
		
	},function(){});
	
	$('.submenu ul.fcontainer li').mouseover(function(){
		$('.submenu-inner').html($(this).find('ul').html());
	});
	
	$('.submenu-inner').html($('.submenu ul.fcontainer li.active ul').html());
	
	$('.submenu-inner,.fcontainer, .col1').css('height',$('.porcentaje').outerHeight(true));
	
	//expert-dash-focus, hide ballon
	$('.requirements .close').click(function(e){
		e.preventDefault();
		$(this).parent().fadeOut(700);
	});

	
	$('.porcentaje').resize(function(e){
		$('.submenu-inner,.fcontainer, .col1').css('height',$(this).outerHeight(true));
	});
	
	
});
