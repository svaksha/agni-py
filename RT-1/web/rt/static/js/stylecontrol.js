$(document).ready(function() {

		//Main Navigation: all tabs clicks
		jQuery(document).ready(function(){
			$('div.navtab-wrap .navtab-content').hide();
			
			$('a.navtab').click(function() {
				$(this).toggleClass("active");
				var activePanel = $(this).attr("href");
				$(activePanel).slideToggle();
				return false;
			});
			
			$('div.navtab-closing div.button').click(function(){  //Closing tab click events
				$("div.navtab-wrap .navtab-content").css('height', $('div.navtab-wrap .navtab-content').height() + 'px').slideUp("");
				$('a.navtab').removeClass('active');
				return false;
			});
			
		});

	
/*
	//Nav Tab Main: when page loads
		jQuery(document).ready(function(){
			$('div.navtab-wrap .navtab-content').hide(); //Hide/close all containers
			$('#nav-main').click(function(){  //Main Tab tab click events
				$(this).toggleClass("active");
				$("#navtab-main").css('height', $('#navtab-main').height() + 'px').slideToggle("");
				return false;
			});
			$('#navtab-main div.navtab-closing div.button').click(function(){  //Main tab click events
				$("#navtab-main").css('height', $('#navtab-main').height() + 'px').slideUp("");
				$('#nav-main').removeClass('active');
				return false;
			});
		});
		
		//Nav Tab Business: when page loads
		jQuery(document).ready(function(){
			$('div.navtab-wrap .navtab-content').hide(); //Hide/close all containers
			$('#nav-biz').click(function(){  //Business tab click events
				$(this).toggleClass("active");
				$("#navtab-biz").css('height', $('#navtab-biz').height() + 'px').slideToggle("");
				return false;
			});
			$('#navtab-biz div.navtab-closing div.button').click(function(){  //Business tab click events
				$("#navtab-biz").css('height', $('#navtab-biz').height() + 'px').slideUp("");
				$('#nav-biz').removeClass('active');
				return false;
			});
		});
		
		//Nav Tab Expert: when page loads
		jQuery(document).ready(function(){
			$('div.navtab-wrap .navtab-content').hide(); //Hide/close all containers
			$('#nav-exp').click(function(){  //Expert tab click events
				$(this).toggleClass("active");
				$("#navtab-exp").css('height', $('#navtab-exp').height() + 'px').slideToggle("");
				return false;
			});
			$('#navtab-exp div.navtab-closing div.button').click(function(){  //Expert tab click events
				$("#navtab-exp").css('height', $('#navtab-exp').height() + 'px').slideUp("");
				$('#nav-exp').removeClass('active');
				return false;
			});
		});
		
		//Nav Tab Vault: when page loads
		jQuery(document).ready(function(){
			$('div.navtab-wrap .navtab-content').hide(); //Hide/close all containers
			$('#nav-vault').click(function(){  //Vault tab click events
				$(this).toggleClass("active");
				$("#navtab-vault").css('height', $('#navtab-vault').height() + 'px').slideToggle("");
				return false;
			});
			$('#navtab-vault div.navtab-closing div.button').click(function(){  //Vault tab click events
				$("#navtab-vault").css('height', $('#navtab-vault').height() + 'px').slideUp("");
				$('#nav-vault').removeClass('active');
				return false;
			});
		});
*/
	
	
	//User Nav Tab: on click events
	$("#b-user-settings").click(function() { //User thumbnail opens user-settings
		$(this).hide(); 
		$("#user-settings").show();
		$("#tools-tab").hide();
		$("#b-tools").show();
		return false;
	});
	
	$("#user-settings div.border-left").click(function() { //Left expandable arrow closes user-settings
		$("#user-settings").hide();
		$("#b-user-settings").show();
		return false;
	});
	
	$("#user-settings div.border-right").click(function() { //User thumbnail closes user-settings
		$("#user-settings").hide();
		$("#b-user-settings").show();
		return false;
	});
	
	$("#b-tools").click(function() { //User thumbnail opens user-settings
		$(this).hide(); 
		$("#tools-tab").show();
		$("#user-settings").hide();
		$("#b-user-settings").show();
		return false;
	});
	
	$("#tools-tab div.border-left").click(function() { //Left expandable arrow closes user-settings
		$("#tools-tab").hide();
		$("#b-tools").show(); 
		return false;
	});
	
	$("#tools-tab div.border-right").click(function() { //User thumbnail closes user-settings
		$("#tools-tab").hide();
		$("#b-tools").show(); 
		return false;
	});
	
	
	//Tabbed Box A: when page loads
	jQuery(document).ready(function(){
		$("div.tabboxA-wrap .tab-content").hide(); //Hide all content
		$("div.tabboxA-wrap ul.tabs li:first").addClass("active").show(); //Activate first tab
		$("div.tabboxA-wrap .tab-content:first").show(); //Show first tab content

		//Tabbed Box A: on click event
		$("div.tabboxA-wrap ul.tabs li").click(function() {

			$("div.tabboxA-wrap ul.tabs li").removeClass("active"); //Remove any "active" class
			$(this).addClass("active"); //Add "active" class to selected tab
			$("div.tabboxA-wrap .tab-content").hide(); //Hide all tab content

			var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
			$(activeTab).fadeIn(); //Fade in the active ID content
			return false;
		});
	});

	
	//Tabbed Box B: when page loads
	jQuery(document).ready(function(){
		$("div.tabboxB-wrap .tab-content").hide(); //Hide all content
		$("div.tabboxB-wrap ul.tabs li:first").addClass("active").show(); //Activate first tab
		$("div.tabboxB-wrap .tab-content:first").show(); //Show first tab content

		//Tabbed Box B: on click event
		$("div.tabboxB-wrap ul.tabs li").click(function() {

			$("div.tabboxB-wrap ul.tabs li").removeClass("active"); //Remove any "active" class
			$(this).addClass("active"); //Add "active" class to selected tab
			$("div.tabboxB-wrap .tab-content").hide(); //Hide all tab content

			var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
			$(activeTab).fadeIn(); //Fade in the active ID content
			return false;
		});
	});
	
	
	//Tabbed Box C: when page loads
	jQuery(document).ready(function(){
		$("div.tabboxC-wrap .tab-content").hide(); //Hide all content
		$("div.tabboxC-wrap ul.tabs li:first").addClass("active").show(); //Activate first tab
		$("div.tabboxC-wrap .tab-content:first").show(); //Show first tab content

		//Tabbed Box C: on click event
		$("div.tabboxC-wrap ul.tabs li").click(function() {

			$("div.tabboxC-wrap ul.tabs li").removeClass("active"); //Remove any "active" class
			$(this).addClass("active"); //Add "active" class to selected tab
			$("div.tabboxC-wrap .tab-content").hide(); //Hide all tab content

			var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
			$(activeTab).fadeIn(); //Fade in the active ID content
			return false;
		});
	});
	
	//Tabbed Box D: when page loads
	jQuery(document).ready(function(){
		$("div.tabboxD-wrap .tabD-content").hide(); //Hide all content
		$("div.tabboxD-wrap ul.tabsD li:first").addClass("active").show(); //Activate first tab
		$("div.tabboxD-wrap .tabD-content:first").show(); //Show first tab content

		//Tabbed Box D: on click event
		$("div.tabboxD-wrap ul.tabsD li").click(function() {

			$("div.tabboxD-wrap ul.tabsD li").removeClass("active"); //Remove any "active" class
			$(this).addClass("active"); //Add "active" class to selected tab
			$("div.tabboxD-wrap .tabD-content").hide(); //Hide all tab content

			var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
			$(activeTab).fadeIn(); //Fade in the active ID content
			return false;
		});
	});
	
	//Tabbed Box E: when page loads
	jQuery(document).ready(function(){
		$("div.tabboxE-wrap .tabE-content").hide(); //Hide all content
		
		$("div.tabboxE-wrap #tab-pledge-content").show(); //Show first tab content

		//Tabbed Box D: on click event
		$("div.tabboxE-wrap ul.tabsE li").click(function() {

			$("div.tabboxE-wrap ul.tabsE li").removeClass("active"); //Remove any "active" class
			$(this).addClass("active"); //Add "active" class to selected tab
			$("div.tabboxE-wrap .tabE-content").hide(); //Hide all tab content

			var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
			$(activeTab).fadeIn(); //Fade in the active ID content
			return false;
		});
	});
	
	
	//Accordion Box A: when page loads
	jQuery(document).ready(function(){
		$('div.accordionA-wrap .accordion-container').hide(); //Hide/close all containers
		$('div.accordionA-wrap .accordion-trigger:first').addClass('active').next().show(); //Add "active" class to first trigger, then show/open the immediate next container

		//On Click
		$('div.accordionA-wrap .accordion-trigger').click(function(){
			if( $(this).next().is(':hidden') ) { //If immediate next container is closed...
				$('div.accordionA-wrap .accordion-trigger').removeClass('active').next().slideUp(); //Remove all "active" state and slide up the immediate next container
				$(this).toggleClass('active').next().slideDown(); //Add "active" state to clicked trigger and slide down the immediate next container
			}
			return false; //Prevent the browser jump to the link anchor
		});
	});
	
	//Accordion Box B: when page loads
	jQuery(document).ready(function(){
		$('div.accordionB-wrap .accordion-container').hide(); //Hide/close all containers
		$('div.accordionB-wrap .accordion-trigger:first').addClass('active').next().show(); //Add "active" class to first trigger, then show/open the immediate next container

		//On Click
		$('div.accordionB-wrap .accordion-trigger').click(function(){
			if( $(this).next().is(':hidden') ) { //If immediate next container is closed...
				$('div.accordionB-wrap .accordion-trigger').removeClass('active').next().slideUp(); //Remove all "active" state and slide up the immediate next container
				$(this).toggleClass('active').next().slideDown(); //Add "active" state to clicked trigger and slide down the immediate next container
			}
			return false; //Prevent the browser jump to the link anchor
		});
	});

	//Accordion Box C: when page loads
	jQuery(document).ready(function(){
		$('div.accordionC-wrap .accordion-container').hide(); //Hide/close all containers
		$('div.accordionC-wrap .accordion-trigger:first').addClass('active').next().show(); //Add "active" class to first trigger, then show/open the immediate next container

		//On Click
		$('div.accordionC-wrap .accordion-trigger').click(function(){
			if( $(this).next().is(':hidden') ) { //If immediate next container is closed...
				$('div.accordionC-wrap .accordion-trigger').removeClass('active').next().slideUp(); //Remove all "active" state and slide up the immediate next container
				$(this).toggleClass('active').next().slideDown(); //Add "active" state to clicked trigger and slide down the immediate next container
			}
			return false; //Prevent the browser jump to the link anchor
		});
	});
	
	//Dropdown-bar: when page loads
	jQuery(document).ready(function(){
		$('div.dropdown-bar-wrap .dropdown-bar-panel').hide(); 
		//On Click
		$('div.dropdown-bar-wrap div.dropdown-bar .button').click(function(){
			$('div.dropdown-bar-wrap .positionfix').slideToggle('slow');
			$('div.dropdown-bar-wrap .dropdown-bar-panel').slideToggle('slow');
			return false; //Prevent the browser jump to the link anchor
		});
	});
	

	//Jquery Controlled Image Rollovers
	$("#rtlogo img").hover(function(){
		$(this).attr('src','images/ui/rt_icon_med_on.png');
			}, function() {
		$(this).attr('src','images/ui/rt_icon_med_off.png'); 
	}); //Simple image hover effect on RT icon	
	
	/*$("#user-tab .user-thumbplus img").hover(function(){
		$(this).attr('src','images/ui/usr_thumbplus_on.png');
			}, function() {
		$(this).attr('src','images/ui/usr_thumbplus_off.png'); 
	}); //Simple image hover effect on User thumbnail

*/
	
});