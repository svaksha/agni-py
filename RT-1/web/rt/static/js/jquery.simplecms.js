(function( $ ) {
	$.fn.SimpleCMS = function(options){
		var settings = {
			rows:5,
			cols:53,
			onLoadInput:function(){},
			onUnLoadInput:function(){}
		};
		$.extend(settings,options);
		
		
		
		$(this).each(function(){
			var $this = $(this);
			var timeout = false;
			
			$(this).hover(function(){
				timeout = setTimeout(
				function(){
					if($this.closest('.fila').find('.edit').length>0 && $this.closest('.fila').find('input,textarea').length==0)
						$this.closest('.fila').find('.edit').css('display','block');
				},1000);
				
			},function(){
				
				if(timeout)
					clearTimeout(timeout);
				if($(this).closest('.fila').find('.edit').length>0 && $(this).closest('.fila').find('input,textarea').length==0)
					$(this).closest('.fila').find('.edit').css('display','none');
				
			});
			
			$(this).click(function(e){
				var oldval = $(this).html();
				e.preventDefault();
				
				if( $(this).find('input,textarea').length==0 )
				{
					$(this).closest('.fila').find('.edit').css('display','none');
					var old_html = $(this).html();
					var html;
					var fieldID = $(this).attr('fieldID');
					var ultimate_action = $(this).attr('action');
					
					if($(this).attr('rel')=='textarea') html = '<textarea rows="'+settings.rows+'" cols="'+settings.cols+'" name="replace">'+old_html+'</textarea>';
					else html = '<input style="width:100%" type="text" name="replace" value="'+old_html+'">';
					
					$(this).html(html);
					
					settings.onLoadInput();
					
					$(this).find('input,textarea').focus();
					$(this).find('input,textarea').keyup(function(e){
						
						if(e.keyCode == 13) {//ENTER KEY: SAVES THE CONTENT
							$val=$(this).val();
							$this.html($val);
							var payload = {}
							payload[fieldID] = $(this).val();
							$.post(ultimate_action,payload);//EDIT
							
							settings.onUnLoadInput();
						}
						else if(e.keyCode==27)//ESC KEY: CANCEL AND BACK TO THE PREVIOUS STATE
						{
							$this.html(oldval);
							settings.onUnLoadInput();
						}
					});
					
					$(this).find('input,textarea').blur(function(e){
						$this.html(oldval);
						settings.onUnLoadInput();
					});
					
				}
				
			});
		});
	};
})( jQuery );