var banner_sizes=Array();
$(document).ready(function(){
	$('.tab:gt(0) .image').each(function(){
		
		var $div = $(this);
		$div.css({'width':0,'display':'block'});
		$div.parent().css({'width':'44px','display':'block'});
	});
	
	$('.jqTransformInput').focusover720();
	
	
	$('.tab a').click(function(){
		if(!$(this).parent().hasClass('select'))
		{
			var idx = $('.tab').index($(this).parent());
			$('.paginator a.select').removeClass('select');
			$('.paginator a:eq('+idx+')').addClass('select');
			
			var $div = $(this).parent().find('.image');
			
			$('.tab.select .image').stop(true,true);
			$('.tab.select').stop(true,true);
			if( $('.tab.select').hasClass('tab0') )
				$('.tab.select').animate({width:'0'},700);
			else
				$('.tab.select').animate({width:'44px'},700);
			
			$('.tab.select .image').animate({width:0},700);
			
			$div.animate({width:'790px'},700);
			if( $div.parent().hasClass('tab0') )
				$div.parent().animate({width:'790px'},700);
			else
				$div.parent().animate({width:'834px'},700);
			
			$('.tab.select').removeClass('select');
			$('.tab:lt('+idx+')').each(function(){ $(this).addClass('select-left');});
			$('.tab:gt('+idx+')').each(function(){ $(this).removeClass('select-left');});
			$(this).parent().removeClass('select-left');
			$(this).parent().addClass('select');
		}
		return false;
	});
	
	$('.paginator a').click(function(){
		var idx = $('.paginator a').index($(this));
		
		
		if(!$('.tab:eq('+idx+')').hasClass('select'))
		{
			$('.paginator a.select').removeClass('select');
			$(this).addClass('select');
			
			var $div = $('.tab:eq('+idx+')').find('.image');
			
			$('.tab.select .image').stop(true,true);
			$('.tab.select').stop(true,true);
			
			if( $('.tab.select').hasClass('tab0') )
				$('.tab.select').animate({width:'0'},700);
			else
				$('.tab.select').animate({width:'44px'},700);
			$('.tab.select').find('.image').animate({width:0},700);
			
			$div.animate({width:'790px'},700);
			if( $div.parent().hasClass('tab0') )
				$div.parent().animate({width:'790px'},700);
			else
				$div.parent().animate({width:'834px'},700);
			
			$('.tab.select').removeClass('select');
			$('.tab:lt('+idx+')').each(function(){ $(this).addClass('select-left');});
			$('.tab:gt('+idx+')').each(function(){ $(this).removeClass('select-left');});
			$('.tab:eq('+idx+')').removeClass('select-left');
			$('.tab:eq('+idx+')').addClass('select');
		}
		
		return false;
	});
	
});