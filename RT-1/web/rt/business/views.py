
from django.views.generic import CreateView, DeleteView

from django.utils import simplejson
from django.core.urlresolvers import reverse

from django.conf import settings
from PIL import Image as PImage
from PIL import ImageOps as PImageOps
from os.path import join as pjoin
from settings import MEDIA_ROOT

from django.http import HttpResponse, HttpResponseNotFound, HttpResponseRedirect
from django.template import RequestContext, Context, loader
from django.core.exceptions import MultipleObjectsReturned
from django.core.files.base import ContentFile

TEMPLATE_CONTEXT_PROCESSORS = ( 
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
)
from django.http import Http404
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import ensure_csrf_cookie
from django.db import IntegrityError, DatabaseError
from django.forms import ModelForm
from django.forms import CharField
from django.forms import ImageField
from django.forms import MultipleChoiceField
from django.forms import ModelChoiceField
from django.forms.widgets import Widget
from django.forms.widgets import SelectMultiple
from django.forms.widgets import ClearableFileInput


from home.models import RevenueTradesMenus, RevenueTradesPerson, RevenueTradesBusiness

from business.models import Ambition, Business, BusinessTypes

from business.forms import LogoForm
from widgets import SpanEditableTextWidget

import milestones.PurchaseOrderAchievement
import milestones.MatchingFundsAchievement

from verification_service.models import VerificationService


    
class BusinessYearsEditForm(ModelForm):
    class Meta:
        model = Business
        fields = {'year_started', }
        
class BusinessEmployeesEditForm(ModelForm):
    class Meta:
        model = Business
        fields = {'employees_count', }
        
class ContactEditForm(ModelForm):
    photo = CharField(widget=ClearableFileInput())
    first_name = CharField(widget=SpanEditableTextWidget(attrs={'size':'40', 'class': 'tit'}), label='', )
    last_name = CharField(widget=SpanEditableTextWidget(attrs={'size':'40', 'class': 'tit'}), label='', )
    title = CharField(widget=SpanEditableTextWidget(attrs={'size':'40'}), label='', )
    email = CharField(widget=SpanEditableTextWidget(attrs={'size':'40'}), label='',  )
    phone = CharField(widget=SpanEditableTextWidget(attrs={'size':'40'}), label='',  )
    
    class Meta:
        model = RevenueTradesPerson 
        fields = {'photo', 'first_name', 'last_name', 'title', 'email', 'phone', }
        
class BusinessEditForm(ModelForm):
    def rtImageFieldWidget():
        pass
    # this need "Click to edit" hovers"
    street_address = CharField(widget=SpanEditableTextWidget(attrs={'size':'40'}), label='')
    city_state_zip = CharField(widget=SpanEditableTextWidget(attrs={'size':'40'}), label='')
    email = CharField(widget=SpanEditableTextWidget(attrs={'size':'40'}), label='')
    phone = CharField(widget=SpanEditableTextWidget(attrs={'size':'12'}), label='')

    class Meta:
        model = RevenueTradesBusiness
        fields = ('street_address', 'city_state_zip', 'email', 'phone', )
        # widgets = { 'logo' : rtImageFieldWidget(), }

class BusinessTypeForm(ModelForm):
    biztype = MultipleChoiceField(widget=SelectMultiple(attrs={'class': 'select'}), label = '', choices=RevenueTradesMenus.menu_list("RevenueTradesIndustries"), )
    class Meta:
        model =   Business
        fields = ( 'biztype', )      

class BusinessLogoForm(ModelForm):
    logo = ImageField(widget=ClearableFileInput(attrs={'width':'100',}), label = '',)
    class Meta:
        model =   RevenueTradesBusiness
        fields = ( 'logo', )      

class BusinessNameForm(ModelForm):
    full_name = CharField(widget=SpanEditableTextWidget(attrs={'size':'40', 'class': 'tit'}), label = '', )
    class Meta:
        model =   RevenueTradesBusiness
        fields = ( 'full_name',  )      

def multi_show_uploaded(request, key):
    image = get_object_or_404(MultiuploaderImage, key_data=key)
    url = settings.MEDIA_URL+image.image.name
    return render_to_response('multiuploader/one_image.html', {"multi_single_url":url,})

def index(request):
    businesses = Business.objects.all()
    template = loader.get_template('business/list.html')
    context = Context({
        'search_results': businesses,
        
    })
    return HttpResponse(template.render(context))
    
@login_required
def user_profile(request):
    contact = None
    business_info = None
    business = None
    try:
        contact = RevenueTradesPerson.objects.get(user=request.user)
    except MultipleObjectsReturned:
        contact = RevenueTradesPerson.objects.filter(user=request.user)[0]
    try: 
        business_info = RevenueTradesBusiness.objects.get(contact=contact)
    except MultipleObjectsReturned:
        business_info = RevenueTradesBusiness.objects.filter(contact=contact)[0]
    try:
        business = Business.objects.get(business_info=business_info)
    except MultipleObjectsReturned:
        business = Business.objects.filter(business_info=business_info)[0]
    except:
        return HttpResponseNotFound('<h1>Business %d not found</h1>'% business_info.id)
    return profile(request, business.id)

def profile(request,bizid = 0):
    editable = False
    template = loader.get_template('business/public-profile.html')
    try:
        contact = RevenueTradesPerson.objects.get(user=request.user)
    except MultipleObjectsReturned:
        contact = RevenueTradesPerson.objects.filter(user=request.user)[0]
    if (bizid == 0):
        try: 
            business_info = RevenueTradesBusiness.objects.get(contact=contact)
        except MultipleObjectsReturned:
            business_info = RevenueTradesBusiness.objects.filter(contact=contact)[0]
        try:
            business = Business.objects.get(business_info=business_info)
        except MultipleObjectsReturned:
            business = Business.objects.filter(business_info=business_info)[0]
    else:    
        business = Business.objects.get(id=bizid)
    if (business.business_info.contact == RevenueTradesPerson.objects.get(user=request.user)):
        editable = True
    context = RequestContext(request, {
        'editable' : editable,
        'business' : business,
        'rtuser' : contact,
    })
    return HttpResponse(template.render(context))

def milestone(request, bizid=0):
    editable = False
    template = loader.get_template('business/business-dash-milestone.html')
    try:
        contact = RevenueTradesPerson.objects.get(user=request.user)
    except MultipleObjectsReturned:
        contact = RevenueTradesPerson.objects.filter(user=request.user)[0]
    if (bizid == 0):
        try: 
            business_info = RevenueTradesBusiness.objects.get(contact=contact)
        except MultipleObjectsReturned:
            business_info = RevenueTradesBusiness.objects.filter(contact=contact)[0]
        try:
            business = Business.objects.get(business_info=business_info)
        except MultipleObjectsReturned:
            business = Business.objects.filter(business_info=business_info)[0]
    else:    
        business = Business.objects.get(id=bizid)
    if (business.business_info.contact == RevenueTradesPerson.objects.get(user=request.user)):
        editable = True
    context = RequestContext(request, {
        'editable' : editable,
        'business' : business,
        'rtuser' : contact,
    })
    return HttpResponse(template.render(context))

@login_required
def prospects(request, bizid=0):
    
    rtuser = RevenueTradesPerson.objects.get(user=request.user)
    if (bizid==0):
        if not rtuser: 
            # no such user
            return HttpResponseNotFound('<h1>You must be registered, logged in and be the contact person to view this page</h1')
        if rtuser.business_verified:
            rtbusiness = RevenueTradesBusiness.objects.get(contact = rtuser)
            if not rtbusiness:
                return HttpResponseNotFound('<h1>You must be the contact person for a business to view this page</h1>' + "user: " + request.user.username + " rtuser: " + rtuser)
            business = Business.objects.get(business_info = rtbusiness)
            if not business:
                # no such business
                return HttpResponseNotFound('<h1>no such business</h1>' + str(rtbusiness) + "<br>" + str(rtuser))
        else:
            # user not a business owner
            return HttpResponseNotFound('<h1>You must be the contact person for this business to view this page</h1')
    else:
        business = Business.objects.get(id=bizid)
        rtbusiness = RevenueTradesBusiness.objects.get(id=business.business_info.id)
    if (rtuser != rtbusiness.contact):
        return HttpResponseNotFound('<h1>You must be the contact person for this business to edit this page</h1')
    template = loader.get_template('business/funders.html')
    context = Context({
        'rtuser': rtuser,
        'business': business,
        
    })
    return HttpResponse(template.render(context))
 
@login_required
def experts(request, bizid=0):
    
    rtuser = RevenueTradesPerson.objects.get(user=request.user)
    if (bizid==0):
        if not rtuser: 
            # no such user
            return HttpResponseNotFound('<h1>You must be registered, logged in and be the contact person to view this page</h1')
        if rtuser.business_verified:
            rtbusiness = RevenueTradesBusiness.objects.get(contact = rtuser)
            if not rtbusiness:
                return HttpResponseNotFound('<h1>You must be the contact person for a business to view this page</h1>' + "user: " + request.user.username + " rtuser: " + rtuser)
            business = Business.objects.get(business_info = rtbusiness)
            if not business:
                # no such business
                return HttpResponseNotFound('<h1>no such business</h1>' + str(rtbusiness) + "<br>" + str(rtuser))
        else:
            # user not a business owner
            return HttpResponseNotFound('<h1>You must be the contact person for this business to view this page</h1')
    else:
        business = Business.objects.get(id=bizid)
        rtbusiness = RevenueTradesBusiness.objects.get(id=business.business_info.id)
    if (rtuser != rtbusiness.contact):
        return HttpResponseNotFound('<h1>You must be the contact person for this business to edit this page</h1')
    template = loader.get_template('business/experts.html')
    context = RequestContext(request, {
        'rtuser': rtuser,
        'business': business,
        
    })
    return HttpResponse(template.render(context))
 
#login_required may not be correct. if not logged in should default to public profile but how to know which one
@login_required
@ensure_csrf_cookie
def dashboard(request, bizid=0):
    rtuser = RevenueTradesPerson.objects.get(user=request.user)
    if (bizid==0):
        if not rtuser: 
            # no such user
            return HttpResponseNotFound('<h1>You must be registered, logged in and be the contact person to view this page</h1')
        if rtuser.business_verified:
            rtbusiness = RevenueTradesBusiness.objects.get(contact = rtuser)
            if not rtbusiness:
                return HttpResponseNotFound('<h1>You must be the contact person for a business to view this page</h1>' + "user: " + request.user.username + " rtuser: " + rtuser)
            business = Business.objects.get(business_info = rtbusiness)
            if not business:
                # no such business
                return HttpResponseNotFound('<h1>no such business</h1>' + str(rtbusiness) + "<br>" + str(rtuser))
        else:
            # user not a business owner
            return HttpResponseNotFound('<h1>You must be the contact person for this business to view this page</h1')
    else:
        business = Business.objects.get(id=bizid)
        rtbusiness = RevenueTradesBusiness.objects.get(id=business.business_info.id)
    if (rtuser != rtbusiness.contact):
        return HttpResponseNotFound('<h1>You must be the contact person for this business to edit this page</h1')
    template = loader.get_template('business/business-profile-editor.html')
    logo_img = None
    user_photo_img = None
    thumbnail_max_size = (400,200)
    thumbnail_filter = PImage.ANTIALIAS
    thumbnail_format = "JPEG"

    if request.method == "POST":
        rtbusiness.full_name = request.POST.get('full_name', rtbusiness.full_name)
        rtbusiness.street_address = request.POST.get('street_address', rtbusiness.street_address)
        rtbusiness.city_state_zip = request.POST.get('city_state_zip', rtbusiness.city_state_zip)
        rtbusiness.email = request.POST.get('email', rtbusiness.email)
        rtbusiness.phone = request.POST.get('phone', rtbusiness.phone)
        #rtbusiness.website = request.POST.get('RT_URL')
        rtuser.title = request.POST.get('title_or_position', rtuser.title)
        #rtbusiness.ownerP = request.POST.get('ownerP')
        rtbusiness.website = request.POST.get('company_website', rtbusiness.website)
        #business.desired_funding = request.POST.get('desired_funding')
        #business.last_year_revenue = request.POST.get('last_year_revenue')
        if request.FILES.get('logo'):
            file_content = ContentFile(request.FILES.get('logo').read())
            logo_path = pjoin(MEDIA_ROOT + 'business_logos/', request.FILES.get('logo').name)
            rtbusiness.logo.save(logo_path, file_content)
            
            
        if request.FILES.get('photo'):
            file_content = ContentFile(request.FILES.get('photo').read())
            photo_path = pjoin(MEDIA_ROOT + 'user_photos/', request.FILES.get('photo').name)
            rtuser.photo.save(photo_path, file_content)
        if request.POST.get('biztype'):
            industries = request.POST.getlist('biztype')
            for industry in industries:
                try:
                    business.biztype.get(RevenueTradesMenus.menu_list("RevenueTradesIndustries")[int(industry)][1])
                except:
                    business.biztype.add(BusinessTypes.create(RevenueTradesMenus.menu_list("RevenueTradesIndustries")[int(industry)][1]))
            business.save()
        if request.POST.get('goal'):
            if not business.mission:
                business.mission = Business.Mission()
            business.mission.text = request.POST.get('goal')
        if request.POST.get('ambition'):
            ambitions = request.POST.get('ambition').split('\n')
            for new_ambition in ambitions:
                found = False
                for ambition in business.ambitions.all():
                    if new_ambition == ambition.text:
                        found = True
                        break
                if not found:
                    ambition_to_add = Ambition(text=new_ambition, short_text=new_ambition)
                    ambition_to_add.save()
                    business.ambitions.add(ambition_to_add)
            for ambition in business.ambitions.all():
                found = False
                for new_ambition in ambitions:
                    if new_ambition == ambition.text:
                        found = True
                if not found:
                    ambition.delete()
        business.employees_count = request.POST.get('employees_count',business.employees_count)
        business.year_started = request.POST.get('year_started',business.year_started)
        business.save()
        rtuser.save()
        rtbusiness.save()

    business_logo_form = LogoForm(instance = rtbusiness)
    # Hideous hack around what should be a DB lookup
    current_type_names = [biztype.description for biztype in business.biztype.all()]
    possible_names = [ type_name[1] for type_name in RevenueTradesMenus.menu_list("RevenueTradesIndustries")]
    current_types = [possible_names.index(biztype) for biztype in current_type_names ]
    business_type_form = BusinessTypeForm(instance = business, initial = {'biztype': current_types})
    business_form = BusinessEditForm(instance = rtbusiness)
    business_name_form = BusinessNameForm(instance = rtbusiness)
    contact_form = ContactEditForm(instance = rtuser)
    business_years_form = BusinessYearsEditForm(instance = business)
    business_employees_form = BusinessEmployeesEditForm(instance = business)
    context = RequestContext(request, {
        'rtuser': rtuser,
        'business': business,
        'business_form' : business_form,
        'contact_form' : contact_form,
        'business_type_form' : business_type_form,
        'business_logo_form' : business_logo_form,
        'business_name_form' : business_name_form,
        'business_years_form' : business_years_form,
        'business_employees_form' : business_employees_form,
        'verifiers' : VerificationService.objects.all(),
        
    })
    return HttpResponse(template.render(context))
    #return HttpResponse("Hello, world. You're at the business dashboard.")

def editMilestone(request):
    return HttpResponse("Hello, world. You're at the business edit/create milestone.")

def expertProspecting(request):
    return HttpResponse("Hello, world. You're at the business expertProspecting.")
    

@login_required
def startup(request):
# if business already exists then redirect to profile
    if request.method == "POST":
        rtuser = RevenueTradesPerson.objects.filter(user=request.user)
        if len(rtuser) == 1:
            rtuser = rtuser[0]
        else: 
            return HttpResponseNotFound('<h1>invalid user count</h1>' + len(rtuser) + " " + rtuser)
        if rtuser.business_verified:
            rtbusiness = RevenueTradesBusiness.objects.filter(contact = rtuser)
            if len(rtbusiness) == 1:
                rtbusiness = rtbusiness[0]
            elif len(rtbusiness) == 0:
                return HttpResponseNotFound('<h1>No business with this contact</h1> '  + rtuser)
            else: 
                # need an interface for users with multiple businesses
                # cleanup hack
                # for biz in rtbusiness:
                    # biz.delete()
                return HttpResponseNotFound('<h1>This contact owns several businesses</h1> '  + repr(rtuser) + ":" + repr(rtbusiness))
            business = Business.create(business_info = rtbusiness)
            if (business):
                rtbusiness.full_name = request.POST.get('business_name')
                #rtbusiness.website = request.POST.get('RT_URL')
                rtuser.title = request.POST.get('title_or_position')
                #rtbusiness.ownerP = request.POST.get('ownerP')
                rtbusiness.website = request.POST.get('company_website')
                rtbusiness.employees_count = request.POST.get('number_of_employees')
                #business.desired_funding = request.POST.get('desired_funding')
                #business.last_year_revenue = request.POST.get('last_year_revenue')
                try:
                    industries = request.POST.getlist('operating_industries')
                    #return HttpResponseNotFound('<h1>industries</h1> '  + repr(industries))
                    business.save()
                    for industry in industries:
                        business.biztype.add(BusinessTypes.create(industry))
                except  KeyError:
                    pass
                business.visibility = request.POST.get('visibility', business.visibility)
                business.TOUPS_agreement = request.POST.get('TOUPS_agreement', business.TOUPS_agreement)
    
                rtuser.save()
                rtbusiness.save()
                business.save()
            else:
                # no business created
                return HttpResponseNotFound('<h1>No business was created for</h1> '  + repr(rtuser) + ":" + repr(rtbusiness))
        logo_img = None
        user_photo_img = None
        thumbnail_max_size = (400,200)
        thumbnail_filter = PImage.ANTIALIAS
        thumbnail_format = "JPEG"

        if request.POST.get('new_logo'):
            logo_path = pjoin(MEDIA_ROOT, rtbusiness.logo.name)
            # resize and save image under same filename
            logo_img = PImage.open(logo_path)
            logo_img.thumbnail(thumbnail_max_size, thumbnail_filter)
            logo_img.save(logo_path, thumbnail_format)
            rtbusiness.save()
        if request.POST.get('new_photo'):
            photo_path = pjoin(MEDIA_ROOT, rtuser.photo.name)
            # resize and save image under same filename
            user_photo_img = PImage.open(photo_path)
            user_photo_img.thumbnail(thumbnail_max_size, thumbnail_filter)
            user_photo_img.save(logo_path, thumbnail_format)
            rtuser.save()

        if rtbusiness.logo:
            logo_img = pjoin(MEDIA_ROOT, rtbusiness.logo.name)
        if rtuser.photo:
            user_photo_img = pjoin(MEDIA_ROOT, rtuser.photo.name)
        business_logo_form = LogoForm(instance = rtbusiness)
        business_type_form = BusinessTypeForm(instance = business)
        business_form = BusinessEditForm(instance = rtbusiness)
        #business_logo_form = BusinessLogoForm(instance = rtbusiness)
        business_name_form = BusinessNameForm(instance = rtbusiness)
        contact_form = ContactEditForm(instance = rtuser)
        business_years_form = BusinessYearsEditForm(instance = business)
        business_employees_form = BusinessEmployeesEditForm(instance = business)
        context = RequestContext(request, {
            'rtuser': rtuser,
            'rtbusiness': rtbusiness,
            'logo_url' : logo_img,
            'photo_url' : user_photo_img,
            'business_form' : business_form,
            'contact_form' : contact_form,
            'business_type_form' : business_type_form,
            'business_logo_form' : business_logo_form,
            'business_name_form' : business_name_form,
            'business_years_form' : business_years_form,
            'business_employees_form' : business_employees_form,
            'verifiers' : VerificationService.objects.all(),
            })
        template = loader.get_template('business/business-profile-editor.html')
        return HttpResponse(template.render(context))
    return HttpResponseRedirect('/business/show_profile/')

def submit_image(request):
    context = RequestContext(request, {})

def response_mimetype(request):
    if "application/json" in request.META['HTTP_ACCEPT']:
        return "application/json"
    else:
        return "text/plain"

class JSONResponse(HttpResponse):
    """JSON response class."""
    def __init__(self,obj='',json_opts={},mimetype="application/json",*args,**kwargs):
        content = simplejson.dumps(obj,**json_opts)
        super(JSONResponse,self).__init__(content,mimetype,*args,**kwargs)
