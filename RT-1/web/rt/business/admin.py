from django.contrib import admin
from .models import *

class BusinessAdmin(admin.ModelAdmin):
    pass

class MissionAdmin(admin.ModelAdmin):
    pass

class OwnerAdmin(admin.ModelAdmin):
    pass

class AmbitionAdmin(admin.ModelAdmin):
    pass


admin.site.register(Business, BusinessAdmin)
#admin.site.register(Owner, OwnerAdmin)
admin.site.register(Mission, MissionAdmin)
admin.site.register(Ambition, AmbitionAdmin)





    

