from django.conf.urls import patterns, include, url

from business import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
	url(r'^(\d+)/milestone/$',  views.milestone, name='milestone'),
	url(r'^milestone2/',  'milestones.views.view', {'template_name': 'business/view_business_milestones.html' }),
	#url(r'^business/',  'business_profiles.views.view', {'template_name': 'business/view_business_profiles.html' }),
	url(r'^(\d+)/dashboard/$',  views.dashboard, name='dashboard'),
	url(r'^(\d+)/dashboard/milestones',  views.milestone, name='milestones'),
	url(r'^(\d+)/dashboard/prospects',  views.prospects, name='prospects'),
	url(r'^(\d+)/dashboard/experts',  views.experts, name='experts'),
	url(r'^(\d+)/milestones',  views.milestone, name='milestones'),
	url(r'^(\d+)/prospects',  views.prospects, name='prospects'),
	url(r'^(\d+)/experts',  views.experts, name='experts'),
	url(r'^prospecting',  'business_prospecting.views.view', {'template_name': 'business/prospecting.html' }),
	url(r'^public_profile/$',  views.profile, name='public profile'),
	url(r'^(\d+)/public_profile/$',  views.profile, name='public profile by business'),
	url(r'^(\d+)/$',  views.profile, name='public profile by business'),
	url(r'^(\d+)/profile/$',  views.profile, name='profile by business'),
	url(r'^show_profile/$',  views.user_profile, name='profile by user'),
	url(r'^startup',  views.startup, name='startup'),
	url(r'^(\d+)/dashboard/submit_image', views.submit_image, name='submit_image'),
	)
