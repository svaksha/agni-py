from django.forms import ModelForm
from django.forms.widgets import ClearableFileInput, CheckboxInput
from django.forms import CharField

from .models import Business
from home.models import RevenueTradesBusiness

class DashboardEditForm(ModelForm):
    class Meta:
        model = Business
        
from django.forms import Form
from widgets import AdvancedFileInput

    
class LogoForm(ModelForm):
    logo = AdvancedFileInput(image_width=500)
    class Meta:
        model = RevenueTradesBusiness
        fields = ( 'logo', )
        widgets = { 'logo' : AdvancedFileInput(image_width=500), }
    
    
# 								<form enctype="multipart/form-data" action="/business/dashboard/" method="POST">
# 									{% csrf_token %}
#                                 <div class="highlightB clearfix">
#                                     <div class="label-border clearfix">
#                                         <img src="{{ logo_url }}" width="221" alt="Business Logo">
#                                     </div>
#                                 </div>
#                                 <div class="links">
#                                 	{{ business.business_info.logo }}
#     								<input type="Submit" value="Upload a Logo" id="submit" />
#                                     <!--<a href="#" target="_self">Upload a Logo</a> -->
#                                 </div>
#                                 </form>

#<input id="id_full_name" type="text" name="full_name" value="Acme" maxlength="100">
#Needs to be
#<span class="simple_cms jqtransformdone" rel="textbox" action="/business/dashboard/">Maketing Consulting, LLC</span>
