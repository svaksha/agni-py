from django.db import models

from django.db.models import BooleanField
from django.db.models import CharField
from django.db.models import DateTimeField
from django.db.models import DecimalField
from django.db.models import EmailField
from django.db.models import ForeignKey 
from django.db.models import IntegerField
from django.db.models import ManyToManyField
from django.db.models import OneToOneField
from django.db.models import URLField

from django.contrib.auth.models import User

from django.forms import ModelForm

from django.utils import timezone

import expert.models 
import funder.models 
import home.models 
from milestones.models import Milestone 
from home.models import RevenueTradesMenus

class BusinessTypes(models.Model):
    description = models.CharField(max_length=200)
    @classmethod
    def create(cls, description):
        biztype = cls(description = description)
        biztype.save()
        return biztype
    def __unicode__(self):
        return str(self.description)
        
        

class Mission(models.Model):
    text = CharField(max_length=500,null=True)
    short_text = CharField(max_length=100,null=True)
    def __unicode__(self):
        return str(self.short_text)
    
class Ambition(models.Model):
    text = CharField(max_length=500,null=True)
    short_text = CharField(max_length=100,null=True)
    def __unicode__(self):
        return str(self.short_text)


   
        
class Question(models.Model):
    question = CharField(max_length=200,null=True)
    answer = CharField(max_length=200,null=True)
    author = ForeignKey('home.RevenueTradesPerson')
        
BUSINESS_AGE_RANGES = tuple(enumerate((
        'Less than 1 month',
        'Up to 6 months',
        'Up to 1 year',
        'Up to 3 years',
        'More than 3 years', )))
BUSINESS_SIZE_RANGES = tuple(enumerate((
        '1',
        '2 to 5',
        '6 to 50',
        'More than 50', )))
class Business(models.Model):
    
    business_info = ForeignKey('home.RevenueTradesBusiness')
    biztype = ManyToManyField(BusinessTypes, null=True, choices=RevenueTradesMenus.menu_list("RevenueTradesIndustries"))

    year_started = IntegerField(max_length=4,null=True, choices=RevenueTradesMenus.menu_list("BusinessAge"))
    mission = ForeignKey(Mission,null=True)
    ambitions = ManyToManyField(Ambition,null=True)
    experts = ManyToManyField('expert.Expert', blank=True, null=True, related_name="business_experts_set")
    employees_count = IntegerField(max_length=1,null=True, choices=RevenueTradesMenus.menu_list("BusinessSize"))
    milestone = ForeignKey(Milestone,null=True)
    investors = ManyToManyField('funder.Funder', blank=True, null=True)
    questions = ManyToManyField(Question, blank=True, null=True)
    visibility = BooleanField()
    TOUPS_agreement = BooleanField()

    @classmethod
    def create(cls,business_info):
        #achievement = Achievement.create()
        #achievement.save()
        biz = cls(business_info = business_info,
            #ambitions = Ambition(text="text", short_text = "short text"), 
            #milestone = Milestone(need_to_raise = Raise(needs = "",
            #    costs_of_needs = 0,
            #    requiredP = False,
            #    bill_of_GoodsP = False,
            #    ), 
            #achievements = achievement, 
            #track = Track(), 
            #financials = Financials(), 
            #terms = Terms(), 
            #scenarios = FundingScenario()),
            )
        return biz

    def __unicode__(self):
        return str("Business " + repr(self.biztype) + ": " + repr(self.business_info.full_name)) # wtf .get_username()
    
        
        

    