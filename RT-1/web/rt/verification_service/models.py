from django.db import models
from django.db.models import CharField, EmailField, ForeignKey, DateField
from django.db.models import URLField, BooleanField, ImageField, ManyToManyField
from django.contrib.auth.models import User
from home.models import RevenueTradesPerson, RevenueTradesBusiness

class Verification(models.Model):
    company = ForeignKey(RevenueTradesBusiness)
    request_date = DateField(auto_now_add=True)
    verification_date = DateField(null=True)
    rejection_date = DateField(null=True)
    
class VerificationService(models.Model):
    contact = ForeignKey(RevenueTradesPerson)
    full_name = CharField(max_length=200)
    street_address = CharField(max_length=200,null=True,default="Street address")
    city_state_zip = CharField(max_length=200,null=True,default="City/state/zip")
    website = URLField(null=True,default="http://")
    email = EmailField(null=True,default="Email address")
    phone = CharField(max_length=12,null=True,default="Phone number")
    #logo = ForeignKey(Picture,null=True)
    logo = ImageField("new_logo", upload_to='business_logos',null=True)
    tagline = CharField(max_length=200,null=True,default="Tag line")
    requests = ManyToManyField(Verification,null=True,related_name="requests",blank=True)
    verifiedcompanies = ManyToManyField(Verification,null=True,blank=True)
    
    def __unicode__(self):
        return str("VerificationService " + str(self.full_name)  + ": " + str(self.contact))

    def request_verification(self, business):
        verification_requests.add(Verification(company = business))
        self.save()