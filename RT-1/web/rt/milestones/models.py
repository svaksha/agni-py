from django.db import models

from django.db.models import BooleanField
from django.db.models import CharField
from django.db.models import DateTimeField
from django.db.models import DecimalField
from django.db.models import EmailField
from django.db.models import ForeignKey 
from django.db.models import IntegerField
from django.db.models import ManyToManyField
from django.db.models import OneToOneField
from django.db.models import URLField

from django.contrib.auth.models import User

from django.forms import ModelForm

from django.utils import timezone

from .Achievement import Achievement

from home.models import RevenueTradesMenus

class Raise(models.Model):
    needs = CharField(max_length=200,null=True)
    costs_of_needs = DecimalField(decimal_places=2, max_digits=10)
    requiredP = BooleanField()
    bill_of_GoodsP = BooleanField()
    def __unicode__(self):
        return str("$%.2d" % self.costs_of_needs + " " + self.needs)
        
    
        
class Track(models.Model):
    BUSINESS_TYPE_DESCRIPTIONS = tuple(enumerate((
            'Volvo',
            'Tesla',
            'Ford',
            'Rolls', )))
    BUSINESS_EXIT_EVENT_HORIZONS = tuple(enumerate((
            'Less than 1 year',
            '1 to 2 years',
            '2 to 5 years',
            'More than 5 years', )))
    BUSINESS_EQUITY_OFFERS = tuple(enumerate((
            'Less than 10%',
            '10 to 50%',
            '50 to 80%',
            'Up to 100%', )))
    BUSINESS_SERVICE_TYPES = tuple(enumerate((
            'Volvo',
            'Fiat',
            'Beater', )))
    BUSINESS_GROWTH_TYPES = tuple(enumerate((
            'Lifestyle',
            'Growth', )))
    description = IntegerField(max_length=2,null=True, choices=RevenueTradesMenus.menu_list("MilestoneBusinessType"))
    exit_event_horizon = IntegerField(max_length=2,null=True, choices=RevenueTradesMenus.menu_list("MilestoneExitEventHorizon"))
    equity_offer = IntegerField(max_length=2,null=True, choices=RevenueTradesMenus.menu_list("MilestoneEquity"))
    service_type = IntegerField(max_length=2,null=True, choices=RevenueTradesMenus.menu_list("MilestoneServiceType"))
    profitableP = BooleanField()
    growth_type = IntegerField(max_length=2,null=True, choices=RevenueTradesMenus.menu_list("MilestoneGrowthType"))
    
class AnnualResult(models.Model):
    year = IntegerField(max_length = 4)
    revenue = DecimalField(decimal_places=2, max_digits=10)
    COGS_percent_of_revenue = DecimalField(decimal_places=2, max_digits=10)
    SGA_expenses = DecimalField(decimal_places=2, max_digits=10)
    
class Financials(models.Model):
    financials = ForeignKey(AnnualResult)
    
    
class Terms(models.Model):
    accepted = BooleanField()
    
class FundingScenario(models.Model):
    trigger = DecimalField(decimal_places=2, max_digits=10)
    interest_rate = DecimalField(decimal_places=4, max_digits=7)
    discount_rate = DecimalField(decimal_places=4, max_digits=7)
    

class Milestone(models.Model):
    need_to_raise = ForeignKey(Raise,null=True)
    achievements = ForeignKey(Achievement,null=True)
    track = ForeignKey(Track,null=True)
    financials = ForeignKey(Financials,null=True)
    terms = ForeignKey(Terms,null=True)
    scenarios = ForeignKey(FundingScenario,null=True)
    @classmethod
    def create(cls):
        milestone = cls(need_to_raise = Raise(),
        track = Track(),
        financials = Financials(),
        terms = Terms(),
        scenarios = FundingScenario(),
        )
        return milestone
        
