from PIL import Image as PImage
from os.path import join as pjoin
from settings import MEDIA_ROOT

from django.http import HttpResponse, HttpResponseNotFound
from django.template import RequestContext, Context, loader
from django.core.exceptions import MultipleObjectsReturned

TEMPLATE_CONTEXT_PROCESSORS = ( 
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
)
from django.http import Http404
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import ensure_csrf_cookie
from django.db import IntegrityError, DatabaseError

from milestones.models import Milestone

from home.models import RevenueTradesPerson, RevenueTradesBusiness

from business.models import Ambition, Business, BusinessTypes, BUSINESS_AGE_RANGES



@login_required
@ensure_csrf_cookie
def milestone_builder(request, bizid=0):
    template = loader.get_template('business/milestone-process.html')
    try:
        contact = RevenueTradesPerson.objects.get(user=request.user)
    except MultipleObjectsReturned:
        contact = RevenueTradesPerson.objects.filter(user=request.user)[0]
    if (bizid == 0):
        try: 
            business_info = RevenueTradesBusiness.objects.get(contact=contact)
        except MultipleObjectsReturned:
            business_info = RevenueTradesBusiness.objects.filter(contact=contact)[0]
        try:
            business = Business.objects.get(business_info=business_info)
        except MultipleObjectsReturned:
            business = Business.objects.filter(business_info=business_info)[0]
    else:    
        business = Business.objects.get(id=bizid)
    if (business.business_info.contact == RevenueTradesPerson.objects.get(user=request.user)):
        editable = True
    context = RequestContext(request, {
        'business' : business,
        'rtuser' : contact,
    })
    return HttpResponse(template.render(context))

