from django.contrib import admin
from .models import *
from milestones.MatchingFundsAchievement import *
from milestones.PurchaseOrderAchievement import *

class RaiseAdmin(admin.ModelAdmin):
    pass
    
class AchievementAdmin(admin.ModelAdmin):
    pass

class PurchaseOrderAchievementAdmin(admin.ModelAdmin):
    pass

class MatchingFundsAchievementAdmin(admin.ModelAdmin):
    pass

class TrackAdmin(admin.ModelAdmin):
    pass

class AnnualResultAdmin(admin.ModelAdmin):
    pass

class FinancialsAdmin(admin.ModelAdmin):
    pass

class TermsAdmin(admin.ModelAdmin):
    pass

class FundingScenarioAdmin(admin.ModelAdmin):
    pass

class MilestoneAdmin(admin.ModelAdmin):
    pass

admin.site.register(Raise, RaiseAdmin)
admin.site.register(Achievement, AchievementAdmin)
admin.site.register(PurchaseOrderAchievement, PurchaseOrderAchievementAdmin)
admin.site.register(MatchingFundsAchievement, MatchingFundsAchievementAdmin)
admin.site.register(Track, TrackAdmin)
admin.site.register(AnnualResult, AnnualResultAdmin)
admin.site.register(Financials, FinancialsAdmin)
admin.site.register(Terms, TermsAdmin)
admin.site.register(FundingScenario, FundingScenarioAdmin)
admin.site.register(Milestone, MilestoneAdmin)





    

