from django.conf.urls import patterns, url

from milestones import views

urlpatterns = patterns('',
	url(r'^(\d)/builder/$',  views.milestone_builder, name='milestone builder'),
	)
