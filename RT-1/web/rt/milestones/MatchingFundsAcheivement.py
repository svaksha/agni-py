from django.db import models

from django.db.models import CharField
from django.db.models import DateTimeField
from django.db.models import DecimalField
from django.db.models import ForeignKey 
from Achievment import Achievment

class MatchingFunds(models.Model):
    amount = DecimalField(decimal_places=2, max_digits=10)
    funder = CharField(max_length=200,null=True)
    
class MatchingFundsAchievement(Achievement):
    matching_funds = ForeignKey(PurchaseOrder, null=True)
    def receive(self, funds):
        self.received += funds.amount
        self.matching_funds.add(funds)
        if (self.completion_date is null):
            if self.received > self.cost:
                self.completion_date = timezone.now()
        
    def percent_complete(self):
        return (self.received*100/self.cost)
        
    def __unicode__(self):
        return str("$%.2d" % self.received + " %.2d percent complete " % self.percent_complete() + self.estimated_completion_date.strftime('%Y-%m-%d'))
