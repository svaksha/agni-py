$(document).ready(function() {
 
	$('div.carousel-bar div.b-next').click(function() {
        //slide the item            
       	$("div.carousel-bar div.slide-wrap").animate({marginLeft:-26},200,function(){
            $(this).find(".slide:last").after($(this).find(".slide:first"));
            $(this).css({marginLeft:0});
        })
    });

	$('div.carousel-bar div.b-previous').click(function() {
        //slide the item            
       	$("div.carousel-bar div.slide-wrap").animate({marginLeft:26},200,function(){
            $(this).find(".slide:first").before($(this).find(".slide:last"));
            $(this).css({marginLeft:0});
        })
    });

});

