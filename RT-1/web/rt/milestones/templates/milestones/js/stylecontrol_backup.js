$(document).ready(function() {
	
	
	
	
	//Main Navigation Tabs: on click events
	jQuery(document).ready(function(){
	$("#nav-biz").click(function(){  //Business tab click events
		$(this).toggleClass("active");
		$("#navtab-biz").slideToggle("slow")
		$("#rtlogo").removeClass("active");
		$("#navtab-main").hide();;
		$("#nav-exp").removeClass("active");
		$("#navtab-exp").hide();
		$("#nav-vault").removeClass("active");
		$("#navtab-vault").hide();
		return false;
	});
	});
	
	jQuery(document).ready(function(){
	$("#navtab-biz div.nav-tab-closing").click(function(){ //Business tab bottom arrow click events
		$("#nav-biz").removeClass("active");
		$("#navtab-biz").slideUp("slow");
		return false;
	});
	});
	
	jQuery(document).ready(function(){
	$("#nav-exp").click(function(){ //Expert tab click events
		$(this).toggleClass("active");
		$("#navtab-exp").slideToggle("slow");
		$("#rtlogo").removeClass("active");
		$("#navtab-main").hide();
		$("#nav-biz").removeClass("active");
		$("#navtab-biz").hide();
		$("#nav-vault").removeClass("active");
		$("#navtab-vault").hide();
		return false;
	});
	});
	
	jQuery(document).ready(function(){
	$("#navtab-exp div.nav-tab-closing").click(function(){ //Expert tab bottom arrow click events
		$("#nav-exp").removeClass("active");
		$("#navtab-exp").slideUp("slow");
		return false;
	});
	});
	
	jQuery(document).ready(function(){
	$("#nav-vault").click(function(){ //Vault tab click events
		$(this).toggleClass("active");
		$("#navtab-vault").slideToggle("slow");
		$("#rtlogo").removeClass("active");
		$("#navtab-main").hide();
		$("#nav-biz").removeClass("active");
		$("#navtab-biz").hide();
		$("#nav-exp").removeClass("active");
		$("#navtab-exp").hide();
		return false;
		
	});
	});
	
	jQuery(document).ready(function(){
	$("#navtab-vault div.nav-tab-closing").click(function(){ //Vault tab bottom arrow click events
		$("#nav-vault").removeClass("active");
		$("#navtab-vault").slideUp("slow");
		return false;
	});
	});
	
	jQuery(document).ready(function(){
	$("#rtlogo img").click(function(){  //Main tab (logo tab) click events
		$("#rtlogo").toggleClass("active");
		$("#navtab-main").slideToggle("slow");
		$("#nav-biz").removeClass("active");
		$("#navtab-biz").hide();
		$("#nav-exp").removeClass("active");
		$("#navtab-exp").hide();
		$("#nav-vault").removeClass("active");
		$("#navtab-vault").hide();
		return false;
	});
	});
	
	jQuery(document).ready(function(){
	$("#navtab-main div.nav-tab-closing").click(function(){ //Main tab (logo tab) bottom arrow click events
		$("#rtlogo").removeClass("active");
		$("#navtab-main").slideUp("slow");
		return false;
	});
	});
	
	//User Nav Tab: on click events
	$("#b-user-settings").click(function() { //User thumbnail opens user-settings
		$(this).hide(); 
		$("#user-settings").show();
		$("#tools-tab").hide();
		$("#b-tools").show();
		return false;
	});
	
	$("#user-settings div.border-left").click(function() { //Left expandable arrow closes user-settings
		$("#user-settings").hide();
		$("#b-user-settings").show();
		return false;
	});
	
	$("#user-settings div.border-right").click(function() { //User thumbnail closes user-settings
		$("#user-settings").hide();
		$("#b-user-settings").show();
		return false;
	});
	
	$("#b-tools").click(function() { //User thumbnail opens user-settings
		$(this).hide(); 
		$("#tools-tab").show();
		$("#user-settings").hide();
		$("#b-user-settings").show();
		return false;
	});
	
	$("#tools-tab div.border-left").click(function() { //Left expandable arrow closes user-settings
		$("#tools-tab").hide();
		$("#b-tools").show(); 
		return false;
	});
	
	$("#tools-tab div.border-right").click(function() { //User thumbnail closes user-settings
		$("#tools-tab").hide();
		$("#b-tools").show(); 
		return false;
	});
	
	
	//Tabbed Box A: when page loads
	jQuery(document).ready(function(){
		$("div.tabboxA-wrap .tab-content").hide(); //Hide all content
		$("div.tabboxA-wrap ul.tabs li:first").addClass("active").show(); //Activate first tab
		$("div.tabboxA-wrap .tab-content:first").show(); //Show first tab content

		//Tabbed Box A: on click event
		$("div.tabboxA-wrap ul.tabs li").click(function() {

			$("div.tabboxA-wrap ul.tabs li").removeClass("active"); //Remove any "active" class
			$(this).addClass("active"); //Add "active" class to selected tab
			$("div.tabboxA-wrap .tab-content").hide(); //Hide all tab content

			var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
			$(activeTab).fadeIn(); //Fade in the active ID content
			return false;
		});
	});

	
	//Tabbed Box B: when page loads
	jQuery(document).ready(function(){
		$("div.tabboxB-wrap .tab-content").hide(); //Hide all content
		$("div.tabboxB-wrap ul.tabs li:first").addClass("active").show(); //Activate first tab
		$("div.tabboxB-wrap .tab-content:first").show(); //Show first tab content

		//Tabbed Box B: on click event
		$("div.tabboxB-wrap ul.tabs li").click(function() {

			$("div.tabboxB-wrap ul.tabs li").removeClass("active"); //Remove any "active" class
			$(this).addClass("active"); //Add "active" class to selected tab
			$("div.tabboxB-wrap .tab-content").hide(); //Hide all tab content

			var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
			$(activeTab).fadeIn(); //Fade in the active ID content
			return false;
		});
	});
	
	
	//Tabbed Box C: when page loads
	jQuery(document).ready(function(){
		$("div.tabboxC-wrap .tab-content").hide(); //Hide all content
		$("div.tabboxC-wrap ul.tabs li:first").addClass("active").show(); //Activate first tab
		$("div.tabboxC-wrap .tab-content:first").show(); //Show first tab content

		//Tabbed Box C: on click event
		$("div.tabboxC-wrap ul.tabs li").click(function() {

			$("div.tabboxC-wrap ul.tabs li").removeClass("active"); //Remove any "active" class
			$(this).addClass("active"); //Add "active" class to selected tab
			$("div.tabboxC-wrap .tab-content").hide(); //Hide all tab content

			var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
			$(activeTab).fadeIn(); //Fade in the active ID content
			return false;
		});
	});
	
	
	
	//Accordion Box A: when page loads
	jQuery(document).ready(function(){
		$('div.accordionA-wrap .accordion-container').hide(); //Hide/close all containers
		$('div.accordionA-wrap .accordion-trigger:first').addClass('active').next().show(); //Add "active" class to first trigger, then show/open the immediate next container

		//On Click
		$('div.accordionA-wrap .accordion-trigger').click(function(){
			if( $(this).next().is(':hidden') ) { //If immediate next container is closed...
				$('div.accordionA-wrap .accordion-trigger').removeClass('active').next().slideUp(); //Remove all "active" state and slide up the immediate next container
				$(this).toggleClass('active').next().slideDown(); //Add "active" state to clicked trigger and slide down the immediate next container
			}
			return false; //Prevent the browser jump to the link anchor
		});
	});
	
	//Accordion Box B: when page loads
	jQuery(document).ready(function(){
		$('div.accordionB-wrap .accordion-container').hide(); //Hide/close all containers
		$('div.accordionB-wrap .accordion-trigger:first').addClass('active').next().show(); //Add "active" class to first trigger, then show/open the immediate next container

		//On Click
		$('div.accordionB-wrap .accordion-trigger').click(function(){
			if( $(this).next().is(':hidden') ) { //If immediate next container is closed...
				$('div.accordionB-wrap .accordion-trigger').removeClass('active').next().slideUp(); //Remove all "active" state and slide up the immediate next container
				$(this).toggleClass('active').next().slideDown(); //Add "active" state to clicked trigger and slide down the immediate next container
			}
			return false; //Prevent the browser jump to the link anchor
		});
	});

	//Accordion Box C: when page loads
	jQuery(document).ready(function(){
		$('div.accordionC-wrap .accordion-container').hide(); //Hide/close all containers
		$('div.accordionC-wrap .accordion-trigger:first').addClass('active').next().show(); //Add "active" class to first trigger, then show/open the immediate next container

		//On Click
		$('div.accordionC-wrap .accordion-trigger').click(function(){
			if( $(this).next().is(':hidden') ) { //If immediate next container is closed...
				$('div.accordionC-wrap .accordion-trigger').removeClass('active').next().slideUp(); //Remove all "active" state and slide up the immediate next container
				$(this).toggleClass('active').next().slideDown(); //Add "active" state to clicked trigger and slide down the immediate next container
			}
			return false; //Prevent the browser jump to the link anchor
		});
	});
	
	
	
	
	//Buttons Effects 
	
	
	
	
	//Jquery Controlled Image Rollovers
	$("#rtlogo img").hover(function(){
		$(this).attr('src','images/ui/rt_icon_med_on.png');
			}, function() {
		$(this).attr('src','images/ui/rt_icon_med_off.png'); 
	}); //Simple image hover effect on RT icon	
	
	$("#user-tab .user-thumbplus img").hover(function(){
		$(this).attr('src','images/ui/usr_thumbplus_on.png');
			}, function() {
		$(this).attr('src','images/ui/usr_thumbplus_off.png'); 
	}); //Simple image hover effect on User thumbnail

	
});