from django.db import models

from django.db.models import CharField
from django.db.models import DateTimeField
from django.db.models import DecimalField
from django.db.models import ForeignKey 
from .Achievement import Achievement

class PurchaseOrder(models.Model):
    amount = DecimalField(decimal_places=2, max_digits=10)
    customer = CharField(max_length=200,null=True)
    
class PurchaseOrderAchievement(Acheivement):
    purchase_orders = ForeignKey(PurchaseOrder, null=True)
    def receive(self, po):
        self.received += po.amount
        self.purchase_orders.add(po)
        if (self.completion_date is null):
            if self.received > self.cost:
                self.completion_date = timezone.now()
        
    def percent_complete(self):
        return (self.received*100/self.cost)
        
    def __unicode__(self):
        return str("$%.2d" % self.received + " %.2d percent complete " % self.percent_complete() + self.estimated_completion_date.strftime('%Y-%m-%d'))
