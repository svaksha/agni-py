from django.db import models

from django.db.models import CharField
from django.db.models import DateTimeField
from django.db.models import DecimalField

class Achievement(models.Model):
    cost = DecimalField(decimal_places=2, max_digits=10)
    estimated_completion_date = DateTimeField()
    received = DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)
    completion_date = DateTimeField(null=True, blank=True)
    @classmethod
    def create(cls):
        acheivement = cls(cost = 0, estimated_completion_date = "2012-01-01 00:00")
        return acheivement
    
    def receive(self, amount):
        self.received += amount
        if (self.completion_date is null):
            if self.received > self.cost:
                self.completion_date = timezone.now()
        
    def percent_complete(self):
        return (self.received*100/self.cost)
        
    def __unicode__(self):
        return str("$%.2d" % self.cost + " " + self.estimated_completion_date.strftime('%Y-%m-%d'))
