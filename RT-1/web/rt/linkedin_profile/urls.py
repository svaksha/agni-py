from django.conf.urls import patterns, include, url
from django.conf import settings
#from django.views.generic.simple import redirect_to
#new version for D1.5
from django.views.generic import RedirectView
from .views import *


urlpatterns = patterns('',
        url(r'^$', 'linkedin_profile.views.linkedin_access'),
        url(r'^process/?', 'linkedin_profile.views.return_url'),
        url(r'^data/', 'linkedin_profile.views.return_data'),
        url(r'^displaydata/', 'linkedin_profile.views.display_data'),
        url(r'^logout/', 'linkedin_profile.views.linkedin_logout', name='logout'),
    )
