
from linkedin import linkedin
from django.http import Http404
import logging
log = logging.getLogger(__name__)

def get_profile(request):

    #Grabbing the access token
    log.info("Grabbing the access token")
    result = request.session['authentication'].get_access_token()
    #log.debug("result ", str(result))
    if result == None:
        log.error("No access token! Redirecting")
        raise Http404

    #Grabbing the profile 
    log.info("Grabbing the profile data")
    #profile = request.session['authentication'].get_profile()
    profile = request.session['application'].get_profile()
    data = profile

    return data
