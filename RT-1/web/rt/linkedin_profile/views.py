"""This view will help grab the user's LinkedIn Data and display it to the screen"""

from django.http import Http404
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import render_to_response
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import logout
from django.conf import settings
from django.db.models.fields import FieldDoesNotExist
from linkedin import linkedin
import logging
import re
from .get_data import get_data
from .models import LinkedInProfile

log = logging.getLogger(__name__)

@login_required
def linkedin_access(request):
    """Simple page used to display link to start process"""
    log.debug("In linkedin_access()")
    request.session['abs_uri'] = request.build_absolute_uri()
    proc_link = request.session['abs_uri'] + '/process'
    return render_to_response('linkedin_profile/login.html', {'proc_link': proc_link})

@login_required
def linkedin_cancel(request):
    """Handle when people cancel linkedin access"""
    proc_link = request.session['abs_uri'] + '/process'
    return render_to_response('linkedin_profile/cancel.html', {'proc_link': proc_link})

@login_required
def return_url(request):
    """Authenticate using keys and redirect to url so user can log in"""
    #Key and return url data
    SECRET = 'kurBD0AkAmpvLVjt'
    KEY = 'r0q16dckjw6s'
    RETURN_URL = "http://proto2.revenuetrades.com/linkedin/data/"

    log.info("getting authentication")
    authentication = linkedin.LinkedInAuthentication(KEY, SECRET, RETURN_URL)
    auth_url = authentication.authorization_url

    #is there any reason to keep the auth token and secret for the entire session?
    #request.session['request_token'] = authentication.token
    #request.session['request_token_secret'] = authentication.secret
    request.session['authentication'] = authentication #stash the api for the duration of the session.
    request.session['application'] = linkedin.LinkedInApplication(authentication)

    return redirect(auth_url)

@login_required
def return_data(request):
    """Grab the oauth token and verifier, then grab the data and stash it"""

    #check for error key in request dict. If there, that means the user cancelled the request.
    if request.REQUEST.get('error'):
        return redirect(linkedin_cancel)

    log.info("Grabbing auth code ")
    request.session['authentication'].authorization_code = request.REQUEST['code']
    profile, connections = get_data(request)

    #stash the profile data in the database 
    lp = LinkedInProfile(firstName=profile['firstName'], lastName=profile['lastName'])
    for k, v in profile.items():
        try:
            if lp._meta.get_field(k):
                setattr(lp, k, v)
        except FieldDoesNotExist as e:
            log.error(str(e))
            log.error("key ", k, " not found. You can get this data from the fullProfile field.")

    lp.fullProfile = profile
    lp.save()

    #Need to add functionality here to stash connections data ----------------------------------------------------------------------------------->

    #------------------------------------------------------------------------------------------------------------------------------------------->

    return redirect('/linkedin/displaydata')

@login_required
def display_data(request):
    """Simple display of users proifle data"""
    #pull info out to display on the page
    user_data = LinkedInProfile.objects.values()
        
    return render_to_response('linkedin_profile/data.html', {'data': user_data})

@login_required
def linkedin_logout(request):
    response = logout(request)
    log.debug(request.COOKIES)
    response.delete_cookie('sessionid')
    return response
