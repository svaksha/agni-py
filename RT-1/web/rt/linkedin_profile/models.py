from django.db import models
from django.db.models import CharField, URLField, BooleanField, ImageField, IntegerField, DateField
from django.contrib.auth.models import User
from django.db.models import ManyToManyField

class LinkedInProfile(models.Model):
    """Basic linkedin profile data"""
    id = CharField(primary_key=True, max_length=200,default="ID")
    firstName = CharField(max_length=200)
    lastName = CharField(max_length=200)
    headline = CharField(max_length=200, null=True)
    siteStandardProfileRequest = CharField(max_length=500, null=True)
    distance = IntegerField(null=True)
    educations = CharField(max_length=500, null=True)
    location = CharField(max_length=200, null=True)
    numConnections = IntegerField(null=True)
    educations = CharField(max_length=1000, null=True)
    skills = CharField(max_length=500,null=True)
    #store all returned fields in on JSON obj.
    fullProfile = CharField(max_length=1000,null=True)

    #these are the sub fields for "educations". Should we store them separately?
    #values = CharField(max_length=200)
    #degree = CharField(max_length=200)
    #endDate = DateField()
    #startdDate = CharField(max_length=200)
    #fieldOfStudy = CharField(max_length=200)
    #notes = CharField(max_length=200)
    #schoolName = CharField(max_length=200)
    #startDate = DateField(default="Last")
