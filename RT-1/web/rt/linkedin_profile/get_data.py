
from linkedin import linkedin
from django.http import Http404
import logging
log = logging.getLogger(__name__)

def get_data(request):
    """Get the users linkedIn proifle and their connections profiles"""
    log.info("Grabbing the access token")
    result = request.session['authentication'].get_access_token()
    if result == None:
        log.error("No access token! Redirecting")
        raise Http404

    log.info("Grabbing the profile data")
    #Using all selectors to get users profile
    profile = request.session['application'].get_profile(selectors=['id', 'first-name', 'last-name', 'location', 'distance', 'num-connections', 'skills', 'educations'])

    #get the users connections and info
    connections = request.session['application'].get_connections()
    print("connections:", connections)

    return profile, connections
