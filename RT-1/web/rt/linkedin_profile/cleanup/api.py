"""Module wrapper that handles the specifics of the getting a working profile from linkedin"""

from linkedin import linkedin
import logging

log = logging.getLogger(__name__)

class Api:
    def __init__(self):
        self.SECRET = 'kurBD0AkAmpvLVjt'
        self.KEY = 'r0q16dckjw6s'
        self.RETURN_URL = "http://proto2.revenuetrades.com/linkedin/data/"
        self.api = linkedin.LinkedIn(self.KEY, self.SECRET, self.RETURN_URL)
        self.request_token = None
        self.request_token_secret = None
        
    def get_auth_url(self):
        """Submit credentials to linkedin and return auth_url"""
        #if result, ok, else throw error
        result = self.api.request_token()
        if result == None:
            #add redirect to error page
            log.error("No result! Redirecting")
         
        self.request_token = self.api._request_token
        self.request_token_secret = self.api._request_token_secret

        message = "class request token, and request token secret in get_auth: %s %s" % (self.request_token, self.request_token_secret)
        log.debug(message)

        return self.api.get_authorize_url(request_token = self.request_token)

    def get_profile(self, ver, rt):
        """Get the linkedin profile"""
        message = "ver, request token, and request token secret in get_profile: %s %s %s" % (ver, self.request_token, self.request_token_secret)
        message2 = "SECRET, KEY get_profile: %s %s" % (self.SECRET, self.KEY)
        log.debug(message)
        log.debug(message2)

        testmessage = "test get_profile: %s" % (self.test)
        log.debug(testmessage)

        #if result, return, else throw error
        result = self.api.access_token(request_token = rt, request_token_secret = self.request_token_secret, verifier = ver)
        return api.get_profile(url = "http://www.linkedin.com/pub/rob-graziano/1/243/334/")
