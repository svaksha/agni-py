"""This view will help grab the user's LinkedIn Data and display it to the screen"""

from django.http import Http404
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import render_to_response
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import logout
from django.conf import settings
from linkedin import linkedin
import logging
import re
from .get_profile import get_profile

log = logging.getLogger(__name__)

@login_required
def linkedin_access(request):
        """Simple page used to display link to start process"""
        request.session['abs_uri'] = request.build_absolute_uri()
        proc_link = request.session['abs_uri'] + '/process'
        return render_to_response('linkedin_profile/login.html', {'proc_link': proc_link})

@login_required
def linkedin_cancel(request):
        """Handle when people cancel linkedin access"""
        proc_link = request.session['abs_uri'] + '/process'
        return render_to_response('linkedin_profile/cancel.html', {'proc_link': proc_link})

@login_required
def return_url(request):
        """Authenticate using keys and redirect to url so user can log in"""
        #Key and return url data
        SECRET = 'kurBD0AkAmpvLVjt'
        KEY = 'r0q16dckjw6s'
        RETURN_URL = "http://proto2.revenuetrades.com/linkedin/data/"

        #create a new api object by passing in our keys
        try:
            api = linkedin.LinkedIn(KEY, SECRET, RETURN_URL)
        except:
            raise Http404

        #get the request token
        result = api.request_token()
        if result == None:
           log.error("No result! Redirecting")
           raise Http404

        #stash the request token and the request token secret
        log.info("Stashing the request token and the request token secret")
        request.session['request_token'] = api._request_token
        request.session['request_token_secret'] = api._request_token_secret

        #Submit the request token to grab the authentication url.
        log.info("We have the request token, going for the authentication url")
        try:
            initial_auth_url = api.get_authorize_url(request_token = request.session['request_token'])
        except:
            log.error("Problem grabbing the authentication url")
            raise Http404

        log.info("We have the auth url, redircting")
        request.session['api'] = api #stash the api for the duration of the session.

        #substitute the word authenticate for authorize in the url. This keeps the user from having
        #to authorize linkedin each time.
        log.info("initial url: %s", initial_auth_url)
        auth_url = re.sub(r"/authorize", "/authenticate", initial_auth_url)
        log.info("new url: %s", auth_url)

        return redirect(auth_url)

@login_required
def return_data(request):
    """Grab the oauth token and verifier"""

    if 'linkedin-response' not in request.session:
        ver = request.GET.get('oauth_verifier', '')
        log.info("Grabbed verifier")
        data = get_profile(request, ver)
        previous_data = None
        #stash the data for the session
        request.session['linkedin-response'] = data
    #stash the data from previous response if there is any
    else:
        previous_data = request.session['linkedin-response']
        request.session['previous_linkedin-response'] = request.session['linkedin-response']
        data = request.session['linkedin-response']
        #log.info("stored previous response: %s" % request.session['previoius_linkedin-response'])

        return render_to_response('linkedin_profile/data.html', {'data': data, 'previous_data': previous_data})

@login_required
def linkedin_logout(request):
    response = logout(request)
    log.debug(request.COOKIES)
    response.delete_cookie('sessionid')
    return response

