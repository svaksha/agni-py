"""This view will help grab the user's LinkedIn Data and display it to the screen"""

from django.http import Http404
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import render_to_response
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import logout
from django.conf import settings
from linkedin import linkedin
import logging
import re
from .get_profile import get_profile
from .models import LinkedInProfile

log = logging.getLogger(__name__)

@login_required
def linkedin_access(request):
    """Simple page used to display link to start process"""
    log.debug("In linkedin_access()")
    request.session['abs_uri'] = request.build_absolute_uri()
    #log.debug("request.session['abs_uri') ", str(request.session['abs_uri']))
    proc_link = request.session['abs_uri'] + '/process'
    #log.debug("proc_link: ", proc_link)
    return render_to_response('linkedin_profile/login.html', {'proc_link': proc_link})

@login_required
def linkedin_cancel(request):
    """Handle when people cancel linkedin access"""
    proc_link = request.session['abs_uri'] + '/process'
    return render_to_response('linkedin_profile/cancel.html', {'proc_link': proc_link})

@login_required
def return_url(request):
    """Authenticate using keys and redirect to url so user can log in"""
    #Key and return url data
    SECRET = 'kurBD0AkAmpvLVjt'
    KEY = 'r0q16dckjw6s'
    RETURN_URL = "http://proto2.revenuetrades.com/linkedin/data/"

    log.debug("getting authentication")
    authentication = linkedin.LinkedInAuthentication(KEY, SECRET, RETURN_URL)
    auth_url = authentication.authorization_url

    #request.session['request_token'] = authentication.token
    #request.session['request_token_secret'] = authentication.secret
    request.session['authentication'] = authentication #stash the api for the duration of the session.
    request.session['application'] = linkedin.LinkedInApplication(authentication)

    return redirect(auth_url)

@login_required
def return_data(request):
    """Grab the oauth token and verifier, grab the data and stash it"""

    if 'linkedin-response' not in request.session:
        request.session['authentication'].authorization_code = request.REQUEST['code']
        log.info("Grabbing auth code ")
        #log.info("NEW GET: ", request.REQUEST["code"])
        data = get_profile(request)
        previous_data = None
        #stash the data for the session
        request.session['linkedin-response'] = data

	#stash the data in the database, pull it out to display on the page
        lp = LinkedInProfile(fname=data['firstName'], lname=data['lastName'], headline=data['headline']) #, siteStandardProfileRequest=data['siteStandardProfileRequest'])
        lp.save()
        user_data = {'First Name':lp.fname, 'Last Name':lp.lname}
        
    #not sure if we need to keep previous data since it will be in the DB -------------------------
    #stash the data from previous response if there is any
    else:
        previous_data = request.session['linkedin-response']
        request.session['previous_linkedin-response'] = request.session['linkedin-response']
	#stash the data in the database, pull it out to display on the page
        data = get_profile(request)
        lp = LinkedInProfile(fname=data['firstName'], lname=data['lastName'], headline=data['headline']) #, siteStandardProfileRequest=data['siteStandardProfileRequest'])
        lp.save()
        user_data = {'First Name':lp.fname, 'Last Name':lp.lname}
        #user_data = request.session['linkedin-response']
        #log.info("stored previous response: %s" % request.session['previoius_linkedin-response'])

    return render_to_response('linkedin_profile/data.html', {'data': user_data, 'previous_data': previous_data})

@login_required
def linkedin_logout(request):
    response = logout(request)
    log.debug(request.COOKIES)
    response.delete_cookie('sessionid')
    return response
