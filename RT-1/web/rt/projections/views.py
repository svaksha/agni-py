import datetime
import json
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.http import HttpResponse,HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.middleware import csrf

@login_required
def date_projection(request):
    start_date = datetime.datetime.today()
    days_in_cycle = 28
    number_of_years = 3
    if request.method == 'POST':
        start_date = request.POST.get('start_date','12 Oct 2012')
        days_in_cycle = int( request.POST.get('days_in_cycle',28) )
        number_of_years = int( request.POST.get('number_of_years',3) )
        start_date = datetime.datetime.strptime(start_date,"%d %b %Y")

        result = [start_date.strftime("%d %b %Y"),days_in_cycle,number_of_years] + [(start_date + datetime.timedelta(days=x)).strftime("%d %b %Y") for x in range(0,days_in_cycle*(number_of_years+1)*12,days_in_cycle)]

        outbound = {"metadata":[
            {"name":"start_date","label":"Start Date","datatype":"date","editable":True},
            {"name":"cycle","label":"Cycle","datatype":"integer","editable":True},
            {"name":"years","label":"Years","datatype":"integer","editable":True}]}

        outbound["metadata"].extend( [{"name":"projection_%s" % x,"label":"Projection %s" % x,"datatype":"string"} for x in range(1,len(result)+1-3)] )

        outbound["data"] = [ {"id":1, "values":{}} ]

        outbound["data"].extend( [{"id":2, "values":[x for x in result]}] )

        return HttpResponse(json.dumps(outbound), mimetype="application/json")

    modded_json = json.dumps({"metadata":[{"name":"start_date","label":"Start Date","datatype":"date","editable":True},{"name":"cycle","label":"Cycle","datatype":"integer","editable":True},{"name":"years","label":"Years","datatype":"integer","editable":True}],"data":[{"id":"1","values":{}},{"id":"2","values":[start_date.strftime("%d %b %Y"),days_in_cycle,number_of_years]}]})

    return render_to_response('projections/date_projections.html', {
        'just_csrf_token': csrf.get_token(request),
        'initial_data': r"%s" % modded_json,
        }, context_instance=RequestContext(request))


@login_required
def ptest(request):
    return HttpResponse("here...")

if __name__ == "__main__":
    print((date_projection("121001",28,3)))
