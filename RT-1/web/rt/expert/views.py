from PIL import Image as PImage
from os.path import join as pjoin
from settings import MEDIA_ROOT

from django.http import HttpResponse, HttpResponseNotFound
from django.template import RequestContext, Context, loader
TEMPLATE_CONTEXT_PROCESSORS = ( 
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
)
from django.http import Http404
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import ensure_csrf_cookie
from django.db import IntegrityError, DatabaseError
from django.forms import ModelForm
from django.forms import CharField, MultipleChoiceField, ChoiceField
from django.forms.widgets import Widget
from django.forms.widgets import ClearableFileInput
from django.forms.widgets import HiddenInput

from home.models import RevenueTradesMenus, RevenueTradesPerson, RevenueTradesBusiness


from widgets import SpanEditableTextWidget
from widgets import SpanViewTextWidget
from widgets import IndustriesWidget
from widgets import IndustriesFormWidget
from widgets import FormattedSelectionWidget
from widgets import FormattedListWidget

from expert.models import Expert, Industry

class ExpertCapacityEditForm(ModelForm):
    max_clients = ChoiceField(
        widget=FormattedSelectionWidget(
            label="What's the maximum number of business clients you would want to work with at the same time?", 
            choices = RevenueTradesMenus.menu_list("ExpertClientCount"), 
        ), label='',)
    preferred_duration = ChoiceField(
        widget=FormattedSelectionWidget(
            label="What's the extent of time you would prefer to help business clients plan milestones to coordinate their fundraising efforts with growth?", 
            choices = RevenueTradesMenus.menu_list("ExpertDuration"), 
        ), label='',)
    availability = ChoiceField(
        widget=FormattedSelectionWidget(
            label="Per business, how many hours a week would you be able to contribute?", 
            choices = RevenueTradesMenus.menu_list("ExpertAvailability"), 
        ), label='', )

    class Meta:
        model = Expert
        fields = ('preferred_duration', 'max_clients', 'availability')

class IndustriesEditForm(ModelForm):
    industries_of_interest = MultipleChoiceField(widget=IndustriesFormWidget(label='Industry Interests'), 
                                                choices = RevenueTradesMenus.menu_list("RevenueTradesIndustries"),
                                                label='', 
                                                )

    class Meta:
        model = Expert
        fields = ('industries_of_interest', )
        
class ContactViewForm(ModelForm):
    first_name = CharField(widget=SpanViewTextWidget(attrs={'size':'40'}), label='',  )
    last_name = CharField(widget=SpanViewTextWidget(attrs={'size':'40'}), label='',  )
    street_address = CharField(widget=SpanViewTextWidget(attrs={'size':'40'}), label='',  )
    city_state_zip = CharField(widget=SpanViewTextWidget(attrs={'size':'40'}), label='',  )
    email = CharField(widget=SpanViewTextWidget(attrs={'size':'40'},), label='',)
    phone = CharField(widget=SpanViewTextWidget(attrs={'size':'40'}), label='',  )
    
    class Meta:
        model = RevenueTradesPerson 
        fields = {'first_name', 'last_name', 'street_address', 'city_state_zip', 'email', 'phone', }

class ContactEditForm(ModelForm):
    first_name = CharField(widget=SpanEditableTextWidget(attrs={'size':'40', }, ), label='', )
    last_name = CharField(widget=SpanEditableTextWidget(attrs={'size':'40', }, ), label='', )
    street_address = CharField(widget=SpanEditableTextWidget(attrs={'size':'40', }, ), label='', )
    city_state_zip = CharField(widget=SpanEditableTextWidget(attrs={'size':'40', }, ), label='', )
    email = CharField(widget=SpanEditableTextWidget(attrs={'size':'40', }, ), label='', initial='Email address', )
    phone = CharField(widget=SpanEditableTextWidget(attrs={'size':'40', },  ),label='', )
    
    class Meta:
        model = RevenueTradesPerson 
        fields = {'first_name', 'last_name', 'street_address', 'city_state_zip', 'email', 'phone', }

# @login_required
# def view( request, *args, **kwargs ):
# 	request.user.profile
# 	if not request.user.profile.focuses.all():
# 		focus = request.user.profile.focuses.get_or_create(user=request.user, industry=Industry.objects.all()[0])[0]
# 	else:
# 		focus = request.user.profile.focuses.all()[0]
# 	kwargs.setdefault('extra_context',{}).update({
# 		'form':  FocusForm( instance = focus ),
# 		'focus': focus, 
# 		'Focus': Focus,
# 	  	'Industry': Industry.objects.all() })
# 	return profile_detail( request, username = request.user, *args, **kwargs )

# from expert_profiles.models import ExpertProfile, ExpertForm, FocusForm, Focus
# @login_required
# def post( request, *args, **kwargs ):
# 	request.user.profile
# 	if u'focus' in request.POST and request.method == 'POST':
# 		data = request.POST.copy()
# 		instance = Focus.objects.filter(id=int(data.pop(u'focus')[0]))[0]
# 		for field in set(FocusForm().fields) - {'user','id'}:
# 			form = FocusForm(fields=[field], data=data, files=request.FILES, instance=instance)
# 			if form.is_valid():
# 				form.save()
# 		return HttpResponseRedirect('')
# 	return edit_profile( request, *args,
# 	  form_class = lambda *a, **kw: ExpertForm(*a,fields=request.POST,**kw),
# 	  success_url = '',
# 	  template_name = 'profiles/edit_profile.html', 
# 	  **kwargs )

def get_expert_and_user(request,  expert_id=0):
    rtuser = None
    if request.user.is_authenticated():
        rtuser = RevenueTradesPerson.objects.filter(user=request.user)
        if len(rtuser) == 1:
            rtuser = rtuser[0]
        else: 
            return (None, None)
    if expert_id == 0:
        if rtuser != None:
            expert = Expert.objects.get(expert=rtuser)
        else:
            return (None, None)
    else:
        expert = Expert.objects.get(id=expert_id)
    return (expert, rtuser)

def expert_page(request, template, expert_id=0):
    editable = False
    expert, rtuser = get_expert_and_user(request,  expert_id)
    if expert == None and rtuser == None:
        return HttpResponseNotFound('<h1>invalid user count</h1>' + len(rtuser) + " " + rtuser)
    elif expert == None:
        return HttpResponseNotFound('<h1>No such expert or maybe this should be a list of expert</h1>')
    if rtuser != None and rtuser == expert.expert:
        editable = True
        
    expert = Expert.objects.get(id=expert_id)
    context = RequestContext(request, {
        'editable': editable,
        'expert' : expert,
        'rtuser' : request.user,
    })
    return HttpResponse(template.render(context))

@login_required
def index(request):
    experts = Expert.objects.all()
    template = loader.get_template('expert/list.html')
    context = Context({
        'search_results': experts,
        
    })
    return HttpResponse(template.render(context))
    

def clients(request, expert_id=0):
    template = loader.get_template('expert/clients.html')
    return expert_page(request, template, expert_id)
    
def services(request, expert_id=0):
    editable = False
    expert, rtuser = get_expert_and_user(request,  expert_id)
    if expert == None and rtuser == None:
        return HttpResponseNotFound('<h1>invalid user count</h1>' + len(rtuser) + " " + rtuser)
    elif expert == None:
        return HttpResponseNotFound('<h1>No such expert or maybe this should be a list of expert</h1>')
    if rtuser != None and rtuser == expert.expert:
        editable = True
        
    template = loader.get_template('expert/services.html')
    expert = Expert.objects.get(id=expert_id)
    context = RequestContext(request, {
        'expert' : Expert,
        'rtuser' : request.user,
    })
    return HttpResponse(template.render(context))
    
def prospects(request, expert_id=0):
    editable = False
    expert, rtuser = get_expert_and_user(request,  expert_id)
    if expert == None and rtuser == None:
        return HttpResponseNotFound('<h1>invalid user count</h1>' + len(rtuser) + " " + rtuser)
    elif expert == None:
        return HttpResponseNotFound('<h1>No such expert or maybe this should be a list of expert</h1>')
    if rtuser != None and rtuser == expert.expert:
        editable = True
        
    template = loader.get_template('expert/prospects.html')
    expert = Expert.objects.get(id=expert_id)
    context = RequestContext(request, {
        'expert' : Expert,
        'rtuser' : request.user,
    })
    return HttpResponse(template.render(context))
    
@login_required
def user_profile(request):
    contact = None
    business = None
    try:
        contact = RevenueTradesPerson.objects.get(user=request.user)
    except MultipleObjectsReturned:
        contact = RevenueTradesPerson.objects.filter(user=request.user)[0]
    try: 
        business_info = RevenueTradesBusiness.objects.get(contact=contact)
    except MultipleObjectsReturned:
        business_info = RevenueTradesBusiness.objects.filter(contact=contact)[0]
    try:
        expert = Expert.objects.get(business=business_info)
    except MultipleObjectsReturned:
        expert = Expert.objects.filter(business=business_info)[0]
    return profile(request, expert.id)

def profile(request, expert_id=0):
    editable = False
    expert, rtuser = get_expert_and_user(request,  expert_id)
    if expert == None and rtuser == None:
        return HttpResponseNotFound('<h1>invalid user count</h1>' + len(rtuser) + " " + rtuser)
    elif expert == None:
        return HttpResponseNotFound('<h1>No such expert or maybe this should be a list of expert</h1>')
    if rtuser != None and rtuser == expert.expert:
        editable = True
        
    if (expert):
        if (request.method == 'POST'):
    
    
            rtuser.first_name = request.POST.get('first_name', rtuser.first_name)
            rtuser.last_name = request.POST.get('last_name', rtuser.last_name)
            rtuser.street_address = request.POST.get('street_address', rtuser.street_address)
            rtuser.city_state_zip = request.POST.get('city_state_zip', rtuser.city_state_zip)
            rtuser.email = request.POST.get('email', rtuser.email)
            rtuser.phone = request.POST.get('phone', rtuser.phone)
            #expert.url = request.POST.get('RT_URL', expert.url)
            business_name = request.POST.get('full_name')
            if (business_name is not None):
                expert.business = RevenueTradesBusiness(full_name=business_name, contact=rtuser)
                
                expert.business.city_state_zip = request.POST.get('location', expert.business.city_state_zip)
                expert.business.save()
            expert.visibility = request.POST.get('visibility', expert.visibility)
            expert.TOUPS_agreement = request.POST.get('TOUPS_agreement', expert.TOUPS_agreement)
            expert.stage_of_interest = request.POST.get('stage_of_interest', expert.stage_of_interest)
            try:
                industries = request.POST.getlist('operating_industries')
                #return HttpResponseNotFound('<h1>industries</h1> '  + repr(industries))
                expert.save()
                for industry in industries:
                    expert.industries_of_interest.add(Industry(industry=industry))
            except  KeyError:
                pass
    
            rtuser.save()
            expert.save()
            if not request.POST.get('initial'):
                return HttpResponse()
        if editable:
            expert_form = ExpertCapacityEditForm(instance = expert)
        else:
            expert_form = loader.get_template('expert/time_commitment.html').render(RequestContext(request, {'expert' : expert,}))
        industries_form = IndustriesEditForm(instance = expert)
        #expert_info = expertViewForm(instance = expert,)
        contact_form = ContactEditForm(instance = expert.expert,)
        contact_info = ContactViewForm(instance = expert.expert, 
                    initial={
                            'street_address': 'Street address', 
                            'city_state_zip': 'City/state/zip', 
                            'email': 'email address', 
                            'phone': 'phone number', },)
        context = RequestContext(request, {
            'rtuser': rtuser,
            'expert' : expert,
            'expert_form' : expert_form,
            'industries_form' : industries_form,
            'contact' : expert.expert,
            'contact_form' : contact_form,
            'editable' : editable,
            
        })
        template = loader.get_template('expert/profile.html')
        return HttpResponse(template.render(context))
    else:
        return HttpResponseNotFound('<h1>No expert was created for</h1> '  + repr(rtuser))

    template = loader.get_template('expert/profile.html')
    expert = Expert.objects.get(id=expert_id)
    context = RequestContext(request, {
        'expert' : Expert,
        'rtuser' : request.user,
    })
    return HttpResponse(template.render(context))
    
    
def dashboard(request, expert_id=0):
    editable = False
    if expert_id == 0:
        return HttpResponseNotFound('<h1>No such expert or maybe this should be a list of expert</h1>')
    else:
        expert = Expert.objects.get(id=expert_id)
        
    if request.user.is_authenticated():
        rtuser = RevenueTradesPerson.objects.filter(user=request.user)
        if len(rtuser) == 1:
            rtuser = rtuser[0]
        else: 
            return HttpResponseNotFound('<h1>invalid user count %d</h1>' % len(rtuser) + " " + repr(rtuser))
        if rtuser == expert.expert:
            editable = True
        else:
            return HttpResponseNotFound('<h1>You are not allowed to view that page</h1>')
        context = RequestContext(request, {
            'expert' : expert,
            'rtuser': rtuser,
        })
        template = loader.get_template('expert/expert-investments.html')
        return HttpResponse(template.render(context))
    return HttpResponseNotFound('<h1>You are not allowed to view that page</h1>')

def focus(request, expert_id=0):
    editable = False
    expert, rtuser = get_expert_and_user(request,  expert_id)
    if expert == None and rtuser == None:
        return HttpResponseNotFound('<h1>invalid user count</h1>' + len(rtuser) + " " + rtuser)
    elif expert == None:
        return HttpResponseNotFound('<h1>No such expert or maybe this should be a list of expert</h1>')
    if rtuser != None and rtuser == expert.expert:
        editable = True
        
    template = loader.get_template('expert/focus.html')
    expert = Expert.objects.get(id=expert_id)
    context = RequestContext(request, {
        'expert' : Expert,
        'rtuser' : request.user,
    })
    return HttpResponse(template.render(context))
        


