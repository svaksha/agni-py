from django.conf.urls import patterns, url
from expert import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
	#url(r'^expert/',  views.profile, name='profile'),
	url(r'^(\d+)/public_profile/$',  views.profile, name='profile'),
	url(r'^show_profile/$',  views.profile, name='profile'),
	url(r'^(\d+)/profile/$',  views.profile, name='profile'),
	url(r'^(\d+)/profile/details/$',  views.profile, name='profile'),
	url(r'^(\d+)/profile/clients/$',  views.clients, name='clients'),
	url(r'^(\d+)/profile/focus/$',    views.focus, name='focus'),
	url(r'^(\d+)/profile/services/$', views.services, name='services'),
	url(r'^(\d+)/profile/prospects/$', views.prospects, name='prospects'),
)