from django.contrib import admin
from .models import *

class ExpertAdmin(admin.ModelAdmin):
    pass

class JobAdmin(admin.ModelAdmin):
    pass

class CredentialAdmin(admin.ModelAdmin):
    pass

class TipAdmin(admin.ModelAdmin):
    pass

class IndustryAdmin(admin.ModelAdmin):
    pass


admin.site.register(Expert, ExpertAdmin)
admin.site.register(Job, JobAdmin)
admin.site.register(Credential, CredentialAdmin)
admin.site.register(Tip, TipAdmin)
admin.site.register(Industry, IndustryAdmin)




    

