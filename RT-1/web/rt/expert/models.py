from django.db import models
from django.db.models import BooleanField
from django.db.models import CharField
from django.db.models import DateTimeField
from django.db.models import DecimalField
from django.db.models import EmailField
from django.db.models import ForeignKey 
from django.db.models import IntegerField
from django.db.models import ManyToManyField
from django.db.models import OneToOneField
from django.db.models import URLField

from django.contrib.auth.models import User

from django.forms import ModelForm

#from business.models import Business
#from business.models import Terms
from home.models import RevenueTradesMenus
# import business.models
# import home.models

class Job(models.Model):
    start_date = DateTimeField()
    end_date = DateTimeField()
    company = CharField(max_length=200,null=True)
    description = CharField(max_length=200,null=True)
    
class Credential(models.Model):
    start_date = DateTimeField()
    end_date = DateTimeField()
    credential = CharField(max_length=200,null=True)
    description = CharField(max_length=200,null=True)

class Tip(models.Model):
    text = CharField(max_length=200,null=True)
    author = ForeignKey('home.RevenueTradesPerson')
    
class Industry(models.Model):
    industry = IntegerField(max_length=1,null=True, choices=RevenueTradesMenus.menu_list("RevenueTradesIndustries"))
    
CLIENT_COUNT_RANGES = tuple(enumerate((
    '1', 
    '1-4', 
    '3-5', 
    '5-10', )))
DURATION_RANGES = tuple(enumerate((
    '1 - 6 months', 
    '6 - 12 months', 
    '9 - 18 months', 
    '12 months or more', )))
AVAILABILITY_RANGES = tuple(enumerate((
    '1 - 2 hours', 
    '2 - 4 hours', 
    '4 - 8 hours', 
    '1 - 2 days', )))
 
    
# Create your models here.
class Expert(models.Model):
    expert = ForeignKey('home.RevenueTradesPerson') 
    business = ForeignKey('home.RevenueTradesBusiness', related_name="expert_business_set", null=True)
    preferred_duration = IntegerField(choices = RevenueTradesMenus.menu_list("ExpertDuration"), null=True)
    max_clients = IntegerField(choices = RevenueTradesMenus.menu_list("ExpertClientCount"), null=True)
    availability = IntegerField(choices = RevenueTradesMenus.menu_list("ExpertAvailability"), null=True)
    linked_in = URLField(null=True)
    job_history = ManyToManyField(Job)
    credentials = ManyToManyField(Credential)
    clients = ManyToManyField('home.RevenueTradesBusiness', related_name="expert_clients_set")
    tips = ManyToManyField(Tip)
    visibility = BooleanField()
    TOUPS_agreement = BooleanField()
    stage_of_interest = IntegerField(max_length=1,null=True, choices=RevenueTradesMenus.menu_list("RevenueTradesDevelopmentStages"))
    industries_of_interest = ManyToManyField(Industry)
    @classmethod
    def create(cls,expert_info):
        xpert = cls(expert = expert_info,
            )
        print(("expert " + repr(expert_info.first_name)))
        return xpert

    def __unicode__(self):
        return str("expert " + self.expert.first_name + " " + self.expert.last_name) # wtf .get_username()
    
