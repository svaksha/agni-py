DEBUG=True
SERVE_STATIC_LOCALLY = True

SPOOF_TO_ADDRESS = 'strangest@comcast.net'
EMAIL_PORT = 1025

DOMAIN = "http://127.0.0.1:8000"
STATIC_DOMAIN = "http://127.0.0.1:8000"

LOCAL_DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'rt_test',
        'USER': 'test',
        'PASSWORD': 'test',
        'HOST': '127.0.0.1',
        #'PORT': '12312',
    }
}

if DEBUG:
	EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'
