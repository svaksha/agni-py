DEBUG=True
SERVE_STATIC_LOCALLY = True

SPOOF_TO_ADDRESS = 'strangest@comcast.net'
HELLO_ADDR = "TEST not PROD <hello@revenuetrades.com>"

EMAIL_PORT = 1025

DOMAIN = "http://173.255.238.117"
STATIC_DOMAIN = DOMAIN

LOCAL_DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'rt_test',
        'USER': 'test',
        'PASSWORD': 'test',
        'HOST': '127.0.0.1',
        #'PORT': '12312',
    }
}

if DEBUG:
	EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'
