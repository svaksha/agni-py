DEBUG=True
SERVE_STATIC_LOCALLY = False

SPOOF_TO_ADDRESS = 'strangest@comcast.net'

#HTTPS settings
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True

#EMAIL_PORT = 1025
EMAIL_PORT = 25
EMAIL_HOST_USER = None
EMAIL_HOST_PASSWORD = None
EMAIL_USE_TLS = False

DOMAIN = "http://alpha.revenuetrades.com"
STATIC_DOMAIN = DOMAIN

LOCAL_DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'rt',
        'USER': 'rt',
        'PASSWORD': '7yh8uj9ik0ol',
        'HOST': '',
        'PORT': '',
    }
}

ALLOWED_HOSTS = ['localhost', '127.0.0.1','alpha.revenuetrades.com'] 

