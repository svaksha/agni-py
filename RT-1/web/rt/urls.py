from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic.base import TemplateView

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
#
admin.autodiscover()
try:
    delete_url = settings.MULTI_FILE_DELETE_URL
except AttributeError:
    delete_url = 'multi_delete'

try:
    image_url = settings.MULTI_IMAGE_URL
except AttributeError:
    image_url = 'multi_image'

# from django.views.generic.simple import redirect_to

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'rt.views.home', name='home'),
    # url(r'^rt/', include('rt.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # Uncomment the next line to enable the admin:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),

	# url(r'^$', redirect_to, {'url': '/login/'}),
	# url(r'^upload/', include('fileupload.urls')),

    url(r'^issues$', TemplateView.as_view(template_name='issues.html')),
	url(r'^$', include('home.urls', namespace="home")),
    url(r'^', include('registration.backends.default.urls')),
    url(r'^home/', include('home.urls', namespace="home")),
    url(r'^business/', include('business.urls', namespace="business")),
    url(r'^funder/', include('funder.urls', namespace="funder")),
    url(r'^expert/', include('expert.urls', namespace="expert")),
    url(r'^milestones/', include('milestones.urls', namespace="milestones")),
    url(r'^profile/', 'home.views.profile'),
    url(r'^smash/', 'home.views.smash'),
    url(r'^search$', 'home.views.search'),
    url(r'^discover$', 'home.views.search'),
	#url(r'^post/',     'expert_profiles.views.post'),
	#url(r'^date_projection/?', 'projections.views.date_projection'),
    url(r'^linkedin/?', include('linkedin_profile.urls')),
    url(r'^linkedin_cancel/', 'linkedin_profile.views.linkedin_cancel'),
	#url(r'^ptest/', 'projections.views.ptest'),
    #url(r'^expert/', include('profiles.urls')),
    #url(r'^expert/manual_edit/', 'profiles.views.edit_profile', {'form_class': ExpertForm}),
)
urlpatterns += staticfiles_urlpatterns()
import os
urlpatterns += patterns('',
        (r'^media/(.*)$', 'django.views.static.serve', {'document_root': os.path.join(os.path.abspath(os.path.dirname(__file__)), 'media')}),
)

if settings.SERVE_STATIC_LOCALLY:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)/?$',
         'django.views.static.serve',
         {'document_root': settings.MEDIA_ROOT, 'show_indexes': True, }),
        (r'^static/(?P<path>.*)/?$',
         'django.views.static.serve',
         {'document_root': settings.STATIC_ROOT, 'show_indexes': True, }),
)

