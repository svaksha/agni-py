#!/usr/bin/env python3.3
# encoding: iso-8859-15
###############################################################################
"""
Created on Thu Jan 17 15:25:23 2013
amortizE.py package module has methods to compute annuity interest Amortization
and other (8)options (ex. "After interest only Balloon", etc..) for Amortization.
"""
#-----------------------------------------------------------------------------
# Imports ; Python3.x
#-----------------------------------------------------------------------------
from __future__ import absolute_import, division, print_function, unicode_literals
import pandas as pd
from pandas import Series, DataFrame as df, concat
import numpy as np
import datetime, time
import os, sys, os.path
ts = time.time()
st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
from pprint import pprint as pp   #print JSON
import json
print('processing date', st)
# local imports
import utils.config
import utils.amort_param_json as apj
#from  utils.amort_param_json import  ParseParamFile as Paramortized_data
import utils.parser_dfcsvjson as csvjson
import utils.parser_json_mongodb as jsonmongo
import amortlib
#==============================================================================
#-----------------------------------------------------------------------------
# Code
#-----------------------------------------------------------------------------

class GetParams():
    '''
    get borrowers details and loan details
    '''
    def __init__(self):
        pass

    def process_paramfile(self):
        '''
        execute 'amort_param_json.py' to get parameter, setup output files,
        create time series for loan repayment schedule
        '''
        for arg in sys.argv:
            param_filename = arg # get param file name from command line
            print (arg)

        apjparaminf = apj.ParseParamFile(param_filename)
        apjparaminf.process_date()  # apjparaminf is an instance of the class
        apjparaminf.build_parampaths()
        utils.config.param_dic = apjparaminf.read_paramfile() # creates utils.config.param_dic

        print (str(utils.config.param_dic.items()))
        print (utils.config.param_dic.items(), utils.config.param_dic.keys())
        print (utils.config.param_dic['Borrower_Id'], utils.config.param_dic['principal'])
        apjparaminf.build_out_filepaths()     #txt_out_file, csv_out_file, json_out_file, err_out_file
        apjparaminf.compute_instalmentdates()  # computes loan instalment dates
        print (utils.config.range)
        self.dt0 = utils.config.range[0].strftime('%Y-%m-%d')   #strips time element
        self.dt5 = utils.config.range[5].strftime('%Y-%m-%d')   #strips time element
        print (self.dt0, self.dt5)


    def print_params(self):
        '''
        print value of parameters read from file
        '''
        print('Borrower_Id-- ', utils.config.param_dict['Borrower_Id'])
        print('Principal',' ',' annualIntRate',' ', 'loanDuration','  ','payment_frequency_code')
        print(utils.config.param_dict['principal'],' ', utils.config.param_dict['annualIntRate'],\
        ' ', utils.config.param_dict['numOfPayments'],\
            ' ',utils.config.param_dict['payment_frequency'])
        print('Int_only_period', '  ',' Amort only_period', '  ''loan_starting_date', '  ','Proc_option')
        print(utils.config.param_dict['int_only_period'], '  ', utils.config.param_dict['amort_only_period'],\
          utils.config.param_dict['loan_starting_date'], '  ', utils.config.param_dict['proc_option'])


class ProcessLoanOptions():
    '''
    process loans as per eight options and other parameters
    '''
    def __init__(self):
        self.num_of_ppy = 0.0
        self.periodic_interest_rate = 0.00000
        self.option = ''
        self.present_value = 0.0
        self.periodicPayment = 0.0
        self.total_interest_pd = 0.0
        self.counter = 0.0
        self.total_interest_pd = 0.0
        self.periodicPayment = 0.0
        self.total_interest_p2 = 0.0
        self.counter_pmt = 0
        self.balloon_payment = 0.0

    def loan_init_calc(self):
            """
            Method to calculate the  Common Calculations for the loan re-payment -
            is called from the __init__().
            ARGS
            ====
            num_of_ppy = payment_frequency_code (alpha) converted to number of PaymentPerYear (integer)
            period or periodic_interest_rate: periodic interest rate (i.e. annualIntRate / period)
            period*(self.num_years+1): total number apj compounding periods
            present_value: the loan amount
            RETURNS
            =======
            self.periodicPpandasayment: regular_payment amount main each payment (float)
            """
            self.num_of_ppy = amortlib.number_of_payments_peryear(utils.config.param_dict['payment_frequency'])
            self.periodic_interest_rate = amortlib.periodic_interest_rate(utils.config.param_dict['annualIntRate'],\
             self.num_of_ppy)
            print('num_of_ppy  - period per year- ', self.num_of_ppy, 'Annual Interest Rate -',  \
              utils.config.param_dict['annualIntRate'], 'periodic_interest_rate-',\
              self.periodic_interest_rate, '\n')
            return self.num_of_ppy, self.periodic_interest_rate


    #==============================================================================
    # Option-1, only Amortize
    #==============================================================================
    def loan_opt1_p1(self, periodic_interest_rate):
        """
         Function to compute compute annuity details  which means the loan is repaid
         completely in equal instalments and each periodic instalment includes interest
         and part of principal.
        """
        self.option = utils.config.param_dict['proc_option'] = '1'
        print('\n',  'Option-1, only Amortize', ' -# apj Annuity period', \
        utils.config.param_dict['numOfPayments'])
        self.present_value = utils.config.param_dict['principal']
        self.periodicPayment = amortlib.payment(utils.config.param_dict['principal'],\
         self.periodic_interest_rate, utils.config.param_dict['numOfPayments'])
        print('Amortization', 'PeriodicPayment-', "%.2f" % self.periodicPayment, '\n')
        self.total_interest_pd, self.df1 = amortlib.amortization( self.option, self.present_value,\
                           self.periodicPayment, self.periodic_interest_rate, \
                        utils.config.param_dict['numOfPayments'])
        return self.df1


    #==============================================================================
    # Option-2 only Interest
    #==============================================================================
    def loan_opt2_p1(self, periodic_interest_rate):
        """
        Function  to compute only Interest details  for the period of loan
        and the final payment to include interest and principal borrowed.
        """
        self.option = utils.config.param_dict['proc_option'] = '2'
        print('\n', '-# Option-2 -- Interest only period', utils.config.param_dict['Borrower_Id'],\
                  utils.config.param_dict['principal'] , self.periodic_interest_rate,\
                     utils.config.param_dict['numOfPayments'])
        self.counter, self.total_interest_pd, self.df2 = amortlib.compute_interest_only( self.option, \
            utils.config.param_dict['principal'], self.periodic_interest_rate, \
            utils.config.param_dict['numOfPayments'])
        return self.df2


    #==============================================================================
    # Option-3 <=> after interest only readjust
    #==============================================================================
    def loan_opt3(self, periodic_interest_rate):
        """
        This option provides for the payment of only interest for a definite initial period
        and the repayment of loan and interest in equated instalment during the remaining
        period of 'loan duration' period interest.
        Loan function to print interest only details.
        """
        self.option = utils.config.param_dict['proc_option'] = '3'
        utils.config.readjust = False   # set readjust to false
        print( '\n',  'Option-3, After interest only Readjust')
        self.counter, self.total_interest_p1, self.df31 = amortlib.compute_interest_only( self.option,  \
                        utils.config.param_dict['principal'], self.periodic_interest_rate, \
                        utils.config.param_dict['int_only_period'])

        """
        Loan function for creating and amortizing loans.
        """
        self.periodicPayment = amortlib.payment(utils.config.param_dict['principal'], \
            self.periodic_interest_rate, utils.config.param_dict['amort_only_period']) #
        print('Interest only period plus Amortization period -->', \
           utils.config.param_dict['int_only_period'],'+',\
           utils.config.param_dict['amort_only_period'], 'Amortization---', \
           'Annuity', "%.2f" % self.periodicPayment, '\n')
        utils.config.temp_store = self.total_interest_p1  #only used for printing
        utils.config.set_amort = True
        self.total_interest_p2, self.df32 = amortlib.amortization( self.option, utils.config.param_dict['principal'],\
                       self.periodicPayment, self.periodic_interest_rate, utils.config.param_dict['numOfPayments'])
        print(utils.config.param_dict['Borrower_Id'], 'interest from intrst only pd', \
          self.total_interest_p1, 'interest from amortisation period', self.total_interest_p2 , \
          'total interest paid',"%.2f" % (self.total_interest_p1 + self.total_interest_p2))
        # pdcj merge interest only period and amortisation period
        return self.df31, self.df32


    ##=============================================================================
    # Option-4 after interest only Balloon
    ##=============================================================================
    def loan_opt4_balloon(self, periodic_interest_rate):
        """
        Function for "After interest only Balloon" which means the loan is repaid
        imediately in lumpsum with interest after thedata element from expiry of period of loan.
        """
        self.option = utils.config.param_dict['proc_option'] = '4'
        print('\n','Option-4 After interest only Balloon')
        utils.config.balloon = True
        self.counter_pmt, self.total_interest_pd, self.df4 = amortlib.compute_interest_only(self.option, \
                        utils.config.param_dict['principal'], self.periodic_interest_rate,\
                         utils.config.param_dict['numOfPayments']) #
        utils.config.balloon = False
        return self.df4


    ##=============================================================================
    # Option-5 after interest only Balloon FINAL  /Requirement not clear -not tested/
    ##=============================================================================
    def loan_opt5_balloon_fnl(self, periodic_interest_rate, mssd_pmt_nointerest):
        """
        Function for "after interest only Balloon FINAL" which means the loan is repaid
        partly paying only interest and later through amortisation. This option allows
        payment default upto three periods before revising up interest rates
        in three grace periods imediately in lumpsum with interest.
        """
        self.option = utils.config.param_dict['proc_option'] = '5'
        print('\n', '141', 'Option-4 after interest only Balloon')
        self.counter_pmt, self.total_interest_pd = amortlib.compute_interest_only(utils.config.param_dict['principal'], \
                      self.periodic_interest_rate, utils.config.param_dict['numOfPayments'])
        self.baloon_interest = amortlib.iPart(utils.config.param_dict['principal'], self.periodic_interest_rate)
        self.counter_pmt += 1
        self.balloon_payment = utils.config.param_dict['principal'] + self.baloon_interest
        print(utils.config.param_dict['Borrower_Id'], self.counter_pmt,'  ', self.interest,'  ',\
          utils.config.param_dict['principal'], "%.2f" % self.balloon_payment, \
         '<-- Balloon Payment')
        print( 'total interest paid', "%.2f" % (self.total_interest_pd + self.baloon_interest))



class ProcessCsvJsonMongoOut():
    '''
    combine output of all options before creating outputs
    '''

    def __init__(self, df):
        ''' defining init lables for dataframes
        '''
        import utils.parser_dfcsvjson as csvjson
        self.amortized_data = ' '
        self.jsonoutFile = ' '

    def merge_dfs(self):
        ''' merge DF outputs of all options for the same borrower
        '''
        pass

    def convert_dfs_to_csvJson(self, df):
        ''' convert dataframe output into csv format and then create Json file
        '''
        parser_dftocsvjson = csvjson.ParseDFtoCsvJson()
        parser_dftocsvjson.df_to_csv(df)
        self.jsonoutFile = parser_dftocsvjson.csv_to_json()

    def insert_json_mongodb(self):
        ''' Call parse function to convert 'JSON' and insert document into mongoDB
        '''
        from utils.parser_json_mongodb import main as pjm_main
        pjm_main(self.jsonoutFile)    # Transfer control to parser_json_mongo.py
        sys.stdout.close
        sys.stderr.close


#==============================================================================
# MAIN(), method for PV, Periodic Interest rate and number apj periods
#==============================================================================

def main():
    '''
    get parameters, setup output files, loan repayment schedule
    '''
    full_path = os.path.realpath(__file__)
    print (full_path)
    getparam = GetParams()
    getparam.process_paramfile() #read file to get params
    getparam.print_params()    # print params

    '''
    Process the interest, repayment statement for each option 1 to 4 for input
    # read from parameter file, output options returned in dataframe
    '''

    processloan = ProcessLoanOptions()
    '''
    create borrowers loan repayment schedule with details for option 1 to 4
    '''
    period, periodic_interest_rate = processloan.loan_init_calc()

    df1 = processloan.loan_opt1_p1(periodic_interest_rate )

    df2 = processloan.loan_opt2_p1(periodic_interest_rate)

    df31,df32 = processloan.loan_opt3(periodic_interest_rate)

    df4 = processloan.loan_opt4_balloon(periodic_interest_rate)
    '''
    concatenate output of all loan options, containing repayment details with schedules
    '''
    dfall = [df1, df2, df31, df32, df4]
    df = pd.concat(dfall)

    create_out = ProcessCsvJsonMongoOut(df)
    create_out.merge_dfs()
    create_out.convert_dfs_to_csvJson(df)
    create_out.insert_json_mongodb()

if __name__ == '__main__':
    main()
