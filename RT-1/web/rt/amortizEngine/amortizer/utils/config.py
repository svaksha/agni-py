#!/usr/bin/env python3.3
# -*- coding: iso-8859-15 -*-
import sys,os
import datetime

param_file = ''
param_fullfile = ' ' 
param_dict = {}
outfile = 'outLoanPmt'
fn_suffix = ''
out_dirpath = ''
fnoutpath = ''
suffix_txt = '.txt'
suffix_csv = '.csv'
suffix_json = '.json'
prefix_error = 'Error'
txt_out_filename = ''
csv_out_filename = ''
json_out_filename = '' 
err_out_filename = ''       
# define lists to store payment details 
outer_list = []
inner_list = []
range = []
# Define and Set switches
readjust = False
balloon = False
set_amort = False
temp_store = 0.0