#!/usr/bin/env python3.3
# encoding: iso-8859-15
###########################################################################
"""
Created on Fri Mar 29 12:23:30 2013
This package module will collect data processing parameter input data from
various sources (ex., the revenuetrades.com site) into JSON.
The data values are read from param-data file in JSON format.
"""
#-----------------------------------------------------------------------------
# Imports, Python3.x
#-----------------------------------------------------------------------------
from __future__ import absolute_import, division, print_function, unicode_literals
import pandas as pd
from pandas import Series, DataFrame, date_range
import numpy as np
import datetime, time
from pprint import pprint as pp   #print JSON
import json
import sys, os.path
import utils.config
#========================================================================
# CODE
#-----------------------------------------------------------------------------

class ParseParamFile():
    '''
    read and process parameter data from file
    '''
    def __init__(self, param_filename):
        import utils.config
        import utils.pathmap
        self.param_file = param_filename

    def process_date(self):
        self.ts = time.time()
        self.st = datetime.datetime.fromtimestamp(self.ts).strftime('%Y-%m-%d %H:%M:%S')
        print('processing date', self.st)


    def build_parampaths(self):
        '''
        build path for parameter file
        '''
        self.dir_amortizer, self.dir_dataMatrix, self.dir_utils = utils.pathmap.finDirPath()
        utils.config.param_fullfile = (os.path.join(self.dir_dataMatrix , self.param_file))
        print ("Constructed path to access files")

    def read_paramfile(self):
        '''
        List of data values read from param-data file in JSON format
        '''
        print ('Reading the borrowers data from ''loanparam n.json'' file')
        with open(utils.config.param_fullfile) as data_file:
            self.data = json.load(data_file)   #, object_hook=_decode_dict
        pp(self.data)
        data_file.close()
        self.DATETIME_FORMAT = '%Y-%m-%d'
        utils.config.param_dict['Borrower_Id'] = (self.data["Loan"]["Borrower_Id"])
        utils.config.param_dict['principal'] = (self.data["Loan"]["principal"])
        utils.config.param_dict['annualIntRate'] = (self.data["Loan"]["annualIntRate"])    # will return 'value'
        utils.config.param_dict['numOfPayments'] = self.payment_period = (self.data["Loan"]["numOfPayments"])   #returns 'value'
        utils.config.param_dict['payment_frequency'] = self.payment_frequency_code = (self.data["Loan"]["paymentFrequency"])
        utils.config.param_dict['int_only_period'] =  (self.data["Loan"]["interestOnlyPeriod"])
        utils.config.param_dict['amort_only_period'] = self.payment_period - utils.config.param_dict['int_only_period']
        utils.config.parse_loanStartDate = (self.data["Loan"]["loanStartDate"])
        utils.config.param_dict['loan_starting_date'] = datetime.datetime.strptime(utils.config.parse_loanStartDate, self.DATETIME_FORMAT)
        utils.config.param_dict['proc_option'] = (self.data["Loan"]["proc_option"])  # choice of processing option number
        return utils.config.param_dict


    def joinFilePath(self):
        '''
        Construct file path from inputs
        '''
        self.out_fn = (utils.config.outfile + str(utils.config.param_dict['numOfPayments']) + utils.config.fn_suffix)
        utils.config.fnoutpath = os.path.join(utils.config.out_dirpath, self.out_fn)
        return utils.config.fnoutpath

    def build_out_filepaths(self):
        '''
        build path for output files in text and csv
        '''
        self.payment_duration = str(utils.config.param_dict['numOfPayments']) # convert duration data type to string
        utils.config.out_dirpath = self.dir_dataMatrix
        utils.config.fn_suffix = utils.config.suffix_txt
        utils.config.txt_out_filename = self.joinFilePath( )
        utils.config.fn_suffix = utils.config.suffix_csv
        utils.config.csv_out_filename = self.joinFilePath()
        utils.config.fn_suffix = utils.config.suffix_json
        utils.config.json_out_filename = self.joinFilePath( )
        utils.config.outfile = (utils.config.outfile + utils.config.prefix_error) #err_out_filename 'outLoanPmtError'
        utils.config.fn_suffix = utils.config.suffix_txt
        utils.config.err_out_filename = self.joinFilePath( )
        print ('Processed Names of ''output files')
        print('List of Output files created')
        print(utils.config.txt_out_filename,' ,', utils.config.csv_out_filename, ',',utils.config.json_out_filename, ',', utils.config.err_out_filename )
        return utils.config.txt_out_filename, utils.config.csv_out_filename, utils.config.json_out_filename, utils.config.err_out_filename



    def compute_instalmentdates(self):
        '''
        create date range to store loan repayment details in pandas file
        '''
        utils.config.range = pd.date_range(utils.config.param_dict['loan_starting_date'], \
         periods = (utils.config.param_dict['numOfPayments'] +1), freq = utils.config.param_dict['payment_frequency'])
        utils.config.outer_array = np.array(utils.config.outer_list)
        print ('List of Due dates of loan instalment calculated')
        print ('First payment due on ----', (utils.config.range[0]))
        self.text_out()
        #return config.range

    def text_out(self):
        '''
        Terminal output is redirected to text file (sys.stdout). All error
        messages, are written on an error file(sys.stderr)
        '''
        sys.stdout = open(utils.config.txt_out_filename,"w")
        sys.stderr = open(utils.config.err_out_filename,"w")
        return()


