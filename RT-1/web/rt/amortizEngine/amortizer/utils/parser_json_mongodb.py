#!/usr/bin/env python3.3
# -*- coding: iso-8859-15 -*-
###############################################################################
"""
Created on Thu Oct 17 12:22:33 2013
parser_json_mongodb.py module has class and methods that will read into and out
of mongoDB using JSON format.
"""
#==============================================================================
# Python-3.3 tested ; stdlib imports
#==============================================================================
from __future__ import absolute_import, division, print_function, unicode_literals
import os, sys
import types
import pymongo
from pymongo import Connection
from bson import BSON
import json
#==============================================================================
# CODE
#-----------------------------------------------------------------------------


class JsonToMongo():
    '''
    A class representing the loading of Json data into mongo database
    '''
    def __init__(self, jsonoutFile):
        '''
        Initializing the DB calls and data values used in the subsequent modules.
        '''
        self.jsonoutFile = jsonoutFile  #json input file name
        self.DATA_SERVER = 'localhost'
        self.DATA_PORT = 27017
        self.DATA_DATABASE = 'amortizEngineDB'
        self.DATA_PARAM_COLLECTION = 'loan_details'
        self.DATA_PAYMENT_COLLECTION = 'loan_instalment'


    def registerDB(self):
        '''
        Create a DB.
        '''
        self.conn = Connection()  # Just connect to the local machine database
        self.db = self.conn[self.DATA_DATABASE] #Selecting Database
        self.annuity_collection = self.db.self.DATA_PAYMENT_COLLECTION   # Returns Collection Object
        self.coll = self.db.collection_name  #get names of all collections in database
        print (self.coll)


    def loadLoanPayement(self):
        '''
        Loading json data from file into the database
        '''
        from bson.json_util import loads
        self.json_data = open(self.jsonoutFile)  #open json input
        self.loaded_data = json.load(self.json_data) # is widget
        self.annuity_collection.insert(self.loaded_data) # Add the json data into the database
        self.json_data.close()         #print ( loaded_data)
        print ("Added Json data into database")

    def queryDB(self):
        '''
        query DB and print all documents
        '''
        self.docs = self.annuity_collection.find()  #cursor
        self.n = 0
        for self.doc in self.docs:
            '''
            self.n = self.n+1
                if self.n > 15:
                    continue
                else:
            '''
            print ("doc",  self.doc)

    def dropCollIfTest(self):
        '''
        Drop a collection from database (use during testing)
        '''
        self.annuity_collection.drop() # Drop a collection.
        self.coll = self.db.collection_name
        # print names of collections, if any
        print (self.coll)


def main(jsonoutFile):
    '''
    parse data between json to mongodb
    '''
    jtm = JsonToMongo(jsonoutFile)  #jtm is an instance of the class
    jtm.registerDB()
    jtm.loadLoanPayement()
    jtm.queryDB()
    jtm.dropCollIfTest()


if __name__ == '__main__':
    main(jsonoutFile)
