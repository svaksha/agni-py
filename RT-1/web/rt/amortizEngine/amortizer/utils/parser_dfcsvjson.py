#!/usr/bin/env python3.3
# encoding: iso-8859-15
###############################################################################
"""
Created on 2013-10-07, Mon 11:34:45
Parse data from dataframe to Excel CSV file and render csv into JSON format.
"""
#==============================================================================
# Python-3.3 tested ; stdlib imports
#==============================================================================
from __future__ import absolute_import, division, print_function, unicode_literals
import pandas as pd
from pandas import Series, DataFrame as df
import json
import csv
# local imports
import utils.config
#==============================================================================
# CODE
#-----------------------------------------------------------------------------

class ParseDFtoCsvJson():
    '''
    Class to build path for csv input and json output file = create csv Json files
    '''
    def __init__(self):
        #self.df = df
        self.CSVinFile = ' '
        self.jsonoutFile = ' '
        self.amortized_data = ' '
        self.parsed_json_data = ' '

    def df_to_csv(self, df):
        '''
        convert df to csv
        '''
        self.CSVinFile = utils.config.csv_out_filename
        df.to_csv(self.CSVinFile)

    def csv_to_json(self):
        '''
        Call parse function to convert 'csv' to 'JSON'
        '''
        self.jsonoutFile = utils.config.json_out_filename
        # Print the data from the 'JSON' file
        opened_file = open(self.CSVinFile, 'r')  # open CSV file

        # Read CSV file and  Change each fieldname to the appropriate field name.
        reader = csv.DictReader( opened_file, fieldnames = ("Serial",'option#', 'Borrower Id', 'Date',  'Payment_Number','  Interest_Part',\
                   'Principal_Part','Outstanding_Principal','Amount_Due_Now' ) )
        #Parse a raw CSV file to a JSON-line object.
        self.parsed_json_data = json.dumps( [ row for row in reader ] ) # Parse the CSV into JSON
        print ("JSON parsed!")
        # Save the JSON
        self.f = open( self.jsonoutFile, 'w')
        self.f.write(self.parsed_json_data)  # data is written into jsonoutFile
        print ('Data from JSON file --->>', '\n' , self.parsed_json_data)
        print ("JSON saved!")
        return(self.jsonoutFile)    #return parsed_data
