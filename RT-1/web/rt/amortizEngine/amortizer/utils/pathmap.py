#!/usr/bin/env python3.3
# encoding: iso-8859-15
###############################################################################
"""
Created on 2013-10-07, Mon 21:34:45
"""
#==============================================================================
# Python-3.3 tested ; stdlib imports
#==============================================================================
from __future__ import absolute_import, division, print_function, unicode_literals
import glob, sys
import os, os.path


def finDirPath():
    '''
    building path for parameter file, output files, etc
    '''
    full_path = os.path.realpath(__file__)
    dir_utils, prog_file = os.path.split(full_path)
    dir_amortizer = os.path.abspath(os.path.join(dir_utils, os.pardir))
    dir_dataMatrix = os.path.abspath(os.path.join((dir_amortizer), '..', 'dataMatrix'))

    print (dir_amortizer, dir_dataMatrix, dir_utils)
    return dir_amortizer, dir_dataMatrix, dir_utils




