#!/usr/bin/env python3.3
# encoding: iso-8859-15
###############################################################################
"""
Created on Thu Jan 17 10:45:23 2013
This package module has library for calculating the amortization (8)options.

Contains library of functions used in interest and Annuity calculations
"""
#-----------------------------------------------------------------------------
# Imports, Python3.x
#-----------------------------------------------------------------------------
from __future__ import absolute_import, division, print_function, unicode_literals
import pandas as pd
from pandas import Series, DataFrame, date_range
import numpy as np
import decimal
from decimal import *  #Decimal, ROUND_HALF_UP
decimal.getcontext().prec = 40
import datetime, time
## local imports
import utils.config
from utils.amort_param_json import  ParseParamFile as Paraminf
import utils.pathmap
#========================================================================
# CODE
#-----------------------------------------------------------------------------
#
# Calculate Payments per year
#-----------------------------------------------------------------------------
def number_of_payments_peryear(payment_frequency_code):
    """
    Calculates the  effective number of PaymentPerYear (num_of_ppy).
    ARGS
    ====
    payment_frequency_code is the alpha code of compounding periods (e.g. 'M' is 12 times per year)
    """
    num_of_ppy = 0
    if payment_frequency_code == 'A':          # Annual
        num_of_ppy = 1
    elif payment_frequency_code == 'H':        # Half yearly
        num_of_ppy = 2
    elif payment_frequency_code == 'Q':        # Quarterly
        num_of_ppy = 4
    elif payment_frequency_code == 'M':        # Monthly
        num_of_ppy = 12
    print (payment_frequency_code, num_of_ppy)
    return num_of_ppy


#=========================================================================
# Decimal, rounding off floats
#=========================================================================
def round_as_decimal(num, decimal_places=2):
    """
    Round a number to a given precision and return as a Decimal
    ARGS:
    =====
    param num: number
    type num: int, float, decimal, or str
    returns: Rounded Decimal
    rtype: decimal.Decimal
    """
    precision = '1.{places}'.format(places='0' * decimal_places)
    return Decimal(str(num)).quantize(Decimal(2), rounding=ROUND_HALF_UP)


#=========================================================================
# method to compute periodic interest rate from yearly interest rate
#=========================================================================
def periodic_interest_rate(yearly_interest_rate, period):
    """
    Calculating the Periodic Interest Rate from the yearly interest rate.
    ARGS:
    =====
    i: nominal rate
    period: number of payment periods in a year

    RETURNS
    =======
    i = periodic interest rate in fraction - retains all decimal places
    """


    return (yearly_interest_rate / (period*100) )

#========================================================================
#  Compute Periodic payment or annuity/EMI
#========================================================================
def payment(present_value, periodic_interest_rate, payment_period):
    """
    Calculating and  rounding (=ROUND_HALF_UP) Annuity to two places
    ARGS: periodic_interest_rate ; payment_periods (# of payment periods to repay loan) ;
    present_value
    RETURNS: payment amount (float) (i.e. payment)
    """
    # value rounded to two decimal places
    reg_pmt =  float(Decimal(str(present_value / ((1 - (1/(1+periodic_interest_rate)**payment_period)\
    )/periodic_interest_rate))).quantize(Decimal(2) ** -2))
    return reg_pmt


#========================================================================
# "compute_interest_only" method calculates the simple interest for each period
# for the entire loan duration.
#========================================================================
def compute_interest_only(option, amount, periodic_interest_rate, period):
    """
    Compute simple interest for each period for full duration of loan. After each payment is made,
    the pmt_count counter will be incremented and balance details will be printed.
    """
    utils.config.outer_list = [[] for i in range(period)]
    if option == '4':
        utils.config.balloon = True
    optionNo ='option_' + option
    total_interest, re_amount, re_interest = 0, 0, 0
    pmt_count = 1   #counter for payment period
    if not utils.config.readjust:
       print('Borrower_Id' ,'Principal',' ','Periodic_interest_rate','  ',\
                          'Periods','    ---> compute_interest_only')
    else:
        print('Borrower_Id', ' Principal',' ',  'Periodic_interest_rate',\
                            '  ','Periods',' ---> compute_to_readjust')
    print(amount, periodic_interest_rate, period)
    principal_part = 0.0
    if utils.config.balloon:
        period = period +1
    while (period >= pmt_count):
            interest = iPart(amount, periodic_interest_rate)
            dt = create_listOfList(optionNo, pmt_count, interest, principal_part, amount, period)
            total_interest += interest
            pmt_count += 1
            if (period < pmt_count):
                break
            else:
                continue
    if utils.config.balloon:
        period = period -1
    # create an np.array from nested list
    otr_array = np.array(utils.config.outer_list)

    #from np.array create dataframe output
    dfx = crt_df(otr_array)
    if utils.config.balloon:
        print(' Balloon Payment No and amount due are ---> ' , (pmt_count-1))
        print (' Principal','    Periodic Interest rate','    Pmt_period','   Total_Interest')
        print( amount, periodic_interest_rate, period, (total_interest ))
        utils.config.balloon = False
    if not utils.config.readjust:
        print('Total interest paid ---- >',  (total_interest ))
        print('  Principal','    Periodic Interest rate','    Pmt_period','   Total_Interest')
        print( amount, periodic_interest_rate, period, (total_interest))

    return pmt_count, total_interest, dfx

#=========================================================================
# create date range for loan repayment (not used) instead use range list
# stored in utils.config.range
#=========================================================================
def make_range():
    utils.config.range = pd.date_range(utils.config.param_dict['loan_starting_date'].timedelta(10), \
                period = utils.config.param_dict['numOfPayments'],\
                 freq = utils.config.param_dict['payment_frequency'])
    return utils.config.range

#=========================================================================
# parse dates in range to remove  %hh-%mm-%ss
#=========================================================================
def dateParser(s):
    return datetime(int(s[0:4]), int(s[5:7]), int(s[8:10]))


#=========================================================================
# Creating nested lists, structured numpy array
#=========================================================================
def create_listOfList(optionNo, pmt_count, interest_p, principal_p, amount, period):

    if pmt_count == 1:
        utils.config.outer_list = [] # initialise or empty the list
    i = pmt_count - 1
    utils.config.inner_list = []
    dt = utils.config.range[i].strftime('%Y-%m-%d')   #strips time element
    utils.config.inner_list.append(optionNo)
    utils.config.inner_list.append(utils.config.param_dict['Borrower_Id'])
    utils.config.inner_list.append(dt)
    utils.config.inner_list.append(pmt_count)
    utils.config.inner_list.append(interest_p)
    principal_p = float(Decimal(str(principal_p)).quantize(Decimal(2) ** -2))
    utils.config.inner_list.append(principal_p)
    utils.config.inner_list.append(amount)
    if utils.config.balloon and ((period) == pmt_count):
        to_pay = interest_p + principal_p + amount
    elif utils.config.param_dict['proc_option'] == '2'  and ((period) == pmt_count):
        print ('188', utils.config.param_dict['proc_option'], ((period) == pmt_count))
        to_pay = interest_p + principal_p + amount
    else:
        to_pay = interest_p + principal_p
    to_pay = float(Decimal(str(to_pay)).quantize(Decimal(2) ** -2))
    utils.config.inner_list.append(to_pay)
    utils.config.outer_list.append(utils.config.inner_list)
    return  dt

#=========================================================================
# Creating and printing Dataframe  from numpy array
#=========================================================================
def crt_df(otr_array):
    column_names = ['option#', 'Borrower Id', 'Date',  'Payment_Number','  Interest_Part',\
               'Principal_Part','Outstanding_Principal','Amount_Due_Now' ]
    df = pd.DataFrame(otr_array, columns= column_names)
    if not utils.config.readjust:
        print(df.values)
    return df


#========================================================================
# "amortization" method calculates the amortization for the full loan duration.
#========================================================================
def amortization(option, amount, periodicPayment, periodic_interest_rate, period):
    """
    Amortizes the loan for full duration of loan. After each payment is made,
    the pmt_count counter will be incremented and balance details will be printed.
    In order to reduce the variations the payment at each schedule is maintained
    as  periodicPayment = periodicPayment - (periodicPayment*100/100)
        periodicPayment, 2  =  principalPart, 2 + ipart, 2 + re_interest_part
        amount = tot_principalPart, 2 + outstanding, 2
    """
    if utils.config.set_amort == True:
        utils.config.outer_list = []
        utils.config.set_amort = False
    tot_principal_part, total_interest, rem_interest_part = 0, 0, 0
    re_outstanding,  re_principal_part, total_remaining_interest_part  = 0, 0, 0
    outstanding = amount
    if not utils.config.readjust:
        pmt_count = 1
    else:
        pmt_count = utils.config.param_dict['int_only_period'] + 1
    if option == '3':
        pmt_count = pmt_count + utils.config.param_dict['int_only_period']
    optionNo ='option_' + option
    while (int(outstanding) > 0) or (utils.config.param_dict['numOfPayments'] > pmt_count):
            outstanding = utils.config.param_dict['principal'] - tot_principal_part
            interest_part, principal_part, rem_interest_part = \
              breakdown(outstanding, periodicPayment, periodic_interest_rate, rem_interest_part)   # rem_interest_part

            outstanding = compute_balanceToBePaid(outstanding, interest_part, principal_part)
            total_interest += interest_part
            tot_principal_part += principal_part
            total_remaining_interest_part = total_remaining_interest_part + rem_interest_part

            # create nested list --> to create  numpy array --> to dataframe
            dt = create_listOfList(optionNo, pmt_count, interest_part, principal_part, outstanding, period)
            if (utils.config.param_dict['numOfPayments']  >= pmt_count):
                pmt_count += 1
                continue
            else:
                break
    print(total_remaining_interest_part)

    # create an np.array from nested list
    otr_array = np.array(utils.config.outer_list)

    #from np.array create dataframe
    dfx = crt_df(otr_array)
    print('Sum of Principal_part repaid -->', tot_principal_part)
    print('Borrower_Id',',', ' Principal',' ',' Periodic Interest rate','\
          ','Payment_period','  ', 'Annuity',' ', 'Total interest paid')
    print( utils.config.param_dict['Borrower_Id'], amount, periodic_interest_rate, period,"%.2f" % periodicPayment,\
                         (total_interest + utils.config.temp_store))
    utils.config.temp_store = 0.0
    return total_interest, dfx


##========================================================================
# method for Principal/Interest component for a payment
##========================================================================
def breakdown(amt, periodicPayment, periodic_interest_rate, rem_interest_part):
    """
    Calculates the P/I breakdown of a payment.
    Adjust interest with float(Decimal(str(interest)).quantize(Decimal(2) ** -2))rounded off part
    ARGS:
    =====
    ppl_part or amt: principal balance outstanding
    periodicPayment: reguar payment
    periodic_interest_rate: periodic interest rate

    RETURNS:
    ========
    a tuple of length 2 (interest, principal)
    """
    interest = iPart(amt, periodic_interest_rate)
    interest_part = (interest + rem_interest_part)
    interest_part = float(Decimal(str(interest)).quantize(Decimal(2) ** -2))
    rem_interest_part = interest - interest_part
    #Activate this print only to know the round-up error to decimal places
    #print interest, interest_part, rem_interest_part
    #rem_interest_part = interest - interest_part
    ppl_part = principalPart(periodicPayment, interest_part)    # two decimal places

    if amt < periodicPayment:
       ppl_part = amt
    return interest_part, ppl_part, rem_interest_part


#========================================================================
# method for Interest component for a payment owed
#========================================================================
def iPart(amt, periodic_interest_rate):
    """
    Calculates interest component of a payment.
    ARGS:
    =====
    ppl_part or amt: principal balance outstanding
    periodic_interest_rate: periodic interest rate (i.e. periodic_interest_rate per period)

    RETURNS
    =======
    i: interest part of periodic payment (float)
    """
    return amt * periodic_interest_rate

##========================================================================
# method calculates the principal component part for a payment
##========================================================================
def principalPart(periodicPayment, i):
    """
    Calculates principal component of a payment.
    ARGS
    ====
    periodicPayment: regular payment
    i: interest part of periodic payment (float)
    RETURNS:
    ========
    principal portion of this payment (float) (i.e. total payment less interest part)
    """
    return float(Decimal(str(periodicPayment - i)).quantize(Decimal(2) ** -2))

##========================================================================
# Method to calculate the remaining payment balance.
##========================================================================
def compute_balanceToBePaid(amt, i_part, p_part):
    """
    Make a payment: reduces the outstanding balance of a loan by 1 payment
    ARGS:: self
    RETURNS:: outstanding balance
    """
    amt = float(Decimal(str(amt - p_part)).quantize(Decimal(2) ** -2))
    if (amt <= 1 or 0):
        amt = 0
    return amt

