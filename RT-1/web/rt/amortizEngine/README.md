== AmortizEngine ==

* Includes a `Loan` class and `amortizEngine` module with several Excel-like
    functions for working with interest rates and loan payments.
* Has a library for Amortization of loans with several modules for different
Loan options on the backend.

== Frontend ==

* The frontend will be in JS (editable grid) with several Excel-like
functions that tabulate interest rates and loan payments.

