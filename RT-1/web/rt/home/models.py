from django.db import models
from django.db.models import CharField, EmailField, ForeignKey
from django.db.models import URLField, BooleanField, ImageField
from django.contrib.auth.models import User
from django.db.models import ManyToManyField



class RevenueTradesMenus(models.Model):
    menu_name = CharField(max_length=200)
    menu_item = CharField(max_length=200) 

    @classmethod
    def menu_list(cls, menu):
        try:
            menu_items = cls.objects.filter(menu_name=menu)
            return [(rti.id, rti.menu_item) for rti in menu_items]
        except:
            return []

    def __unicode__(self):
        return str(self.menu_name + ": " + self.menu_item)
      
class ContactRequest(models.Model):
    requester =  ForeignKey('RevenueTradesPerson',null=True)
    request = CharField(max_length=200,null=True,default="Request")
    responded = BooleanField(default=False)
    
    def __unicode__(self):
        return str(repr(self.requester.first_name) + " " + repr(self.requester.last_name) + ": " + repr(self.request))
    

class RevenueTradesPerson(models.Model):
    user = ForeignKey(User, primary_key=True)
    first_name = CharField(max_length=100,null=True)
    last_name = CharField(max_length=100,null=True)
    title = CharField(max_length=100,null=True,default="Title")
    about_me = CharField(max_length=500,null=True)
    street_address = CharField(max_length=200,null=True,default="Street address")
    city_state_zip = CharField(max_length=200,null=True,default="City/state/zip")
    email = EmailField(null=True,default="Email address")
    phone = CharField(max_length=12,null=True,default="Phone number")
    photo = ImageField("new_photo",upload_to='user_photos',null=True)
    business_verified = BooleanField()
    investor_verified = BooleanField()
    expert_verified = BooleanField()
    requests = ManyToManyField(ContactRequest)
    # At some point want to close this loop
    # business profiles?
    # investor profile?
    # expert profile? 
    
    def __unicode__(self):
        return str(repr(self.user.email) + ": " + repr(self.first_name) + " " + repr(self.last_name)) # wtf .get_username()
    
    def get_full_name(self):
        return str(repr(self.first_name) + " " + self.last_name) 
    
class RevenueTradesBusiness(models.Model):
    contact = ForeignKey(RevenueTradesPerson,null=True)
    full_name = CharField(max_length=100,null=True)
    street_address = CharField(max_length=200,null=True,default="Street address")
    city_state_zip = CharField(max_length=200,null=True,default="City/state/zip")
    website = URLField(null=True,default="http://")
    email = EmailField(null=True,default="Email address")
    phone = CharField(max_length=12,null=True,default="Phone number")
    #logo = ForeignKey(Picture,null=True)
    logo = ImageField("new_logo", upload_to='business_logos',null=True)
    # investor profiles?
    
    def __unicode__(self):
        return str("RevenueTradesBusiness " + str(self.full_name)  + ": " + str(self.contact))

    
