from django.conf.urls import patterns, url

from home import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^signup/lightbox', views.signup, name='signup'),
    url(r'^signup/step1', views.register, name='register'),
    url(r'^signup/step2', views.signup_interests, name='signup_interests'),
    url(r'^signup/details', views.signup_details, name='signup_details'),
    url(r'^signup/business', views.signup_business, name='signup_business'),
    url(r'^signup/expert', views.signup_expert, name='signup_expert'),
    url(r'^signup/funder', views.signup_funder, name='signup_funder'),
    url(r'^profile', views.profile, name='profile'),
    url(r'^account', views.account, name='account'),
    url(r'^settings', views.settings, name='settings'),
    url(r'^other', views.other, name='other'),
    url(r'^register', views.register, name='register'),
	)
