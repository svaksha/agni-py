from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import Http404
from django.template import RequestContext, loader
TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
)
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.db import IntegrityError, DatabaseError

from home.models import RevenueTradesMenus, RevenueTradesPerson, RevenueTradesBusiness
from funder.models import Funder
from business.models import Business, BUSINESS_AGE_RANGES, BUSINESS_SIZE_RANGES
from expert.models import Expert


def index(request):
    template = loader.get_template('home/index.html')
    context = RequestContext(request, {
        
    })
    return HttpResponse(template.render(context))

def signup(request):
    template = loader.get_template('home/signup_lightbox.html')
    context = RequestContext(request, { 
        
    })
    return HttpResponse(template.render(context))

def register(request):
    # need to deal with bad email address
    newuser = authenticate(username=request.POST.get('email_addr'), password='newuserpassword')
    if newuser is None:
        try:
            newuser = User.objects.create_user(request.POST['email_addr'], request.POST['email_addr'], 'newuserpassword')
            newuser.save()
            newuser = authenticate(username=request.POST['email_addr'], password='newuserpassword')

        except IntegrityError:
            pass
    login(request,newuser)      
    rtuser = RevenueTradesPerson(user = newuser, email=newuser.username)
    rtuser.save()
    # else need to start over with the user
    # need to deal with user_type
    template = loader.get_template('home/signup_verification.html')
    template = loader.get_template('home/signup_step1.html')
    context = RequestContext(request,{
        'email_addr' : request.POST['email_addr'],
        
    })
    return HttpResponse(template.render(context))

@login_required
def signup_interests(request):
    # need to reconcile password1 and password2 
    # if same then set first name, last name and password for user
    # else ask again
    rtuser = RevenueTradesPerson.objects.filter(user=request.user)
    if len(rtuser) == 1:
        rtuser = rtuser[0]
    else: 
        raise Http404
    if request.POST['password1'] == request.POST['password2']:
        request.user.set_password(request.POST['password1'])
        request.user.save()
    rtuser.full_name = request.POST['first_name'] + " " + request.POST['last_name']
    rtuser.first_name = request.POST['first_name']
    rtuser.last_name = request.POST['last_name']
    rtuser.save()
    template = loader.get_template('home/signup_step2.html')
    context = RequestContext(request,{
    })
    return HttpResponse(template.render(context))

@login_required
def signup_details(request):
    rtuser = RevenueTradesPerson.objects.get(user=request.user)
    if not rtuser:
        raise Http404
    # need to decide what kind of user we are and what the state of registration is
    rtentity = None
    if rtuser.business_verified:
        rtbusiness = RevenueTradesBusiness.objects.filter(contact=rtuser)[0]
        rtentity = Business.create(rtbusiness)
        rtentity.save()
        template = loader.get_template('home/signup_business.html')
    elif rtuser.expert_verified:
        rtentity = Expert.create(rtuser)
        rtentity.save()
        template = loader.get_template('home/signup_expert.html')
    elif rtuser.investor_verified:
        rtentity = Funder.create(rtuser)
        rtentity.save()
        template = loader.get_template('home/signup_funder.html')
    else:
        raise Http404
    
    context = RequestContext(request,{
        'rtentity': rtentity,
        'new_user': rtuser,
        'ages' : BUSINESS_AGE_RANGES,
        'sizes' : BUSINESS_SIZE_RANGES,
        'industries' :     RevenueTradesMenus.menu_list("RevenueTradesIndustries")
,
        'development_stages' : RevenueTradesMenus.menu_list("RevenueTradesDevelopmentStages"),
    })
    return HttpResponse(template.render(context))


    
@login_required
def signup_business(request):
    rtuser = RevenueTradesPerson.objects.filter(user=request.user)
    if len(rtuser) == 1:
        rtuser = rtuser[0]
    else: 
        raise Http404
    rtuser.business_verified = True
    rtuser.save()
    rtbusiness = RevenueTradesBusiness(contact = rtuser)
    rtbusiness.save()
    template = loader.get_template('home/signup_verification.html')
    context = RequestContext(request,{
    })
    return HttpResponse(template.render(context))


@login_required
def signup_expert(request):
    rtuser = RevenueTradesPerson.objects.filter(user=request.user)
    if len(rtuser) == 1:
        rtuser = rtuser[0]
    else: 
        raise Http404
    rtuser.expert_verified = True
    rtuser.save()
    template = loader.get_template('home/signup_verification.html')
    context = RequestContext(request,{
        
    })
    return HttpResponse(template.render(context))

@login_required
def signup_funder(request):
    rtuser = RevenueTradesPerson.objects.filter(user=request.user)
    if len(rtuser) == 1:
        rtuser = rtuser[0]
    else: 
        raise Http404
    rtuser.investor_verified = True
    rtuser.save()
    template = loader.get_template('home/signup_verification.html')
    context = RequestContext(request,{
        
    })
    return HttpResponse(template.render(context))

def profile(request):
    rtuser = RevenueTradesPerson.objects.filter(user=request.user)
    if len(rtuser) == 1:
        rtuser = rtuser[0]
    elif request.user.is_staff or request.user.is_superuser:
        return HttpResponseRedirect('/admin/')
    else: 
        raise Http404
    # need to decide what kind of user we are and what the state of registration is
    rtentity = None
    if rtuser.business_verified:
        return HttpResponseRedirect('/business/show_profile/')
    elif rtuser.expert_verified:
        return HttpResponseRedirect('/expert/show_profile/')
    elif rtuser.investor_verified:
        return HttpResponseRedirect('/funder/show_profile/')
    else:
        raise Http404

def account(request):
    rtuser = RevenueTradesPerson.objects.filter(user=request.user)
    if len(rtuser) == 1:
        rtuser = rtuser[0]
    else: 
        raise Http404
    # need to decide what kind of user we are and what the state of registration is
    rtentity = None
    if rtuser.business_verified:
        return HttpResponseRedirect('/business/show_profile/')
    elif rtuser.expert_verified:
        return HttpResponseRedirect('/expert/show_profile/')
    elif rtuser.investor_verified:
        return HttpResponseRedirect('/funder/show_profile/')
    else:
        raise Http404

def settings(request):
    rtuser = RevenueTradesPerson.objects.filter(user=request.user)
    if len(rtuser) == 1:
        rtuser = rtuser[0]
    else: 
        raise Http404
    # need to decide what kind of user we are and what the state of registration is
    rtentity = None
    if rtuser.business_verified:
        return HttpResponseRedirect('/business/show_profile/')
    elif rtuser.expert_verified:
        return HttpResponseRedirect('/expert/show_profile/')
    elif rtuser.investor_verified:
        return HttpResponseRedirect('/funder/show_profile/')
    else:
        raise Http404

def other(request):
    rtuser = RevenueTradesPerson.objects.filter(user=request.user)
    if len(rtuser) == 1:
        rtuser = rtuser[0]
    else: 
        raise Http404
    # need to decide what kind of user we are and what the state of registration is
    rtentity = None
    if rtuser.business_verified:
        return HttpResponseRedirect('/business/show_profile/')
    elif rtuser.expert_verified:
        return HttpResponseRedirect('/expert/show_profile/')
    elif rtuser.investor_verified:
        return HttpResponseRedirect('/funder/show_profile/')
    else:
        raise Http404


def search(request):
    template = loader.get_template('home/search-milestones.html')
    context = RequestContext(request,{
    
    })
    return HttpResponse(template.render(context))

def smash(request):
    user = User.objects.get(id=1)

    user.set_password('freeble');

    user.save()
    raise Http404

def issues(request):
    template = loader.get_template('/static/issues.html')
    context = RequestContext(request,{
    
    })
    return HttpResponse(template.render(context))
    
