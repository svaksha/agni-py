/**

Jquery script to make slides sliders
@author: Franco Risso <franco@720desarrollos.com>

       Structure
======================

<MainContent>
	<arrow_left />
	<arrow_right />
	<subContent>
		<item1 />
		...
		<itemn />
	</subcontent>
	<paginator />
</MainContent>


     NOTES
===============

MainContent=> siempre debe tener height,width y position:relative y overflow:hidden;
arrows=> position:absolute
subContent=> position:absolute;
item=> float:left
paginator=> a este le agrego <a> desde aca, la clase seleccionada debe ser select siempre

**/
var BannerSlide = function(options)
{
	var settings = {
		mainContent:'', 
		subContent:'', 
		item:'', 
		carrusel:false,
		paginator:false,//optional 
		arrowLeft:false,//optional 
		arrowRight:false, //optional
		autoMove:false,//optional
		autoMoveTime:10000//optional
	};
	$.extend(settings,options);
	
	var $this = this;
	$this._carrusel = settings.carrusel;
	$this._mainContent = $(settings.mainContent);
	$this._subContent = $this._mainContent.find(settings.subContent);
	$this._item = $this._mainContent.find(settings.item);
	$this._position = 0;
	$this._total = $this._item.length;
	
	$this._itemSize = 0;
	$this._itemSizeTotal = 0;
	$this._itemSizeEach = new Array();
	$this._itemSizeEach.push(0);
	$this._item.each(function(){
		$this._itemSizeTotal += parseInt($(this).css('width'));
		$this._itemSizeEach.push($this._itemSizeTotal);
	});
	
	
	$this._autoMove=settings.autoMove;
	$this._autoMoveTime=settings.autoMoveTime;
	
	if(settings.paginator)
	{
		$this._paginator = $this._mainContent.find(settings.paginator);
		for(i = 0; i<$this._total; i++)
		{
			$this._paginator.append('<a href="javascript:void(0);">&nbsp;</a>');
		}
		$this._paginator.find('a:first').addClass('select');
		
		
		$this._paginator.find('a').click(function(e){ e.preventDefault(); $this.MoveTo(parseInt($this._paginator.find('a').index($(this)))); });
	}
	
	$this._subContent.css({ 'width': ( $this._itemSizeTotal*2 ) + 'px'});
	
	if(settings.arrowLeft && settings.arrowRight)
	{
		$this._arrowLeft = $this._mainContent.find(settings.arrowLeft);
		$this._arrowRight = $this._mainContent.find(settings.arrowRight);
		if( parseInt($('.tabsD2').css('width')) >= $this._itemSizeTotal )
		{
			$this._arrowLeft.css('display','none');
			$this._arrowRight.css('display','none');
		}
		else
		{
			$this._arrowRight.click(function(e){ e.preventDefault(); $this.Move(1); });
			$this._arrowLeft.click(function(e){ e.preventDefault(); $this.Move(-1); });
		}
	}
	
	$this.Move = function (dir)
	{
		if($this._autoMove){ 
			if($this._timeout)
				clearTimeout($this._timeout);
		}
		
		if(dir>0 || dir<0)
		{
			$this._position += dir;
			if($this._carrusel)
			{
				if( $this._position < 0 )
				{
					$this._position = $this._total-4;
				}
				else if($this._itemSizeEach[$this._position+3] >= $this._itemSizeTotal)
				{
					$this._position = 0;
				}
				
				$this._subContent.animate({'left':( -$this._itemSizeEach[$this._position] ) + 'px'});
			}
			else
			{
				
				if( $this._itemSizeEach[$this._position]>=0 
				&&  $this._itemSizeEach[$this._position] < $this._itemSizeTotal )
					$this._subContent.animate({'left':( -$this._itemSizeEach[$this._position] ) + 'px'});
				else
					$this._position -= dir;
			}
		}
		else
			$this._subContent.animate({'left':( -$this._itemSizeEach[$this._position] ) + 'px'});
		
		if($this._paginator)
		{
			$this._paginator.find('a').removeClass('select');
			$this._paginator.find('a:eq('+$this._position+')').addClass('select');
		}
		if($this._autoMove) $this._timeout=setTimeout(function(){$this.Move(1)},$this._autoMoveTime);
	};
	$this.MoveTo = function (newPos)
	{
		$this._position = newPos;
		$this.Move(0);	
	};
	
	if($this._autoMove) $this._timeout=setTimeout(function(){$this.Move(1)},$this._autoMoveTime);
	
	return $this;
};