from django.contrib import admin
from .models import *

class RevenueTradesMenusAdmin(admin.ModelAdmin):
    fields = (('menu_name', 'menu_item'))
    
class RevenueTradesPersonAdmin(admin.ModelAdmin):
    pass
	
	
class RevenueTradesBusinessAdmin(admin.ModelAdmin):
    pass
    
admin.site.register(RevenueTradesMenus, RevenueTradesMenusAdmin)
admin.site.register(RevenueTradesPerson, RevenueTradesPersonAdmin)
admin.site.register(RevenueTradesBusiness, RevenueTradesBusinessAdmin)
    