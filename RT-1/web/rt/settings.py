import os, sys
import logging

import socket
from importlib import import_module
hostname = socket.gethostname( ).replace('.','_').replace('-','_')
local_settings = import_module( 'local_settings.%s' % hostname )

DOMAIN = getattr(local_settings,'DOMAIN',"http://127.0.0.1:8000") if local_settings else "http://django.revenuetrades.com"
STATIC_DOMAIN = getattr(local_settings,'STATIC_DOMAIN',DOMAIN)

ALLOWED_HOSTS = getattr(local_settings,'ALLOWED_HOSTS',['proto2.revenuetrades.com','127.0.0.1'])

abspath = lambda *p: os.path.abspath(os.path.join(*p))

PROJECT_ROOT = abspath(os.path.dirname(__file__))

APPEND_SLASH = True

DEBUG = getattr(local_settings,'DEBUG',True)
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Gloria W', 'strangest@comcast.net'),
)

MANAGERS = ADMINS

LOCAL_DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': '',                      # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

DATABASES = getattr(local_settings,'LOCAL_DATABASES',LOCAL_DATABASES)


# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/New_York'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

MEDIA_ROOT = os.path.join(PROJECT_ROOT,'media') + '/'
MEDIA_URL = STATIC_DOMAIN + '/media/'

STATIC_ROOT = os.path.join(PROJECT_ROOT,'static')
STATIC_URL = STATIC_DOMAIN + '/static/'

ADMIN_MEDIA_PREFIX = DOMAIN + '/static/admin/'

SERVE_STATIC_LOCALLY = getattr(local_settings,'SERVE_STATIC_LOCALLY',DOMAIN)

DEBUG_PROPAGATE_EXCEPTIONS = getattr(local_settings,'DEBUG_PROPAGATE_EXCEPTIONS',True)

# django-registration
ACCOUNT_ACTIVATION_DAYS = 3
LOGIN_URL = '/login/'
LOGOUT_URL = '/logout/'
LOGIN_REDIRECT_URL = '/profile/'
if hasattr(local_settings, 'EMAIL_BACKEND'):
	EMAIL_BACKEND = getattr(local_settings,'EMAIL_BACKEND')
# django-profiles
AUTH_PROFILE_MODULE = 'expert_profiles.ExpertProfile'

# locations of fixture files
FIXTURE_DIRS = ('/fixtures/',
	'/expert_profiles/fixtures/',
	'/expert/fixtures/',
	'/business/fixtures/',
	'/funder/fixtures/',
)

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '$*(b%v+m%3g3&amp;zhj&amp;bd%0kta-p&amp;_3uy#&amp;qzii1vpzd3&amp;fgdwir'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # 'django.middleware.exceptions.ProcessExceptionMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
'django.core.context_processors.request',
'django.contrib.auth.context_processors.auth',)

ROOT_URLCONF = 'urls' ##'rt.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'wsgi.application' ##'rt.wsgi.application'

TEMPLATE_DIRS = (
    (os.path.join(PROJECT_ROOT,'templates')), # for topmost base templates only.
)
for root, dirs, files in os.walk(PROJECT_ROOT):
    if 'templates' in dirs:
        #app_name = os.path.basename(root)
        TEMPLATE_DIRS += (os.path.join(root, 'templates'),)


INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    'registration',
    'projections',
    'home',
    'business',
    'milestones',
    'funder',
    'expert',
    'verification_service',
    'linkedin',
    'linkedin_profile',
)


# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
         'standard': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
            },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'logfile': {
            'level': 'DEBUG',
            'class':'logging.handlers.RotatingFileHandler',
            'filename': PROJECT_ROOT + "/logs/logfile",
            'formatter':'standard',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'linkedin_profile': {
            'handlers': ['logfile'],
            'level': 'DEBUG',
            'propagate': True,
        },
    }
}
