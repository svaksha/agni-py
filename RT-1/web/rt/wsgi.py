import os
import sys

for path in (
    '/usr/local/RT-1/web',
    '/usr/local/RT-1/web/rt',
    '/usr/local/RT-1/web/django',
    '/usr/local/RT-1/web/3rd_party'
    ):
    if path not in sys.path:
        sys.path.append(path)

os.environ['DJANGO_SETTINGS_MODULE'] = 'rt.settings'
os.environ['PYTHON_EGG_CACHE'] = '/usr/local/RT-1/python_eggs'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

