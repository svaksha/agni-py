"""
Login Credentials
UserName        RevenueTra37F504
Password        y8%@T~mNAx
Account Profile Information
Name    Gloria Willadsen
Company Name    Revenue Trades
Reference Number        Reve3ED362
ControlGroup Number     50102
Your System Accounts
Administrative  501020001000
Affiliate       501020000501
Billing         BA-Group-50102-0
Disbursement    DA446
"""


import logging
from suds.client import Client
from suds.sax.element import Element 
from suds.plugin import MessagePlugin

class BodyFixOnCreate(MessagePlugin):
    def __init__(self,*args,**kwargs):
        self.tns_acct = kwargs['tns_acct']
        del kwargs['tns_acct']
        # Not a new style class (still uses type(0 instead of object())
        # Do old style base class constructor call.
        #super(BodyFixOnCreate,self).__init__(*args,**kwargs)

        # No base class constructor exists.
        #MessagePlugin.__init__(self,*args,**kwargs)

    def marshalled(self,context):
        root = context.envelope.getRoot()
        envelope = root.getChild("Envelope")
        envelope.getChildren()[1].setPrefix("SOAP-ENV")
        envelope.getChildren()[1].getChildren()[0].applyns(('tns','http://tempuri.org/'))
        envelope.getChildren()[1].getChildren()[0].append(self.tns_acct)
        #print envelope.getChildren()[1]

class MeracordUserCreate(object):
    def init_logging(self):
        '''
        Be careful with this in production.
        '''
        logging.basicConfig(level=logging.INFO)
        logging.getLogger('suds.client').setLevel(logging.DEBUG)
        logging.getLogger('suds.transport').setLevel(logging.DEBUG)
        logging.getLogger('suds.xsd.schema').setLevel(logging.DEBUG)
        logging.getLogger('suds.wsdl').setLevel(logging.DEBUG)

    def create_user(self):
        security = Element('Security', ns=('wsse','http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'))
        #print dir(security)

        # No, it's not normal.
        #untoken = Element('UsernameToken', ns=('wsu','http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd'))
        
        untoken = Element('wsse:UsernameToken')
        untoken.set('xmlns:wsu','http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd')
        untoken.set('wsu:Id','Id-0001348612326227-ae6c2b32506230e6033d0000-2')
        untoken.append( Element('wsse:Username').setText('RevenueTra37F504') )
        untoken.append( Element('wsse:Password').setText('y8%@T~mNAx') )
        untoken.append( Element('wsu:Created').setText('2012-11-5T22:32:06Z') )
        security.append(untoken)
        
        create_account = Element('Create', ns=('tns','http://tempuri.org/'))
        
        tns_acct = Element('tns:account')
        tns_acct.set('xmlns:ns0','http://schemas.datacontract.org/2004/07/NoteWorld.DataServices.Common.Transport')
        
        tns_acct.append( Element('ns0:AccountTypeId').setText(3) )
        tns_acct.append( Element('ns0:Address').setText('123 Any Street') )
        tns_acct.append( Element('ns0:City').setText('Tacoma') )
        tns_acct.append( Element('ns0:ClientId').setText('123412431244') ) # generated by us
        tns_acct.append( Element('ns0:EMailAddress').setText('gloria@revenuetrades.com') )
        tns_acct.append( Element('ns0:GroupNumber').setText('50102') )
        tns_acct.append( Element('ns0:HoldTypeId').setText('98') )
        tns_acct.append( Element('ns0:Name2').setText('Jane Doe') )
        tns_acct.append( Element('ns0:NameFirst').setText('John') )
        tns_acct.append( Element('ns0:NameLast').setText('Doe') )
        tns_acct.append( Element('ns0:PhoneNumber').setText('2125551212') )
        tns_acct.append( Element('ns0:State').setText('NY') )
        tns_acct.append( Element('ns0:TaxId').setText('123456789') )
        tns_acct.append( Element('ns0:Zip').setText('10017') )
        
        #create_account.append(tns_acct)
        #body = Element('SOAP-ENV:Body')
        #body.append(create_account)
        
        url = 'https://sandbox.meracord.com/api/data/Account.svc?wsdl'
        client = Client(url,plugins=[BodyFixOnCreate(tns_acct=tns_acct)])
        cache = client.options.cache
        cache.setduration(days=10)
        client.set_options(soapheaders=security)  
        
        print create_account
        c = client.factory.create('Create')
        
        # Doesn't work! I HATE SOAP wrappers!
        #c.account.AccountTypeId = 3
        
        result = client.service.Create()
        print result
        
        #print client
        #print security
        #print create_account


if __name__ == "__main__":
    
    m = MeracordUserCreate().create_user()
