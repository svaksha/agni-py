--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: plpgsql; Type: PROCEDURAL LANGUAGE; Schema: -; Owner: postgres
--

CREATE OR REPLACE PROCEDURAL LANGUAGE plpgsql;


ALTER PROCEDURAL LANGUAGE plpgsql OWNER TO postgres;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: rt; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO rt;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: rt
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO rt;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rt
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: rt; Tablespace: 
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO rt;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: rt
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO rt;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rt
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: rt; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO rt;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: rt
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO rt;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rt
--

SELECT pg_catalog.setval('auth_permission_id_seq', 45, true);


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: rt; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(75) NOT NULL,
    password character varying(128) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    is_superuser boolean NOT NULL,
    last_login timestamp with time zone NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO rt;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: rt; Tablespace: 
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO rt;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: rt
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO rt;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rt
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: rt
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO rt;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rt
--

SELECT pg_catalog.setval('auth_user_id_seq', 22, true);


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: rt; Tablespace: 
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO rt;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: rt
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO rt;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rt
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: rt; Tablespace: 
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    user_id integer NOT NULL,
    content_type_id integer,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO rt;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: rt
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO rt;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rt
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 17, true);


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: rt; Tablespace: 
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO rt;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: rt
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO rt;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rt
--

SELECT pg_catalog.setval('django_content_type_id_seq', 15, true);


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: rt; Tablespace: 
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO rt;

--
-- Name: django_site; Type: TABLE; Schema: public; Owner: rt; Tablespace: 
--

CREATE TABLE django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.django_site OWNER TO rt;

--
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: rt
--

CREATE SEQUENCE django_site_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_site_id_seq OWNER TO rt;

--
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt
--

ALTER SEQUENCE django_site_id_seq OWNED BY django_site.id;


--
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rt
--

SELECT pg_catalog.setval('django_site_id_seq', 1, true);


--
-- Name: expert_profiles_businessquestion; Type: TABLE; Schema: public; Owner: rt; Tablespace: 
--

CREATE TABLE expert_profiles_businessquestion (
    id integer NOT NULL,
    question character varying(250),
    must_answer integer
);


ALTER TABLE public.expert_profiles_businessquestion OWNER TO rt;

--
-- Name: expert_profiles_businessquestion_id_seq; Type: SEQUENCE; Schema: public; Owner: rt
--

CREATE SEQUENCE expert_profiles_businessquestion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.expert_profiles_businessquestion_id_seq OWNER TO rt;

--
-- Name: expert_profiles_businessquestion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt
--

ALTER SEQUENCE expert_profiles_businessquestion_id_seq OWNED BY expert_profiles_businessquestion.id;


--
-- Name: expert_profiles_businessquestion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rt
--

SELECT pg_catalog.setval('expert_profiles_businessquestion_id_seq', 1, false);


--
-- Name: expert_profiles_expertprofile; Type: TABLE; Schema: public; Owner: rt; Tablespace: 
--

CREATE TABLE expert_profiles_expertprofile (
    user_id integer NOT NULL,
    full_name character varying(100),
    title character varying(100),
    organization character varying(200),
    street_address character varying(200),
    city_state_zip character varying(200),
    email character varying(75),
    phone character varying(12),
    website character varying(200),
    tenure integer,
    max_clients integer,
    time_commitment integer,
    pitch character varying(250),
    services character varying(500)
);


ALTER TABLE public.expert_profiles_expertprofile OWNER TO rt;

--
-- Name: expert_profiles_expertprofile_business_questions; Type: TABLE; Schema: public; Owner: rt; Tablespace: 
--

CREATE TABLE expert_profiles_expertprofile_business_questions (
    id integer NOT NULL,
    expertprofile_id integer NOT NULL,
    businessquestion_id integer NOT NULL
);


ALTER TABLE public.expert_profiles_expertprofile_business_questions OWNER TO rt;

--
-- Name: expert_profiles_expertprofile_business_questions_id_seq; Type: SEQUENCE; Schema: public; Owner: rt
--

CREATE SEQUENCE expert_profiles_expertprofile_business_questions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.expert_profiles_expertprofile_business_questions_id_seq OWNER TO rt;

--
-- Name: expert_profiles_expertprofile_business_questions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt
--

ALTER SEQUENCE expert_profiles_expertprofile_business_questions_id_seq OWNED BY expert_profiles_expertprofile_business_questions.id;


--
-- Name: expert_profiles_expertprofile_business_questions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rt
--

SELECT pg_catalog.setval('expert_profiles_expertprofile_business_questions_id_seq', 1, false);


--
-- Name: expert_profiles_expertprofile_focuses; Type: TABLE; Schema: public; Owner: rt; Tablespace: 
--

CREATE TABLE expert_profiles_expertprofile_focuses (
    id integer NOT NULL,
    expertprofile_id integer NOT NULL,
    focus_id integer NOT NULL
);


ALTER TABLE public.expert_profiles_expertprofile_focuses OWNER TO rt;

--
-- Name: expert_profiles_expertprofile_focuses_id_seq; Type: SEQUENCE; Schema: public; Owner: rt
--

CREATE SEQUENCE expert_profiles_expertprofile_focuses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.expert_profiles_expertprofile_focuses_id_seq OWNER TO rt;

--
-- Name: expert_profiles_expertprofile_focuses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt
--

ALTER SEQUENCE expert_profiles_expertprofile_focuses_id_seq OWNED BY expert_profiles_expertprofile_focuses.id;


--
-- Name: expert_profiles_expertprofile_focuses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rt
--

SELECT pg_catalog.setval('expert_profiles_expertprofile_focuses_id_seq', 5, true);


--
-- Name: expert_profiles_focus; Type: TABLE; Schema: public; Owner: rt; Tablespace: 
--

CREATE TABLE expert_profiles_focus (
    id integer NOT NULL,
    user_id integer NOT NULL,
    industry_id integer NOT NULL,
    business_maturity integer,
    funding_target integer,
    margin integer
);


ALTER TABLE public.expert_profiles_focus OWNER TO rt;

--
-- Name: expert_profiles_focus_id_seq; Type: SEQUENCE; Schema: public; Owner: rt
--

CREATE SEQUENCE expert_profiles_focus_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.expert_profiles_focus_id_seq OWNER TO rt;

--
-- Name: expert_profiles_focus_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt
--

ALTER SEQUENCE expert_profiles_focus_id_seq OWNED BY expert_profiles_focus.id;


--
-- Name: expert_profiles_focus_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rt
--

SELECT pg_catalog.setval('expert_profiles_focus_id_seq', 5, true);


--
-- Name: expert_profiles_industry; Type: TABLE; Schema: public; Owner: rt; Tablespace: 
--

CREATE TABLE expert_profiles_industry (
    id integer NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE public.expert_profiles_industry OWNER TO rt;

--
-- Name: expert_profiles_industry_id_seq; Type: SEQUENCE; Schema: public; Owner: rt
--

CREATE SEQUENCE expert_profiles_industry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.expert_profiles_industry_id_seq OWNER TO rt;

--
-- Name: expert_profiles_industry_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt
--

ALTER SEQUENCE expert_profiles_industry_id_seq OWNED BY expert_profiles_industry.id;


--
-- Name: expert_profiles_industry_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rt
--

SELECT pg_catalog.setval('expert_profiles_industry_id_seq', 42, true);


--
-- Name: profiles_businessquestion; Type: TABLE; Schema: public; Owner: rt; Tablespace: 
--

CREATE TABLE profiles_businessquestion (
    id integer NOT NULL,
    question character varying(250),
    must_answer integer
);


ALTER TABLE public.profiles_businessquestion OWNER TO rt;

--
-- Name: profiles_businessquestion_id_seq; Type: SEQUENCE; Schema: public; Owner: rt
--

CREATE SEQUENCE profiles_businessquestion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.profiles_businessquestion_id_seq OWNER TO rt;

--
-- Name: profiles_businessquestion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt
--

ALTER SEQUENCE profiles_businessquestion_id_seq OWNED BY profiles_businessquestion.id;


--
-- Name: profiles_businessquestion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rt
--

SELECT pg_catalog.setval('profiles_businessquestion_id_seq', 1, false);


--
-- Name: profiles_businessquestion_industries; Type: TABLE; Schema: public; Owner: rt; Tablespace: 
--

CREATE TABLE profiles_businessquestion_industries (
    id integer NOT NULL,
    businessquestion_id integer NOT NULL,
    industry_id integer NOT NULL
);


ALTER TABLE public.profiles_businessquestion_industries OWNER TO rt;

--
-- Name: profiles_businessquestion_industries_id_seq; Type: SEQUENCE; Schema: public; Owner: rt
--

CREATE SEQUENCE profiles_businessquestion_industries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.profiles_businessquestion_industries_id_seq OWNER TO rt;

--
-- Name: profiles_businessquestion_industries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt
--

ALTER SEQUENCE profiles_businessquestion_industries_id_seq OWNED BY profiles_businessquestion_industries.id;


--
-- Name: profiles_businessquestion_industries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rt
--

SELECT pg_catalog.setval('profiles_businessquestion_industries_id_seq', 1, false);


--
-- Name: profiles_expert; Type: TABLE; Schema: public; Owner: rt; Tablespace: 
--

CREATE TABLE profiles_expert (
    user_id integer NOT NULL,
    full_name character varying(100),
    title character varying(100),
    organization character varying(200),
    street_address character varying(200),
    city_state_zip character varying(200),
    email character varying(75),
    phone character varying(12),
    website character varying(200),
    tenure integer,
    max_clients integer,
    time_commitment integer,
    business_maturity integer,
    funding_target integer,
    margin integer,
    pitch character varying(500)
);


ALTER TABLE public.profiles_expert OWNER TO rt;

--
-- Name: profiles_expert_business_questions; Type: TABLE; Schema: public; Owner: rt; Tablespace: 
--

CREATE TABLE profiles_expert_business_questions (
    id integer NOT NULL,
    expert_id integer NOT NULL,
    businessquestion_id integer NOT NULL
);


ALTER TABLE public.profiles_expert_business_questions OWNER TO rt;

--
-- Name: profiles_expert_business_questions_id_seq; Type: SEQUENCE; Schema: public; Owner: rt
--

CREATE SEQUENCE profiles_expert_business_questions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.profiles_expert_business_questions_id_seq OWNER TO rt;

--
-- Name: profiles_expert_business_questions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt
--

ALTER SEQUENCE profiles_expert_business_questions_id_seq OWNED BY profiles_expert_business_questions.id;


--
-- Name: profiles_expert_business_questions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rt
--

SELECT pg_catalog.setval('profiles_expert_business_questions_id_seq', 1, false);


--
-- Name: profiles_expert_industries; Type: TABLE; Schema: public; Owner: rt; Tablespace: 
--

CREATE TABLE profiles_expert_industries (
    id integer NOT NULL,
    expert_id integer NOT NULL,
    industry_id integer NOT NULL
);


ALTER TABLE public.profiles_expert_industries OWNER TO rt;

--
-- Name: profiles_expert_industries_id_seq; Type: SEQUENCE; Schema: public; Owner: rt
--

CREATE SEQUENCE profiles_expert_industries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.profiles_expert_industries_id_seq OWNER TO rt;

--
-- Name: profiles_expert_industries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt
--

ALTER SEQUENCE profiles_expert_industries_id_seq OWNED BY profiles_expert_industries.id;


--
-- Name: profiles_expert_industries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rt
--

SELECT pg_catalog.setval('profiles_expert_industries_id_seq', 1, false);


--
-- Name: profiles_industry; Type: TABLE; Schema: public; Owner: rt; Tablespace: 
--

CREATE TABLE profiles_industry (
    id integer NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE public.profiles_industry OWNER TO rt;

--
-- Name: profiles_industry_id_seq; Type: SEQUENCE; Schema: public; Owner: rt
--

CREATE SEQUENCE profiles_industry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.profiles_industry_id_seq OWNER TO rt;

--
-- Name: profiles_industry_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt
--

ALTER SEQUENCE profiles_industry_id_seq OWNED BY profiles_industry.id;


--
-- Name: profiles_industry_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rt
--

SELECT pg_catalog.setval('profiles_industry_id_seq', 1, false);


--
-- Name: registration_registrationprofile; Type: TABLE; Schema: public; Owner: rt; Tablespace: 
--

CREATE TABLE registration_registrationprofile (
    id integer NOT NULL,
    user_id integer NOT NULL,
    activation_key character varying(40) NOT NULL
);


ALTER TABLE public.registration_registrationprofile OWNER TO rt;

--
-- Name: registration_registrationprofile_id_seq; Type: SEQUENCE; Schema: public; Owner: rt
--

CREATE SEQUENCE registration_registrationprofile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.registration_registrationprofile_id_seq OWNER TO rt;

--
-- Name: registration_registrationprofile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt
--

ALTER SEQUENCE registration_registrationprofile_id_seq OWNED BY registration_registrationprofile.id;


--
-- Name: registration_registrationprofile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rt
--

SELECT pg_catalog.setval('registration_registrationprofile_id_seq', 12, true);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rt
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rt
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rt
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rt
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rt
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rt
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rt
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rt
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rt
--

ALTER TABLE ONLY django_site ALTER COLUMN id SET DEFAULT nextval('django_site_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rt
--

ALTER TABLE ONLY expert_profiles_businessquestion ALTER COLUMN id SET DEFAULT nextval('expert_profiles_businessquestion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rt
--

ALTER TABLE ONLY expert_profiles_expertprofile_business_questions ALTER COLUMN id SET DEFAULT nextval('expert_profiles_expertprofile_business_questions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rt
--

ALTER TABLE ONLY expert_profiles_expertprofile_focuses ALTER COLUMN id SET DEFAULT nextval('expert_profiles_expertprofile_focuses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rt
--

ALTER TABLE ONLY expert_profiles_focus ALTER COLUMN id SET DEFAULT nextval('expert_profiles_focus_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rt
--

ALTER TABLE ONLY expert_profiles_industry ALTER COLUMN id SET DEFAULT nextval('expert_profiles_industry_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rt
--

ALTER TABLE ONLY profiles_businessquestion ALTER COLUMN id SET DEFAULT nextval('profiles_businessquestion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rt
--

ALTER TABLE ONLY profiles_businessquestion_industries ALTER COLUMN id SET DEFAULT nextval('profiles_businessquestion_industries_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rt
--

ALTER TABLE ONLY profiles_expert_business_questions ALTER COLUMN id SET DEFAULT nextval('profiles_expert_business_questions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rt
--

ALTER TABLE ONLY profiles_expert_industries ALTER COLUMN id SET DEFAULT nextval('profiles_expert_industries_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rt
--

ALTER TABLE ONLY profiles_industry ALTER COLUMN id SET DEFAULT nextval('profiles_industry_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rt
--

ALTER TABLE ONLY registration_registrationprofile ALTER COLUMN id SET DEFAULT nextval('registration_registrationprofile_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: rt
--

COPY auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: rt
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: rt
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add permission	1	add_permission
2	Can change permission	1	change_permission
3	Can delete permission	1	delete_permission
4	Can add group	2	add_group
5	Can change group	2	change_group
6	Can delete group	2	delete_group
7	Can add user	3	add_user
8	Can change user	3	change_user
9	Can delete user	3	delete_user
10	Can add content type	4	add_contenttype
11	Can change content type	4	change_contenttype
12	Can delete content type	4	delete_contenttype
13	Can add session	5	add_session
14	Can change session	5	change_session
15	Can delete session	5	delete_session
16	Can add site	6	add_site
17	Can change site	6	change_site
18	Can delete site	6	delete_site
19	Can add log entry	7	add_logentry
20	Can change log entry	7	change_logentry
21	Can delete log entry	7	delete_logentry
22	Can add industry	8	add_industry
23	Can change industry	8	change_industry
24	Can delete industry	8	delete_industry
25	Can add business question	9	add_businessquestion
26	Can change business question	9	change_businessquestion
27	Can delete business question	9	delete_businessquestion
28	Can add expert	10	add_expert
29	Can change expert	10	change_expert
30	Can delete expert	10	delete_expert
31	Can add registration profile	11	add_registrationprofile
32	Can change registration profile	11	change_registrationprofile
33	Can delete registration profile	11	delete_registrationprofile
34	Can add industry	12	add_industry
35	Can change industry	12	change_industry
36	Can delete industry	12	delete_industry
37	Can add focus	13	add_focus
38	Can change focus	13	change_focus
39	Can delete focus	13	delete_focus
40	Can add business question	14	add_businessquestion
41	Can change business question	14	change_businessquestion
42	Can delete business question	14	delete_businessquestion
43	Can add expert profile	15	add_expertprofile
44	Can change expert profile	15	change_expertprofile
45	Can delete expert profile	15	delete_expertprofile
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: rt
--

COPY auth_user (id, username, first_name, last_name, email, password, is_staff, is_active, is_superuser, last_login, date_joined) FROM stdin;
1	gloriajw			aagg@comcast.net	pbkdf2_sha256$10000$a49yAd2pGWgQ$O2ZiwIkg3exnst98Goiow2gP8PQabuMqvpEmvgxKEN0=	t	t	t	2012-12-06 15:09:41.615176-05	2012-07-11 14:00:52.343649-04
17	testrt			strangest@comcast.net	pbkdf2_sha256$10000$uf4f2LYrYSFv$mGXeNYhQcFg1sLJgDeONeu2y6AgjvDOebpDbX6d59og=	t	t	t	2012-12-14 12:27:32.42769-05	2012-07-18 00:41:08-04
21	james			james@example.com	pbkdf2_sha256$10000$pw3FGRgSXcNd$umyGzagB6XUTWr+esncxnQeIqaQY09TzfQUI3HugW6E=	t	t	t	2012-10-01 20:47:52.749081-04	2012-10-01 20:47:43.065536-04
20	rtvjp			rt.vjp@example.com	pbkdf2_sha256$10000$EIc9zcOw5t5p$r3emnGY+dUNfvuwVXLIUGoJ07AnE7MRTmGP0fq0bmeo=	f	t	f	2012-10-01 20:48:52.191241-04	2012-10-01 20:45:35-04
22	sarah				pbkdf2_sha256$10000$eSgqgKIj1r2L$yEgoXOB4737y+dqc8zyyU6eybFAXPPvOzhuVRKJyQ5A=	f	t	f	2012-10-21 14:39:22.770934-04	2012-10-21 14:39:22.770954-04
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: rt
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: rt
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: rt
--

COPY django_admin_log (id, action_time, user_id, content_type_id, object_id, object_repr, action_flag, change_message) FROM stdin;
1	2012-07-18 00:03:18.63177-04	1	3	11	glroiajw	3	
2	2012-07-18 00:08:37.192492-04	1	3	13	gloriajw2	3	
3	2012-07-18 00:14:14.520628-04	1	3	14	gloriajw2	2	Changed password and is_active.
4	2012-07-18 00:40:46.700781-04	1	3	14	gloriajw2	3	
5	2012-07-18 00:42:44.860604-04	1	3	17	testrt	2	Changed password and is_active.
6	2012-09-14 21:08:06.006951-04	1	3	18	nola	2	Changed password, is_active and is_superuser.
7	2012-09-28 01:30:15.499341-04	1	3	15	asdf	3	
8	2012-09-28 01:30:15.502587-04	1	3	16	viv	3	
9	2012-09-28 01:30:15.50385-04	1	3	10	vivek	3	
10	2012-09-28 01:30:15.504858-04	1	3	9	vjp	3	
11	2012-09-28 01:30:15.570328-04	1	3	12	vjp2	3	
12	2012-09-28 01:30:46.795948-04	1	3	17	testrt	2	Changed password and is_superuser.
13	2012-09-28 01:30:59.162778-04	1	3	17	testrt	2	Changed password and is_staff.
14	2012-10-01 20:48:19.958766-04	21	3	19	vjp	3	
15	2012-10-01 20:48:37.52654-04	21	3	20	rtvjp	2	Changed password and is_active.
16	2012-10-21 14:39:04.848984-04	17	3	18	nola	3	
17	2012-10-21 14:39:22.948453-04	17	3	22	sarah	1	
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: rt
--

COPY django_content_type (id, name, app_label, model) FROM stdin;
1	permission	auth	permission
2	group	auth	group
3	user	auth	user
4	content type	contenttypes	contenttype
5	session	sessions	session
6	site	sites	site
7	log entry	admin	logentry
8	industry	profiles	industry
9	business question	profiles	businessquestion
10	expert	profiles	expert
11	registration profile	registration	registrationprofile
12	industry	expert_profiles	industry
13	focus	expert_profiles	focus
14	business question	expert_profiles	businessquestion
15	expert profile	expert_profiles	expertprofile
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: rt
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
f164219440571f0dc990a7b74f585a75	ZTczZTU2M2U2MTEwNjA1MmQ3M2Y0MTU3YzYzMzJlODdiODRmZTQ1ZjqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n	2012-07-26 16:36:46.349595-04
13f4515aaaa0576c5e1e0995832c009e	MDFhMjg5NDU3NzUxZGQ0NDlhNzUxNjI3ZTVhNjIwMWMxMmE5ZTRlNTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLEHUu\n	2012-08-01 00:36:38.760297-04
d7e6ec5b27a9f43698ae6b5dfce8bf29	NDUyYzhkMzk4MTQ5NTc0ZjkxMWY0YTVhYTQ0Yzg5NmU2NmIwNmI4YTqAAn1xAS4=\n	2012-08-01 00:38:36.336777-04
2af090f5367f5a249689c7da39fbb226	ZmFiYzAzMGZhMmVjYTRkMzJjN2IyZTYyYWFiNTk0YmM4MGIyMTUwMTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLEXUu\n	2012-09-11 13:05:09.399349-04
b812baa86b7a600a4bb91448f1a034cb	ZTM4YmIyMzgwZTY3MmNiZGQ1ZTRhMGNlMzhiYTFhY2I4NjZmZjdhYzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZFUpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmRxAlUN\nX2F1dGhfdXNlcl9pZEsBdS4=\n	2012-08-07 00:16:09.876327-04
80583f4349efbafbadc8b1deeff4ea12	MDZiMmY5MTAwNmQ1OGIxOTM3MGE1OTAxNTJlNDFkN2QxNWUyOTRkYzqAAn1xAVUKdGVzdGNvb2tp\nZXECVQZ3b3JrZWRxA3Mu\n	2012-09-26 19:36:45.479528-04
0c674230aaa8f589b02eba21ddfaf43b	NGNhMmFjNDQyNGM2YWU4OWY0ZDU0ODAxMzNmYzc1YWIwODM0NWRmYTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZFUpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmRxAlUN\nX2F1dGhfdXNlcl9pZEsRdS4=\n	2012-08-07 14:57:05.741429-04
a4b6ee2afa8856ba3dfc6432cfc0ba30	NTY2ZmEwOGNiMzk3ODNjZjU0ZTk0ODI1MGEyODg3YmJlZjRiYTI0NzqAAn1xAVUKdGVzdGNvb2tp\nZVUGd29ya2VkcQJzLg==\n	2012-07-31 03:10:47.294373-04
4a5967df7c6ddb29da9d7c55483f684b	NTY2ZmEwOGNiMzk3ODNjZjU0ZTk0ODI1MGEyODg3YmJlZjRiYTI0NzqAAn1xAVUKdGVzdGNvb2tp\nZVUGd29ya2VkcQJzLg==\n	2012-07-31 12:55:22.96805-04
cefeca053a81278529d7816560a59a68	ZDNmZmVmZDkwOTRhYmQ2YzBlMmM4YmVhNDNjZmJiMWJiMDNmMTUwODqAAn1xAShVCnRlc3Rjb29r\naWVVBndvcmtlZHECVQ1fYXV0aF91c2VyX2lkSxJVEl9hdXRoX3VzZXJfYmFja2VuZFUpZGphbmdv\nLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmR1Lg==\n	2012-10-15 20:44:20.318806-04
d6614bd40f51d8024d09aee58fec52fe	NGNhMmFjNDQyNGM2YWU4OWY0ZDU0ODAxMzNmYzc1YWIwODM0NWRmYTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZFUpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmRxAlUN\nX2F1dGhfdXNlcl9pZEsRdS4=\n	2012-08-08 18:06:40.155279-04
ed0b4d54c0ea9e3b0b3600341883388e	ZTM4YmIyMzgwZTY3MmNiZGQ1ZTRhMGNlMzhiYTFhY2I4NjZmZjdhYzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZFUpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmRxAlUN\nX2F1dGhfdXNlcl9pZEsBdS4=\n	2012-11-10 12:48:27.018075-05
0f599686e239bc1a92e8a7365da339d2	ZmFiYzAzMGZhMmVjYTRkMzJjN2IyZTYyYWFiNTk0YmM4MGIyMTUwMTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLEXUu\n	2012-08-09 09:41:35.896232-04
c74a1bab856a682631f2bbeda96abddb	NGNhMmFjNDQyNGM2YWU4OWY0ZDU0ODAxMzNmYzc1YWIwODM0NWRmYTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZFUpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmRxAlUN\nX2F1dGhfdXNlcl9pZEsRdS4=\n	2012-08-09 16:46:33.424309-04
017931bb93b52281c684cce62e2ed054	NDUyYzhkMzk4MTQ5NTc0ZjkxMWY0YTVhYTQ0Yzg5NmU2NmIwNmI4YTqAAn1xAS4=\n	2012-08-01 00:46:23.737564-04
0c53d8cda50b052a705e0e241c35e1cd	MDZiMmY5MTAwNmQ1OGIxOTM3MGE1OTAxNTJlNDFkN2QxNWUyOTRkYzqAAn1xAVUKdGVzdGNvb2tp\nZXECVQZ3b3JrZWRxA3Mu\n	2013-01-01 18:32:13.192696-05
e7b0ba4e37f1f558af95ba843f827f4c	ZmFiYzAzMGZhMmVjYTRkMzJjN2IyZTYyYWFiNTk0YmM4MGIyMTUwMTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLEXUu\n	2012-08-01 00:46:39.865267-04
f0a2796d66681b92e4f504533eb71fc2	ZmFiYzAzMGZhMmVjYTRkMzJjN2IyZTYyYWFiNTk0YmM4MGIyMTUwMTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLEXUu\n	2012-08-14 07:35:16.764366-04
7f5b1125f9f3ef7e9e7421c85a5007aa	ZmFiYzAzMGZhMmVjYTRkMzJjN2IyZTYyYWFiNTk0YmM4MGIyMTUwMTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLEXUu\n	2012-08-01 12:05:54.041851-04
4e7636a6218bdc9393f69cd3d10813c3	ZGJkNDJlZmFiMTRiM2Q3MTRkODMzMjg1ZmY3MzVjNzBlZWQwODZmOTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLEnUu\n	2012-09-28 21:27:07.689199-04
e521a46d06c5ae052184c37947e3a53b	MzU4NDNlMmJmODllOGYxYzBhYWRlYTk4NmIwMDU0MjI4NDIxYmM3NjqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLFHUu\n	2012-10-15 20:48:52.196093-04
ca1ef9b037ca5b5297fe61ee294d33af	NGNhMmFjNDQyNGM2YWU4OWY0ZDU0ODAxMzNmYzc1YWIwODM0NWRmYTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZFUpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmRxAlUN\nX2F1dGhfdXNlcl9pZEsRdS4=\n	2012-08-01 15:53:13.90861-04
5744a0d180e84f5b397c8114b0f85eb9	ZmFiYzAzMGZhMmVjYTRkMzJjN2IyZTYyYWFiNTk0YmM4MGIyMTUwMTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLEXUu\n	2012-08-02 09:10:48.801938-04
d59e0f7981eaa9bde2a9637e533f691d	ZTM4YmIyMzgwZTY3MmNiZGQ1ZTRhMGNlMzhiYTFhY2I4NjZmZjdhYzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZFUpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmRxAlUN\nX2F1dGhfdXNlcl9pZEsBdS4=\n	2012-09-30 20:35:52.390015-04
159434164cd188ea5a8a77ca6afdef59	NTFlZjEwYzMwNGFiYTM5Y2NjZDM0OGEyOTEzMWQ1NDIzMDI4NDY5NTqAAn1xAShVDV9hdXRoX3Vz\nZXJfaWRLEVUSX2F1dGhfdXNlcl9iYWNrZW5kVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRz\nLk1vZGVsQmFja2VuZHECdS4=\n	2012-08-16 07:13:49.442421-04
db7c9ffb3ac98d63b7b096d8231f1fb8	MDZiMmY5MTAwNmQ1OGIxOTM3MGE1OTAxNTJlNDFkN2QxNWUyOTRkYzqAAn1xAVUKdGVzdGNvb2tp\nZXECVQZ3b3JrZWRxA3Mu\n	2012-08-03 10:59:30.652629-04
a7ace770777af700ddf7ba65d0c94857	NjZhZmJkZjA3NTM0NGZjYzU5YmY1OWZhYmZjOGZkOGY2ZjEwMTlkZTqAAn1xAShVCnRlc3Rjb29r\naWVxAlUGd29ya2VkcQNVEl9hdXRoX3VzZXJfYmFja2VuZFUpZGphbmdvLmNvbnRyaWIuYXV0aC5i\nYWNrZW5kcy5Nb2RlbEJhY2tlbmRVDV9hdXRoX3VzZXJfaWRLEXUu\n	2012-10-22 14:09:39.06765-04
0f9411bb423fcafc2449452d73ad1d75	ZmFiYzAzMGZhMmVjYTRkMzJjN2IyZTYyYWFiNTk0YmM4MGIyMTUwMTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLEXUu\n	2012-10-11 21:18:35.21987-04
a0e92739b738254fa3a6cece3f8f5b61	ODY3ZWQ2MDM0YTlmZjk2MDI4ODRhMzFkN2Q5ZDNlNDE4NjU2MGI5ZTqAAn1xAShVCnRlc3Rjb29r\naWVVBndvcmtlZHECVQ1fYXV0aF91c2VyX2lkSxFVEl9hdXRoX3VzZXJfYmFja2VuZFUpZGphbmdv\nLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmR1Lg==\n	2012-08-31 18:30:24.17875-04
30b35d23ced9076b3f9fdf86308f7603	MDZiMmY5MTAwNmQ1OGIxOTM3MGE1OTAxNTJlNDFkN2QxNWUyOTRkYzqAAn1xAVUKdGVzdGNvb2tp\nZXECVQZ3b3JrZWRxA3Mu\n	2012-08-31 21:18:00.107884-04
f10bc5b6450396247231bfd745c58c6c	NGNhMmFjNDQyNGM2YWU4OWY0ZDU0ODAxMzNmYzc1YWIwODM0NWRmYTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZFUpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmRxAlUN\nX2F1dGhfdXNlcl9pZEsRdS4=\n	2012-09-05 22:56:49.94589-04
b3f288128f937b200244ecac59cb37ed	ZmZlYmY3ZWU0Yzc4MmYyMjY1MzUzYWRhZTJhZmZhMDIwYTJiNDVkYTqAAn1xAShVCnRlc3Rjb29r\naWVVBndvcmtlZHECVRJfYXV0aF91c2VyX2JhY2tlbmRVKWRqYW5nby5jb250cmliLmF1dGguYmFj\na2VuZHMuTW9kZWxCYWNrZW5kVQ1fYXV0aF91c2VyX2lkSxF1Lg==\n	2012-11-04 13:37:06.16444-05
073881c40d3d1a4a588ab20ac7bbacae	ZTczZTU2M2U2MTEwNjA1MmQ3M2Y0MTU3YzYzMzJlODdiODRmZTQ1ZjqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n	2012-11-04 15:43:13.609979-05
999cd635279da44f04ea3c5f164c552a	ZTczZTU2M2U2MTEwNjA1MmQ3M2Y0MTU3YzYzMzJlODdiODRmZTQ1ZjqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n	2012-11-04 17:06:46.198355-05
7524163550a67b6d1ec5d7f0a373c1ff	ZGJkNDJlZmFiMTRiM2Q3MTRkODMzMjg1ZmY3MzVjNzBlZWQwODZmOTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLEnUu\n	2012-10-20 14:12:27.027374-04
e95760e630b1092dc0c15b40c82de1fa	NGNhMmFjNDQyNGM2YWU4OWY0ZDU0ODAxMzNmYzc1YWIwODM0NWRmYTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZFUpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmRxAlUN\nX2F1dGhfdXNlcl9pZEsRdS4=\n	2012-10-22 14:23:11.297037-04
7136c1d519fe8e72a4078fa9533eccde	ZmFiYzAzMGZhMmVjYTRkMzJjN2IyZTYyYWFiNTk0YmM4MGIyMTUwMTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLEXUu\n	2012-11-05 09:39:02.0661-05
dc77c640a283e8123c9cc1f5a7acd68b	ZTczZTU2M2U2MTEwNjA1MmQ3M2Y0MTU3YzYzMzJlODdiODRmZTQ1ZjqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n	2012-12-02 12:02:12.125645-05
918549125e0ed290aab94353340bf898	ZTczZTU2M2U2MTEwNjA1MmQ3M2Y0MTU3YzYzMzJlODdiODRmZTQ1ZjqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n	2012-12-20 15:09:41.782995-05
4863923572fbb933b06a5fd58f8ec665	ZmFiYzAzMGZhMmVjYTRkMzJjN2IyZTYyYWFiNTk0YmM4MGIyMTUwMTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLEXUu\n	2012-12-23 09:44:57.231014-05
709cdce9bc0a2cda8f8292ab41b3480e	ZmFiYzAzMGZhMmVjYTRkMzJjN2IyZTYyYWFiNTk0YmM4MGIyMTUwMTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLEXUu\n	2012-12-28 12:27:32.43239-05
\.


--
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: rt
--

COPY django_site (id, domain, name) FROM stdin;
1	example.com	example.com
\.


--
-- Data for Name: expert_profiles_businessquestion; Type: TABLE DATA; Schema: public; Owner: rt
--

COPY expert_profiles_businessquestion (id, question, must_answer) FROM stdin;
\.


--
-- Data for Name: expert_profiles_expertprofile; Type: TABLE DATA; Schema: public; Owner: rt
--

COPY expert_profiles_expertprofile (user_id, full_name, title, organization, street_address, city_state_zip, email, phone, website, tenure, max_clients, time_commitment, pitch, services) FROM stdin;
1	jjjjjjjjjjjjjkhkhkjkh	asfdasfdasdfasf	aaaaaaaaaaaaaa	at	sadfasfasdf	\N	Phone #	\N	1	0	\N	\N	\N
17	Maggie Barnett	Maggie Barnett	Mocha	617	City, State, ZIP	Maggs.barnett@gmail.com	Phone #	\N	1	1	2	test	\N
20	\N	Title	Fart	Street Address	City, State, ZIP	\N	\N	\N	\N	\N	\N	\N	\N
\.


--
-- Data for Name: expert_profiles_expertprofile_business_questions; Type: TABLE DATA; Schema: public; Owner: rt
--

COPY expert_profiles_expertprofile_business_questions (id, expertprofile_id, businessquestion_id) FROM stdin;
\.


--
-- Data for Name: expert_profiles_expertprofile_focuses; Type: TABLE DATA; Schema: public; Owner: rt
--

COPY expert_profiles_expertprofile_focuses (id, expertprofile_id, focus_id) FROM stdin;
1	1	1
3	17	3
5	20	5
\.


--
-- Data for Name: expert_profiles_focus; Type: TABLE DATA; Schema: public; Owner: rt
--

COPY expert_profiles_focus (id, user_id, industry_id, business_maturity, funding_target, margin) FROM stdin;
1	1	1	\N	\N	\N
3	17	10	2	1	5
5	20	1	\N	\N	\N
\.


--
-- Data for Name: expert_profiles_industry; Type: TABLE DATA; Schema: public; Owner: rt
--

COPY expert_profiles_industry (id, name) FROM stdin;
1	Advertising/Public Relations
2	Aerospace/Aviation
3	Arts/Entertainment/Publishing
4	Automotive
5	Banking/Mortgage
6	Business Development
7	Business Opportunity
8	Clerical/Administrative
9	Construction/Facilities
10	Consumer Goods
11	Customer Service
12	Education/Training
13	Energy/Utilities
14	Engineering
15	Government/Military
16	Green
17	Healthcare
18	Hospitality/Travel
19	Human Resources
20	Installation/Maintenance
21	Insurance
22	Internet
23	Job Search Aids
24	Law Enforcement/Security
25	Legal
26	Management/Executive
27	Manufacturing/Operations
28	Marketing
29	Non-Profit/Volunteer
30	Pharmaceutical/Biotech
31	Professional Services
32	QA/Quality Control
33	Real Estate
34	Restaurant/Food Service
35	Retail
36	Sales
37	Science/Research
38	Skilled Labor
39	Technology
40	Telecommunications
41	Transportation/Logistics
42	Other
\.


--
-- Data for Name: profiles_businessquestion; Type: TABLE DATA; Schema: public; Owner: rt
--

COPY profiles_businessquestion (id, question, must_answer) FROM stdin;
\.


--
-- Data for Name: profiles_businessquestion_industries; Type: TABLE DATA; Schema: public; Owner: rt
--

COPY profiles_businessquestion_industries (id, businessquestion_id, industry_id) FROM stdin;
\.


--
-- Data for Name: profiles_expert; Type: TABLE DATA; Schema: public; Owner: rt
--

COPY profiles_expert (user_id, full_name, title, organization, street_address, city_state_zip, email, phone, website, tenure, max_clients, time_commitment, business_maturity, funding_target, margin, pitch) FROM stdin;
1	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N	\N	\N	\N	\N
\.


--
-- Data for Name: profiles_expert_business_questions; Type: TABLE DATA; Schema: public; Owner: rt
--

COPY profiles_expert_business_questions (id, expert_id, businessquestion_id) FROM stdin;
\.


--
-- Data for Name: profiles_expert_industries; Type: TABLE DATA; Schema: public; Owner: rt
--

COPY profiles_expert_industries (id, expert_id, industry_id) FROM stdin;
\.


--
-- Data for Name: profiles_industry; Type: TABLE DATA; Schema: public; Owner: rt
--

COPY profiles_industry (id, name) FROM stdin;
\.


--
-- Data for Name: registration_registrationprofile; Type: TABLE DATA; Schema: public; Owner: rt
--

COPY registration_registrationprofile (id, user_id, activation_key) FROM stdin;
9	17	41d0a9a67bed3f608397ecdcffc60591452abefb
12	20	de42966796252c3b33c6145d9e9b01f13a53bc67
\.


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_key UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_codename_key; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_key UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_group_id_key; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_key UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_key UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_model_key; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_key UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);


--
-- Name: expert_profiles_businessquestion_pkey; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY expert_profiles_businessquestion
    ADD CONSTRAINT expert_profiles_businessquestion_pkey PRIMARY KEY (id);


--
-- Name: expert_profiles_expertprofile_business_questions_pkey; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY expert_profiles_expertprofile_business_questions
    ADD CONSTRAINT expert_profiles_expertprofile_business_questions_pkey PRIMARY KEY (id);


--
-- Name: expert_profiles_expertprofile_expertprofile_id_businessques_key; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY expert_profiles_expertprofile_business_questions
    ADD CONSTRAINT expert_profiles_expertprofile_expertprofile_id_businessques_key UNIQUE (expertprofile_id, businessquestion_id);


--
-- Name: expert_profiles_expertprofile_foc_expertprofile_id_focus_id_key; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY expert_profiles_expertprofile_focuses
    ADD CONSTRAINT expert_profiles_expertprofile_foc_expertprofile_id_focus_id_key UNIQUE (expertprofile_id, focus_id);


--
-- Name: expert_profiles_expertprofile_focuses_pkey; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY expert_profiles_expertprofile_focuses
    ADD CONSTRAINT expert_profiles_expertprofile_focuses_pkey PRIMARY KEY (id);


--
-- Name: expert_profiles_expertprofile_pkey; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY expert_profiles_expertprofile
    ADD CONSTRAINT expert_profiles_expertprofile_pkey PRIMARY KEY (user_id);


--
-- Name: expert_profiles_focus_pkey; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY expert_profiles_focus
    ADD CONSTRAINT expert_profiles_focus_pkey PRIMARY KEY (id);


--
-- Name: expert_profiles_industry_pkey; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY expert_profiles_industry
    ADD CONSTRAINT expert_profiles_industry_pkey PRIMARY KEY (id);


--
-- Name: profiles_businessquestion_ind_businessquestion_id_industry__key; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY profiles_businessquestion_industries
    ADD CONSTRAINT profiles_businessquestion_ind_businessquestion_id_industry__key UNIQUE (businessquestion_id, industry_id);


--
-- Name: profiles_businessquestion_industries_pkey; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY profiles_businessquestion_industries
    ADD CONSTRAINT profiles_businessquestion_industries_pkey PRIMARY KEY (id);


--
-- Name: profiles_businessquestion_pkey; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY profiles_businessquestion
    ADD CONSTRAINT profiles_businessquestion_pkey PRIMARY KEY (id);


--
-- Name: profiles_expert_business_ques_expert_id_businessquestion_id_key; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY profiles_expert_business_questions
    ADD CONSTRAINT profiles_expert_business_ques_expert_id_businessquestion_id_key UNIQUE (expert_id, businessquestion_id);


--
-- Name: profiles_expert_business_questions_pkey; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY profiles_expert_business_questions
    ADD CONSTRAINT profiles_expert_business_questions_pkey PRIMARY KEY (id);


--
-- Name: profiles_expert_industries_expert_id_industry_id_key; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY profiles_expert_industries
    ADD CONSTRAINT profiles_expert_industries_expert_id_industry_id_key UNIQUE (expert_id, industry_id);


--
-- Name: profiles_expert_industries_pkey; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY profiles_expert_industries
    ADD CONSTRAINT profiles_expert_industries_pkey PRIMARY KEY (id);


--
-- Name: profiles_expert_pkey; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY profiles_expert
    ADD CONSTRAINT profiles_expert_pkey PRIMARY KEY (user_id);


--
-- Name: profiles_industry_pkey; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY profiles_industry
    ADD CONSTRAINT profiles_industry_pkey PRIMARY KEY (id);


--
-- Name: registration_registrationprofile_pkey; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY registration_registrationprofile
    ADD CONSTRAINT registration_registrationprofile_pkey PRIMARY KEY (id);


--
-- Name: registration_registrationprofile_user_id_key; Type: CONSTRAINT; Schema: public; Owner: rt; Tablespace: 
--

ALTER TABLE ONLY registration_registrationprofile
    ADD CONSTRAINT registration_registrationprofile_user_id_key UNIQUE (user_id);


--
-- Name: auth_group_permissions_group_id; Type: INDEX; Schema: public; Owner: rt; Tablespace: 
--

CREATE INDEX auth_group_permissions_group_id ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id; Type: INDEX; Schema: public; Owner: rt; Tablespace: 
--

CREATE INDEX auth_group_permissions_permission_id ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id; Type: INDEX; Schema: public; Owner: rt; Tablespace: 
--

CREATE INDEX auth_permission_content_type_id ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id; Type: INDEX; Schema: public; Owner: rt; Tablespace: 
--

CREATE INDEX auth_user_groups_group_id ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id; Type: INDEX; Schema: public; Owner: rt; Tablespace: 
--

CREATE INDEX auth_user_groups_user_id ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id; Type: INDEX; Schema: public; Owner: rt; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_permission_id ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id; Type: INDEX; Schema: public; Owner: rt; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_user_id ON auth_user_user_permissions USING btree (user_id);


--
-- Name: django_admin_log_content_type_id; Type: INDEX; Schema: public; Owner: rt; Tablespace: 
--

CREATE INDEX django_admin_log_content_type_id ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id; Type: INDEX; Schema: public; Owner: rt; Tablespace: 
--

CREATE INDEX django_admin_log_user_id ON django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date; Type: INDEX; Schema: public; Owner: rt; Tablespace: 
--

CREATE INDEX django_session_expire_date ON django_session USING btree (expire_date);


--
-- Name: expert_profiles_expertprofile_business_questions_businessqu6f61; Type: INDEX; Schema: public; Owner: rt; Tablespace: 
--

CREATE INDEX expert_profiles_expertprofile_business_questions_businessqu6f61 ON expert_profiles_expertprofile_business_questions USING btree (businessquestion_id);


--
-- Name: expert_profiles_expertprofile_business_questions_expertprof3c3c; Type: INDEX; Schema: public; Owner: rt; Tablespace: 
--

CREATE INDEX expert_profiles_expertprofile_business_questions_expertprof3c3c ON expert_profiles_expertprofile_business_questions USING btree (expertprofile_id);


--
-- Name: expert_profiles_expertprofile_focuses_expertprofile_id; Type: INDEX; Schema: public; Owner: rt; Tablespace: 
--

CREATE INDEX expert_profiles_expertprofile_focuses_expertprofile_id ON expert_profiles_expertprofile_focuses USING btree (expertprofile_id);


--
-- Name: expert_profiles_expertprofile_focuses_focus_id; Type: INDEX; Schema: public; Owner: rt; Tablespace: 
--

CREATE INDEX expert_profiles_expertprofile_focuses_focus_id ON expert_profiles_expertprofile_focuses USING btree (focus_id);


--
-- Name: expert_profiles_focus_industry_id; Type: INDEX; Schema: public; Owner: rt; Tablespace: 
--

CREATE INDEX expert_profiles_focus_industry_id ON expert_profiles_focus USING btree (industry_id);


--
-- Name: expert_profiles_focus_user_id; Type: INDEX; Schema: public; Owner: rt; Tablespace: 
--

CREATE INDEX expert_profiles_focus_user_id ON expert_profiles_focus USING btree (user_id);


--
-- Name: profiles_businessquestion_industries_businessquestion_id; Type: INDEX; Schema: public; Owner: rt; Tablespace: 
--

CREATE INDEX profiles_businessquestion_industries_businessquestion_id ON profiles_businessquestion_industries USING btree (businessquestion_id);


--
-- Name: profiles_businessquestion_industries_industry_id; Type: INDEX; Schema: public; Owner: rt; Tablespace: 
--

CREATE INDEX profiles_businessquestion_industries_industry_id ON profiles_businessquestion_industries USING btree (industry_id);


--
-- Name: profiles_expert_business_questions_businessquestion_id; Type: INDEX; Schema: public; Owner: rt; Tablespace: 
--

CREATE INDEX profiles_expert_business_questions_businessquestion_id ON profiles_expert_business_questions USING btree (businessquestion_id);


--
-- Name: profiles_expert_business_questions_expert_id; Type: INDEX; Schema: public; Owner: rt; Tablespace: 
--

CREATE INDEX profiles_expert_business_questions_expert_id ON profiles_expert_business_questions USING btree (expert_id);


--
-- Name: profiles_expert_industries_expert_id; Type: INDEX; Schema: public; Owner: rt; Tablespace: 
--

CREATE INDEX profiles_expert_industries_expert_id ON profiles_expert_industries USING btree (expert_id);


--
-- Name: profiles_expert_industries_industry_id; Type: INDEX; Schema: public; Owner: rt; Tablespace: 
--

CREATE INDEX profiles_expert_industries_industry_id ON profiles_expert_industries USING btree (industry_id);


--
-- Name: auth_group_permissions_permission_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_permission_id_fkey FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_fkey FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions_permission_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_permission_id_fkey FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: businessquestion_id_refs_id_772c11f7; Type: FK CONSTRAINT; Schema: public; Owner: rt
--

ALTER TABLE ONLY profiles_businessquestion_industries
    ADD CONSTRAINT businessquestion_id_refs_id_772c11f7 FOREIGN KEY (businessquestion_id) REFERENCES profiles_businessquestion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: content_type_id_refs_id_728de91f; Type: FK CONSTRAINT; Schema: public; Owner: rt
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT content_type_id_refs_id_728de91f FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_content_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_fkey FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: expert_id_refs_user_id_54f10359; Type: FK CONSTRAINT; Schema: public; Owner: rt
--

ALTER TABLE ONLY profiles_expert_business_questions
    ADD CONSTRAINT expert_id_refs_user_id_54f10359 FOREIGN KEY (expert_id) REFERENCES profiles_expert(user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: expert_id_refs_user_id_c909e5b; Type: FK CONSTRAINT; Schema: public; Owner: rt
--

ALTER TABLE ONLY profiles_expert_industries
    ADD CONSTRAINT expert_id_refs_user_id_c909e5b FOREIGN KEY (expert_id) REFERENCES profiles_expert(user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: expert_profiles_expertprofile_business_businessquestion_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt
--

ALTER TABLE ONLY expert_profiles_expertprofile_business_questions
    ADD CONSTRAINT expert_profiles_expertprofile_business_businessquestion_id_fkey FOREIGN KEY (businessquestion_id) REFERENCES expert_profiles_businessquestion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: expert_profiles_expertprofile_focuses_focus_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt
--

ALTER TABLE ONLY expert_profiles_expertprofile_focuses
    ADD CONSTRAINT expert_profiles_expertprofile_focuses_focus_id_fkey FOREIGN KEY (focus_id) REFERENCES expert_profiles_focus(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: expert_profiles_expertprofile_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt
--

ALTER TABLE ONLY expert_profiles_expertprofile
    ADD CONSTRAINT expert_profiles_expertprofile_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: expert_profiles_focus_industry_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt
--

ALTER TABLE ONLY expert_profiles_focus
    ADD CONSTRAINT expert_profiles_focus_industry_id_fkey FOREIGN KEY (industry_id) REFERENCES expert_profiles_industry(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: expert_profiles_focus_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt
--

ALTER TABLE ONLY expert_profiles_focus
    ADD CONSTRAINT expert_profiles_focus_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: expertprofile_id_refs_user_id_4ee1c8f; Type: FK CONSTRAINT; Schema: public; Owner: rt
--

ALTER TABLE ONLY expert_profiles_expertprofile_business_questions
    ADD CONSTRAINT expertprofile_id_refs_user_id_4ee1c8f FOREIGN KEY (expertprofile_id) REFERENCES expert_profiles_expertprofile(user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: expertprofile_id_refs_user_id_59c1279a; Type: FK CONSTRAINT; Schema: public; Owner: rt
--

ALTER TABLE ONLY expert_profiles_expertprofile_focuses
    ADD CONSTRAINT expertprofile_id_refs_user_id_59c1279a FOREIGN KEY (expertprofile_id) REFERENCES expert_profiles_expertprofile(user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: group_id_refs_id_3cea63fe; Type: FK CONSTRAINT; Schema: public; Owner: rt
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT group_id_refs_id_3cea63fe FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: profiles_businessquestion_industries_industry_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt
--

ALTER TABLE ONLY profiles_businessquestion_industries
    ADD CONSTRAINT profiles_businessquestion_industries_industry_id_fkey FOREIGN KEY (industry_id) REFERENCES profiles_industry(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: profiles_expert_business_questions_businessquestion_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt
--

ALTER TABLE ONLY profiles_expert_business_questions
    ADD CONSTRAINT profiles_expert_business_questions_businessquestion_id_fkey FOREIGN KEY (businessquestion_id) REFERENCES profiles_businessquestion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: profiles_expert_industries_industry_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt
--

ALTER TABLE ONLY profiles_expert_industries
    ADD CONSTRAINT profiles_expert_industries_industry_id_fkey FOREIGN KEY (industry_id) REFERENCES profiles_industry(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: profiles_expert_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt
--

ALTER TABLE ONLY profiles_expert
    ADD CONSTRAINT profiles_expert_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: registration_registrationprofile_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt
--

ALTER TABLE ONLY registration_registrationprofile
    ADD CONSTRAINT registration_registrationprofile_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_7ceef80f; Type: FK CONSTRAINT; Schema: public; Owner: rt
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT user_id_refs_id_7ceef80f FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_dfbab7d; Type: FK CONSTRAINT; Schema: public; Owner: rt
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT user_id_refs_id_dfbab7d FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

