#!/bin/env python3.3
import sys
import os
import re
sys.path.append(os.path.dirname(os.path.join(__file__)))
from datetime import datetime
import pprint
import traceback
import json
import pprint
import urllib
import bottle

import logging
from logger import local_logger
l = local_logger('landing','/var/log/nginx/landing_hits.log',logging.DEBUG)

LOCAL_DIR = "/usr/local/revenuetrades/RT-1/landing"

#@bottle.route('/',method='GET')
@bottle.route('/')
def index():
    return bottle.template('index')

@bottle.route('/track',method='POST')
def email():

    email = bottle.request.forms.get('inputtext')
    usertype = bottle.request.forms.get('question')

    l.info("Email received: %s, option: %s" % (email,usertype))

    if usertype == 'Invest':
        return bottle.template('email')

    elif usertype == 'Raise Money':
        return bottle.redirect('http://wp.int.revtrades.com/apply/business')

    elif usertype == 'Be an Expert':
        return bottle.redirect('http://wp.int.revtrades.com/apply/expert')

if __name__ == "__main__":
    bottle.run(host='localhost', port=8888)
else:
    # Mod WSGI launch
    os.chdir(os.path.dirname(__file__))
    application = bottle.default_app()
