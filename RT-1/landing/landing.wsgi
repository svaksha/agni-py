import sys
import os
import re
sys.path.append(os.path.dirname(os.path.join(__file__)))
from datetime import datetime
import pprint
import traceback
import web
import urllib
import json
import pprint
import urllib

import logging
from logger import local_logger
l = local_logger('landing','/var/log/httpd/landing_hits.log',logging.DEBUG)

LOCAL_DIR = "/usr/local/revenuetrades/RT-1/landing"

urls = (
        '/?', 'Email',
        )

class Email(object):
    def POST(self):
        data = dict([(i.split('=',1)) for i in urllib.unquote(web.data()).split('&')])
        #return pprint.pformat(data)

        email = data['inputtext']

        l.info("Email received: %s" % email)
        usertype = data['question']
        if usertype == 'Invest':
            email = web.template.frender(LOCAL_DIR + '/email.html')
            return email()

        elif usertype == 'Raise+Money':
            web.redirect('http://wp.int.revtrades.com/apply/business')

        elif usertype == 'Be+an+Expert':
            web.redirect('http://wp.int.revtrades.com/apply/expert')

application = web.application(urls, globals()).wsgifunc()

