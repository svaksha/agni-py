<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type='text/css' media="all" href="static/css/styles.css"/>
		<link rel="stylesheet" type='text/css' media="all" href="static/css/styleguide-page.css"/>
	</head>
	
<body>
	<div id="all-wrap" class="clearfix">	
		<div id="header" class="clearfix">
			
				<div id="mainnav" class="clearfix">		

					<div class="navbar clearfix">
						<div class="box960 clearfix">
							<h1 id="rtlogo">
								<a id="nav-main2" href="http://revenuetrades.com">Start</a>
								<a id="rt-text2" href="http://revenuetrades.com">Revenue Trades</a>
							</h1>
						</div> <!-- END div.box960: this centers the content at 960px width -->
					</div> <!-- END navbar: wraps all the navigation tabs -->
				</div> <!-- ENDS mainnav -->
			 <!-- END nav-user -->
		</div> <!-- ENDS header -->	
		<div id="content-wrap" class="clearfix">
			<div id="content" class="clearfix">
                        Thank you for your interest in Revenue Trades. Your will be notified of unique and exciting opportunities very soon.
			</div> <!-- ENDS content -->
		</div> <!-- ENDS content-wrap -->
		
		<div id="footer" class="clearfix">
			<div class="footer clearfix">
				<span><a href="http://twitter.com/#!/revtrades">Twitter</a> | <a href="#" target="_self">Blog</a> | <a href="http://www.linkedin.com/company/revenue-trades">LinkedIn</a> | <a href="http://www.facebook.com/revenuetrades">Facebook</a> | Email Us: <a href="mailto:info@revenuetrades.com" target="_self" class="mail">info@revenuetrades.com</a>
				</span>
				Copyright 2012. Revenue Trades. All Rights Reserved.
			</div>
		</div> <!-- ENDS footer -->
	</div> <!-- ENDS all-wrap -->	
	<script type="text/javascript" src="static/js/jquery-ui-1.8.16.custom.min.js"></script>
	<script type="text/javascript" src="static/js/my_accordion.js"></script>
	<script type="text/javascript" src="static/js/jquery.focusover720.js"></script>
</body>
</html>
