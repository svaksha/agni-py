<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
		<script type="text/javascript" src="static/js/stylecontrol.js"></script>
		<script type="text/javascript" src="static/js/jqtransformplugin/jquery.jqtransform.js" ></script>
		<script language="javascript">
			$(function(){
				$('form').jqTransform({imgPath:'static/js/jqtransformplugin/img/'});
			});
		</script>
		<link rel="stylesheet" href="static/js/jqtransformplugin/jqtransform.css" type="text/css" media="all" />
		<link rel="stylesheet" type='text/css' media="all" href="static/css/styles.css"/>
		<link rel="stylesheet" type='text/css' media="all" href="static/css/styleguide-page.css"/>
	</head>
	
<body>
	<div id="all-wrap" class="clearfix">	
		<div id="header" class="clearfix">
			
				<div id="mainnav" class="clearfix">		

					<div class="navbar clearfix">
						<div class="box960 clearfix">
							<h1 id="rtlogo">
								<a id="nav-main2" href="http://revenuetrades.com">Start</a>
								<a id="rt-text2" href="http://revenuetrades.com">Revenue Trades</a>
							</h1>
						</div> <!-- END div.box960: this centers the content at 960px width -->
					</div> <!-- END navbar: wraps all the navigation tabs -->
				</div> <!-- ENDS mainnav -->
			 <!-- END nav-user -->
		</div> <!-- ENDS header -->	
		<div id="content-wrap" class="clearfix">
			<div id="content" class="clearfix">
				<div class="phrase">
					Raise Money For Your Business or<br>
					Invest & Guide them for Income
				</div>
				<div class="blue-button">
					<div class="button-level2"><div class="border"><a href="#" target="_self">What is Revenue trades?</a></div></div>
					<div class="button-level2-arrow"></div>
				</div>
				<div class="vertical-accordion">
					<div class="highlightB">
						<div class="label-border">
							<div class="tab0 tab select">
								<div class="image" style="display: block;">
									<img src="static/images/tab-0-content.jpg" width="790" height="317" alt="Image">
								</div>
							</div>
							<div class="tab1 tab">
								<a href="#" target="_self" class="link">move faster with expErt support</a>
								<div class="image">
									<img src="static/images/tab-1-content.jpg" width="790" height="317" alt="Image">
								</div>
							</div>
							<div class="tab2 tab">
								<a href="#" target="_self" class="link">INVEST IN PROGRESS</a>
								<div class="image">
									<img src="static/images/tab-2-content.jpg" width="790" height="317" alt="Image">
								</div>
							</div>
							<div class="tab3 tab">
								<a href="#" target="_self" class="link">Share your Success</a>
								<div class="image">
									<img src="static/images/tab-3-content.jpg" width="790" height="317" alt="Image">
								</div>
							</div>
						</div>
					</div>
					<div class="paginator">
						<a href="#" target="_self" class="select">Introduction</a> | <a href="#" target="_self">1</a> | <a href="#" target="_self">2</a> | <a href="#" target="_self">3</a> 
					</div>
				</div>
				<div class="sign-up">
					<div class="boxstyle-E-wrap">
						<div class="boxstyle-E-tab">
							<div class="wrap"><div class="inside-border">Get Started Now</div></div>
						</div>
						<div class="boxstyle-E">
							<div class="inside-border">
								<div class="form-sign clearfix">
									<form action="http://revenuetrades.com/track" method="POST">
										<div class="rowElem clearfix">
											<label style="width: 105px; text-align: right;">My Email:</label>
											<input name="inputtext" type="text" value="me@MyDomain.com"/>
										</div>
										<div class="rowElem clearfix">
											<label style="width: 105px; text-align: right;">I would like to:</label> 
											<input type="radio" id="" name="question" value="Invest" checked ><label style="width: 65px; text-align: left;">Invest</label>
											<input type="radio" id="" name="question" value="Raise Money" ><label style="width: 110px; text-align: left;">Raise Money</label>
											<input type="radio" id="" name="question" value="Be an Expert" ><label style="width: 100px; text-align: left;">Be an Expert</label>
										</div>
										<div class="btn">
											<input class="blue" type="submit" value="SIGN UP FOR EARLY ACCESS" />
										</div> 
									</form>
								</div>
							</div>
						</div> <!-- ends boxstyle-E -->
					</div> <!-- ends boxstyle-E-wrap -->
				</div>
			</div> <!-- ENDS content -->
		</div> <!-- ENDS content-wrap -->
		
		<div id="footer" class="clearfix">
			<div class="footer clearfix">
				<span><a href="http://twitter.com/#!/revtrades">Twitter</a> | <a href="http://revenuetrades.tumblr.com/">Blog</a> | <a href="http://www.linkedin.com/company/revenue-trades">LinkedIn</a> | <a href="http://www.facebook.com/revenuetrades">Facebook</a> | Email Us: <a href="mailto:info@revenuetrades.com" target="_self" class="mail">info@revenuetrades.com</a>
				</span>
				Copyright 2012. Revenue Trades. All Rights Reserved.
			</div>
		</div> <!-- ENDS footer -->
	</div> <!-- ENDS all-wrap -->	
	<script type="text/javascript" src="static/js/jquery-ui-1.8.16.custom.min.js"></script>
	<script type="text/javascript" src="static/js/my_accordion.js"></script>
	<script type="text/javascript" src="static/js/jquery.focusover720.js"></script>

<script type="text/javascript">
    var GoSquared={};
    GoSquared.acct = "GSN-861699-F";
    (function(w){
        function gs(){
            w._gstc_lt=+(new Date); var d=document;
            var g = d.createElement("script"); g.type = "text/javascript"; g.async = true; g.src = "//d1l6p2sc9645hc.cloudfront.net/tracker.js";
            var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(g, s);
        }
        w.addEventListener?w.addEventListener("load",gs,false):w.attachEvent("onload",gs);
    })(window);
</script>
          
</body>
</html>
