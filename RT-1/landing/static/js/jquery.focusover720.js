/**
Script: focusover720
Desc: focus and blur over the inputs

dependencies: JQUERY>=1.4
Version: 1.0
Created: 4/05/2011
Author: Franco Risso for 720desarrollos
*/
(function($) {
	
	$.fn.focusover720=function(settings)
	{
		settings = $.extend({
			originalMsg:''
		},settings);
		
		var jQueryMatchedObj = this; // This, in this context, refer to jQuery object
		this.originalMsg=$(jQueryMatchedObj).val();
		
		function _start($jQueryObj)
		{
			var originalMsg=$($jQueryObj).val();
			
			$($jQueryObj).attr('data-validation-placeholder',$($jQueryObj).val());
			
			$($jQueryObj).focus(function(){
				if($(this).val()==originalMsg)
				{
					$(this).val('');
				}
			});
			
			$($jQueryObj).blur(function(){
				if(trim($(this).val())=='')
				{
					$(this).val(originalMsg);
				}
			});
			
		}
		
		function trim(string)
		{
			return string.replace(/^\s+/g,'').replace(/\s+$/g,'');
		}
		
		this.each(function(){
			_start(this);
		});
		
		
		return this;
	}
	
})(jQuery);