RT-1
====

Landing: landing pages which are visible to the public right now.

Web: not yet visible to the public.

To run web locally:

- git clone it
- cd to web/rt/local_settings and create a file matching your_machine_name.py, replacing all dots with underscores.
- cd to web/rt
- source single_instance_setup.sh
- ./manage.py syncdb # have postgres running, create the db and user according to your settings file.
- ./manage.py runserver
