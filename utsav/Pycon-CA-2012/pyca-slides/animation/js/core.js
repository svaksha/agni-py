var current_viewport = null;
var canvas = null;
var pane = null;
var current_objects = {};
var text_objects = {};

var runner = null;
var current_frame = 0;
var current_scene = null;

function init() {
	canvas = document.getElementById("render_canvas");
	pane = document.getElementById("pane");

	$("#pane").click(function(e) {
		canvas_clicked(e.clientX, e.clientY);
	});
	$("#scenes").change(function(e) {
		load_scene();
	});
	$("#startstop").click(function(e) {
		if ($("#startstop").attr("value") == "start") {
			resume_scene();
		}
		else {
			stop_scene();
		}
	});
	$("#step").click(function(e) {
		step_scene();
	});
	$("#reset").click(function(e) {
		load_scene();
	});
	$("#delay").focusout(function(e) {
		stop_scene();
		resume_scene();
	});

	$("#console_drag").draggable({ axis: 'x',
		start: function(event, ui) {
			left_start = $('#console').width();
			right_start = $('#annotation').width();
		},
		drag: function(event, ui) {
			$('#console').width( left_start + (ui.position.left-ui.originalPosition.left) );
			$('#annotation').width( right_start - (ui.position.left-ui.originalPosition.left));
		}
	});
	$("#top_drag").draggable({axis: 'y',
		start: function(event, ui) {
			top_start = $('#top').height();
			drag_start = $("#console_drag").height();
			bottom_start = $('#pane').height();
		},
		drag: function(event, ui) {
			$("#top").height(top_start + (ui.position.top-ui.originalPosition.top));
			$("#console_drag").height(drag_start + (ui.position.top-ui.originalPosition.top));
			$("#pane").height(bottom_start - (ui.position.top-ui.originalPosition.top));
		}
	});


	draw_default_viewport();
	window_size();
	load_scene();

	$(window).resize(window_size);
}

function window_size() {
	/*get_layer("background", dpis[0], function(layer_obj) {
		var img = layer_obj.img;
		$("#pane").height(img.height + 20);
	});*/

	var width = $(window).width() - 200;
	$("#pane").width(width);
	$("#top_drag").width(width + 20);
	$("#annotation").width(width - $("#console_drag").position().left + $("#console_drag").width());

	var height = $(window).height() - 80 ;
	$("#pane").height(height - $("#top_drag").position().top);
}

function load_scene() {
	clear_render();
	var scene = $("#scenes option:selected").attr("name");
	if (scene) {
		var fileref = document.createElement('script');
		fileref.setAttribute("type","text/javascript");
		fileref.setAttribute("src", "datafiles/" + scene + ".js");
		current_frame = 0;
		if (typeof fileref!="undefined") {
			document.getElementsByTagName("head")[0].appendChild(fileref);
		}
	}
	else {
		no_scene();
	}
}

function no_scene() {
	$("#startstop, #step, #reset").attr("disabled", "disabled");
	$("#steps").html("");
}

function init_scene() {
	var text = "";
	for (var i = 1; i<=scene_data.length; i++) {
		step = scene_data[i - 1];
		step.index = i;
		text += "<span id=\"step_marker_" + step.index + "\" >.</span>\n";
	}
	$("#steps").html(text);
	$("#startstop, #step, #reset").removeAttr("disabled");
}

function stop_scene() {
	if (runner !== null) {
		$("#startstop").attr("value", "start");
		clearInterval(runner);
	}
}

function step_scene() {
	stop_scene();
	_step_scene_impl();
}

function _step_scene_impl() {
	var current = scene_data[current_frame];
	var previous;

	if (current_frame === 0) {
		previous = scene_data[scene_data.length - 1];
	}
	else {
		previous = scene_data[current_frame - 1];
	}
	show_step(previous, current);
	current_frame = current_frame + 1;
	if (current_frame >= scene_data.length) {
		current_frame = 0;
	}
}

function resume_scene() {
	$("#startstop").attr("value", "stop");
	runner = window.setInterval(
					_step_scene_impl,
					parseInt($("#delay").attr("value"), 10)
				);
}

function show_step(previous_step, step) {
	if (step.index == 1) {
		$("#console").html("");
	}
	if (previous_step) {
		$("#step_marker_" + previous_step.index).removeClass("highlight_step");
	}
	$("#step_marker_" + step.index).addClass("highlight_step");
	console.log("step:", step.index);

	for (var cmd_step_ind = 0; cmd_step_ind < step.cmds.length; cmd_step_ind++) {
		var cmd_step = step.cmds[cmd_step_ind];
		var cmd = cmd_step.cmd;
		console.log("cmd:", cmd);
		if (cmd == "console") {
			console_log(cmd_step.text);
		}
		else if (cmd == "draw_object") {
			draw_object(cmd_step.object);
		}
		else if (cmd == "draw_text") {
			draw_text(cmd_step.object, cmd_step.text);
		}
		else if (cmd == "erase_object") {
			draw_object(cmd_step.object, true);
		}
		else if (cmd == "annotate") {
			annotate(cmd_step.text);
		}
		else if (cmd == "clear") {
			clear_render();
		}
	}

}

function console_log(text) {
	$("#console").append(text);
	var max = $("#console").get(0).scrollHeight;
	$("#console").attr("scrollTop", max);
}

function annotate(text) {
	$("#annotation ul").append("<li>" + text + "</li>");
	var max = $("#annotation").get(0).scrollHeight;
	$("#annotation").attr("scrollTop", max);
}
function clear_annotations(){
	$("#annotation ul li").remove();
}
function clear_console() {
	$("#console").html("");
}


function draw_text(object, text) {
	draw_object(object, false, null, null, text);
}

function draw_object(object, clear, next, skip_dependency, text) {
	if(typeof(clear)==='undefined') clear = false;
	if (typeof(skip_dependency)==='undefined') skip_dependency = false;
	if (typeof(next)==='undefined') next = null;

	var object_obj = objects[object];
	if (typeof(object_obj) === 'undefined') {
		console.error("Can't find object", object);
		return;
	}

	if (!skip_dependency && !clear &&
			object_obj.dependency &&
			!current_objects[object_obj.dependency]) {
		draw_object(object_obj.dependency, false, function() {
			draw_object(object, clear, next, true, text);
		});
		return;
	}


	var draw_text = typeof(text)!=='undefined' || typeof(text_objects[object]) !== 'undefined';

	var layer;
	if (clear || draw_text) {
		if (object_obj.depth === 0) {
			layer = 'background';
		}
		else {
			layer = object_obj.depth - 1;
		}
	}
	else {
		layer = object_obj.depth;
	}

	get_layer(layer, current_viewport.dpi, function(layer_obj) {
		var ctx = canvas.getContext('2d');
		var img = layer_obj.img;
		var scale_adjust = current_viewport.dpi / 400;

		var x = Math.floor(object_obj.x * scale_adjust);
		var y = Math.floor(object_obj.y * scale_adjust);

		ctx.mozImageSmoothingEnabled = false;
		if (!clear && draw_text) {
			if (typeof(text)==='undefined') {
				text = text_objects[object];
			}
			else if (text_objects[object]) {
				ctx.drawImage(img,
						x - layer_obj.offset_x,
						y - layer_obj.offset_y,
						Math.floor(object_obj.width * scale_adjust),
						Math.floor(object_obj.height * scale_adjust),
						x,
						y,
						Math.floor(object_obj.width * scale_adjust),
						Math.floor(object_obj.height * scale_adjust)

					);
			}

			console.debug("draw text", object, x, y, layer, text);
			var text_px;
			ctx.strokeStyle = "#000000";
			ctx.fillStyle = "#000000";

			if (object_obj.font_size) {
				text_px = object_obj.font_size * scale_adjust;
				ctx.font = (text_px * 0.7) + "px Arial, Helvetica, Sans Serif";
				y += text_px * 0.9;
			}
			else {
				text_px = object_obj.height * scale_adjust;
				ctx.font = (text_px * 0.4) + "px Arial, Helvetica, Sans Serif";
				y += Math.floor(text_px * 0.8);
			}


			//ctx.textBaseline = 'bottom';
			var width = object_obj.width * scale_adjust;
			ctx.fillText(text, x, y, width);
			text_objects[object] = text;
		}
		else {
			console.debug("draw", object, x, y, layer, clear);
			if (clear &&
					(
						layer_obj.offset_x > x ||
						layer_obj.offset_y > y
					)
				){
				console.log("erasing via fillrect");
				ctx.strokeStyle = "#FFFFFF";
				ctx.fillStyle = "#FFFFFF";
				ctx.fillRect(x, y,
						object_obj.width * scale_adjust,
						object_obj.height * scale_adjust
					);
			}
			else {

				ctx.drawImage(img,
						x - layer_obj.offset_x,
						y - layer_obj.offset_y,
						Math.ceil(object_obj.width * scale_adjust),
						Math.ceil(object_obj.height * scale_adjust),
						x,
						y,
						Math.ceil(object_obj.width * scale_adjust),
						Math.ceil(object_obj.height * scale_adjust)

					);
			}

		}
		current_objects[object] = !clear;
		if (clear) {
			text_objects[object] = undefined;
		}
		// if we cleared, recursively set "clear" for
		// everything that's dependent on us
		if (clear) {
			var stack = [object];
			while (stack.length > 0) {
				var todo = stack.shift();
				todo = objects[todo];
				current_objects[todo['name']] = false;
				for (var dep_ind = 0; dep_ind < todo.dependents.length; dep_ind++) {
					var dependee = todo.dependents[dep_ind];
					stack.push(dependee);
				}
			}
		}

		// if we have a "call next" function, call it
		// tail recursion FTW
		if (next) {
			next();
		}

	});
}

function draw_default_viewport() {
	draw_viewport({'dpi':dpis[0], 'x':0, 'y':0});
}


function draw_viewport(viewport_obj) {
	console.log("draw viewport", viewport_obj);
	canvas.width = canvas.width;
	var is_new_viewport = !current_viewport || viewport_obj.dpi != current_viewport.dpi;

	current_viewport = viewport_obj;
	get_layer("background", viewport_obj.dpi, function(layer_obj) {

		var img = layer_obj.img;
		canvas.width = img.width + layer_obj.offset_x;
		canvas.height = img.height + layer_obj.offset_y;

		var ctx = canvas.getContext('2d');
		ctx.mozImageSmoothingEnabled = false;

		ctx.drawImage(img, layer_obj.offset_x, layer_obj.offset_y);

		if (is_new_viewport) {
			var width = pane.getBoundingClientRect().width;
			var height = pane.getBoundingClientRect().height;
			pane.scrollLeft = viewport_obj.x - (width / 2);
			pane.scrollTop = viewport_obj.y - (height / 2);
		}

	});
	$("#dpi_display").html(viewport_obj.dpi);
	redraw_objects();
}

function clear_render() {
	clear_console();
	clear_annotations();

	current_objects = {};
	text_objects = {};
	draw_viewport(current_viewport);
}

function redraw_objects() {
	var stack = [];
	var drawn = {};
	for (var objname in current_objects) {
		if (!current_objects[objname]) {
			continue;
		}
		stack.push(objname);
	}
	while (stack.length) {
		objname = stack.shift();
		if (drawn[objname]) {
			continue;
		}
		var obj = objects[objname];
		if (typeof(obj)==='undefined') {
			console.warn("Can't find object " + objname + " in object list");
			continue;
		}
		if (obj.dependency && !drawn[obj.dependency]) {
			stack.push(objname);
			if (!current_objects[obj.dependency]) {
				stack.unshift(obj.dependency);
			}
			continue;
		}
		else {
			draw_object(objname);
			drawn[objname] = true;
		}
	}
}

function get_layer(layer, dpi, callback) {
	var layer_obj = layers[layer][dpi + "dpi"];
	if (!layer_obj.img) {
		var img = new Image();
		img.onload = function() {
			layer_obj.img = img;
			callback(layer_obj);
		};
		img.src = layer_obj.src;
	}
	else {
		callback(layer_obj);
	}
}

function canvas_clicked(click_x, click_y) {
	var abs_x = pane.scrollLeft + click_x;
	var abs_y = pane.scrollTop + click_y;

	var dpi_index = dpis.indexOf(current_viewport.dpi);

	if (dpi_index == dpis.length - 1) {
		draw_default_viewport();
		return;
	}
	else {
		new_dpi = dpis[dpi_index + 1];
		scale_adjust = current_viewport.dpi / new_dpi;
		current_viewport.dpi = new_dpi;
	}

	current_viewport.x = abs_x / scale_adjust;
	current_viewport.y = abs_y / scale_adjust;

	draw_viewport(current_viewport);
}




