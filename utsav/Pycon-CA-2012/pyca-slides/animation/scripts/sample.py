from sqlalchemy import *
from sqlalchemy.orm import *
from sqlalchemy.ext.declarative import declarative_base
from . import generate_events
import gc

Base = declarative_base()


class User(Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    addresses = relationship("Address",
                    collection_class=generate_events.make_list("user1", "addresses"))

class Address(Base):
    __tablename__ = "address"

    id = Column(Integer, primary_key=True)
    email = Column(String)
    user_id = Column(Integer, ForeignKey('user.id'))

generate_events.object_lifespan(
                    User,
                    "name",
                    {"ed": "user1"}
                )
generate_events.object_lifespan(
                    Address,
                    "email",
                    {
                        "ed@ed.com": "address1",
                        "ed@gmail.com": "address2",
                        "edward@python.net": "address3",
                    }
                )

def setup():
    global e
    e = create_engine("sqlite://")
    Base.metadata.create_all(e)

def main():
    u1 = User(name="ed")

    u1.addresses.extend([
        Address(email="ed@ed.com"),
        Address(email="ed@gmail.com"),
        Address(email="edward@python.net"),
    ])

    s = Session(e)

    s.add(u1)
    s.commit()

    u1.addresses[1].email = "edward@gmail.com"
    s.commit()


setup()
#main()
generate_events.with_render(main)