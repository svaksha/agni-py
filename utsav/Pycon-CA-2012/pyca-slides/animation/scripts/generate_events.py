# becomes new
# becomes dirty
# becomes deleted
# enters/leaves identity map
# transaction starts/ends/connects/etc
# exists

from sqlalchemy import event, inspect, Table
from sqlalchemy.engine import Engine
from sqlalchemy.orm import Session
import weakref
import json

cmds = []
current_cmd = []

render_enabled = False
entities = weakref.WeakKeyDictionary()
refs = set()

sess = weakref.WeakKeyDictionary()

def requires_render(fn):
    def decorate(*arg, **kw):
        if render_enabled:
            return fn(*arg, **kw)
    return decorate

def _expire(entity):
    name = entities[entity]
    mapper = inspect(entity).mapper
    attributes = [p.key for p in mapper.attr]
    for attrname in attributes:
        render_entity = "%s_entity_%s" % (name, attrname)
        erase(render_entity)
        update_state_on_attr(entity, attrname)
    annotate("Expire attributes %s on entity '%s'" % (
            ", ".join("'%s'" % attrname for attrname in attributes),
            name
        ))

state_key = "%(name)s_state_key"
state_modified = "%(name)s_state_modified"
state_committed = "%(name)s_state_committed_state"

def update_state_on_attr(target, attrname):
    name = entities[target]
    state = inspect(target)
    if not state.modified:
        render_text(state_modified % {"name": name}, "True")
    render_text(
            state_committed % {"name": name},
            ", ".join(state.committed_state.keys() + [attrname])
        )

def object_lifespan(cls,
        id_attr,
        ent_name
        ):


    @event.listens_for(cls, "load")
    def load(target, context):
        id_val = getattr(target, id_attr, None)
        if id_val not in ent_name:
            return
        entities[target] = id_name = ent_name[id_val]
        render_state = "%s_instance_state" % id_name
        #render_entity = "%s_entity" % id_name
        render(render_state)

        _render_identity_map(target)

        mapper = inspect(cls)
        attributes = [p.key for p in mapper.column_attrs]
        for attrname in attributes:
            value = getattr(target, attrname)
            render_entity = "%s_entity_%s" % (id_name, attrname)
            render_text(render_entity, value)
            update_state_on_attr(target, attrname)

        annotate("Entity '%s' is loaded" % id_name)
        flush()

        def teardown(ref):
            erase("%s_entity" % id_name)
            erase("%s_new" % id_name)
            erase("%s_identity" % id_name)
            erase("%s_identity_key" % id_name)
            erase("%s_dirty" % id_name)
            erase("%s_deleted" % id_name)
            annotate("Entity '%s' is garbage collected" % id_name)
            flush()
            refs.discard(ref)

        refs.add(weakref.ref(target, teardown))


    @event.listens_for(cls, "init")
    def init(target, args, kwargs):
        if id_attr in kwargs:
            entities[target] = id_name = ent_name[kwargs[id_attr]]
            render_state = "%s_instance_state" % id_name
            render(render_state)
            annotate("Entity '%s' is instantiated" % id_name)
            flush()

            def teardown(ref):
                erase("%s_entity" % id_name)
                erase("%s_new" % id_name)
                erase("%s_identity" % id_name)
                erase("%s_identity_key" % id_name)
                erase("%s_dirty" % id_name)
                erase("%s_deleted" % id_name)
                annotate("Entity '%s' is garbage collected" % id_name)
                flush()
                refs.discard(ref)

            refs.add(weakref.ref(target, teardown))


    def update_dirty(target):
        name = entities[target]
        state = inspect(target)
        if state.key and state.session_id:
            session = state.session
            if target not in session.dirty:
                render("%s_dirty" % name)
                annotate("Entity '%s' is added to the session.dirty list" % name)
                flush()

    mapper = inspect(cls)
    attributes = [p.key for p in mapper.column_attrs]
    for attrname in attributes:

        def do(attrname):
            @event.listens_for(getattr(cls, attrname), "set")
            def attribute_set(target, value, oldvalue, initiator):
                if target in entities:
                    name = entities[target]
                    render_entity = "%s_entity_%s" % (name, attrname)
                    render_text(render_entity, value)
                    update_state_on_attr(target, attrname)
                    annotate("Attribute '%s' on entity '%s' is set to value %r" %
                            (attrname, name, value))
                    flush()
                    update_dirty(target)
        do(attrname)

def make_list(name, attrname):
    class EventList(list):
        def __new__(self, *arg, **kw):
            render("%s_entity_%s" % (name, attrname))
            annotate("Create empty collection '%s' on entity '%s'" % (attrname, name))
            flush()
            return list.__new__(self, *arg, **kw)

        def append(self, object):
            if object in entities:
                obj_ent = entities[object]
                render("%s_entity_%s" % (name, obj_ent))
                annotate("Append entity '%s' to collection '%s' on entity '%s'" % \
                        (obj_ent, attrname, name))
                flush()
            super(EventList, self).append(object)
    return  EventList


@event.listens_for(Session, "after_attach")
@requires_render
def attached(session, instance):
    if instance not in entities:
        return
    name = entities[instance]
    state = inspect(instance)
    if instance in session.new:
        render("%s_new" % name)
        annotate("Entity '%s' moves from transient to pending" % name)
    else:
        render_text("%s_identity_key" % name, state.key)
        render("%s_identity" % name)
        annotate("Entity '%s' moves from detached to persistent" % name)
    sess[instance] = True
    flush()

@event.listens_for(Session, "after_transaction_create")
@requires_render
def after_begin(session, transaction):
    if not transaction._parent:
        render("sessiontransaction")
        annotate("session starts a transaction container")
        flush()

@event.listens_for(Session, "after_commit")
@requires_render
def after_commit(session):
    for entity in sess:
        _expire(entity)
    flush()

@event.listens_for(Session, "after_transaction_end")
@requires_render
def after_commit(session, transaction):
    if not transaction._parent:
        erase("sessiontransaction")
        annotate("session's transaction container ended")

        flush()


@event.listens_for(Session, "before_flush")
@requires_render
def before_flush(*arg):
    annotate("flush begins")
    flush()

@event.listens_for(Session, "after_flush")
@requires_render
def after_flush(session, ctx):
    ctx._memo = {
        'new': set(session.new),
        'identity': set(session.identity_map.values()),
        'dirty': set(session.dirty),
        'deleted': set(session.deleted)
    }

@event.listens_for(Session, "after_flush_postexec")
@requires_render
def after_flush_postexec(session, ctx):
    if not hasattr(ctx, '_memo'):
        return

    for remove_deleted in ctx._memo['deleted'].difference(session.deleted):
        if remove_deleted not in entities:
            continue
        name = entities[remove_deleted]
        erase("%s_deleted" % name)
        annotate("Entity '%s' is removed from the 'deleted' collection" % name)
        flush()

    for remove_dirty in ctx._memo['dirty'].difference(session.dirty):
        if remove_dirty not in entities:
            continue
        name = entities[remove_dirty]
        erase("%s_dirty" % name)

    for added_ident in set(session.identity_map.values()).difference(ctx._memo['identity']):
        if added_ident not in entities:
            continue
        name = entities[added_ident]
        erase("%s_new" % name)
        _render_identity_map(added_ident)
        annotate("Entity '%s' moves from pending to persistent" % name)
        flush()

    annotate("flush ends")
    flush()

def _render_identity_map(entity):
    name = entities[entity]
    state = inspect(entity)
    render_text("%s_identity_key" % name,
            "(%s, %s) -->" % (state.class_.__name__, state.identity, ))
    render("%s_identity" % name)

@event.listens_for(Engine, "after_cursor_execute")
@requires_render
def log_sql(conn, cursor, statement, parameters, context, executemany):
    console(statement + "\n")
    if context:
        if context.is_crud:
            tablename = context.compiled.statement.table.name
            annotate("execute %s statement on table '%s'" % (
                    "INSERT" if context.isinsert else
                        ("UPDATE" if context.isupdate else "DELETE"),
                    tablename
            ))

            _draw_rows(context.connection, "snapshot", [tablename])
        else:
            froms = context.compiled.statement.froms
            _draw_rows(context.connection, "snapshot", from_context=context)
            annotate("SELECT rows from tables %s" %
                    ", ".join("'%s'" % f.name for f in froms)
            )
    flush()


@event.listens_for(Engine, "checkout")
@requires_render
def checkout(*args):
    render("connection")
    render("sessiontransaction_conn_ref")
    annotate("Connection established")
    flush()

@event.listens_for(Engine, "checkin")
@requires_render
def checkin(*args):
    erase("sessiontransaction_conn_ref")
    erase("connection")
    annotate("Connection removed")
    flush()

@event.listens_for(Engine, "begin")
@requires_render
def begin(*args):
    render("transaction_snapshot")
    annotate("a 'snapshot' is established in the database session")
    flush()

@event.listens_for(Engine, "commit")
@requires_render
def commit(connection):
    erase("transaction_snapshot")
    annotate("the 'snapshot' is committed")
    _draw_rows(connection, "perm")
    flush()

def _draw_rows(connection, type_, tables=None, from_context=None):
    global render_enabled
    render_enabled = False


    symbols = []

    if from_context:
        selected_rows = connection.execute(from_context.statement,
                                        from_context.parameters)
        catch_rows = {}
        for t in from_context.compiled.statement.froms:
            if isinstance(t, Table):
                catch_rows[t] = dest = {}
                if t.name == 'user':
                    symbols.append((1, 'user1', dest))
                elif t.name == 'address':
                    symbols.extend([
                        (1, 'address1', dest),
                        (2, 'address2', dest),
                        (3, 'address3', dest),
                    ])

        for row in selected_rows:
            for t, dest in catch_rows.items():
                dest[row[t.c.id]] = dict((c.key, row[c]) for c in t.c)
    else:
        if not tables or 'user' in tables:
            user = dict(
                (row['id'], row)
                for row in connection.execute("select * from user")
            )
            symbols.append((1, 'user1', user))
        if not tables or 'address' in tables:
            address = dict(
                (row['id'], row)
                for row in connection.execute("select * from address")
            )
            symbols.extend([
                (1, 'address1', address),
                (2, 'address2', address),
                (3, 'address3', address),
            ])
    render_enabled = True

    for id_, rowid, coll in symbols:
        if id_ not in coll:
            erase("%s_%s_row" % (rowid, type_))
        else:
            row = coll[id_]
            for col in row.keys():
                render_text("%s_%s_col_%s" % (rowid, type_, col), row[col])


def flush():
    global current_cmd
    cmds.append({"cmds": current_cmd})
    current_cmd = []

def annotate(text):
    current_cmd.append({"cmd": "annotate", "text": text})

def console(text):
    current_cmd.append({"cmd": "console", "text": text})

def render(entity):
    current_cmd.append({"cmd": "draw_object", "object": entity})

def erase(entity):
    current_cmd.append({"cmd": "erase_object", "object": entity})

def render_text(entity, value):
    current_cmd.append({"cmd": "draw_text", "object": entity, "text": value})

def with_render(fn):
    global render_enabled
    render_enabled = True
    cmds[:] = []
    current_cmd[:] = []
    fn()
    flush()
    current_cmd.append(
        {"cmd":"clear"}
    )
    flush()
    render_enabled = False
    json_str = json.dumps(cmds, indent=4)
    print "var scene_data = %s;\n\ninit_scene();" % json_str
