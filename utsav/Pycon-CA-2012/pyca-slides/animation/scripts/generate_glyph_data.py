# parses the diagram from an OmniGraffle Pro 4.1.2 (v127.10)
# document.
import sys
from lxml import etree
import re
import collections
import json

DPI = 400

def setup_dependencies(graphics):
    _grid = collections.defaultdict(list)
    byname = {}
    x_boundary = 1000000
    y_boundary = 1000000

    # expand dimensions to account for
    # line thickness
    x_coord_fudge = 6
    y_coord_fudge = 7
    width_dimension_fudge = 10
    height_dimension_fudge = 10

    for grph in graphics:
        grph['dependents'] = []
        if grph['name'] in byname:
            raise Exception("repeated name: %s" % grph['name'])
        byname[grph['name']] = grph
        bounds = grph['bounds']
        x_boundary = min(x_boundary, bounds.x)
        y_boundary = min(y_boundary, bounds.y)

        if not grph.get('fixed_depth'):
            for x in xrange(int(bounds.x), int(bounds.x) + int(bounds.width)):
                for y in xrange(int(bounds.y), int(bounds.y) + int(bounds.height)):
                    _list = _grid[(x, y)]

                    if _list:
                        potential_dependent = _list[-1]
                        dependent_bounds = potential_dependent['bounds']
                        is_centerpoint = x == (int(dependent_bounds.x) + int(dependent_bounds.width) / 2) and \
                                    y == (int(dependent_bounds.y) + int(dependent_bounds.height) / 2)

                        if is_centerpoint:
                            potential_dependent['dependency'] = grph['name']
                            grph['dependents'].append(potential_dependent['name'])
                            assert grph['name'] not in grph['dependents']

                    _list.append(grph)

    for glyph in byname.values():
        bounds = glyph.pop('bounds')
        glyph['x'] = _adjust_coord(bounds.x, DPI, x_boundary) + x_coord_fudge
        glyph['y'] = _adjust_coord(bounds.y, DPI, y_boundary) + y_coord_fudge
        glyph['width'] = _adjust_coord(bounds.width, DPI, 0) + width_dimension_fudge
        glyph['height'] = _adjust_coord(bounds.height, DPI, 0) + height_dimension_fudge
        if 'font_size' in glyph:
            glyph['font_size'] = _adjust_coord(glyph['font_size'], DPI, 0)
        if "fixed_depth" in glyph:
            glyph["depth"] = int(glyph["fixed_depth"])
        else:
            d_g = glyph
            depth = 0
            while "dependency" in d_g:
                depth += 1
                d_g = byname[d_g['dependency']]
            glyph["depth"] = depth

    return byname

def locate_noted_graphics(document):
    for element in shaped_graphics(document):
        if "Notes" in element:
            glyph = _parse_notes(element['Notes'])
            bounds = _parse_bounds(element['Bounds'])
            glyph['bounds'] = bounds
            font_info = _parse_font(element.get('FontInfo'))
            if font_info is not None:
                glyph['font_size'] = font_info
            yield glyph

def _parse_font(font_info):
    if font_info is None:
        return None
    if 'Size' in font_info:
        return font_info['Size']

def _parse_notes(notes):
    contents = re.sub(r'[{}]', '', notes)
    contents = re.sub(r'\\\w+', '', contents)

    lines = contents.split("\n")
    result = {}
    for line in lines:
        if ":" not in line:
            continue
        key, value = line.split(":", 1)
        key = key.strip()
        value = value.strip().replace("\\", "")
        result[key] = value
    return result

_bounds = collections.namedtuple("bounds", ["x", "y", "width", "height"])
def _parse_bounds(bounds):
    # {{614.951, 389.751}, {24.2216, 8.78806}}
    bounds = re.sub(r'[{}]', '', bounds)
    return _bounds(*[float(x) for x in re.split(r",", bounds)])


def _adjust_coord(graffle_coord, dpi, offset):
    # an observed constant, 72 thingies per inch
    graffle_coord -= offset
    inches = graffle_coord * .01388889521789
    return int(round(inches * dpi))

def shaped_graphics(document):
    for element in document['GraphicsList']:
        if element.get("Class") == "ShapedGraphic":
            yield element

def line_graphics(document):
    for element in document['GraphicsList']:
        if element.get("Class") == "LineGraphic":
            yield element

def plist_to_struct(filename):
    dom = etree.parse(open(filename))
    for node in dom.getroot().iterchildren():
        return _parse_node(node)

def _parse_node(node):
    parser = _parsers.get(node.tag)
    if parser:
        return parser(node)
    else:
        return None

def _parse_dict(node):
    result = {}
    for elem in node.iterchildren():
        if elem.tag == 'key':
            key = elem.text
        else:
            assert key
            result[key] = _parse_node(elem)
            key = None
    return result

def _parse_array(node):
    return [
        _parse_node(n) for n in node.iterchildren()
    ]

_parsers = {
    "dict": _parse_dict,
    "array": _parse_array,
    "false": lambda node: False,
    "true": lambda node: True,
    "string": lambda node: node.text,
    "integer": lambda node: int(node.text),
    "real": lambda node: float(node.text),
    "string": lambda node: node.text,
}

if __name__ == '__main__':
    filename = sys.argv[1]
    struct = plist_to_struct(filename)
    graphics = list(locate_noted_graphics(struct))
    byname = setup_dependencies(graphics)
    json_str = json.dumps(byname, indent=4)
    print "var objects = %s;" % json_str
