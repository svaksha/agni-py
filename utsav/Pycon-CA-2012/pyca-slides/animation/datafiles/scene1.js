console.log("scene1.js");

var scene_data = [
	{
		"cmds":[
			{"cmd":"console", "text":"SELECT * FROM TABLE\n"},
			{"cmd":"draw_object", "object":'user1_entity_address1'},
			{"cmd":"draw_object", "object":'user1_entity_address2'},
			{"cmd":"draw_object", "object":'user1_entity_address3'}
		]
	},
	{
		"cmds":[
			{"cmd":"draw_text", "object":'address1_state_key', 'text':'(an id,)'},
			{"cmd":"draw_text", "object":'address2_state_key', 'text':'(an id,)'},
			{"cmd":"draw_text", "object":'address3_state_key', 'text':'(an id,)'},
			{"cmd":"console", "text":"UPDATE USER\n"},
			{"cmd":"draw_object", "object":'user1_perm_row'},
			{"cmd":"draw_object", "object":'transaction_snapshot'}
		]
	},
	{
		"cmds":[
			{"cmd":"draw_object", "object":'user1_new'},
			{"cmd":"draw_object", "object":'address1_dirty'},
			{"cmd":"draw_object", "object":'address2_dirty'},
			{"cmd":"draw_object", "object":'address3_dirty'},

			{"cmd":"console", "text":"UPDATE ADDRESS\n"},
			{"cmd":"draw_object", "object":'address1_perm_row'},
			{"cmd":"draw_object", "object":'address2_perm_row'},
			{"cmd":"console", "text":"INSERT INTO ADDRESS\n"},
			{"cmd":"draw_object", "object":'address3_perm_row'}
		]
	},
	{
		"cmds":[
			{"cmd":"erase_object", "object":'transaction_snapshot'},
			{"cmd":"clear"}
		]
	}
];

init_scene();