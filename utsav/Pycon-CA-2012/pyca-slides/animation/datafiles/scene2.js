var scene_data = [
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "user1_instance_state"
            }, 
            {
                "text": "Entity 'user1' is instantiated", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "text": "ed", 
                "cmd": "draw_text", 
                "object": "user1_entity_name"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "user1_state_modified"
            }, 
            {
                "text": "name", 
                "cmd": "draw_text", 
                "object": "user1_state_committed_state"
            }, 
            {
                "text": "Attribute 'name' on entity 'user1' is set to value 'ed'", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "user1_entity_addresses"
            }, 
            {
                "text": "Create empty collection 'addresses' on entity 'user1'", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "address1_instance_state"
            }, 
            {
                "text": "Entity 'address1' is instantiated", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "text": "ed@ed.com", 
                "cmd": "draw_text", 
                "object": "address1_entity_email"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "address1_state_modified"
            }, 
            {
                "text": "email", 
                "cmd": "draw_text", 
                "object": "address1_state_committed_state"
            }, 
            {
                "text": "Attribute 'email' on entity 'address1' is set to value 'ed@ed.com'", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "address2_instance_state"
            }, 
            {
                "text": "Entity 'address2' is instantiated", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "text": "ed@gmail.com", 
                "cmd": "draw_text", 
                "object": "address2_entity_email"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "address2_state_modified"
            }, 
            {
                "text": "email", 
                "cmd": "draw_text", 
                "object": "address2_state_committed_state"
            }, 
            {
                "text": "Attribute 'email' on entity 'address2' is set to value 'ed@gmail.com'", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "address3_instance_state"
            }, 
            {
                "text": "Entity 'address3' is instantiated", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "text": "edward@python.net", 
                "cmd": "draw_text", 
                "object": "address3_entity_email"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "address3_state_modified"
            }, 
            {
                "text": "email", 
                "cmd": "draw_text", 
                "object": "address3_state_committed_state"
            }, 
            {
                "text": "Attribute 'email' on entity 'address3' is set to value 'edward@python.net'", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "user1_entity_address1"
            }, 
            {
                "text": "Append entity 'address1' to collection 'addresses' on entity 'user1'", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "user1_entity_address2"
            }, 
            {
                "text": "Append entity 'address2' to collection 'addresses' on entity 'user1'", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "user1_entity_address3"
            }, 
            {
                "text": "Append entity 'address3' to collection 'addresses' on entity 'user1'", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "sessiontransaction"
            }, 
            {
                "text": "session starts a transaction container", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "user1_new"
            }, 
            {
                "text": "Entity 'user1' moves from transient to pending", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "address1_new"
            }, 
            {
                "text": "Entity 'address1' moves from transient to pending", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "address2_new"
            }, 
            {
                "text": "Entity 'address2' moves from transient to pending", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "address3_new"
            }, 
            {
                "text": "Entity 'address3' moves from transient to pending", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "text": "flush begins", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "connection"
            }, 
            {
                "cmd": "draw_object", 
                "object": "sessiontransaction_conn_ref"
            }, 
            {
                "text": "Connection established", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "transaction_snapshot"
            }, 
            {
                "text": "a 'snapshot' is established in the database session", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "text": "INSERT INTO user (name) VALUES (?)\n", 
                "cmd": "console"
            }, 
            {
                "text": "execute INSERT statement on table 'user'", 
                "cmd": "annotate"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "user1_snapshot_col_id"
            }, 
            {
                "text": "ed", 
                "cmd": "draw_text", 
                "object": "user1_snapshot_col_name"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "user1_entity_id"
            }, 
            {
                "text": "name, addresses, id", 
                "cmd": "draw_text", 
                "object": "user1_state_committed_state"
            }, 
            {
                "text": "Attribute 'id' on entity 'user1' is set to value 1", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address1_entity_user_id"
            }, 
            {
                "text": "email, user_id", 
                "cmd": "draw_text", 
                "object": "address1_state_committed_state"
            }, 
            {
                "text": "Attribute 'user_id' on entity 'address1' is set to value 1", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address2_entity_user_id"
            }, 
            {
                "text": "email, user_id", 
                "cmd": "draw_text", 
                "object": "address2_state_committed_state"
            }, 
            {
                "text": "Attribute 'user_id' on entity 'address2' is set to value 1", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address3_entity_user_id"
            }, 
            {
                "text": "email, user_id", 
                "cmd": "draw_text", 
                "object": "address3_state_committed_state"
            }, 
            {
                "text": "Attribute 'user_id' on entity 'address3' is set to value 1", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "text": "INSERT INTO address (email, user_id) VALUES (?, ?)\n", 
                "cmd": "console"
            }, 
            {
                "text": "execute INSERT statement on table 'address'", 
                "cmd": "annotate"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address1_snapshot_col_id"
            }, 
            {
                "text": "ed@ed.com", 
                "cmd": "draw_text", 
                "object": "address1_snapshot_col_email"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address1_snapshot_col_user_id"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address2_snapshot_row"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address3_snapshot_row"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address1_entity_id"
            }, 
            {
                "text": "user_id, email, id", 
                "cmd": "draw_text", 
                "object": "address1_state_committed_state"
            }, 
            {
                "text": "Attribute 'id' on entity 'address1' is set to value 1", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "text": "INSERT INTO address (email, user_id) VALUES (?, ?)\n", 
                "cmd": "console"
            }, 
            {
                "text": "execute INSERT statement on table 'address'", 
                "cmd": "annotate"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address1_snapshot_col_id"
            }, 
            {
                "text": "ed@ed.com", 
                "cmd": "draw_text", 
                "object": "address1_snapshot_col_email"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address1_snapshot_col_user_id"
            }, 
            {
                "text": 2, 
                "cmd": "draw_text", 
                "object": "address2_snapshot_col_id"
            }, 
            {
                "text": "ed@gmail.com", 
                "cmd": "draw_text", 
                "object": "address2_snapshot_col_email"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address2_snapshot_col_user_id"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address3_snapshot_row"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "text": 2, 
                "cmd": "draw_text", 
                "object": "address2_entity_id"
            }, 
            {
                "text": "user_id, email, id", 
                "cmd": "draw_text", 
                "object": "address2_state_committed_state"
            }, 
            {
                "text": "Attribute 'id' on entity 'address2' is set to value 2", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "text": "INSERT INTO address (email, user_id) VALUES (?, ?)\n", 
                "cmd": "console"
            }, 
            {
                "text": "execute INSERT statement on table 'address'", 
                "cmd": "annotate"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address1_snapshot_col_id"
            }, 
            {
                "text": "ed@ed.com", 
                "cmd": "draw_text", 
                "object": "address1_snapshot_col_email"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address1_snapshot_col_user_id"
            }, 
            {
                "text": 2, 
                "cmd": "draw_text", 
                "object": "address2_snapshot_col_id"
            }, 
            {
                "text": "ed@gmail.com", 
                "cmd": "draw_text", 
                "object": "address2_snapshot_col_email"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address2_snapshot_col_user_id"
            }, 
            {
                "text": 3, 
                "cmd": "draw_text", 
                "object": "address3_snapshot_col_id"
            }, 
            {
                "text": "edward@python.net", 
                "cmd": "draw_text", 
                "object": "address3_snapshot_col_email"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address3_snapshot_col_user_id"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "text": 3, 
                "cmd": "draw_text", 
                "object": "address3_entity_id"
            }, 
            {
                "text": "user_id, email, id", 
                "cmd": "draw_text", 
                "object": "address3_state_committed_state"
            }, 
            {
                "text": "Attribute 'id' on entity 'address3' is set to value 3", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "erase_object", 
                "object": "address3_new"
            }, 
            {
                "text": "(Address, (3,)) -->", 
                "cmd": "draw_text", 
                "object": "address3_identity_key"
            }, 
            {
                "cmd": "draw_object", 
                "object": "address3_identity"
            }, 
            {
                "text": "Entity 'address3' moves from pending to persistent", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "erase_object", 
                "object": "address2_new"
            }, 
            {
                "text": "(Address, (2,)) -->", 
                "cmd": "draw_text", 
                "object": "address2_identity_key"
            }, 
            {
                "cmd": "draw_object", 
                "object": "address2_identity"
            }, 
            {
                "text": "Entity 'address2' moves from pending to persistent", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "erase_object", 
                "object": "user1_new"
            }, 
            {
                "text": "(User, (1,)) -->", 
                "cmd": "draw_text", 
                "object": "user1_identity_key"
            }, 
            {
                "cmd": "draw_object", 
                "object": "user1_identity"
            }, 
            {
                "text": "Entity 'user1' moves from pending to persistent", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "erase_object", 
                "object": "address1_new"
            }, 
            {
                "text": "(Address, (1,)) -->", 
                "cmd": "draw_text", 
                "object": "address1_identity_key"
            }, 
            {
                "cmd": "draw_object", 
                "object": "address1_identity"
            }, 
            {
                "text": "Entity 'address1' moves from pending to persistent", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "text": "flush ends", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "erase_object", 
                "object": "transaction_snapshot"
            }, 
            {
                "text": "the 'snapshot' is committed", 
                "cmd": "annotate"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "user1_perm_col_id"
            }, 
            {
                "text": "ed", 
                "cmd": "draw_text", 
                "object": "user1_perm_col_name"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address1_perm_col_id"
            }, 
            {
                "text": "ed@ed.com", 
                "cmd": "draw_text", 
                "object": "address1_perm_col_email"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address1_perm_col_user_id"
            }, 
            {
                "text": 2, 
                "cmd": "draw_text", 
                "object": "address2_perm_col_id"
            }, 
            {
                "text": "ed@gmail.com", 
                "cmd": "draw_text", 
                "object": "address2_perm_col_email"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address2_perm_col_user_id"
            }, 
            {
                "text": 3, 
                "cmd": "draw_text", 
                "object": "address3_perm_col_id"
            }, 
            {
                "text": "edward@python.net", 
                "cmd": "draw_text", 
                "object": "address3_perm_col_email"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address3_perm_col_user_id"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "erase_object", 
                "object": "address3_entity_id"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "address3_state_modified"
            }, 
            {
                "text": "id", 
                "cmd": "draw_text", 
                "object": "address3_state_committed_state"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address3_entity_email"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "address3_state_modified"
            }, 
            {
                "text": "email", 
                "cmd": "draw_text", 
                "object": "address3_state_committed_state"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address3_entity_user_id"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "address3_state_modified"
            }, 
            {
                "text": "user_id", 
                "cmd": "draw_text", 
                "object": "address3_state_committed_state"
            }, 
            {
                "text": "Expire attributes 'id', 'email', 'user_id' on entity 'address3'", 
                "cmd": "annotate"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address1_entity_id"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "address1_state_modified"
            }, 
            {
                "text": "id", 
                "cmd": "draw_text", 
                "object": "address1_state_committed_state"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address1_entity_email"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "address1_state_modified"
            }, 
            {
                "text": "email", 
                "cmd": "draw_text", 
                "object": "address1_state_committed_state"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address1_entity_user_id"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "address1_state_modified"
            }, 
            {
                "text": "user_id", 
                "cmd": "draw_text", 
                "object": "address1_state_committed_state"
            }, 
            {
                "text": "Expire attributes 'id', 'email', 'user_id' on entity 'address1'", 
                "cmd": "annotate"
            }, 
            {
                "cmd": "erase_object", 
                "object": "user1_entity_addresses"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "user1_state_modified"
            }, 
            {
                "text": "addresses", 
                "cmd": "draw_text", 
                "object": "user1_state_committed_state"
            }, 
            {
                "cmd": "erase_object", 
                "object": "user1_entity_id"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "user1_state_modified"
            }, 
            {
                "text": "id", 
                "cmd": "draw_text", 
                "object": "user1_state_committed_state"
            }, 
            {
                "cmd": "erase_object", 
                "object": "user1_entity_name"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "user1_state_modified"
            }, 
            {
                "text": "name", 
                "cmd": "draw_text", 
                "object": "user1_state_committed_state"
            }, 
            {
                "text": "Expire attributes 'addresses', 'id', 'name' on entity 'user1'", 
                "cmd": "annotate"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address2_entity_id"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "address2_state_modified"
            }, 
            {
                "text": "id", 
                "cmd": "draw_text", 
                "object": "address2_state_committed_state"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address2_entity_email"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "address2_state_modified"
            }, 
            {
                "text": "email", 
                "cmd": "draw_text", 
                "object": "address2_state_committed_state"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address2_entity_user_id"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "address2_state_modified"
            }, 
            {
                "text": "user_id", 
                "cmd": "draw_text", 
                "object": "address2_state_committed_state"
            }, 
            {
                "text": "Expire attributes 'id', 'email', 'user_id' on entity 'address2'", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "erase_object", 
                "object": "address3_entity"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address3_new"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address3_identity"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address3_identity_key"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address3_dirty"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address3_deleted"
            }, 
            {
                "text": "Entity 'address3' is garbage collected", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "erase_object", 
                "object": "address2_entity"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address2_new"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address2_identity"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address2_identity_key"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address2_dirty"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address2_deleted"
            }, 
            {
                "text": "Entity 'address2' is garbage collected", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "erase_object", 
                "object": "address1_entity"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address1_new"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address1_identity"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address1_identity_key"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address1_dirty"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address1_deleted"
            }, 
            {
                "text": "Entity 'address1' is garbage collected", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "erase_object", 
                "object": "sessiontransaction_conn_ref"
            }, 
            {
                "cmd": "erase_object", 
                "object": "connection"
            }, 
            {
                "text": "Connection removed", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "erase_object", 
                "object": "sessiontransaction"
            }, 
            {
                "text": "session's transaction container ended", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "sessiontransaction"
            }, 
            {
                "text": "session starts a transaction container", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "connection"
            }, 
            {
                "cmd": "draw_object", 
                "object": "sessiontransaction_conn_ref"
            }, 
            {
                "text": "Connection established", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "transaction_snapshot"
            }, 
            {
                "text": "a 'snapshot' is established in the database session", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "text": "SELECT user.id AS user_id, user.name AS user_name \nFROM user \nWHERE user.id = ?\n", 
                "cmd": "console"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "user1_snapshot_col_id"
            }, 
            {
                "text": "ed", 
                "cmd": "draw_text", 
                "object": "user1_snapshot_col_name"
            }, 
            {
                "text": "SELECT rows from tables 'user'", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "text": "SELECT address.id AS address_id, address.email AS address_email, address.user_id AS address_user_id \nFROM address \nWHERE ? = address.user_id\n", 
                "cmd": "console"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address1_snapshot_col_user_id"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address1_snapshot_col_id"
            }, 
            {
                "text": "ed@ed.com", 
                "cmd": "draw_text", 
                "object": "address1_snapshot_col_email"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address2_snapshot_col_user_id"
            }, 
            {
                "text": 2, 
                "cmd": "draw_text", 
                "object": "address2_snapshot_col_id"
            }, 
            {
                "text": "ed@gmail.com", 
                "cmd": "draw_text", 
                "object": "address2_snapshot_col_email"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address3_snapshot_col_user_id"
            }, 
            {
                "text": 3, 
                "cmd": "draw_text", 
                "object": "address3_snapshot_col_id"
            }, 
            {
                "text": "edward@python.net", 
                "cmd": "draw_text", 
                "object": "address3_snapshot_col_email"
            }, 
            {
                "text": "SELECT rows from tables 'address'", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "address1_instance_state"
            }, 
            {
                "text": "(Address, (1,)) -->", 
                "cmd": "draw_text", 
                "object": "address1_identity_key"
            }, 
            {
                "cmd": "draw_object", 
                "object": "address1_identity"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address1_entity_id"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "address1_state_modified"
            }, 
            {
                "text": "id", 
                "cmd": "draw_text", 
                "object": "address1_state_committed_state"
            }, 
            {
                "text": "ed@ed.com", 
                "cmd": "draw_text", 
                "object": "address1_entity_email"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "address1_state_modified"
            }, 
            {
                "text": "email", 
                "cmd": "draw_text", 
                "object": "address1_state_committed_state"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address1_entity_user_id"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "address1_state_modified"
            }, 
            {
                "text": "user_id", 
                "cmd": "draw_text", 
                "object": "address1_state_committed_state"
            }, 
            {
                "text": "Entity 'address1' is loaded", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "address2_instance_state"
            }, 
            {
                "text": "(Address, (2,)) -->", 
                "cmd": "draw_text", 
                "object": "address2_identity_key"
            }, 
            {
                "cmd": "draw_object", 
                "object": "address2_identity"
            }, 
            {
                "text": 2, 
                "cmd": "draw_text", 
                "object": "address2_entity_id"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "address2_state_modified"
            }, 
            {
                "text": "id", 
                "cmd": "draw_text", 
                "object": "address2_state_committed_state"
            }, 
            {
                "text": "ed@gmail.com", 
                "cmd": "draw_text", 
                "object": "address2_entity_email"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "address2_state_modified"
            }, 
            {
                "text": "email", 
                "cmd": "draw_text", 
                "object": "address2_state_committed_state"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address2_entity_user_id"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "address2_state_modified"
            }, 
            {
                "text": "user_id", 
                "cmd": "draw_text", 
                "object": "address2_state_committed_state"
            }, 
            {
                "text": "Entity 'address2' is loaded", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "address3_instance_state"
            }, 
            {
                "text": "(Address, (3,)) -->", 
                "cmd": "draw_text", 
                "object": "address3_identity_key"
            }, 
            {
                "cmd": "draw_object", 
                "object": "address3_identity"
            }, 
            {
                "text": 3, 
                "cmd": "draw_text", 
                "object": "address3_entity_id"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "address3_state_modified"
            }, 
            {
                "text": "id", 
                "cmd": "draw_text", 
                "object": "address3_state_committed_state"
            }, 
            {
                "text": "edward@python.net", 
                "cmd": "draw_text", 
                "object": "address3_entity_email"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "address3_state_modified"
            }, 
            {
                "text": "email", 
                "cmd": "draw_text", 
                "object": "address3_state_committed_state"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address3_entity_user_id"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "address3_state_modified"
            }, 
            {
                "text": "user_id", 
                "cmd": "draw_text", 
                "object": "address3_state_committed_state"
            }, 
            {
                "text": "Entity 'address3' is loaded", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "user1_entity_addresses"
            }, 
            {
                "text": "Create empty collection 'addresses' on entity 'user1'", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "user1_entity_address1"
            }, 
            {
                "text": "Append entity 'address1' to collection 'addresses' on entity 'user1'", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "user1_entity_address2"
            }, 
            {
                "text": "Append entity 'address2' to collection 'addresses' on entity 'user1'", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "user1_entity_address3"
            }, 
            {
                "text": "Append entity 'address3' to collection 'addresses' on entity 'user1'", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "text": "edward@gmail.com", 
                "cmd": "draw_text", 
                "object": "address2_entity_email"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "address2_state_modified"
            }, 
            {
                "text": "email", 
                "cmd": "draw_text", 
                "object": "address2_state_committed_state"
            }, 
            {
                "text": "Attribute 'email' on entity 'address2' is set to value 'edward@gmail.com'", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "address2_dirty"
            }, 
            {
                "text": "Entity 'address2' is added to the session.dirty list", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "text": "flush begins", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "text": "UPDATE address SET email=? WHERE address.id = ?\n", 
                "cmd": "console"
            }, 
            {
                "text": "execute UPDATE statement on table 'address'", 
                "cmd": "annotate"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address1_snapshot_col_id"
            }, 
            {
                "text": "ed@ed.com", 
                "cmd": "draw_text", 
                "object": "address1_snapshot_col_email"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address1_snapshot_col_user_id"
            }, 
            {
                "text": 2, 
                "cmd": "draw_text", 
                "object": "address2_snapshot_col_id"
            }, 
            {
                "text": "edward@gmail.com", 
                "cmd": "draw_text", 
                "object": "address2_snapshot_col_email"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address2_snapshot_col_user_id"
            }, 
            {
                "text": 3, 
                "cmd": "draw_text", 
                "object": "address3_snapshot_col_id"
            }, 
            {
                "text": "edward@python.net", 
                "cmd": "draw_text", 
                "object": "address3_snapshot_col_email"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address3_snapshot_col_user_id"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "erase_object", 
                "object": "address2_dirty"
            }, 
            {
                "text": "flush ends", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "erase_object", 
                "object": "transaction_snapshot"
            }, 
            {
                "text": "the 'snapshot' is committed", 
                "cmd": "annotate"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "user1_perm_col_id"
            }, 
            {
                "text": "ed", 
                "cmd": "draw_text", 
                "object": "user1_perm_col_name"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address1_perm_col_id"
            }, 
            {
                "text": "ed@ed.com", 
                "cmd": "draw_text", 
                "object": "address1_perm_col_email"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address1_perm_col_user_id"
            }, 
            {
                "text": 2, 
                "cmd": "draw_text", 
                "object": "address2_perm_col_id"
            }, 
            {
                "text": "edward@gmail.com", 
                "cmd": "draw_text", 
                "object": "address2_perm_col_email"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address2_perm_col_user_id"
            }, 
            {
                "text": 3, 
                "cmd": "draw_text", 
                "object": "address3_perm_col_id"
            }, 
            {
                "text": "edward@python.net", 
                "cmd": "draw_text", 
                "object": "address3_perm_col_email"
            }, 
            {
                "text": 1, 
                "cmd": "draw_text", 
                "object": "address3_perm_col_user_id"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "erase_object", 
                "object": "user1_entity_addresses"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "user1_state_modified"
            }, 
            {
                "text": "addresses", 
                "cmd": "draw_text", 
                "object": "user1_state_committed_state"
            }, 
            {
                "cmd": "erase_object", 
                "object": "user1_entity_id"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "user1_state_modified"
            }, 
            {
                "text": "id", 
                "cmd": "draw_text", 
                "object": "user1_state_committed_state"
            }, 
            {
                "cmd": "erase_object", 
                "object": "user1_entity_name"
            }, 
            {
                "text": "True", 
                "cmd": "draw_text", 
                "object": "user1_state_modified"
            }, 
            {
                "text": "name", 
                "cmd": "draw_text", 
                "object": "user1_state_committed_state"
            }, 
            {
                "text": "Expire attributes 'addresses', 'id', 'name' on entity 'user1'", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "erase_object", 
                "object": "address3_entity"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address3_new"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address3_identity"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address3_identity_key"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address3_dirty"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address3_deleted"
            }, 
            {
                "text": "Entity 'address3' is garbage collected", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "erase_object", 
                "object": "address2_entity"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address2_new"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address2_identity"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address2_identity_key"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address2_dirty"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address2_deleted"
            }, 
            {
                "text": "Entity 'address2' is garbage collected", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "erase_object", 
                "object": "address1_entity"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address1_new"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address1_identity"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address1_identity_key"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address1_dirty"
            }, 
            {
                "cmd": "erase_object", 
                "object": "address1_deleted"
            }, 
            {
                "text": "Entity 'address1' is garbage collected", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "erase_object", 
                "object": "sessiontransaction_conn_ref"
            }, 
            {
                "cmd": "erase_object", 
                "object": "connection"
            }, 
            {
                "text": "Connection removed", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "erase_object", 
                "object": "sessiontransaction"
            }, 
            {
                "text": "session's transaction container ended", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "draw_object", 
                "object": "sessiontransaction"
            }, 
            {
                "text": "session starts a transaction container", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": [
            {
                "cmd": "erase_object", 
                "object": "user1_entity"
            }, 
            {
                "cmd": "erase_object", 
                "object": "user1_new"
            }, 
            {
                "cmd": "erase_object", 
                "object": "user1_identity"
            }, 
            {
                "cmd": "erase_object", 
                "object": "user1_identity_key"
            }, 
            {
                "cmd": "erase_object", 
                "object": "user1_dirty"
            }, 
            {
                "cmd": "erase_object", 
                "object": "user1_deleted"
            }, 
            {
                "text": "Entity 'user1' is garbage collected", 
                "cmd": "annotate"
            }
        ]
    }, 
    {
        "cmds": []
    }, 
    {
        "cmds": [
            {
                "cmd": "clear"
            }
        ]
    }
];

init_scene();
