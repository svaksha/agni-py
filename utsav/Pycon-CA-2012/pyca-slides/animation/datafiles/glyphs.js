var objects = {
    "address2_snapshot_col_id": {
        "font_size": 28, 
        "name": "address2_snapshot_col_id", 
        "width": 116, 
        "height": 38, 
        "dependency": "address2_snapshot_row", 
        "depth": 4, 
        "y": 1024, 
        "x": 2813, 
        "dependents": []
    }, 
    "user1_identity_key": {
        "font_size": 28, 
        "name": "user1_identity_key", 
        "width": 149, 
        "height": 38, 
        "dependency": "session", 
        "depth": 1, 
        "y": 1143, 
        "x": 1182, 
        "dependents": []
    }, 
    "user1_identity": {
        "font_size": 67, 
        "name": "user1_identity", 
        "width": 145, 
        "height": 59, 
        "dependency": "session", 
        "depth": 1, 
        "y": 1132, 
        "x": 1351, 
        "dependents": []
    }, 
    "address1_entity": {
        "reset": "layer1", 
        "layer": "layer2", 
        "font_size": 78, 
        "name": "address1_entity", 
        "height": 392, 
        "width": 466, 
        "depth": 0, 
        "y": 815, 
        "x": 6, 
        "dependents": [
            "address1_entity_id", 
            "address1_entity_email", 
            "address1_entity_user_id", 
            "address1_entity_user", 
            "address1_instance_state"
        ]
    }, 
    "address3_entity_id": {
        "font_size": 28, 
        "name": "address3_entity_id", 
        "width": 111, 
        "text": "true", 
        "height": 32, 
        "dependency": "address3_entity", 
        "depth": 1, 
        "y": 1922, 
        "x": 208, 
        "dependents": []
    }, 
    "address_snapshot_table": {
        "name": "address_snapshot_table", 
        "width": 475, 
        "height": 291, 
        "dependency": "transaction_snapshot", 
        "depth": 2, 
        "y": 880, 
        "x": 2752, 
        "dependents": [
            "address1_snapshot_row", 
            "address2_snapshot_row", 
            "address3_snapshot_row"
        ]
    }, 
    "session": {
        "font_size": 67, 
        "name": "session", 
        "height": 2203, 
        "width": 1086, 
        "depth": 0, 
        "y": 36, 
        "x": 655, 
        "dependents": [
            "user1_new", 
            "address2_new", 
            "user1_dirty", 
            "address2_dirty", 
            "user1_deleted", 
            "address2_deleted", 
            "user1_identity_key", 
            "address1_identity_key", 
            "address2_identity_key", 
            "address3_identity_key", 
            "sessiontransaction", 
            "user1_identity", 
            "address1_identity", 
            "address2_identity", 
            "address3_identity", 
            "address1_new", 
            "address3_new", 
            "address1_dirty", 
            "address3_dirty", 
            "address1_deleted", 
            "address3_deleted"
        ]
    }, 
    "address1_new": {
        "font_size": 67, 
        "name": "address1_new", 
        "width": 150, 
        "height": 59, 
        "dependency": "session", 
        "depth": 1, 
        "y": 251, 
        "x": 1361, 
        "dependents": []
    }, 
    "user1_deleted": {
        "font_size": 67, 
        "name": "user1_deleted", 
        "width": 145, 
        "height": 59, 
        "dependency": "session", 
        "depth": 1, 
        "y": 823, 
        "x": 1184, 
        "dependents": []
    }, 
    "address2_perm_col_user_id": {
        "font_size": 28, 
        "name": "address2_perm_col_user_id", 
        "width": 120, 
        "height": 38, 
        "dependency": "address2_perm_row", 
        "depth": 2, 
        "y": 888, 
        "x": 3495, 
        "dependents": []
    }, 
    "address2_entity": {
        "reset": "layer1", 
        "layer": "layer2", 
        "font_size": 78, 
        "name": "address2_entity", 
        "height": 392, 
        "width": 466, 
        "depth": 0, 
        "y": 1262, 
        "x": 6, 
        "dependents": [
            "address2_entity_id", 
            "address2_entity_email", 
            "address2_entity_user_id", 
            "address2_entity_user", 
            "address2_instance_state"
        ]
    }, 
    "address2_state_committed_state": {
        "font_size": 17, 
        "name": "address2_state_committed_state", 
        "width": 88, 
        "text": "true", 
        "height": 32, 
        "dependency": "address2_instance_state", 
        "depth": 2, 
        "y": 1372, 
        "x": 311, 
        "dependents": []
    }, 
    "user1_perm_row": {
        "name": "user1_perm_row", 
        "width": 475, 
        "height": 54, 
        "dependency": "database", 
        "depth": 1, 
        "y": 512, 
        "x": 3328, 
        "dependents": [
            "user1_perm_col_id", 
            "user1_perm_col_name"
        ]
    }, 
    "address1_perm_col_user_id": {
        "font_size": 28, 
        "name": "address1_perm_col_user_id", 
        "width": 120, 
        "height": 38, 
        "dependency": "address1_perm_row", 
        "depth": 2, 
        "y": 823, 
        "x": 3495, 
        "dependents": []
    }, 
    "address1_entity_email": {
        "font_size": 28, 
        "name": "address1_entity_email", 
        "width": 111, 
        "text": "true", 
        "height": 32, 
        "dependency": "address1_entity", 
        "depth": 1, 
        "y": 1048, 
        "x": 208, 
        "dependents": []
    }, 
    "address2_instance_state": {
        "name": "address2_instance_state", 
        "width": 191, 
        "height": 118, 
        "dependency": "address2_entity", 
        "depth": 1, 
        "y": 1307, 
        "x": 208, 
        "dependents": [
            "address2_state_modified", 
            "address2_state_key", 
            "address2_state_committed_state"
        ]
    }, 
    "transaction_snapshot": {
        "name": "transaction_snapshot", 
        "width": 575, 
        "height": 804, 
        "dependency": "database", 
        "depth": 1, 
        "y": 454, 
        "x": 2701, 
        "dependents": [
            "user_snapshot_table", 
            "address_snapshot_table"
        ]
    }, 
    "address3_entity_user_id": {
        "font_size": 28, 
        "name": "address3_entity_user_id", 
        "width": 111, 
        "text": "true", 
        "height": 32, 
        "dependency": "address3_entity", 
        "depth": 1, 
        "y": 1967, 
        "x": 208, 
        "dependents": []
    }, 
    "user1_snapshot_col_id": {
        "font_size": 28, 
        "name": "user1_snapshot_col_id", 
        "width": 177, 
        "height": 38, 
        "dependency": "user1_snapshot_row", 
        "depth": 4, 
        "y": 643, 
        "x": 2790, 
        "dependents": []
    }, 
    "address3_entity_email": {
        "font_size": 28, 
        "name": "address3_entity_email", 
        "width": 111, 
        "text": "true", 
        "height": 32, 
        "dependency": "address3_entity", 
        "depth": 1, 
        "y": 1944, 
        "x": 208, 
        "dependents": []
    }, 
    "address3_perm_col_email": {
        "font_size": 28, 
        "name": "address3_perm_col_email", 
        "width": 154, 
        "height": 38, 
        "dependency": "address3_perm_row", 
        "depth": 2, 
        "y": 952, 
        "x": 3629, 
        "dependents": []
    }, 
    "address3_snapshot_col_id": {
        "font_size": 28, 
        "name": "address3_snapshot_col_id", 
        "width": 116, 
        "height": 38, 
        "dependency": "address3_snapshot_row", 
        "depth": 4, 
        "y": 1089, 
        "x": 2813, 
        "dependents": []
    }, 
    "address1_snapshot_row": {
        "font_size": 22, 
        "name": "address1_snapshot_row", 
        "width": 475, 
        "height": 54, 
        "dependency": "address_snapshot_table", 
        "depth": 3, 
        "y": 952, 
        "x": 2752, 
        "dependents": [
            "address1_snapshot_col_id", 
            "address1_snapshot_col_user_id", 
            "address1_snapshot_col_email"
        ]
    }, 
    "address2_new": {
        "font_size": 67, 
        "name": "address2_new", 
        "width": 150, 
        "height": 59, 
        "dependency": "session", 
        "depth": 1, 
        "y": 344, 
        "x": 1184, 
        "dependents": []
    }, 
    "address1_identity": {
        "font_size": 67, 
        "name": "address1_identity", 
        "width": 150, 
        "height": 59, 
        "dependency": "session", 
        "depth": 1, 
        "y": 1206, 
        "x": 1351, 
        "dependents": []
    }, 
    "entine": {
        "name": "entine", 
        "height": 291, 
        "width": 513, 
        "depth": 0, 
        "y": 2230, 
        "x": 1024, 
        "dependents": []
    }, 
    "user1_entity": {
        "name": "user1_entity", 
        "height": 533, 
        "width": 463, 
        "depth": 0, 
        "y": 206, 
        "x": 6, 
        "dependents": [
            "user1_entity_id", 
            "user1_entity_name", 
            "user1_instance_state", 
            "user1_entity_addresses"
        ]
    }, 
    "user1_state_committed_state": {
        "font_size": 17, 
        "name": "user1_state_committed_state", 
        "width": 88, 
        "text": "true", 
        "height": 32, 
        "dependency": "user1_instance_state", 
        "depth": 2, 
        "y": 316, 
        "x": 308, 
        "dependents": []
    }, 
    "address3_state_modified": {
        "font_size": 17, 
        "name": "address3_state_modified", 
        "width": 67, 
        "text": "true", 
        "height": 32, 
        "dependency": "address3_instance_state", 
        "depth": 2, 
        "y": 1793, 
        "x": 307, 
        "dependents": []
    }, 
    "address1_perm_col_id": {
        "font_size": 28, 
        "name": "address1_perm_col_id", 
        "width": 120, 
        "height": 38, 
        "dependency": "address1_perm_row", 
        "depth": 2, 
        "y": 823, 
        "x": 3373, 
        "dependents": []
    }, 
    "address3_snapshot_col_email": {
        "font_size": 28, 
        "name": "address3_snapshot_col_email", 
        "width": 154, 
        "height": 38, 
        "dependency": "address3_snapshot_row", 
        "depth": 4, 
        "y": 1089, 
        "x": 3053, 
        "dependents": []
    }, 
    "address1_state_modified": {
        "font_size": 17, 
        "name": "address1_state_modified", 
        "width": 62, 
        "text": "true", 
        "height": 32, 
        "dependency": "address1_instance_state", 
        "depth": 2, 
        "y": 897, 
        "x": 308, 
        "dependents": []
    }, 
    "address3_perm_col_id": {
        "font_size": 28, 
        "name": "address3_perm_col_id", 
        "width": 120, 
        "height": 38, 
        "dependency": "address3_perm_row", 
        "depth": 2, 
        "y": 952, 
        "x": 3373, 
        "dependents": []
    }, 
    "address3_instance_state": {
        "name": "address3_instance_state", 
        "width": 191, 
        "height": 118, 
        "dependency": "address3_entity", 
        "depth": 1, 
        "y": 1756, 
        "x": 208, 
        "dependents": [
            "address3_state_key", 
            "address3_state_modified", 
            "address3_state_committed_state"
        ]
    }, 
    "address2_state_modified": {
        "font_size": 17, 
        "name": "address2_state_modified", 
        "width": 63, 
        "text": "true", 
        "height": 32, 
        "dependency": "address2_instance_state", 
        "depth": 2, 
        "y": 1344, 
        "x": 311, 
        "dependents": []
    }, 
    "sessiontransaction_conn_ref": {
        "font_size": 67, 
        "name": "sessiontransaction_conn_ref", 
        "width": 618, 
        "height": 127, 
        "dependency": "sessiontransaction", 
        "depth": 2, 
        "y": 1653, 
        "x": 1212, 
        "dependents": []
    }, 
    "address2_state_key": {
        "font_size": 17, 
        "name": "address2_state_key", 
        "width": 88, 
        "text": "true", 
        "height": 32, 
        "dependency": "address2_instance_state", 
        "depth": 2, 
        "y": 1322, 
        "x": 300, 
        "dependents": []
    }, 
    "user1_state_key": {
        "font_size": 17, 
        "name": "user1_state_key", 
        "width": 88, 
        "text": "true", 
        "height": 32, 
        "dependency": "user1_instance_state", 
        "depth": 2, 
        "y": 269, 
        "x": 308, 
        "dependents": []
    }, 
    "address3_deleted": {
        "font_size": 67, 
        "name": "address3_deleted", 
        "width": 150, 
        "height": 59, 
        "dependency": "session", 
        "depth": 1, 
        "y": 910, 
        "x": 1361, 
        "dependents": []
    }, 
    "address2_dirty": {
        "font_size": 67, 
        "name": "address2_dirty", 
        "width": 150, 
        "height": 59, 
        "dependency": "session", 
        "depth": 1, 
        "y": 630, 
        "x": 1184, 
        "dependents": []
    }, 
    "address3_state_key": {
        "font_size": 17, 
        "name": "address3_state_key", 
        "width": 88, 
        "text": "true", 
        "height": 32, 
        "dependency": "address3_instance_state", 
        "depth": 2, 
        "y": 1773, 
        "x": 300, 
        "dependents": []
    }, 
    "address1_identity_key": {
        "font_size": 28, 
        "name": "address1_identity_key", 
        "width": 149, 
        "height": 38, 
        "dependency": "session", 
        "depth": 1, 
        "y": 1212, 
        "x": 1182, 
        "dependents": []
    }, 
    "address1_snapshot_col_id": {
        "font_size": 28, 
        "name": "address1_snapshot_col_id", 
        "width": 116, 
        "height": 38, 
        "dependency": "address1_snapshot_row", 
        "depth": 4, 
        "y": 960, 
        "x": 2813, 
        "dependents": []
    }, 
    "address2_entity_id": {
        "font_size": 28, 
        "name": "address2_entity_id", 
        "width": 111, 
        "text": "true", 
        "height": 32, 
        "dependency": "address2_entity", 
        "depth": 1, 
        "y": 1474, 
        "x": 208, 
        "dependents": []
    }, 
    "address2_entity_email": {
        "font_size": 28, 
        "name": "address2_entity_email", 
        "width": 111, 
        "text": "true", 
        "height": 32, 
        "dependency": "address2_entity", 
        "depth": 1, 
        "y": 1496, 
        "x": 208, 
        "dependents": []
    }, 
    "user1_perm_col_name": {
        "font_size": 28, 
        "name": "user1_perm_col_name", 
        "width": 199, 
        "height": 38, 
        "dependency": "user1_perm_row", 
        "depth": 2, 
        "y": 521, 
        "x": 3553, 
        "dependents": []
    }, 
    "user1_snapshot_row": {
        "name": "user1_snapshot_row", 
        "width": 475, 
        "height": 54, 
        "dependency": "user_snapshot_table", 
        "depth": 3, 
        "y": 635, 
        "x": 2751, 
        "dependents": [
            "user1_snapshot_col_id", 
            "user1_snapshot_col_name"
        ]
    }, 
    "address2_snapshot_col_email": {
        "font_size": 28, 
        "name": "address2_snapshot_col_email", 
        "width": 154, 
        "height": 38, 
        "dependency": "address2_snapshot_row", 
        "depth": 4, 
        "y": 1024, 
        "x": 3053, 
        "dependents": []
    }, 
    "address1_state_key": {
        "font_size": 17, 
        "name": "address1_state_key", 
        "width": 88, 
        "text": "true", 
        "height": 32, 
        "dependency": "address1_instance_state", 
        "depth": 2, 
        "y": 875, 
        "x": 308, 
        "dependents": []
    }, 
    "address3_snapshot_row": {
        "font_size": 22, 
        "name": "address3_snapshot_row", 
        "width": 475, 
        "height": 54, 
        "dependency": "address_snapshot_table", 
        "depth": 3, 
        "y": 1081, 
        "x": 2752, 
        "dependents": [
            "address3_snapshot_col_id", 
            "address3_snapshot_col_user_id", 
            "address3_snapshot_col_email"
        ]
    }, 
    "connection": {
        "font_size": 67, 
        "name": "connection", 
        "height": 861, 
        "width": 538, 
        "depth": 0, 
        "y": 992, 
        "x": 1825, 
        "dependents": []
    }, 
    "address2_deleted": {
        "font_size": 67, 
        "name": "address2_deleted", 
        "width": 150, 
        "height": 59, 
        "dependency": "session", 
        "depth": 1, 
        "y": 910, 
        "x": 1184, 
        "dependents": []
    }, 
    "address2_perm_col_email": {
        "font_size": 28, 
        "name": "address2_perm_col_email", 
        "width": 154, 
        "height": 38, 
        "dependency": "address2_perm_row", 
        "depth": 2, 
        "y": 888, 
        "x": 3629, 
        "dependents": []
    }, 
    "sessiontransaction": {
        "font_size": 22, 
        "name": "sessiontransaction", 
        "width": 441, 
        "height": 222, 
        "dependency": "session", 
        "depth": 1, 
        "y": 1589, 
        "x": 1160, 
        "dependents": [
            "sessiontransaction_conn_ref"
        ]
    }, 
    "user1_entity_id": {
        "font_size": 22, 
        "name": "user1_entity_id", 
        "width": 111, 
        "text": "true", 
        "height": 32, 
        "dependency": "user1_entity", 
        "depth": 1, 
        "y": 404, 
        "x": 201, 
        "dependents": []
    }, 
    "address1_entity_user": {
        "font_size": 72, 
        "name": "address1_entity_user", 
        "width": 145, 
        "height": 59, 
        "dependency": "address1_entity", 
        "depth": 1, 
        "y": 1097, 
        "x": 213, 
        "dependents": []
    }, 
    "address3_dirty": {
        "font_size": 67, 
        "name": "address3_dirty", 
        "width": 150, 
        "height": 59, 
        "dependency": "session", 
        "depth": 1, 
        "y": 630, 
        "x": 1361, 
        "dependents": []
    }, 
    "user1_snapshot_col_name": {
        "font_size": 28, 
        "name": "user1_snapshot_col_name", 
        "width": 199, 
        "height": 38, 
        "dependency": "user1_snapshot_row", 
        "depth": 4, 
        "y": 643, 
        "x": 2975, 
        "dependents": []
    }, 
    "user1_entity_address2": {
        "font_size": 67, 
        "name": "user1_entity_address2", 
        "width": 150, 
        "height": 59, 
        "dependency": "user1_entity_addresses", 
        "depth": 2, 
        "y": 541, 
        "x": 240, 
        "dependents": []
    }, 
    "user1_entity_address3": {
        "font_size": 67, 
        "name": "user1_entity_address3", 
        "width": 150, 
        "height": 59, 
        "dependency": "user1_entity_addresses", 
        "depth": 2, 
        "y": 611, 
        "x": 240, 
        "dependents": []
    }, 
    "user1_entity_address1": {
        "font_size": 67, 
        "name": "user1_entity_address1", 
        "width": 150, 
        "height": 59, 
        "dependency": "user1_entity_addresses", 
        "depth": 2, 
        "y": 474, 
        "x": 240, 
        "dependents": []
    }, 
    "user1_entity_addresses": {
        "name": "user1_entity_addresses", 
        "width": 228, 
        "height": 249, 
        "dependency": "user1_entity", 
        "depth": 1, 
        "y": 457, 
        "x": 201, 
        "dependents": [
            "user1_entity_address1", 
            "user1_entity_address2", 
            "user1_entity_address3"
        ]
    }, 
    "address1_dirty": {
        "font_size": 67, 
        "name": "address1_dirty", 
        "width": 150, 
        "height": 59, 
        "dependency": "session", 
        "depth": 1, 
        "y": 537, 
        "x": 1361, 
        "dependents": []
    }, 
    "address1_deleted": {
        "font_size": 67, 
        "name": "address1_deleted", 
        "width": 150, 
        "height": 59, 
        "dependency": "session", 
        "depth": 1, 
        "y": 822, 
        "x": 1361, 
        "dependents": []
    }, 
    "user_snapshot_table": {
        "name": "user_snapshot_table", 
        "width": 475, 
        "height": 291, 
        "dependency": "transaction_snapshot", 
        "depth": 2, 
        "y": 544, 
        "x": 2752, 
        "dependents": [
            "user1_snapshot_row"
        ]
    }, 
    "address2_perm_col_id": {
        "font_size": 28, 
        "name": "address2_perm_col_id", 
        "width": 120, 
        "height": 38, 
        "dependency": "address2_perm_row", 
        "depth": 2, 
        "y": 887, 
        "x": 3373, 
        "dependents": []
    }, 
    "address3_perm_col_user_id": {
        "font_size": 28, 
        "name": "address3_perm_col_user_id", 
        "width": 120, 
        "height": 38, 
        "dependency": "address3_perm_row", 
        "depth": 2, 
        "y": 952, 
        "x": 3495, 
        "dependents": []
    }, 
    "user1_state_modified": {
        "font_size": 17, 
        "name": "user1_state_modified", 
        "width": 63, 
        "text": "true", 
        "height": 32, 
        "dependency": "user1_instance_state", 
        "depth": 2, 
        "y": 288, 
        "x": 308, 
        "dependents": []
    }, 
    "address1_perm_row": {
        "font_size": 22, 
        "name": "address1_perm_row", 
        "width": 475, 
        "height": 54, 
        "dependency": "database", 
        "depth": 1, 
        "y": 815, 
        "x": 3328, 
        "dependents": [
            "address1_perm_col_id", 
            "address1_perm_col_user_id", 
            "address1_perm_col_email"
        ]
    }, 
    "database": {
        "name": "database", 
        "height": 1467, 
        "width": 1039, 
        "depth": 0, 
        "y": 7, 
        "x": 2893, 
        "dependents": [
            "transaction_snapshot", 
            "user1_perm_row", 
            "address1_perm_row", 
            "address2_perm_row", 
            "address3_perm_row"
        ]
    }, 
    "address1_state_committed_state": {
        "font_size": 17, 
        "name": "address1_state_committed_state", 
        "width": 88, 
        "text": "true", 
        "height": 32, 
        "dependency": "address1_instance_state", 
        "depth": 2, 
        "y": 924, 
        "x": 308, 
        "dependents": []
    }, 
    "address1_entity_id": {
        "font_size": 28, 
        "name": "address1_entity_id", 
        "width": 111, 
        "text": "true", 
        "height": 32, 
        "dependency": "address1_entity", 
        "depth": 1, 
        "y": 1026, 
        "x": 208, 
        "dependents": []
    }, 
    "address1_snapshot_col_email": {
        "font_size": 28, 
        "name": "address1_snapshot_col_email", 
        "width": 154, 
        "height": 38, 
        "dependency": "address1_snapshot_row", 
        "depth": 4, 
        "y": 960, 
        "x": 3053, 
        "dependents": []
    }, 
    "address1_entity_user_id": {
        "font_size": 28, 
        "name": "address1_entity_user_id", 
        "width": 111, 
        "text": "true", 
        "height": 32, 
        "dependency": "address1_entity", 
        "depth": 1, 
        "y": 1071, 
        "x": 208, 
        "dependents": []
    }, 
    "user1_instance_state": {
        "name": "user1_instance_state", 
        "width": 191, 
        "height": 118, 
        "dependency": "user1_entity", 
        "depth": 1, 
        "y": 251, 
        "x": 201, 
        "dependents": [
            "user1_state_modified", 
            "user1_state_key", 
            "user1_state_committed_state"
        ]
    }, 
    "address3_identity": {
        "font_size": 67, 
        "name": "address3_identity", 
        "width": 150, 
        "height": 59, 
        "dependency": "session", 
        "depth": 1, 
        "y": 1358, 
        "x": 1351, 
        "dependents": []
    }, 
    "address3_entity_user": {
        "font_size": 72, 
        "name": "address3_entity_user", 
        "width": 145, 
        "height": 59, 
        "dependency": "address3_entity", 
        "depth": 1, 
        "y": 1993, 
        "x": 213, 
        "dependents": []
    }, 
    "address2_snapshot_row": {
        "font_size": 22, 
        "name": "address2_snapshot_row", 
        "width": 475, 
        "height": 54, 
        "dependency": "address_snapshot_table", 
        "depth": 3, 
        "y": 1016, 
        "x": 2752, 
        "dependents": [
            "address2_snapshot_col_id", 
            "address2_snapshot_col_user_id", 
            "address2_snapshot_col_email"
        ]
    }, 
    "address2_entity_user": {
        "font_size": 72, 
        "name": "address2_entity_user", 
        "width": 145, 
        "height": 59, 
        "dependency": "address2_entity", 
        "depth": 1, 
        "y": 1544, 
        "x": 213, 
        "dependents": []
    }, 
    "address1_instance_state": {
        "name": "address1_instance_state", 
        "width": 191, 
        "height": 118, 
        "dependency": "address1_entity", 
        "depth": 1, 
        "y": 860, 
        "x": 208, 
        "dependents": [
            "address1_state_modified", 
            "address1_state_key", 
            "address1_state_committed_state"
        ]
    }, 
    "user1_perm_col_id": {
        "font_size": 28, 
        "name": "user1_perm_col_id", 
        "width": 120, 
        "height": 66, 
        "dependency": "user1_perm_row", 
        "depth": 2, 
        "y": 521, 
        "x": 3402, 
        "dependents": []
    }, 
    "address1_perm_col_email": {
        "font_size": 28, 
        "name": "address1_perm_col_email", 
        "width": 154, 
        "height": 38, 
        "dependency": "address1_perm_row", 
        "depth": 2, 
        "y": 823, 
        "x": 3629, 
        "dependents": []
    }, 
    "address3_perm_row": {
        "font_size": 22, 
        "name": "address3_perm_row", 
        "width": 475, 
        "height": 54, 
        "dependency": "database", 
        "depth": 1, 
        "y": 944, 
        "x": 3328, 
        "dependents": [
            "address3_perm_col_id", 
            "address3_perm_col_user_id", 
            "address3_perm_col_email"
        ]
    }, 
    "address3_state_committed_state": {
        "font_size": 17, 
        "name": "address3_state_committed_state", 
        "width": 88, 
        "text": "true", 
        "height": 32, 
        "dependency": "address3_instance_state", 
        "depth": 2, 
        "y": 1820, 
        "x": 311, 
        "dependents": []
    }, 
    "user1_new": {
        "font_size": 67, 
        "name": "user1_new", 
        "width": 145, 
        "height": 59, 
        "dependency": "session", 
        "depth": 1, 
        "y": 251, 
        "x": 1184, 
        "dependents": []
    }, 
    "address2_identity_key": {
        "font_size": 28, 
        "name": "address2_identity_key", 
        "width": 149, 
        "height": 38, 
        "dependency": "session", 
        "depth": 1, 
        "y": 1290, 
        "x": 1182, 
        "dependents": []
    }, 
    "address3_entity": {
        "reset": "layer1", 
        "layer": "layer2", 
        "font_size": 78, 
        "name": "address3_entity", 
        "height": 392, 
        "width": 466, 
        "depth": 0, 
        "y": 1711, 
        "x": 6, 
        "dependents": [
            "address3_entity_id", 
            "address3_entity_email", 
            "address3_entity_user_id", 
            "address3_entity_user", 
            "address3_instance_state"
        ]
    }, 
    "address1_snapshot_col_user_id": {
        "font_size": 28, 
        "name": "address1_snapshot_col_user_id", 
        "width": 120, 
        "height": 38, 
        "dependency": "address1_snapshot_row", 
        "depth": 4, 
        "y": 960, 
        "x": 2919, 
        "dependents": []
    }, 
    "address2_entity_user_id": {
        "font_size": 28, 
        "name": "address2_entity_user_id", 
        "width": 111, 
        "text": "true", 
        "height": 32, 
        "dependency": "address2_entity", 
        "depth": 1, 
        "y": 1518, 
        "x": 208, 
        "dependents": []
    }, 
    "address2_identity": {
        "font_size": 67, 
        "name": "address2_identity", 
        "width": 150, 
        "height": 59, 
        "dependency": "session", 
        "depth": 1, 
        "y": 1279, 
        "x": 1351, 
        "dependents": []
    }, 
    "address3_new": {
        "font_size": 67, 
        "name": "address3_new", 
        "width": 150, 
        "height": 59, 
        "dependency": "session", 
        "depth": 1, 
        "y": 344, 
        "x": 1361, 
        "dependents": []
    }, 
    "pool": {
        "name": "pool", 
        "height": 341, 
        "width": 513, 
        "depth": 0, 
        "y": 2216, 
        "x": 1821, 
        "dependents": []
    }, 
    "address2_perm_row": {
        "font_size": 22, 
        "name": "address2_perm_row", 
        "width": 475, 
        "height": 54, 
        "dependency": "database", 
        "depth": 1, 
        "y": 879, 
        "x": 3328, 
        "dependents": [
            "address2_perm_col_id", 
            "address2_perm_col_user_id", 
            "address2_perm_col_email"
        ]
    }, 
    "address3_snapshot_col_user_id": {
        "font_size": 28, 
        "name": "address3_snapshot_col_user_id", 
        "width": 120, 
        "height": 38, 
        "dependency": "address3_snapshot_row", 
        "depth": 4, 
        "y": 1089, 
        "x": 2919, 
        "dependents": []
    }, 
    "user1_entity_name": {
        "font_size": 22, 
        "name": "user1_entity_name", 
        "width": 111, 
        "text": "true", 
        "height": 32, 
        "dependency": "user1_entity", 
        "depth": 1, 
        "y": 426, 
        "x": 201, 
        "dependents": []
    }, 
    "address3_identity_key": {
        "font_size": 28, 
        "name": "address3_identity_key", 
        "width": 149, 
        "height": 38, 
        "dependency": "session", 
        "depth": 1, 
        "y": 1367, 
        "x": 1184, 
        "dependents": []
    }, 
    "address2_snapshot_col_user_id": {
        "font_size": 28, 
        "name": "address2_snapshot_col_user_id", 
        "width": 120, 
        "height": 38, 
        "dependency": "address2_snapshot_row", 
        "depth": 4, 
        "y": 1024, 
        "x": 2919, 
        "dependents": []
    }, 
    "user1_dirty": {
        "font_size": 67, 
        "name": "user1_dirty", 
        "width": 145, 
        "height": 59, 
        "dependency": "session", 
        "depth": 1, 
        "y": 537, 
        "x": 1184, 
        "dependents": []
    }
};
